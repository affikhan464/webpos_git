﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="DateWiseCashBookNew.aspx.cs" Inherits="WebPOS.Reports.CashBook.DateWiseCashBookNew" %>

   <link rel="stylesheet" href="/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/reset.css" type="text/css" />
    <link href="/css/allitems.css" rel="stylesheet" />
    <link href="/css/sweetalert.css" rel="stylesheet" />
    <link href="/css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="/css/GridStyle.css" rel="stylesheet" />
  <link rel="stylesheet" href="/css/vendor.min.css">
    <link rel="stylesheet" href="/css/app.css">
   <form runat="server"> 
<div class="reports"> 
    <div class="contentContainer1">
        <h2>All Party Incentive Given Summary</h2>
       <div class="BMSearchWrapper row">
           <div class="col-12 col-sm-6 col-md-2">
                    From
                    <asp:TextBox style="width: 105px;" ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
           </div>
            <div class="col col-12 col-sm-6 col-md-2">
		            <span class="userlLabel">To</span>
                    <asp:TextBox ID="TextBox1" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>	
		    </div>
            <div class="col col-12 col-sm-6 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i> Get Report</a>
		    </div>
            <div class="col col-12 col-sm-6 col-md-2">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-edit"></i>  Print</a>
		    </div>
            
        </div>
    </div>
</div>  

       </form>



