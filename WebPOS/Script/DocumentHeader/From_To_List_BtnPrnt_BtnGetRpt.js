﻿            <div class="BMSearchWrapper row align-items-end">
                      <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
                        <div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>



					
                        <div class="col col-12 col-sm-6 col-md-2">
                           
                             <div class="d-inline-flex w-100">
                                <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                                    <label>
                                        <input id="brandCheck" class="checkbox" type="checkbox" />
                                        <span></span>
                                    </label>
                                </span>
                                <select id="BrandNameDD" class="dropdown" data-type="BrandName">
                                    <option value="0">Select Brand</option>
                                </select>
                            </div>
                        </div>
                        
						 <div class="col col-12 col-sm-3 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-search"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-2">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>

			</div><!-- searchAppSection -->