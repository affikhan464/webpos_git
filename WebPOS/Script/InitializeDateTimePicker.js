﻿function initializeDatePicker() {

    $('.datetimepicker').datetimepicker({
        format: 'd/m/Y',
        timepicker: false,
        autoclose: true
    });
    var dt = new Date();
    var nowDate = ("0" + dt.getDate()).slice(-2) + "/" + ("0" + (dt.getMonth() + 1)).slice(-2) + "/" + dt.getFullYear();
    $(".Date").val(nowDate);
    $(".Date.input__field--hoshi").parent().addClass('input--filled');
}