﻿function AppendIMEList(imeList) {
    AppendInModal(imeList,"#IMEDetailModal");
}
function AppendItemIMEList(imeList) {
    AppendInModal(imeList, "#ItemIMEDetailModal");
}
function AppendInModal(imeList,modalId) {
    var rows = "";
    imecount = -1;
    for (var ime in imeList) {
        var isAlreadyExist = $("#IMEITable [name=IMEI][value=" + imeList[ime].IME + "]").length;
        var alreadyAddedClass = "";
        if (isAlreadyExist) {
            alreadyAddedClass = "alreadyAdded"
        }
        var row = `<tr class="${alreadyAddedClass}">
                   <td data-ime="${imeList[ime].IME}">${imeList[ime].IME}</td>
                   <td>${imeList[ime].ItemName}</td>
                   <td>${imeList[ime].IMEDescrition}</td>
               </tr>`;
        rows += row;
    }

    $(modalId +" .imeModalRows").html(rows);
}
$(document).keydown(function (e) {

    if ($("#IMEDetailModal").css("display") === "block") {
        updownArrowAndEnterKeyIME(e, "#IMEDetailModal");
    }
    if ($("#ItemIMEDetailModal").css("display") === "block") {
        updownArrowAndEnterKeyIME(e, "#ItemIMEDetailModal");
    }

});
var imecount = -1;
function updownArrowAndEnterKeyIME(e, modalId) {

    var autoCompleteTable;
    var row;
    
    if (e.keyCode === 40) {

        autoCompleteTable = $(modalId+ " table tbody");
        row = $(autoCompleteTable.children());
        $(row).removeClass("ui-state-focus").focus();
        if (imecount === row.length) {
            imecount = 0;

            $(row[imecount]).addClass("ui-state-focus").focus();
            $(modalId).scrollTop((35 * imecount) - 200)
            loadSelectedItemsFromAutocompleteIME(row[imecount]);
        }
        else if (imecount < row.length && row.length !== 0) {
            imecount++;

            $(row[imecount]).addClass("ui-state-focus").focus();

            $(modalId).scrollTop((35 * imecount) - 200)
            loadSelectedItemsFromAutocompleteIME(row[imecount]);

        }
    }
    else if (e.keyCode === 38) {
        autoCompleteTable = $(modalId+" table tbody");
        row = $(autoCompleteTable.children());
        $(row).removeClass("ui-state-focus").focus();
        if (imecount === 0) {
            imecount = row.length;
            $(row[imecount]).addClass("ui-state-focus").focus();
            $(modalId).scrollTop((35 * imecount) - 200)
            loadSelectedItemsFromAutocompleteIME(row[imecount]);


        }
        else if (imecount !== -1 && row.length !== 0) {
            imecount--;

            $(row[imecount]).addClass("ui-state-focus").focus();
            $(modalId).scrollTop((35 * imecount) - 200)
            loadSelectedItemsFromAutocompleteIME(row[imecount]);

        }
    }

    else if (e.keyCode === 13) {
        debugger
        var ime = $(".selectedIMEModalIMENumber").val();
        if ($("#ItemIMEDetailModal").css("display") === "block") {
            var rowNumber = $("#ItemIMEDetailModal .IME_ItemRowNumber").val();
            var txbx = $('#mainTable [name="ImeiTxbx"][data-rownumber=' + rowNumber + ']')
            txbx.val(ime);
            pressEnterKeyProgrammatically(txbx);
        }
        else if ($("#IMEDetailModal").css("display") === "block") {

            var txbx = $(".BarcodeTxbx");
            txbx.val(ime);
            pressEnterKeyProgrammatically(txbx);
        }
        var currentTarget = $(modalId+" table tr.ui-state-focus");
        currentTarget.addClass("alreadyAdded");
    }

}
$(document).on({
    click: function (e) {
        var ime = $(e.currentTarget).find("[data-ime]").data("ime");       
        appendIMEInTextBoxFromAutocompleteIMEModal(ime);
        var currentTarget = $(e.currentTarget);
        currentTarget.addClass("alreadyAdded");
    },
    mouseover: function (e) {
        $('.modal table tbody tr ').removeClass("ui-state-focus");
        $(this).addClass("ui-state-focus");
        loadSelectedItemsFromAutocompleteIME(this);
    }

}, '#IMEDetailModal table tbody tr, #ItemIMEDetailModal table tbody tr ');

function loadSelectedItemsFromAutocompleteIME(row) {
    var ime = $(row).find("[data-ime]").data("ime");
    $(".selectedIMEModalIMENumber").remove();
    $("<input type='hidden' class='selectedIMEModalIMENumber selected' value='" + ime + "' />").appendTo("body");
}
function appendIMEInTextBoxFromAutocompleteIMEModal(ime) {
    if ($("#ItemIMEDetailModal").css("display") === "block") {
        var rowNumber = $("#ItemIMEDetailModal .IME_ItemRowNumber").val();
        var txbx = $('#mainTable [name="ImeiTxbx"][data-rownumber=' + rowNumber+']')
        txbx.val(ime);
        pressEnterKeyProgrammatically(txbx);
    } else {
        var txbx = $(".BarcodeTxbx");
        txbx.val(ime);
        pressEnterKeyProgrammatically(txbx);
    }

}
function pressEnterKeyProgrammatically(txbx) {
    var e = jQuery.Event("keypress");    
    
    e.which = 13; //enter keycode
    e.keyCode = 13;
    $(txbx).trigger(e);
}

$(document).on('keydown', ".BarcodeTxbx", function (e) {
    if ((e.ctrlKey || e.metaKey) && (e.key.toLowerCase() == "l" || e.charCode == 76)) { //Ctrl + L on BarcodeTxbx
        e.preventDefault();
        openIMEModal('#IMEDetailModal');
    }
});
$(document).on('keydown', "#mainTable [name=ImeiTxbx]", function (e) {
    if ((e.ctrlKey || e.metaKey) && (e.key.toLowerCase() == "l" || e.charCode == 76)) { //Ctrl + L on item row Scan Item text box
        e.preventDefault();
        var tr = $(e.currentTarget).parents("tr");
        var qty = Number(tr.find("[name=Qty]").val());
        var scannedQty = Number(tr.find("[name=SQ]").val());
        if (qty > scannedQty) {
            openIMEModal('#ItemIMEDetailModal', e.currentTarget);
        } else if(qty == scannedQty){
            swal('Error!','Please Increase Item Quantity','error')
        } else if (qty < scannedQty) {
            swal('Error!', 'Please Correct the Item Quantity first,  Item Quantity must be equal or greater', 'error')
        }
    }
});
function openIMEModal(modalId, item) {
    debugger
    $(modalId+' tr').remove();
    $(modalId).modal({
        backdrop: false,
        show: true
    }).draggable({
        handle: ".generic_head_content .head"
    });
    if (item) {
        let itemName = $(item).parents("tr").find("[name=ItemName]").val();
        $(modalId + " input.IME_ItemCode").val(item.dataset.itemcode)
        $(modalId + " .IME_ItemName").text(itemName)
        $(modalId + " .IME_ItemCode:not(input)").text(item.dataset.itemcode)
        $(modalId + " .IME_ItemRowNumber").val(item.dataset.rownumber)
    }
    setTimeout(() => {
        $(modalId+' .txbxIMEList').val("").focus();
        $(modalId + ' .txbxItemIMEList').val("").focus();
    }, 500)
}