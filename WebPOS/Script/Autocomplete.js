﻿
$(document).ready(function () {
    $(document).keydown(function (e) {

        if ($("#" + dataType + "Table").css("display") === "block") {
            updownArrowAndEnterKey(e);
        }

    });
});
/*....................................*/
isNotUpDownArrow = true;
var textboxid = "";
var dataFunction = "";
var dataType = "";
var dataCallback = "";
var dataPlacement = "";
var nextfocus = "";
var glcode = "";
var preventAutocomplete = false;
var dataService = "WebPOSService";
var dataEvent = "";

function setAutocompleteData(dataset) {
    textboxid = dataset.id;
    dataFunction = dataset.function;
    dataType = dataset.type;
    dataCallback = dataset.callback;
    dataPlacement = dataset.placement;
    nextfocus = dataset.nextfocus;
    glcode = dataset.glcode;
    preventAutocomplete = dataset.preventautocomplete;
    dataEvent = dataset.event;
    dataService = dataset.service == undefined ? "WebPOSService" : dataset.service;
}
$(".autocomplete").bind("keydown", function (event) {
    isNotUpDownArrow = true;
    if (event.keyCode === 40 || event.keyCode === 38) {
        event.preventDefault();
        isNotUpDownArrow = false;
    }
});
bindCounter = 0;
$(".autocomplete").autocomplete({

    source: function (request, response) {

        if (isNotUpDownArrow) {
            setAutocompleteData(this.bindings[0].dataset);
            if (!dataEvent)
                getRecords();

        }

    },
    html: true
    ,
    select: function (event, ui) {

        event.preventDefault();


        for (var i = 0; i < Object.keys(ui.item).length; i++) {

            var splitedText = Object.keys(ui.item)[i].split('_');

            var itemClass = splitedText[0];
            var itemValue = Object.values(ui.item)[i] == undefined || Object.values(ui.item)[i] == "" || Object.values(ui.item)[i] == null ? "0" : Object.values(ui.item)[i];
            $(".selected" + dataType + itemClass + "").remove();
            $("<input type='hidden' class='selected" + dataType + itemClass + "' value='" + itemValue + "' />").appendTo("body");

        }

        $("[data-id=" + textboxid + "]").val("");
        insert(event);



        $("[data-id=" + textboxid + "]").val("");
        updateInputStyle();


    }

});
/*....................................*/


$(document).on("keypress", "#txtBarCodeSR", function (e) {
    if (e.which == 13 && $("#txtBarCodeSR").val().length && $('.PartyCode').val().length) {
        dataService = "WebPOSService";
        preventAutocomplete = false;
        dataFunction = "GetRecordSR";
        textboxid = "txtBarCodeSR";
        getRecords();
        $("#" + textboxid).select();
    } else if (!$("#txtBarCodeSR").val().length) {
        playError();
    } else if (!$('.PartyCode').val().length) {
        playError();
        swal('Select Party First!!', '', 'error');
    }
})

$(document).on("keypress", "#txtBarCodePR", function (e) {
    if (e.which == 13 && $("#txtBarCodePR").val().length && $('.PartyCode').val().length) {
        dataService = "WebPOSService";
        preventAutocomplete = false;
        dataFunction = "GetRecordPR";
        textboxid = "txtBarCodePR";
        getRecords();

        $("#" + textboxid).select();
    } else if (!$("#txtBarCodePR").val().length) {
        playError();
    } else if (!$('.PartyCode').val().length) {
        playError();
        swal('Select Party First!!', '', 'error');
    }
});

//$(document).on("keypress", "#txtBarCodeP", function (e) {
//    if (e.which == 13 && $("#txtBarCodeP").val().length && $('.PartyCode').val().length) {
//        dataFunction = "GetRecordP";
//        textboxid = "txtBarCodeP";
//        getRecords();

//        $("#" + textboxid).select();
//    } else if (!$("#txtBarCodeP").val().length) {
//        playError();
//    } else if (!$('.PartyCode').val().length) {
//        playError();
//        swal('Select Party First!!', '', 'error');
//    }
//})



$(document).ready(function () {

    var textBoxes = $("[data-id]");

    for (var i = 0; i < textBoxes.length; i++) {

        $("[data-id=" + textBoxes[i].dataset.id + "]").keydown(function (e) {

            setAutocompleteData(this.dataset);

            if (e.keyCode === 13) {

                enterKeyPressedOnAutoCompleteRow(e);
            }
        });

        $(document).on({
            click: function (e) {

                $("#" + dataType + "HeadTable").hide();
                $("#" + dataType + "Table").hide();
                $("[data-id=" + textboxid + "]").val("");
                insert(e);
            },
            mouseover: function () {

                $('#' + dataType + 'Table table tr').removeClass("ui-state-focus");
                $(this).addClass("ui-state-focus");
                loadSelectedItemsFromAutocomplete(this);
            }

        }, '#' + textBoxes[i].dataset.type + 'Table table tbody tr ');

        $(document).on("click", function (event) {
            var $trigger = $("#" + dataType + "Table table tr");
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $("#" + dataType + "HeadTable").hide();
                $("#" + dataType + "Table").hide();
            }
        });
    }

});


function getRecords() {   //  Sale New/Edit 11111111111111

    var ajaxData = "";

    if (dataFunction == "GLList") {
        ajaxData = { "TitleName": $("[data-id=" + textboxid + "]").val(), "GLCode": glcode };

    }
    else if (dataFunction == "GetRecords") {    //<----sale and sale edit

        var itemName = $("[data-id=" + textboxid + "]").length ? $("[data-id=" + textboxid + "]").val() : $("#" + textboxid).val();
        ajaxData = { "key": itemName };
    }
    else if (dataFunction == "GetIMEList") {
        ajaxData = { "key": $("#" + textboxid).val(), "descriptionType": $("[data-id=" + textboxid + "]").data("descriptiontype") };
    }
    else if (dataFunction == "GetItemIMEList") {
        ajaxData = { "key": $("#" + textboxid).val(), "itemCode": $("#ItemIMEDetailModal input.IME_ItemCode").val(), "descriptionType": $("[data-id=" + textboxid + "]").data("descriptiontype") };
    }
    else if (dataFunction == "GetRecordsSale") {    //<----sale and sale edit
        ajaxData = { "key": itemName, "stationId": $("#AppStationId").val() };
    }
    else if (dataFunction == "GetRecordSR") {

        var itemName = $("[data-id=" + textboxid + "]").length ? $("[data-id=" + textboxid + "]").val() : $("#" + textboxid).val();
        if (itemName.length > 10 && !isNaN(itemName)) {
            ajaxData = { "key": itemName + ',' + $('.PartyCode').val() + ',' + $('.PartyDealApplyNo').val() };
        }
        else
            ajaxData = { "key": itemName };

    }
    else if (dataFunction == "GetRecordPR") {

        var itemName = $("[data-id=" + textboxid + "]").length ? $("[data-id=" + textboxid + "]").val() : $("#" + textboxid).val();
        if (itemName.length > 10 && !isNaN(itemName)) {
            ajaxData = { "key": itemName + ',' + $('.PartyCode').val() + ',' + $('.PartyDealApplyNo').val() };
        }
        else
            ajaxData = { "key": itemName };

    }
    //else if (dataFunction == "GetRecordP") {

    //    var itemName = $("[data-id=" + textboxid + "]").length ? $("[data-id=" + textboxid + "]").val() : $("#" + textboxid).val();
    //    if (itemName.length > 10 && !isNaN(itemName)) {
    //        ajaxData = { "key": itemName + ',' + $('.PartyCode').val() + ',' + $('.PartyDealApplyNo').val() };
    //    }
    //    else
    //        ajaxData = { "key": itemName };

    //}
    else if (dataFunction == "GetItemsStockMovement") {
        ajaxData = { "key": $("[data-id=" + textboxid + "]").val() };
    }
    else {
        ajaxData = { "key": $("[data-id=" + textboxid + "]").val() };
    }

    $.ajax({
        async: false,
        url: `/${dataService}.asmx/` + dataFunction,
        type: "Post",

        data: ajaxData,

        success: function (items) {

            if (items == "") {
                playError();
            }
            if (preventAutocomplete == "true") {
                window[dataCallback](items);
            }
            else {
                debugger
                if (items.length != 0) {

                    if (items[0].isIME) {
                        var imei = $("#" + textboxid).val();
                        if (!items[0].isItemAvailble) {
                            swal('IME Not Found!!', '', 'error');
                            playError();
                            return false;
                        }
                        var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                        var Sno = rowCount;
                        var itemExist = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                        if (itemExist > 0 && !isDuplicateIMEI(imei)) {
                            Sno = itemExist;
                            var qty = Number($("#myTable ." + Sno + "_Qty").val()) + 1;
                            $("#myTable ." + Sno + "_Qty").val(qty);
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row
                            if (items[0].BarCodeCategory == 1) {
                                $("#IMEIRow [name=ImeiTxbx]").val(imei);
                                var element = $("#myTable tr[data-rownumber=" + Sno + "]")[0];
                                appendRowInIMEI(element, true, true);
                            }
                            //Ime Row Insert End
                        }
                        else if (!isDuplicateIMEI(imei)) {
                            insertSaleRow(Sno, items[0].Code, items[0].Description, items[0].Qty, items[0].Rate, 0, items[0].ItemDis, items[0].PerDis, items[0].DealDis, 0, 0, items[0].avrgCost, items[0].revCode, items[0].cgsCode, items[0].actualSellingPrice, items[0].PerPieceCommission)
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row
                            if (items[0].BarCodeCategory == 1) {
                                $("#IMEIRow [name=ImeiTxbx]").val(imei);
                                var rowNumer = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                                var element = $("#myTable tr[data-rownumber=" + rowNumer + "]")[0];
                                appendRowInIMEI(element, true, true);
                            }
                            //Ime Row Insert End
                        }
                        else if (isDuplicateIMEI(imei)) {
                            swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
                            playError();

                            return false;
                        }
                        rerenderSerialNumber();
                    }
                    else if (items[0].isIMESR) {
                        var imei = $("#" + textboxid).val();
                        if (!items[0].isItemAvailble) {
                            swal('IME Not Found or Sold to Other Party!!', '', 'error');
                            playError();
                            return false;
                        }
                        var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                        var Sno = rowCount;
                        var itemExist = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                        if (itemExist > 0 && !isDuplicateIMEI(imei)) {
                            Sno = itemExist;
                            var qty = Number($("#myTable ." + Sno + "_Qty").val()) + 1;
                            $("#myTable ." + Sno + "_Qty").val(qty);
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row

                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var element = $("#myTable tr[data-rownumber=" + Sno + "]")[0];
                            appendRowInIMEI(element, true, true);
                            //Ime Row Insert End
                        }
                        else if (!isDuplicateIMEI(imei)) {
                            insertSaleRow(Sno, items[0].Code, items[0].Description, items[0].Qty, items[0].Rate, 0, items[0].ItemDis, items[0].PerDis, items[0].DealDis, 0, 0, items[0].avrgCost, items[0].revCode, items[0].cgsCode, items[0].actualSellingPrice)
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row

                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var rowNumer = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                            var element = $("#myTable tr[data-rownumber=" + rowNumer + "]")[0];
                            appendRowInIMEI(element, true, true);
                            //Ime Row Insert End
                        }
                        else if (isDuplicateIMEI(imei)) {
                            swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
                            playError();

                            return false;
                        }




                        rerenderSerialNumber();

                    }
                    else if (items[0].isIMEPR) {
                        var imei = $("#" + textboxid).val();
                        if (!items[0].isItemAvailble) {
                            swal('IME Not Found or it is Purchased from Other Party!!', '', 'error');
                            playError();
                            return false;
                        }
                        var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                        var Sno = rowCount;
                        var itemExist = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                        if (itemExist > 0 && !isDuplicateIMEI(imei)) {
                            Sno = itemExist;
                            var qty = Number($("#myTable ." + Sno + "_Qty").val()) + 1;
                            $("#myTable ." + Sno + "_Qty").val(qty);
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row

                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var element = $("#myTable tr[data-rownumber=" + Sno + "]")[0];
                            appendRowInIMEI(element, true, true);
                            //Ime Row Insert End
                        }
                        else if (!isDuplicateIMEI(imei)) {
                            insertPRRow(Sno, items[0].Code, items[0].Description, items[0].Qty, items[0].Rate, 0, items[0].ItemDis, items[0].PerDis, items[0].DealDis, 0, 0, items[0].avrgCost, items[0].revCode, items[0].cgsCode, items[0].actualSellingPrice)
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row

                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var rowNumer = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                            var element = $("#myTable tr[data-rownumber=" + rowNumer + "]")[0];
                            appendRowInIMEI(element, true, true);
                            //Ime Row Insert End
                        }
                        else if (isDuplicateIMEI(imei)) {
                            swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
                            playError();

                            return false;
                        }
                        rerenderSerialNumber();
                    }
                    else if (items[0].isIMEP) {
                        var imei = $("#" + textboxid).val();
                        if (!items[0].isItemAvailble) {
                            swal('IME Not Found!!', '', 'error');
                            playError();
                            return false;
                        }
                        var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                        var Sno = rowCount;
                        var itemExist = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                        if (itemExist > 0 && !isDuplicateIMEI(imei)) {
                            Sno = itemExist;
                            var qty = Number($("#myTable ." + Sno + "_Qty").val()) + 1;
                            $("#myTable ." + Sno + "_Qty").val(qty);
                            calculationSale();

                            //Ime Row Insert Start --> This insert IME Row

                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var element = $("#myTable tr[data-rownumber=" + Sno + "]")[0];
                            appendRowInIMEI(element, true, true);
                            //Ime Row Insert End
                        }
                        else if (!isDuplicateIMEI(imei)) {
                            insertPRow(Sno, items[0].Code, items[0].Description, items[0].Qty, items[0].Rate, 0, items[0].ItemDis, items[0].PerDis, items[0].DealDis, 0, 0, items[0].avrgCost, items[0].revCode, items[0].cgsCode, items[0].actualSellingPrice)
                            calculationSale();
                            //Ime Row Insert Start --> This insert IME Row

                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var rowNumer = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                            var element = $("#myTable tr[data-rownumber=" + rowNumer + "]")[0];
                            appendRowInIMEI(element, true, true);
                            //Ime Row Insert End
                        }
                        else if (isDuplicateIMEI(imei)) {
                            swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
                            playError();

                            return false;
                        }




                        rerenderSerialNumber();

                    }
                    else {

                        var itemThs = "";
                        for (var i = 0; i < Object.keys(items[0]).length; i++) {

                            var splitedText = Object.keys(items[0])[i].split('_')
                            if (splitedText.length < 2) {

                                if (splitedText[0] == "Description") {
                                    itemThs += "<th class='" + dataType + splitedText[0] + "td width4' >" + splitedText[0] + "</th>"
                                } else if (splitedText[0] == "Name") {
                                    itemThs += "<th class='" + dataType + splitedText[0] + "td width3' >" + splitedText[0] + "</th>"
                                } else if (splitedText[0] == "Code") {
                                    itemThs += "<th class='" + dataType + splitedText[0] + "td width1' >" + splitedText[0] + "</th>"
                                } else {
                                    itemThs += "<th class='" + dataType + splitedText[0] + "td width2' >" + splitedText[0] + "</th>"

                                }

                            }

                        }
                        $("#" + dataType + "Table").remove();
                        $("#" + dataType + "HeadTable").remove();
                        $("<div id='" + dataType + "HeadTable'  class='autocompleteHeadTable'></div>").appendTo("body");
                        $("<div id='" + dataType + "Table' class='autocompleteTable'></div>").appendTo("body");

                        $("#" + dataType + "Table table").remove();
                        $("#" + dataType + "HeadTable table").remove();
                        $("#" + dataType + "Table").show();
                        $("#" + dataType + "HeadTable").show();
                        $("#" + dataType + "HeadTable ").append("<table class='headTable'><thead><tr>" + itemThs + "</tr></thead></table>")
                        $("#" + dataType + "Table ").append("<table class='bodyTable'><tbody class='cas'></tbody></table>")
                        /*For Up down Arrow count shoult be reset*/
                        count = -1;
                        /****************************************/
                        var appWidth = $(".app").width();

                        $("#" + dataType + "Table table").css("width", "100%");
                        $("#" + dataType + "HeadTable table").css("width", "99.15%");
                        $("#" + dataType + "Table,#" + dataType + "HeadTable").css("width", "99%");
                        $("#" + dataType + "Table,#" + dataType + "HeadTable").css("position", "absolute");
                        var maxHeight = 250;
                        if (dataPlacement == "top") {
                            $("#" + dataType + "HeadTable").css("top", $("[data-id=" + textboxid + "]").offset().top - maxHeight - 30);
                            $("#" + dataType + "Table").css("top", $("[data-id=" + textboxid + "]").offset().top - maxHeight);

                        } else {
                            $("#" + dataType + "HeadTable").css("top", $("[data-id=" + textboxid + "]").offset().top + 30);
                            $("#" + dataType + "Table").css("top", $("[data-id=" + textboxid + "]").offset().top + 30 + $("#" + dataType + "HeadTable thead tr").height());
                        }
                        $("#" + dataType + "Table").css("left", 10);
                        $("#" + dataType + "HeadTable").css("left", 9);
                        $("#" + dataType + "Table").css("max-height", maxHeight);
                        $("#" + dataType + "Table").css("overflow-y", "auto");
                        for (var i = 0; i < items.length; i++) {
                            var itemTds = "";
                            for (var j = 0; j < Object.values(items[i]).length; j++) {

                                var splitedText = Object.keys(items[i])[j].split('_');

                                if (splitedText.length < 2) {
                                    if (splitedText[0] == "Description") {
                                        itemTds += "<td class='" + dataType + splitedText[0] + "td width4  name' >" + Object.values(items[i])[j] + "</td>";
                                    }
                                    else if (splitedText[0] == "Code") {
                                        itemTds += "<td class='" + dataType + splitedText[0] + "td width1' >" + Object.values(items[i])[j] + "</td>";
                                    }
                                    else if (splitedText[0] == "Name") {
                                        itemTds += "<td class='" + dataType + splitedText[0] + "td width3 name' >" + Object.values(items[i])[j] + "</td>";
                                    }
                                    else if (splitedText[0] == "Balance") {
                                        itemTds += "<td class='" + dataType + splitedText[0] + "td width2' >" + (Number(Object.values(items[i])[j])).toFixed(2) + "</td>";
                                    }
                                    else {
                                        itemTds += "<td class='" + dataType + splitedText[0] + "td width2' >" + Object.values(items[i])[j] + "</td>";
                                    }

                                }
                                else {
                                    itemTds += "<td class='" + dataType + splitedText[0] + "td' style='display:none' >" + Object.values(items[i])[j] + "</td>";
                                }

                            }
                            $("<tr class='ui-menu-item'></tr>")
                                .append(itemTds)
                                .appendTo("#" + dataType + "Table .bodyTable  tbody");



                        }
                        $(".selected").remove();
                    }

                }
                else {
                    $("#" + dataType + "Table").remove();
                    $("#" + dataType + "HeadTable").remove();
                    $("#" + dataType + "Table table").remove();
                    $("#" + dataType + "HeadTable table").remove();
                    $(".selected").remove();
                }
            }
        }, fail: function (jqXhr, exception) {

        }
    });
}


//$("[data-id="+textboxid+"]").keydown(function (e) {

//    updownArrowAndEnterKey(e);

//});

function updownArrowAndEnterKey(e) {

    var autoCompleteTable;
    var row;
    if (e.keyCode === 40) {

        autoCompleteTable = $("#" + dataType + "Table table tbody");
        row = $(autoCompleteTable.children());
        $(row).removeClass("ui-state-focus").focus();
        if (count === row.length) {
            count = 0;

            $(row[count]).addClass("ui-state-focus").focus();
            $("#" + dataType + "Table").scrollTop((35 * count) - 200)
            loadSelectedItemsFromAutocomplete(row[count]);
        }
        else if (count < row.length && row.length !== 0) {
            count++;

            $(row[count]).addClass("ui-state-focus").focus();

            $("#" + dataType + "Table").scrollTop((35 * count) - 200)
            loadSelectedItemsFromAutocomplete(row[count]);

        }
    }
    else if (e.keyCode === 38) {
        autoCompleteTable = $("#" + dataType + "Table table tbody");
        row = $(autoCompleteTable.children());
        $(row).removeClass("ui-state-focus").focus();
        if (count === 0) {
            count = row.length;
            $(row[count]).addClass("ui-state-focus").focus();
            $("#" + dataType + "Table").scrollTop((35 * count) - 200)
            loadSelectedItemsFromAutocomplete(row[count]);


        }
        else if (count !== -1 && row.length !== 0) {
            count--;

            $(row[count]).addClass("ui-state-focus").focus();
            $("#" + dataType + "Table").scrollTop((35 * count) - 200)
            loadSelectedItemsFromAutocomplete(row[count]);

        }
    }
}

function loadSelectedItemsFromAutocomplete(row) {

    for (var i = 0; i < $(row).children().length; i++) {
        var itemClass = $($(row).children()[i]).attr("class").replace('td', "").split(' ')[0];
        $(".selected" + itemClass + "").remove();
        $("<input type='hidden' class='selected" + itemClass + " selected' value='" + $($(row).children()[i]).text() + "' />").appendTo("body");

    }
}


function enterKeyPressedOnAutoCompleteRow(e) {
    if (e.which === 13 && e.shiftKey) {          // if ctrl+End jump to Cash Paid
        save();
    }

    //Item Insert
    if (e.keyCode === 13 && $("[data-id=" + textboxid + "]").val() !== "" && $(".selected" + dataType + "Code").val() != undefined) {

        e.preventDefault();
        $("#" + dataType + "HeadTable").hide();
        $("#" + dataType + "Table").hide();
        $("[data-id=" + textboxid + "]").val("");
        insert(e);
    }
    else if (e.keyCode === 13 && $("[data-id=" + textboxid + "]").val().trim() === "" && $(".selected" + dataType + "Code").val() === undefined) {
        e.preventDefault();
        getRecords();


    }
    else if (e.keyCode === 13 && $("[data-id=" + textboxid + "]").val().trim() !== "" && $(".selected" + dataType + "Code").val() === undefined) { //ime
        e.preventDefault();
        $("#" + dataType + "HeadTable").hide();
        $("#" + dataType + "Table").hide();
        getRecords();

    }
    else if (e.keyCode === 13 && $("[data-id=" + textboxid + "]").val().trim() === "" && $(".selected" + dataType + "Code").val() !== undefined && $("#" + dataType + "HeadTable").css('display') == "none") {
        e.preventDefault();
        $("#" + dataType + "HeadTable").hide();
        $("#" + dataType + "Table").hide();
        getRecords();


    }
    else if (e.keyCode === 13 && $("[data-id=" + textboxid + "]").val().trim() === "" && $(".selected" + dataType + "Code").val() !== undefined) {
        e.preventDefault();
        $("#" + dataType + "HeadTable").hide();
        $("#" + dataType + "Table").hide();
        $("[data-id=" + textboxid + "]").val("");
        insert(e);
    }
    updateInputStyle();
}

function addFooterRow(ItemCode, QTY, inHandQty) {

    if ($(".footer" + ItemCode + "").length === 0) {
        $("<tr><td>" + ItemCode + " </td><td class='footer" + ItemCode + "'>" + parseFloat(QTY).toFixed(0) + "</td><td>" + parseFloat(inHandQty).toFixed(0) + " </td></tr>").appendTo(".footerTable .tbody tbody");
    } else {
        $(".footer" + ItemCode).text(parseFloat(QTY).toFixed(0))

    }
}

function tog(v) { return v ? 'addClass' : 'removeClass'; }
$(document).on('input', '.clearable', function () {
    $(this)[tog(this.value)]('x');

}).on('mousemove', '.x', function (e) {
    $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
}).on('touchstart click', '.onX', function (ev) {
    ev.preventDefault();
    $(this).removeClass('x onX').val('').change();


    var inputs = $(".clientInput");
    for (var i = 0; i < inputs.length; i++) {
        $(inputs[i]).val("");
    }
    var url = window.location.href;
    if (url.indexOf("PurchaseEdit") > -1 || url.indexOf("SaleEdit") > -1 || url.indexOf("SaleNewItems") > -1 || url.indexOf("PurchaseNewItems") > -1) {
        calculationSale()
    }


}).on('keyup', '[name=PartyName]', function (ev) {
    ev.preventDefault();
    if (ev.keyCode == 27) {
        $(this).removeClass('x onX').val('').change();


        var inputs = $(".clientInput");
        for (var i = 0; i < inputs.length; i++) {
            $(inputs[i]).val("");
        }
        if (url.indexOf("PurchaseEdit") > -1 || url.indexOf("SaleEdit") > -1 || url.indexOf("SaleNewItems") > -1 || url.indexOf("PurchaseNewItems") > -1) {
            calculationSale()
        }
    }


});
$(document).on('keyup', document, function (ev) {
    ev.preventDefault();
    if (ev.keyCode == 27) {
        $('.autocompleteHeadTable').hide();
        $('.autocompleteTable').hide();
    }
});
function insertDataToInputSimple() {
    $("[data-id=" + textboxid + "]").val("");
    var selectedPartyProperties = $("[class*=selected" + dataType + "]");
    for (var a = 0; a < selectedPartyProperties.length; a++) {

        var className = selectedPartyProperties[a].className.split(' ')[0].replace("selected", "");
        var value = selectedPartyProperties[a].value.trim();

        var isInputAvailbaleByName = $("[name=" + className + "]").length == 1;
        var isInputAvailbaleById = $("[id=" + className + "]").length == 1;
        var isInputAvailbaleByClass = $("." + className).length == 1;



        if (isInputAvailbaleByName) {
            $("[name=" + className + "]").text(value);
            $("[name=" + className + "]").val(value);

        }
        else if (isInputAvailbaleById) {
            $("[id=" + className + "]").text(value);
            $("[id=" + className + "]").val(value);

        }
        else if (isInputAvailbaleByClass) {
            $("." + className + "").text(value);
            $("." + className + "").val(value);

        }

        else {
            $("." + className + "").text(value);
            $("." + className + "").val(value);

        }

    }



    if (typeof ExpenceCash == "function") { // call from cashpayment to party through cash
        $(".CashPaid").focus();
    }
    $(nextfocus).focus();
    updateInputStyle();
}
function insert(e) {
    if (dataCallback) {
        window[dataCallback]();
    }
    else if (dataType == "Item") {
        if (typeof insertDataInSale === "function") {
            insertDataInSale();
        }
        else if (typeof insertVoucherRow === "function") {
            insertVoucherRow();
        } else {
            insertDataToInputSimple();
        }

    } else if (dataType == "Party") {

        if (typeof insertPartyDetailToInputFields === "function") {
            insertPartyDetailToInputFields(e);
        } else {
            insertDataToInputSimple();
        }
        if ($(".salePage .PartyBalance").length > 0) {
            let norBalance = $(".selectedPartyNorBalance").val();
            let balance = Number($(".selectedPartyBalance").val());
            if (norBalance == "1" && (balance > 0 || balance < 0)) {
                $(".salePage .PartyBalance").val(balance);
            }
            if (norBalance == "2" && (balance > 0 || balance < 0)) {
                $(".salePage .PartyBalance").val(-1 * balance);

            }
        }
        if ($("#chkAutoDeal").length > 0) {
            document.getElementById("chkAutoDeal").checked = Number($(".selectedPartyAutoDealRs").val());
        }
        if ($("#chkAutoPerDis").length > 0) {
            document.getElementById("chkAutoPerDis").checked = Number($(".selectedPartyAutoDisPer").val());
        }
        if ($("#SearchBox").length > 0) {
            $("#SearchBox").focus();
            $("#SearchBox").select();
        }
    }
    else if (dataType == "ExpenceHead") {

        insertDataToInputSimple();

    }
    else if (dataType == "AllVouchers") {

        insertVoucherRow();

    }
    else if (dataType == "MainItem") {
        if (typeof focusToInGredientTxbx === "function") {

            insertDataToInputSimple();
            focusToInGredientTxbx();
        }
    }
    else if (dataType == "Ingredient") {
        if (typeof focusToQtyTxbx === "function") {

            insertDataToInputSimple();
            focusToQtyTxbx();
        }
    }


    else {
        insertDataToInputSimple();
    }

    if (typeof GetData == "function") {
        GetData();
        $("[id=txtSellingPrice1]").focus();
    }
    if (typeof GetItemData == "function") {
        GetItemData();
    }

    if (typeof insertVoucherValuesToInputs == "function") {
        insertVoucherValuesToInputs();
    }
    if (typeof GetBrand_PartyWiseDisc_Rate == "function") {
        GetBrand_PartyWiseDisc_Rate($(".PartyCode").val());
    }
    if (typeof GetPartyData == "function") {
        GetPartyData($(".PartyCode").val());
    }
    if (typeof updateDealRs == "function" && dataType == "Party") {
        updateDealRs();
    } if (typeof updateDisPer == "function" && dataType == "Party") {
        updateDisPer();
    }
    if (typeof calculation == "function" && dataType == "Party") {
        calculation();
    }

    if (typeof GetPartyOpeningBalance == "function") {
        GetPartyOpeningBalance();
    }
    if (typeof GetInventoryOpening == "function" && dataType == "Item") {
        GetInventoryOpening();
    }
    if (typeof insertDataInStockMovement == "function" && dataType == "Item") {
        insertDataInStockMovement();
    }
    $(nextfocus).focus().select();
    updateInputStyle();
}