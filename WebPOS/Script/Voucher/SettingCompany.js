﻿function saveCostingMethod() {
   
    var ModelSetting = {
        CGSMethod: $('input[name="CostingMethod"]:checked').val(),
        
    }


    $.ajax({
        url: "/Setting/GeneralSetting.aspx/SaveCostingMethod",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelSetting: ModelSetting }),
        success: function (BaseModel) {

            if (BaseModel.d.Success) {

                smallSwal("Success", BaseModel.d.Message, "success");
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}
function saveIMELength() {
 
    var ModelSetting = {
        IMEL1: $(".IMEL1").val(),
        IMEL2: $(".IMEL2").val(),
        IMEL3: $(".IMEL3").val()
    }


    $.ajax({
        url: "/Setting/GeneralSetting.aspx/SaveIMELEngth",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelSetting: ModelSetting }),
        success: function (BaseModel) {

            if (BaseModel.d.Success) {

                smallSwal("Success", BaseModel.d.Message, "success");
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}
function SaveBefore() {
    var SaveOk = "Yes";
    var CompanyName = $(".CompanyName").val();
    if (CompanyName == "") {
        SaveOk = "No";
        swal("Error", "Type Company Name", "error");
    }

    if (SaveOk == "Yes") {
        swal({
            title: "Business Manager",
            text: "Are You Sure to save?????",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }
}

function save() {
    
    var ModelSetting = {
        CompanyName: $(".CompanyName").val(),
        CompanyAddress1: $(".CompanyAddress1").val(),
        CompanyAddress2: $(".CompanyAddress2").val(),
        CompanyAddress3: $(".CompanyAddress3").val(),
        CompanyAddress4: $(".CompanyAddress4").val(),
        CompanyPhone: $(".Phone").val(),
        
    }

  
    $.ajax({
        url: "/Setting/GeneralSetting.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelSetting: ModelSetting }),
        success: function (BaseModel) {
            
            if (BaseModel.d.Success) {
                
                smallSwal("Success", BaseModel.d.Message, "success");
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

function SaveBeforeInvoiceFormat() {
    var SaveOk = "Yes";
    
    if (SaveOk == "Yes") {
        swal({
            title: "Business Manager",
            text: "Are You Sure to save invoice format?????",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    saveInvoiceFormat();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }
}

function saveInvoiceFormat() {
    var ModelSetting = {
        InvoiceFormat: $(".InvoiceFormat").val(),
    }


    $.ajax({
        url: "/Setting/GeneralSetting.aspx/SaveInvoiceFormat",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelSetting: ModelSetting }),
        success: function (BaseModel) {

            if (BaseModel.d.Success) {

                smallSwal("Success", BaseModel.d.Message, "success");
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}



$(document).on('keydown', ".CompanyName", function (e) {
    //enter
    if (e.which == 13) {
        $(".Address1").focus().select();
      
        e.preventDefault();
    }
});
$(document).on('keydown', ".Address1", function (e) {
    //enter
    if (e.which == 13) {
        $(".Address2").focus().select();
        e.preventDefault();
    }
});
$(document).on('keydown', ".Address2", function (e) {
    //enter
    if (e.which == 13) {
        $(".Address3").focus().select();
        e.preventDefault();
    }
});
$(document).on('keydown', ".Address3", function (e) {
    //enter
    if (e.which == 13) {
        $(".Phone").focus().select();
        e.preventDefault();
    }
});
$(document).on('keydown', ".Phone", function (e) {
    //enter
    if (e.which == 13) {
          SaveBefore();
        e.preventDefault();
    }
});
