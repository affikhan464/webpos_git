﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Correction/IncentiveGivenCashEdit.aspx/SaveIncentiveExpenceCashEdit",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                swal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
               
                $(".Date").val(date);
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}


function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        text: "Are You Sure to Delete this transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                var ModelPaymentVoucher = {
                    VoucherNo: $(".VoucherNo").val(),
                    Date: $(".Date").val(),
                    PartyCode: $(".PartyCode").val(),
                    PartyName: $(".PartyName").val(),
                    CashPaid: $(".CashPaid").val(),
                    Narration: $(".Narration").val()
                }
                debugger
                $.ajax({
                    url: "/Correction/IncentiveGivenCashEdit.aspx/Delete",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                    success: function (BaseModel) {
                        debugger
                        if (BaseModel.d.Success) {
                            var date = $(".Date").val();
                            swal("Success", BaseModel.d.Message, "success");
                            //alert(BaseModel.d.Message);
                            $(".empt").val("");
                            updateInputStyle();

                            $(".Date").val(date);
                        }
                        else {
                            swal("Error", BaseModel.d.Message, "error");
                        }
                    }
                })
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}

$(document).ready(function () {
    $("#txtCashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            calculation();
            $("#txtNarration").focus();
        }
    });

});
$(document).ready(function () {
    $("#txtNarration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            calculation();
            $("#btnSave").focus();
        }
    });

});

function InCentiveGivenCashEdit() {//call from

}
function calculation() {
    
    var Balance = $(".PartyBalanceActual").val();
    var Paid = $(".CashPaid").val();
    
    Balance = isNaN(Balance) ? 0 : Number(Balance);
    Paid = isNaN(Paid) ? 0 : Number(Paid);

    var NewBalance = 0;
    if (Paid == "") { Paid = 0; }
    NewBalance = (Balance - Paid);
    //$(".PartyBalance").val(NewBalance);
}
function GetVoucherData() {
    $(".empt").val("");
    if ($(".VoucherNo").val().trim() != "") {
        $.ajax({
            url: '/WebPOSService.asmx/GetIncentiveGivenVoucherDataCash',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },
            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".PartyCode").val(ModelPaymentVoucher.PartyCode);
                    $(".PartyName").val(ModelPaymentVoucher.PartyName);
                    $(".CashPaid").val(ModelPaymentVoucher.CashPaid);
                    $(".PartyAddress").val(ModelPaymentVoucher.Address);
                    $(".PartyBalance").val(ModelPaymentVoucher.Balance);
                    $(".PartyBalanceActual").val(ModelPaymentVoucher.Balance);
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                updateInputStyle();
               
                } else {
                    smallSwal("Error", ModelPaymentVoucher.Message, "error");
                }
                $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                manageNextPreviousBtn();
                $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }

}