﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".PartyCode").val();
    var OpeningUnits = $(".OpeningUnits").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (OpeningUnits == "") {
        SaveOk = "No";
        swal("Error", "Type Opening Units", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save() {
    var model = {
        ItemName: $(".ItemName").val(),
        ItemCode: $(".ItemCode").val(),
        OpeningUnits: Number($(".OpeningUnits").val()),
        ReceivedUnits: Number($(".ReceivedUnits").val()),
        IssuedUnits: Number($(".IssuedUnits").val()),
        ClosingUnits: Number($(".ClosingUnits").val()),
        RequiredClosingUnits: Number($(".RequiredClosingUnits").val()),
        PurchasingCost: Number($(".PurchasingCost").val()),
        SellingCost: Number($(".SellingCost").val()),
        TotalValue: Number($(".TotalValue").val()),

    }
    debugger
    $.ajax({
        url: "/Transactions/OpenningInventory.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: model }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                swal("Success", BaseModel.d.Message, "success");
                $(".empt").val("");
                updateInputStyle();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();

    $(".inv1").val("");
    $(".inv3").val("0");


    //$("input").val("0");
    //calculationSale();
}