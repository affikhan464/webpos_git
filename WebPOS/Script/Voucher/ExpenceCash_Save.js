﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".Code").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Expence Head", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".ExpenceHeadCode").val(),
        PartyName: $(".ExpenceHeadTitle").val(),
        CashPaid: $(".CashPaid").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Transactions/ExpenseEntry.aspx/SaveExpenceVoucher",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
               
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                $(".Date").val(date);
                updateInputStyle();
                $(".ExpenceHeadTitle").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();
    
    $(".inv1").val("");
    $(".inv3").val("0");
    

    //$("input").val("0");
    //calculationSale();
}
$(document).ready(function () {

    $(".ExpenceHeadTitle").focus();

    $("#txtCashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#txtNarration").focus();
        }
    });
    $("#txtNarration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#btnSave").focus();
        }
    });
    $(".VoucherNo").keypress(function (event) {
        if (event.which == 13) {
            // event.preventDefault();
            if ($(".VoucherNo").val() != "" || $(".VoucherNo").val() != null) {
                GetVoucherData();
                $(".ExpenceHeadTitle").focus();
            }
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            SaveBefore();

        }
    });

    $(".ExpenceHeadTitle").keydown (function (event) {
        if (event.keyCode == 27) {

            $(".ExpenceHeadTitle").val("");
            $(".ExpenceHeadCode").val("");

        }
    });




});

