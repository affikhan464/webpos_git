﻿
function SaveBefore() {



    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var PartyCode = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {
        swal({
            title: "Business Manager",
            text: "Are You Sure to made a transaction?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
       
    }

}





function save() {
    
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Address: $(".PartyAddress").val(),
        Narration: $(".Narration").val()
        
    }

    debugger
    $.ajax({
        url: "/Transactions/PaymentToPartyThroughCash.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                $(".Date").val(date);
                updateInputStyle();
                $(".PartyName").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}
$(document).on('keydown', ".Narration", function (e) {
    //enter
    if (e.which == 13) {
        SaveBefore();
        e.preventDefault();
    }
});
$(document).on('keydown', ".CashPaid", function (e) {
    //enter
    if (e.which == 13) {
        $(".Narration").focus().select();
        e.preventDefault();
    }
});
function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();
    
    $(".inv1").val("");
    $(".inv3").val("0");
    

    //$("input").val("0");
    //calculationSale();
}