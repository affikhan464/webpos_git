﻿
function SaveBefore() {

    

    var SaveOk = "Yes";
    var BankCode = $(".BankCode").val();
    var PartyCode = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    

    if (BankCode == "") {
        SaveOk = "No";
        swal("Error", "Select Bank", "error");
    }
    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save() {
    
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Address: $(".PartyAddress").val(),
        BankCode: $(".BankCode").val(),
        BankName: $(".BankTitle").val(),
        Narration: $(".Narration").val(),
        ChqNo: $(".CheqNo").val()
        
    }

    debugger
    $.ajax({
        url: "/Transactions/PaymentToPartyThroughBank.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                swal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
                $(".Date").val(date);
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();
    
    $(".inv1").val("");
    $(".inv3").val("0");
    

    //$("input").val("0");
    //calculationSale();
}