﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {
        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Transactions/IncentiveReceiving.aspx/SaveIncentiveInCome",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                swal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
               
                $(".Date").val(date);
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}



 

$(document).ready(function () {
    $("#txtCashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            calculation();
            $("#txtNarration").focus();
        }
    });

});
$(document).ready(function () {
    $("#txtNarration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            calculation();
            $("#btnSave").focus();
        }
    });

});


function calculation() {

    
    var Balance = $(".PartyBalanceActual").val();
    var Paid = $(".CashPaid").val();
    
    Balance = isNaN(Balance) ? 0 : Number(Balance);
    Paid = isNaN(Paid) ? 0 : Number(Paid);

    var NewBalance = 0;
    if (Paid == "") { Paid = 0; }
    NewBalance = (Balance - Paid);
    $(".PartyBalance").val(NewBalance);


}