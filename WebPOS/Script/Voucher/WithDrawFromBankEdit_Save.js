﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".BankCode").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Bank", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".BankCode").val(),
        PartyName: $(".BankTitle").val(),
        CashPaid: $(".CashPaid").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Correction/WithDrawFromBankEdit.aspx/SaveWidthDrawFromBankEdit",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                swal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
                $(".Date").val(date);
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }

        }
    })
}

function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        text: "Are You Sure to Delete this transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {


                var ModelPaymentVoucher = {
                    VoucherNo: $(".VoucherNo").val(),
                    Date: $(".Date").val(),
                    PartyCode: $(".BankCode").val(),
                    PartyName: $(".BankTitle").val(),
                    CashPaid: $(".CashPaid").val(),
                    Narration: $(".Narration").val()
                }
                debugger
                $.ajax({
                    url: "/Correction/WithDrawFromBankEdit.aspx/Delete",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                    success: function (BaseModel) {
                        debugger
                        if (BaseModel.d.Success) {
                            var date = $(".Date").val();
                            swal("Success", BaseModel.d.Message, "success");
                            //alert(BaseModel.d.Message);
                            $(".empt").val("");
                            updateInputStyle();
                            $(".Date").val(date);
                        }
                        else {
                            swal("Error", BaseModel.d.Message, "error");
                        }

                    }
                })
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function GetVoucherData() {
    if ($(".VoucherNo").val().trim() != "") {
        $.ajax({
            url: '/WebPOSService.asmx/GetCashWithDrawFromBankVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },
            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".BankCode").val(ModelPaymentVoucher.PartyCode);
                    $(".BankTitle").val(ModelPaymentVoucher.PartyName);
                    $(".AccountNo").val(ModelPaymentVoucher.AccountNo);
                    $(".CashPaid").val(ModelPaymentVoucher.CashPaid);
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                    updateInputStyle();

                } else {
                    smallSwal("Error", ModelPaymentVoucher.Message, "error");
                }
                $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                manageNextPreviousBtn();
                $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }

}