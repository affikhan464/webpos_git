﻿
function GetVoucherData() {
    if ($(".VoucherNo").val().trim() != "") {
        $.ajax({
            url: '/WebPOSService.asmx/GetCashReceiptVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },

            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".PartyCode").val(ModelPaymentVoucher.PartyCode);
                    $(".PartyCodeOld").val(ModelPaymentVoucher.PartyCode);
                    $(".PartyName").val(ModelPaymentVoucher.PartyName);
                    $(".PartyAddress").val(ModelPaymentVoucher.Address);
                    $(".CashPaid").val(Number(ModelPaymentVoucher.CashPaid).toFixed(2));
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                    $(".PartyBalance").val(Number(ModelPaymentVoucher.Balance).toFixed(2));
                    $(".DisRs").val(Number(ModelPaymentVoucher.DisRs).toFixed(2));

                    updateInputStyle();
                    calculation();
                    $(".fa-spinner").removeClass('fa-spin');
                } else {
                    smallSwal("Error", ModelPaymentVoucher.Message, "error");
                    $(".fa-spinner").removeClass('fa-spin');
                }
                $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                manageNextPreviousBtn();
            },
            fail: function (jqXhr, exception) {
                $(".fa-spinner").removeClass('fa-spin');
            }
        })

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }

}

function SaveBefore() {



    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var PartyCode = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save() {

    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Address: $(".PartyAddress").val(),
        Narration: $(".Narration").val(),
        PartyCodeOld: $(".PartyCodeOld").val(),
        DepartmentId: $("[name=DepartmentDD]").val(),
        CashGLCode: $("[name=CashReceivedCategoryDD]").val(),
        DisRs: $(".DisRs").val()
    }

    debugger
    $.ajax({
        url: "/Correction/ReceiptFromPartyThroughCashEdit.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
                $(".Date").val(date);
                LoadPaymentDetail();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        text: "Are You Sure to Delete this transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                var ModelPaymentVoucher = {
                    VoucherNo: $(".VoucherNo").val(),
                    Date: $(".Date").val(),
                    PartyCode: $(".PartyCode").val(),
                    PartyName: $(".PartyName").val(),
                    CashPaid: $(".CashPaid").val(),
                    Address: $(".PartyAddress").val(),
                    Narration: $(".Narration").val(),
                    PartyCodeOld: $(".PartyCodeOld").val(),
                }

                
                $.ajax({
                    url: "/Correction/ReceiptFromPartyThroughCashEdit.aspx/Delete",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                    success: function (BaseModel) {
                   
                        if (BaseModel.d.Success) {
                            var date = $(".Date").val();
                            smallSwal("Success", BaseModel.d.Message, "success");
                            LoadPaymentDetail();
                            $(".empt").val("");
                            $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                            updateInputStyle();
                            $(".Date").val(date);
                        }
                        else {
                            swal("Error", BaseModel.d.Message, "error");
                        }
                    }
                });
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();

    $(".inv1").val("");
    $(".inv3").val("0");


    //$("input").val("0");
    //calculationSale();
}
function LoadPaymentDetail() {

    $(".loader").show();
    var counter = 0;
    var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
    $.ajax({
        url: '/Reports/ReceiptandCapitalReports/DateWiseReceiptDetail.aspx/getReportReceiptDetail',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ "PartyCode": $(".PartyCode").val() }),
        success: function (response) {

            if (response.d.Success) {
                ClearVoucherData();

                if (response.d.ListofInvoices.length > 0) {

                    for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                        counter++;
                        $("<div class='itemRow newRow'>" +
                            "<div class='seven' style='width:30%!important'><span>" + counter + "</span></div>" +
                            "<div class='seven' style='width:50%!important'><span>" + response.d.ListofInvoices[i].Date + "</span></div>" +
                            "<div class='thirteen' style='width:300%!important'><span>" + response.d.ListofInvoices[i].PartyName + "</span></div>" +
                            "<div class='thirteen'><input name='a' type='text' id='newQtyTextbox_" + i + "' value=" + response.d.ListofInvoices[i].NumberOfInvoices + " disabled /></div>" +
                            "<div class='thirteen'><input name='Paid'  id='newRateTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + " disabled /></div>" +
                            "<div class='five'> <i  data-vno='" + response.d.ListofInvoices[i].NumberOfInvoices + "' onclick='edit(this)' class='fas fa-edit mr-3'></i></div>" +

                            "</div>").appendTo(".dayCashAndCreditSale .itemsSection");

                    }
                    $(".loader").hide();


                } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                    $(".loader").hide(); swal({
                        title: "No Result Found ",
                        text: "No Result Found!"
                    });
                }

            }
            else {
                $(".loader").hide();
                swal({
                    title: "there is some error",
                    text: response.d.Message
                });
            }

        },
        error: function (error) {
            swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

        }
    });
}
function ClearVoucherData() {
    $(".dayCashAndCreditSale .itemsSection .itemRow").remove();
}
function edit(element) {
    $(".empt").val("");
    var Vno = $(element).data('vno');
    $(".VoucherNo").val(Vno);
    GetVoucherData();
    //updateInputStyle();

}

