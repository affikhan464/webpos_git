﻿

function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".Code").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Expence Head", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".ExpenceHeadCode").val(),
        PartyName: $(".ExpenceHeadTitle").val(),
        CashPaid: $(".CashPaid").val(),
        //Address: $(".PartyAddress").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Correction/ExpenseEntryEdit.aspx/SaveExpenceVoucherEdit",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                $(".Date").val(date);
                updateInputStyle();
                LoadPaymentDetail();
                $(".VoucherNo").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        text: "Are You Sure to Delete this transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                var ModelPaymentVoucher = {
                    VoucherNo: $(".VoucherNo").val(),
                    Date: $(".Date").val(),
                    PartyCode: $(".ExpenceHeadCode").val(),
                    PartyName: $(".ExpenceHeadTitle").val(),
                    CashPaid: $(".CashPaid").val(),
                    //Address: $(".PartyAddress").val(),
                    Narration: $(".Narration").val()
                }
                
                $.ajax({
                    url: "/Correction/ExpenseEntryEdit.aspx/Delete",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                    success: function (BaseModel) {
                        
                        if (BaseModel.d.Success) {
                            var date = $(".Date").val();
                            smallSwal("Success", BaseModel.d.Message, "success");
                            //alert(BaseModel.d.Message);
                            $(".empt").val("");
                            $(".Date").val(date);
                            updateInputStyle();
                            LoadPaymentDetail();
                        }
                        else {
                            swal("Error", BaseModel.d.Message, "error");
                        }
                    }
                })
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();
    
    $(".inv1").val("");
    $(".inv3").val("0");
    

    //$("input").val("0");
    //calculationSale();
}


function GetVoucherData() {
    if ($(".VoucherNo").val().trim() != "") {
        $(".empt").val("");
        $.ajax({
            url: '/WebPOSService.asmx/GetCashExpenceVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },

            success: function (ModelPaymentVoucher) {
                 if (ModelPaymentVoucher.Success) {
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".ExpenceHeadCode").val(ModelPaymentVoucher.PartyCode);
                    $(".ExpenceHeadTitle").val(ModelPaymentVoucher.PartyName);
                    $(".CashPaid").val(ModelPaymentVoucher.CashPaid);
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                    updateInputStyle();

                } else {
                     smallSwal("Error", ModelPaymentVoucher.Message, "error");
                 }
                 $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                 manageNextPreviousBtn();
                 $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }

}
function LoadPaymentDetail() {

    $(".loader").show();
    var counter = 0;
    var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
    $.ajax({
        url: '/Reports/PaymentExpenceDrawings/DateWiseExpenceDetail.aspx/getReportVoucherDetail',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ "GLCode": $(".ExpenceHeadCode").val() }),
        success: function (response) {

            if (response.d.Success) {
                ClearVoucherData();

                if (response.d.ListofInvoices.length > 0) {

                    for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                        counter++;
                        $("<div class='itemRow newRow'>" +
                            "<div class='seven' style='width:30% !important'><span>" + counter + "</span></div>" +
                            "<div class='thirteen' style='width:70% !important'><span>" + response.d.ListofInvoices[i].Date + "</span></div>" +
                            "<div class='thirteen' style='width:200% !important'><span>" + response.d.ListofInvoices[i].Particular + "</span></div>" +
                            "<div class='thirteen' style='width:250% !important'><span>" + response.d.ListofInvoices[i].Description + "</span></div>" +
                            "<div class='thirteen'><input  value=" + response.d.ListofInvoices[i].VoucherNo + " disabled name='22' /></div>" +
                            "<div class='thirteen'><input  name='Amount' value=" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + " disabled /></div>" +
                            "<div class='five'> <i  data-vno='" + response.d.ListofInvoices[i].VoucherNo + "' onclick='edit(this)' class='fas fa-edit mr-3'></i></div>" +

                            "</div>").appendTo(".dayCashAndCreditSale .itemsSection");

                    }
                    $(".loader").hide();


                } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                    $(".loader").hide(); swal({
                        title: "No Result Found ",
                        text: "No Result Found!"
                    });
                }

            }
            else {
                $(".loader").hide();
                swal({
                    title: "there is some error",
                    text: response.d.Message
                });
            }

        },
        error: function (error) {
            swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

        }
    });
}
function ClearVoucherData() {
    $(".dayCashAndCreditSale .itemsSection .itemRow").remove();
}
function edit(element) {
    $(".empt").val("");
    var Vno = $(element).data('vno');
    $(".VoucherNo").val(Vno);
    GetVoucherData();
    //updateInputStyle();

}
$(document).ready(function () {
    LoadPaymentDetail();
    var InvoiceNumber = getQueryString("v");
    $(".VoucherNo").focus().select();
    if (InvoiceNumber != "" || InvoiceNumber != null) {
        $(".VoucherNo").val(InvoiceNumber);
        //GetVoucherData();
    }

    $("#txtCashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#txtNarration").focus();
        }
    });
    $("#txtNarration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#btnSave").focus();
        }
    });
    $(".VoucherNo").keypress(function (event) {
        if (event.which == 13) {
            // event.preventDefault();
            if ($(".VoucherNo").val() != "" || $(".VoucherNo").val() != null) {
                GetVoucherData();
                $(".ExpenceHeadTitle").focus();
            }
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            SaveBefore();
            
        }
    });

    $(".ExpenceHeadTitle").keydown(function (event) {
        if (event.keyCode == 27) {

            $(".ExpenceHeadTitle").val("");
            $(".ExpenceHeadCode").val("");

        }
    });




});



