﻿var global_RevenuAccount;
var global_COGSAccount;
function appendAttribute(Dropdown_ID, TableName, SelectId) {
    var model = { "attribute": TableName,"code":"0" };

    var dropdown_ID = Dropdown_ID;
    if (TableName == "GLCode") {
        model.code = $("#" + dropdown_ID).data('glcode') != undefined ? $("#" + dropdown_ID).data('glcode') : '0';
    }

    SelectId = SelectId == null || SelectId == undefined ? 1 : SelectId;
    $.ajax({
        url: '/WebPOSService.asmx/GetAttribute',
        type: "Post",
        data: model,
        success: function (data) {
            var ddtype = $("#" + dropdown_ID).data('ddtype');
            if (ddtype != undefined && ddtype == "classic") {
                populateClassicDropdown(Dropdown_ID, data, SelectId);
            } else {
                if (Dropdown_ID != "") {
                    populateDropdown(Dropdown_ID, data, SelectId);
                } else {
                    var ddData = "{";
                    for (var i = 0; i < data.length; i++) {
                        var code = data[i].Code
                        ddData += '"' + code + '":{"title":"' + data[i].Name + '","amount":"0"},'
                    }
                    ddData = ddData.substring(0, ddData.length - 1);
                    ddData += "}"

                    if (TableName == "COGSAccount") {

                        global_COGSAccount = JSON.parse(ddData);
                    }
                    else {
                        global_RevenuAccount = JSON.parse(ddData);
                    }
                }

            }

        }, fail: function (jqXhr, exception) {

        }
    });
}

function populateDropdown(Dropdown_ID, data, SelectId) {
    var ddData = [];

    SelectId = SelectId.toString();

    var isMultipleIds = Dropdown_ID.split(',').length > 1;
    var isMultipleSelectIds = SelectId.split(',').length > 1;
    if (isMultipleIds && !isMultipleSelectIds) {
        var ids = Dropdown_ID.split(',');
        ids.pop();
        for (var i = 0; i < data.length; i++) {
            if (data[i].Code == SelectId) {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: true,

                });
            } else {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: false,

                });
            }


        }

        for (var i = 0; i < ids.length; i++) {
            var parent = $(document.getElementById(ids[i]).parentElement);
            $("#" + ids[i] + "").remove();
            parent.append("<select id='" + ids[i] + "' class='dropdown'> </select>");

            $("#" + ids[i] + "").ddslick({
                width: "100%",
                data: ddData,
                onSelected: function (data) {
                    if (typeof selectedChanged == "function") {
                        selectedChanged(data);
                    }
                }

            });
        }

    }
    else if (isMultipleSelectIds && isMultipleSelectIds) {
        debugger
        var ids = Dropdown_ID.split(',');
        ids.pop();
        var selectIds = SelectId.split(',');
        selectIds.pop();
        for (var x = 0; x < selectIds.length; x++) {
            var multipleDdData = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].Code == selectIds[x]) {
                    multipleDdData.push({
                        text: data[i].Name,
                        value: data[i].Code,
                        selected: true,

                    });
                } else {
                    multipleDdData.push({
                        text: data[i].Name,
                        value: data[i].Code,
                        selected: false,

                    });
                }

            }
            ddData.push(multipleDdData);

        }


        for (var i = 0; i < ids.length; i++) {
            var parent = $(document.getElementById(ids[i]).parentElement);
            $("#" + ids[i] + "").remove();
            parent.append("<select id='" + ids[i] + "' class='dropdown'> </select>");

            $("#" + ids[i] + "").ddslick({
                width: "100%",
                data: ddData[i],
                onSelected: function (data) {
                    if (typeof selectedChanged == "function") {
                        selectedChanged(data);
                    }
                }

            });
        }

    }
    else {

        for (var i = 0; i < data.length; i++) {
            if (data[i].Code == SelectId) {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: true,

                });
            } else {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: false,

                });
            }


        }
        var parent = $(document.getElementById(Dropdown_ID).parentElement);
        $("#" + Dropdown_ID + "").remove();
        parent.append("<select id='" + Dropdown_ID + "' class='dropdown'> </select>");

        $("#" + Dropdown_ID + "").ddslick({
            width: "100%",
            data: ddData,
            onSelected: function (data) {
                if (typeof selectedChanged == "function") {
                    selectedChanged(data.selectedData);
                }
            }

        });
    }


}

function populateClassicDropdown(Dropdown_ID, data, SelectId) {
    var ddData = "";
    for (var i = 0; i < data.length; i++) {
        var code = data[i].Code
        var title = data[i].Name
        if (SelectId == code) {
            ddData += `<option selected="selected" value="${code}">${title}</option>`;
        } else {
            ddData += `<option value="${code}">${title}</option>`;
        }
    }
    $("#" + Dropdown_ID).html(ddData);
    return false;
}