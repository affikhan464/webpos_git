﻿function insertVoucherRow() {
    var rowNumber = $(".itemsSection .itemRow").length > 0 ? $(".itemsSection .itemRow").last().data().rownumber + 1 : 1;

  $("<div class='itemRow' data-rownumber='" + rowNumber + "'>" +
            "<div class='seven text-align-center'><span class='serialNumber '></span></div>" +

            "<div class='sixteen name'><input name='Code'  id='AllVouchersCode_" + rowNumber + "' disabled  /></div>" +
            "<div class='sixteen name'><input name='Description'  id='AllVouchersTitle_" + rowNumber + "' disabled   /></div>" +
            "<div class='sixteen name'><input name='Narration' id='narrationTextbox_" + rowNumber + "'   /></div>" +
            "<div class='thirteen'><input  type='number' name='Debit' type='text' id='debitTextbox_" + rowNumber + "'  /></div>" +
            "<div class='thirteen'><input type='number' name='Credit' id='creditTextbox_" + rowNumber + "'  /></div>" +
            "<div class='float-right'><i class='fas fa-times'></i></div>" +

            "</div>").appendTo(".BMtable .itemsSection");
    


    var selectedPartyProperties = $("[class*=selected" + dataType + "]");
    for (var a = 0; a < selectedPartyProperties.length; a++) {
      
        $("[id=" + selectedPartyProperties[a].className.split(' ')[0].replace("selected", "") + "_" + rowNumber+"]").val(selectedPartyProperties[a].value.trim());

    }
    assignSerialNumber();
    $("#narrationTextbox_" + rowNumber + "").focus();
    $("#narrationTextbox_" + rowNumber + "").select();


}
$(document).on("keydown", "[name='Narration']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13) {
        $("#debitTextbox_" + row + "").focus();
        $("#debitTextbox_" + row + "").select();
    }

    
});

$(document).on("keydown", "[name='Debit']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() == "")
        $("#creditTextbox_" + row + "").focus();
    else if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() != "" && $("#creditTextbox_" + row + "").val().trim() != "") {
        $("#creditTextbox_" + row + "").focus();
        $("#creditTextbox_" + row + "").select();
    }
    else if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() != "")
    {
        $(".AllVouchersTitle").focus();
        $(".AllVouchersTitle").select();
    }

    sum("Debit");

});
$(document).on("input", "[name='Debit']", function (e) {

    sum("Debit");

});
$(document).on("keydown", "[name='Credit']", function (e) {
 
    if (e.keyCode == 13) {
        $(".AllVouchersTitle").focus();
        $(".AllVouchersTitle").select();
    }
    sum("Credit");

})
$(document).on("input", "[name='Credit']", function () {
    
    sum("Credit");

})
function assignSerialNumber() {
    var serialNumbers = $(".serialNumber");

    serialNumbers.each(function (index, element) {
        $(element).text(index + 1);
    })

}
function sum(name) {

    var sumValue = 0;
    var values = $("[name=" + name + "]");
    values.each(function (index, element) {
        var value = $(element).val().trim() == "" ? 0 : $(element).val();
        sumValue += Number(value);
    });

    $("[name=total" + name + "]").val(sumValue);


}

function save() {

    if ($("[name=totalCredit]").val() == $("[name=totalDebit]").val()) {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    resolve();
                    saveVoucher();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

    }
    else {
        swal("Error", "Debit amount is not equal to credit amount.", "error");
    }
}
function saveVoucher() {
    var serialNumbers = $(".itemsSection .itemRow");

    var addedRows = [];
    serialNumbers.each(function (index, element) {

        var row = $(element);
        var model = {
            Code: row.find("[name=Code]").val(),
            Title: row.find("[name=Description]").val(),
            Narration: row.find("[name=Narration]").val(),
            Credit: row.find("[name=Credit]").val().trim() == "" ? "0" : row.find("[name=Credit]").val(),
            Debit: row.find("[name=Debit]").val().trim() == "" ? "0" : row.find("[name=Debit]").val(),
        }
        addedRows.push(model);
    });

    var voucherType = $("#VoucherTypeDD .dd-selected-value").val();
    var voucherModel = {
        Rows: addedRows,
        VoucherType: voucherType
    };
    $.ajax({
        url: "/Transactions/Voucher.aspx/Save" + voucherType + "",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: voucherModel }),
        success: function (response) {
            debugger
            if (response.d.Success) {
                swal("Success", response.d.Message, "success");
            }
            else {
                swal("Error", response.d.Message, "error");

            }


        }



    })
}
$(document).on("click", "#saveBtn", function () {
    save();
});
$(document).on("keydown",  function (e) {

   
    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }
   

})
$(document).on("click", ".reports .fa-times", function () {
    var row = $(this).parents(".itemRow");
    row.remove();


    sum("Credit");
    sum("Debit");
    assignSerialNumber();
});