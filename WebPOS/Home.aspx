﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WebPOS.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Home - Business Manager </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="home-page">
        <section id="main-section" class="section" style="display: none;">
            <div class="row">
                <div data-page="179" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/NewSale/SaleNewItems.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon bluecorel">
                                <i class="fas fa-shopping-cart"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Sale New Item</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="177" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/PurchaseNew/PurchaseNewItems.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon purplish">
                                <i class="fas fa-cart-arrow-down"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Purchase New Item</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="180" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/SaleReturnNew/SaleReturnNew.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon lightgreen">
                                <i class="fas fa-shopping-basket"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Sale Return</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="178" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/PurchaseReturnNew.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon darkpink">
                                <i class="fas fa-shipping-fast"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Purchase Return</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div  data-page="211" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/SaleEdit/SaleEditNew.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon red">
                                <i class="fas fa-cart-plus"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Sale Edit</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="209" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/PurchaseEdit/PurchaseEditNew.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon green">
                                <i class="fas fa-pen-square"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Purchase Edit</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="212" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/SaleReturnEdit/SaleReturnEditNew.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon blue">
                                <i class="fas fa-user-plus"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Sale Return Edit</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="210" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/PurchaseReturnEditNew.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon green">
                                <i class="fas fa-user-edit"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Purchase Return Edit</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="181" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Transactions/PaymentToPartyThroughCash.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Cash Payment</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="183" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Transactions/ReceiptFromPartyThroughCash.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Cash Receipt</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="187" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Transactions/ExpenseEntry.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Expence Entry</p>
                            </div>
                        </div>
                    </a>
                </div>


                <div data-page="13" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" class="chartOfAccountBtn float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Chart Of Accounts</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="121" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Reports/ReceivablePayable/PartyLedgerMedium.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Party Ledger</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="142" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Reports/StockReports/ItemWiseLedger.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Stock Ledger</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="149" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Reports/GeneralLedger/GeneralLedger.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Single Ledger</p>
                            </div>
                        </div>
                    </a>
                </div>
                 <div data-page="175" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Reports/CashBook/DateWiseCashBookOld1.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Cash Book Old</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="175" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Reports/SerialNo/FindIME.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-barcode"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Find IMIE</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="382" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Transactions/LPReceiving/LPReceiving.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-barcode"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>LP Receiving</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="175" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Reports/TrialBalance/TrialBalance.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-barcode"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Trial Balance</p>
                            </div>
                        </div>
                    </a>
                </div>
                
                

            </div>
           
             <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
     <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
         
        </section>

         <% if (WebPOS.Model.UserRole.Admin == HttpContext.Current.Session["UserId"].ToString())
            { %>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col col-12 col-sm-12 col-md-6 col-xl-5 stats-col">
                    <div class="card sameheight-item stats" data-exclude="xs">
                        <div class="card-block">
                            <div class="title-block">
                                <h4 class="title">Stats </h4>
                                <p class="title-description">
                                    Website metrics for
                                           
                                    
                                </p>
                            </div>

                            <div class="row row-sm stats-container">
                                <div class="col-12 col-sm-6 stat-col ActiveItems">
                                    <div class="stat-icon">
                                        <i class="fas fa-rocket"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Active items </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 stat-col ItemsSold">
                                    <div class="stat-icon">
                                        <i class="fas fa-shopping-cart"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value "><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Items sold </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  stat-col MonthlyIncome">
                                    <div class="stat-icon">
                                        <i class="fas fa-chart-line"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value ">N/A </div>
                                        <div class="name">Monthly income </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 stat-col TotalIncome">
                                    <div class="stat-icon">
                                        <i class="fas fa-dollar-sign"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value ">N/A </div>
                                        <div class="name">Total income </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  stat-col TotalClient">
                                    <div class="stat-icon">
                                        <i class="fas fa-users"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value "><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Total Clients </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  stat-col TotalVendors">
                                    <div class="stat-icon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value "><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Total Vendors </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-sm-12 col-md-6 col-xl-7 history-col">
                    <div class="card sameheight-item" data-exclude="xs" id="dashboard-history">
                        <div class="card-header card-header-sm bordered">
                            <div class="header-block">
                                <h3 class="title">History</h3>
                            </div>
                            <ul class="nav nav-tabs pull-right" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#visits" role="tab" data-toggle="tab">Visits</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#downloads" role="tab" data-toggle="tab">Downloads</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-block">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade show" id="visits">
                                    <p class="title-description">Number of unique visits last 30 days </p>
                                    <div id="dashboard-visits-chart"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="downloads">
                                    <p class="title-description">Number of downloads last 30 days </p>
                                    <div id="dashboard-downloads-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section itemsList">
            <div class="row ">
                <div class="col-xl-8">
                    <div class="card items" data-exclude="xs,sm,lg">
                        <div class="card-header bordered">
                            <div class="header-block w-100">
                                <h3 class="title">Items </h3>
                                <a target="_blank" href="/Registration/ItemRegistrationnew.aspx" class="w-100p float-right btn btn-bm btn-sm">Add new </a>
                            </div>

                        </div>
                        <ul class="item-list striped">
                            <li class="item item-list-header">
                                <div class="item-row">
                                    <div class="item-col item-col-header fixed item-col-img xs"></div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div>
                                            <span>Name</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-sales">
                                        <div>
                                            <span>Sales</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-date">
                                        <div>
                                            <span>Last Date Sold</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
               <%-- <div class="col-xl-4">
                    <div class="card sameheight-item sales-breakdown" data-exclude="xs,sm,lg">
                        <div class="card-header">
                            <div class="header-block">
                                <h3 class="title">Sales breakdown </h3>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="dashboard-sales-breakdown-chart" id="dashboard-sales-breakdown-chart"></div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </section>
         <%}
          %>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script>
        $(document).ready(function () {
            $.ajax({
                url: '/Services/HomePageService.asmx/GetStats',
                type: "Post",
                success: function (stats) {
                    $(".ActiveItems .value").text(stats.ActiveItems);
                    $(".ActiveItems .progress-bar").css("width", stats.ActiveItemPercentage + "%");

                    $(".ItemsSold .value").text(stats.ItemsSold);
                    $(".ItemsSold .progress-bar").css("width", stats.ItemsSoldPercentage + "%");

                    $(".TotalClient .value").text(stats.TotalClient);
                    $(".TotalClient .progress-bar").css("width", stats.TotalClientPercentage + "%");

                    $(".TotalVendors .value").text(stats.TotalVendors);
                    $(".TotalVendors .progress-bar").css("width", stats.TotalVendorsPercentage + "%");
                    retreiveItemsWithSale();
                },
                fail: function (jqXhr, exception) {

                }
            });
        })
        function retreiveItemsWithSale() {
            $.ajax({
                url: '/Services/HomePageService.asmx/GetItemsWithSales',
                type: "Post",
                success: function (items) {
                    for (var i = 0; i < items.length; i++) {
                        $(".itemsList .item-list").append('<li class="item"><div class="item-row">  <div class="item-col fixed item-col-img xs"><a href=""><div class="item-img xs rounded" ><i class="fas fa-shopping-cart"></i></div></a></div><div class="item-col item-col-title no-overflow"> <div><a href="" class=""><h4 class="item-title no-wrap">' + items[i].Description + ' </h4></a></div></div><div class="item-col item-col-sales"><div class="item-heading">Sales</div><div>' + items[i].Qty + '</div></div><div class="item-col item-col-date"><div class="item-heading">Last Sold Date</div><div>' + items[i].LastDateSold + '  </div></div></div></li>');
                    }
                    var $dashboardSalesBreakdownChart = $('#dashboard-sales-breakdown-chart');
                    var $sameheightContainer = $dashboardSalesBreakdownChart.closest(".sameheight-container");

                    setSameHeights($sameheightContainer);
                },
                fail: function (jqXhr, exception) {

                }
            });
        }
    </script>
</asp:Content>
