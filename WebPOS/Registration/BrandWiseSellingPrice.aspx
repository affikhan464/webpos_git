﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="BrandWiseSellingPrice.aspx.cs" Inherits="WebPOS.Registration.BrandWiseSellingPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header">Brand Wise Selling Price</h2>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi  input--filled">
                            <select id="lstBrand" class="round input__field--hoshi">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Brand Name </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Brand"><i class="fas fa-plus-circle"></i>Add brand </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" onclick="GetAllList()"><span>Get Items </span></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100"><span>Update Prices</span></a>
                        </span>
                    </div>

                </div>
            </section>
        </div>

    </div>

    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of items </h3>
                </div>
            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-3">
                            <div>
                                <span>Description</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Selling Price 1</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Selling Price 2</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Selling Price 3</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Selling Price 4</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Selling Price 5</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Last Purchase</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-1-5">
                            <div>
                                <span>Average Cost</span>
                            </div>
                        </div>

                    </div>
                </li>

            </ul>
        </div>
    </div>


    <%@ Register Src="~/Registration/_AttributeModal.ascx" TagName="_AttributeModal" TagPrefix="uc" %>
    <uc:_AttributeModal ID="_AttributeModal1" runat="server" />
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Autocomplete.js"></script>
    <script src="/Script/Attribute.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            GetAllList();
            GetBrandList2();



        });

        function GetAllList() {
            var brandCode=$("#lstBrand").val()==""||$("#lstBrand").val()==undefined?1:$("#lstBrand").val();
            $.ajax({
                url: '/WebPOSService.asmx/ListItemsByBrandId',
                type: "Post",
                data: { "brandId": brandCode },

                success: function (BrandList) {
                    $(".item-list .item-data").remove();
                    for (var i = 0; i < BrandList.length; i++) {

                        var code = BrandList[i].Code;
                        var name = BrandList[i].Description;
                        var SellingPrice1 = Number(BrandList[i].SellingCost1);
                        var SellingPrice2 = Number(BrandList[i].SellingCost2);
                        var SellingPrice3 = Number(BrandList[i].SellingCost3);
                        var SellingPrice4 = Number(BrandList[i].SellingCost4);
                        var SellingPrice5 = Number(BrandList[i].SellingCost5);
                        var AverageCost = Number(BrandList[i].Ave_Cost);
                        var LastPurchase = Number(BrandList[i].LastPurchase);

                        $(".item-list").append('<li class="item item-data" data-code="' + code + '"><div class="item-row"><div class="item-col flex-1"><div class="item-heading">Sr. No.</div><div>' + (i + 1) + '</div></div> <div class="item-col  flex-1"><div class="item-heading">Code</div><div>' + code + '</div></div><div class="item-col  flex-3 no-overflow"> <div><a class=""><h4 class="item-title no-wrap" data-toggle="tooltip" title="' + name + '">' + name + ' </h4></a></div></div>      <div class="item-col  flex-1-5"><div class="item-heading">Selling Price 1</div><div>  <input data-code="' + code + '" name="SellingPrice1" class="' + i + '_Rate form-control" value="' + SellingPrice1 + '" />  </div></div>        <div class="item-col  flex-1-5"><div class="item-heading">Selling Price 2</div><div>  <input data-code="' + code + '" name="SellingPrice2" class="' + i + '_Rate form-control"  value="' + SellingPrice2 + '" />  </div></div>        <div class="item-col  flex-1-5"><div class="item-heading">Selling Price 3</div><div>  <input data-code="' + code + '" name="SellingPrice3" class="' + i + '_Rate form-control"  value="' + SellingPrice3 + '" />  </div></div>         <div class="item-col  flex-1-5"><div class="item-heading">Selling Price 4</div><div>  <input data-code="' + code + '" name="SellingPrice4" class="' + i + '_Rate form-control"  value="' + SellingPrice4 + '"/>  </div></div>        <div class="item-col  flex-1-5"><div class="item-heading">Selling Price 5</div><div>  <input data-code="' + code + '" name="SellingPrice5" class="' + i + '_Rate form-control"  value="' + SellingPrice5 + '"/>  </div></div>         <div class="item-col  flex-1-5"><div class="item-heading">Last Price</div><div>  <input data-code="' + code + '" name="LastPurchase" class="' + i + '_Rate form-control"  value="' + LastPurchase + '" />  </div></div>        <div class="item-col  flex-1-5"><div class="item-heading">Average Cost</div><div>  <input data-code="' + code + '" name="AverageCost" class="' + i + '_Rate form-control"  value="' + AverageCost + '" />  </div></div>       </div></li>');


                    }

                    for (var i = 0; i < BrandList.length; i++) {




                    }

                    applyTooltip();

                },
                fail: function (jqXhr, exception) {

                }
            });



        }

        function save() {

            var itemsList = [];
            var items = $(".item-data");
            for (var i = 0; i < items.length; i++) {

                var ItemCode = items[i].dataset.code;
                var SellingCost1 = $(items[i]).find("[name=SellingPrice1]").val();
                var SellingCost2 = $(items[i]).find("[name=SellingPrice2]").val();
                var SellingCost3 = $(items[i]).find("[name=SellingPrice3]").val();
                var SellingCost4 = $(items[i]).find("[name=SellingPrice4]").val();
                var SellingCost5 = $(items[i]).find("[name=SellingPrice5]").val();
                var LastPurchase = $(items[i]).find("[name=LastPurchase]").val();
                var Ave_Cost = $(items[i]).find("[name=AverageCost]").val();

                var ItemModel = {
                    Code: ItemCode,
                    SellingCost1: Number(SellingCost1),
                    SellingCost2: Number(SellingCost2),
                    SellingCost3: Number(SellingCost3),
                    SellingCost4: Number(SellingCost4),
                    SellingCost5: Number(SellingCost5),
                    Ave_Cost: Number(Ave_Cost),
                    LastPurchase: Number(LastPurchase),
                }

                itemsList.push(ItemModel);

            }


            $.ajax({
                url: "/Registration/BrandWiseSellingPrice.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ model: itemsList }),
                success: function (BaseModel) {

                    if (BaseModel.d.Success) {

                        smallSwal("Updated!!", BaseModel.d.Message, "success");

                    }
                    else {
                        smallSwal("Failed!!", BaseModel.d.Message, "error");
                    }


                }

            });

        }


    </script>


</asp:Content>
