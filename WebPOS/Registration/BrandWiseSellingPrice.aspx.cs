﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class BrandWiseSellingPrice : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(List<ItemModel> model)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                foreach (var item in model)
                {
                    var ItemCode = item.Code;
                    var SellingCost1 = item.SellingCost1;
                    var SellingCost2 = item.SellingCost2;
                    var SellingCost3 = item.SellingCost3;
                    var SellingCost4 = item.SellingCost4;
                    var SellingCost5 = item.SellingCost5;
                    var Ave_Cost = item.Ave_Cost;
                    

                    SqlCommand cmdUpdate = new SqlCommand("update InvCode set Ave_Cost=" + Ave_Cost + " ,  SellingCost2=" + SellingCost2 + " ,  SellingCost3=" + SellingCost3 + ", SellingCost4=" + SellingCost4 + ", SellingCost5=" + SellingCost5 + ", SellingCost=" + SellingCost1 + " where CompID='" + CompID + "' and Code=" + ItemCode, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "All Items Updated Successfully!" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }
}