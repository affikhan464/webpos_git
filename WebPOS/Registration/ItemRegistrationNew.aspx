﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemRegistrationNew.aspx.cs" Inherits="WebPOS.ItemRegistrationNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-cart-plus">Item Registration</h2>

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Main Item Entries</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Other Details</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input id="txtItemCode" class="ItemCode input__field input__field--hoshi" disabled type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Code</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1">
                                <span class="input input--hoshi">
                                    <input id="txtBarCode" class="BarCode empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Bar Code </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1">
                               <span class="input input--hoshi  input--filled">
                                   <select id="lstModel" class="round input__field--hoshi">
                                   </select>
                                   <label class="input__label input__label--hoshi">
                                       <span class="input__label-content input__label-content--hoshi">Model </span>
                                   </label>
                               </span>
                           </div>
                            <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1">
                               <span class="input input--hoshi  input--filled">
                                   <select id="lstGroup" class="round input__field--hoshi">
                                   </select>
                                   <label class="input__label input__label--hoshi">
                                       <span class="input__label-content input__label-content--hoshi">Group </span>
                                   </label>
                               </span>
                           </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstBarCodeCategory" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">BarCode Category </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstUnit" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Unit </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="AccountUnit"><i class="fas fa-plus-circle"></i>Add Unit </a>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            
                            
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <span class="input input--hoshi">
                                    <input id="txtItemName" class="ItemName empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Name </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input id="LP" class="LP empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">LP Receiving</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input id="txtReference1" class="Reference1 empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Refernce-1 </span>
                                    </label>
                                </span>
                            </div>
                             <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input id="txtReference2" class="Reference1 empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Refernce-2 </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" href="" id="btnSave1"><i class="fas fa-sync"></i>Reset </a>
                                </span>
                            </div>
                      </div>      
                      <div class="row">  
                          <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtSellingPrice" class="SellingPrice empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Sale Price/Unit  </span>
                                    </label>
                                </span>
                            </div>
                            
                            
                            
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtMSP" class="MSP empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Market Selling Price (MSP)  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input id="PerPieceCommission" class="PerPieceCommission empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Per Piece Commission  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtMinLimit" class="MinLimit empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Min Limit </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtMaxLimit" class="MaxLimit empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Max Limit </span>
                                    </label>
                                </span>
                            </div>
                </div>
                <div class="row"> 
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstBrand" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Brand Name </span>
                                    </label>
                                </span>
                            </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Brand"><i class="fas fa-plus-circle"></i>Add Brand </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="1" id="txtPiecesInPacking" class="PiecesInPacking empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Pieces in Packing  </span>
                                    </label>
                                </span>
                            </div>

                            
                            
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="1" id="txtReOrdLevel" class="ReOrdLevel empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Reorder Point  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="1" id="txtReorderQty" class="ReorderQty empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Reorder Quantity </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="chkItemIsActive" checked="checked" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Is Active Item?
                                    </label>
                                </span>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="ChkVisiable" checked="checked" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Is Visible?
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <span class="input input--hoshi">
                                    <input id="txtOtherInFo" class="OtherInFo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Other Information </span>
                                    </label>
                                </span>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">


                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstManufacturer" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Manufacturer</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Manufacturer"><i class="fas fa-plus-circle"></i>Add Manufacturer </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstPacking" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Packing </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Packing"><i class="fas fa-plus-circle"></i>Add Packing </a>
                                </span>
                            </div>


                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstCategory" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Category </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Category"><i class="fas fa-plus-circle"></i>Add Category </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstClass" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Class/Warranty  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Class"><i class="fas fa-plus-circle"></i>Add Class/Warranty </a>
                                </span>
                            </div>


                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstGoDown" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Godown </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Godown"><i class="fas fa-plus-circle"></i>Add Godown  </a>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtOrderQTY" class="OrderQTY  empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Order Quantity  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtBonusQTY" class="BonusQTY  empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Bonus Quantity </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="row justify-content-start">
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtGST_Rate" class="GST_Rate   empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">GST % </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="chkGSTApply" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Apply GST?
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstItemNature" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Nature </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstIncomeAccount" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Income Account </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstCOGS" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">CGS Account </span>
                                    </label>
                                </span>
                            </div>




                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstWidth" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Width </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Width"><i class="fas fa-plus-circle"></i>Add Width  </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstHeight" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Height </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Height"><i class="fas fa-plus-circle"></i>Add Height  </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstColor" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Color </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Color"><i class="fas fa-plus-circle"></i>Add Color  </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstItemType" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Type </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>



                <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a href="/Correction/ItemRegistrationEditNew.aspx" id="btnEdit" class=" btn btn-3 btn-bm btn-3e fa-edit w-100">Edit</a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

    <%-- Modals --%>

    <%@ Register Src="~/Registration/_AttributeModal.ascx" TagName="_AttributeModal" TagPrefix="uc" %>
    <uc:_AttributeModal ID="_AttributeModal1" runat="server" />
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Attribute.js"></script>
    <script src="../Script/ItemRegistrationNew.js"></script>
    <script type="text/javascript">


        function empty1()
        {
            $(".empty1").val("");
        }
        function save()
        {
            var LP = $(".LP").val();
            var BarCode = $(".BarCode").val();
            var Description = $(".ItemName").val();
            var Reference1 = $(".Reference1").val();
            var Reference2 = $(".Reference2").val();
            var PiecesInPacking = $(".PiecesInPacking").val();
            var Brand = $("#lstBrand").val();
            var chkVisiable = document.getElementById("ChkVisiable").checked ? "1" : "0";
            var ReOrdLevel = $(".ReOrdLevel").val();
            var Manufacturer = $("#lstManufacturer").val();
            var Packing = $("#lstPacking").val();
            var Category = $("#lstCategory").val();
            var Class = $("#lstClass").val();
            var GoDown = $("#lstGoDown").val();
            var ReOrdQuantity = $(".ReorderQty").val();
            var OrderQty = $(".OrderQTY").val();
            var BonusQty = $(".BonusQTY").val();
            var ItemNature = $("#lstItemNature").val();
            var AccountUnit = $("#lstUnit").val();
            var SellingCost = $(".SellingPrice").val();
            var Revenue_Code = $("#lstIncomeAccount").val();
            var chkActive = document.getElementById("chkItemIsActive").checked ? "1" : "0";
            var GSTRate = $(".GST_Rate").val();
            var chkGSTApply = document.getElementById("chkGSTApply").checked ? "1" : "0";
            var MinLimit = $(".MinLimit").val();
            var MaxLimit = $(".MaxLimit").val();
            var lstCGS_Code = $("#lstCOGS").val();
            var lstItemType = $("#lstItemType").val();
            var lstWidth = $("#lstWidth").val();
            var lstHeight = $("#lstHeight").val();
            var lstBarCodeCategory = $("#lstBarCodeCategory").val();
            var lstColor = $("#lstColor").val();
            var OtherInFo = $(".OtherInFo").val();
            var MSP = $(".MSP").val();
            var lstGroup = $("#lstGroup").val();
            var PerPieceCommission = $("#PerPieceCommission").val();

            if (PiecesInPacking == "") { PiecesInPacking = 1 }
            if (ReOrdLevel == "") { ReOrdLevel = 1 }
            if (ReOrdQuantity == "") { ReOrdQuantity = 1 }
            if (OrderQty == "") { OrderQty = 0 }
            if (BonusQty == "") { BonusQty = 0 }
            if (SellingCost == "") { SellingCost = 0 }
            if (GSTRate == "") { GSTRate = 0 }
            if (MinLimit == "") { MinLimit = 0 }
            if (MaxLimit == "") { MaxLimit = 0 }

            PiecesInPacking = Number(PiecesInPacking);
            ReOrdLevel = Number(ReOrdLevel);
            ReOrdQuantity = Number(ReOrdQuantity);
            OrderQty = Number(OrderQty);
            BonusQty = Number(BonusQty);
            SellingCost = Number(SellingCost);
            GSTRate = Number(GSTRate);
            MinLimit = Number(MinLimit);
            MaxLimit = Number(MaxLimit);
            if (Description.trim() == "")
            {
                swal("Error", "Add Item Name", "error");
                $(".ItemName").focus();
                $(".ItemName").select();
            } else
            {

                var ItemModel = {
                    BarCode: BarCode,
                    Description: Description,
                    Reference: Reference1,
                    Reference2: Reference2,
                    PiecesInPacking: PiecesInPacking,
                    Brand: Brand,
                    Visiable: chkVisiable,
                    ReorderLevel: ReOrdLevel,
                    Manufacturer: Manufacturer,
                    Packing: Packing,
                    Category: Category,
                    Class: Class,
                    Godown: GoDown,
                    ReorderQty: ReOrdQuantity,
                    OrderQTY: OrderQty,
                    BonusQTY: BonusQty,
                    Nature: ItemNature,
                    AccountUnit: AccountUnit,
                    SellingCost1: SellingCost,
                    Revenue_Code: Revenue_Code,
                    Active: chkActive,
                    GST_Rate: GSTRate,
                    GST_Apply: chkGSTApply,
                    MinLimit: MinLimit,
                    MaxLimit: MaxLimit,
                    CGS_Code: lstCGS_Code,
                    ItemType: lstItemType,
                    Length: lstWidth,
                    Height: lstHeight,
                    BarCodeCategory: lstBarCodeCategory,
                    Color: lstColor,
                    OtherInFo: OtherInFo,
                    Group: lstGroup,
                    MSP: MSP,
                    LP: LP,
                    PerPieceCommission:PerPieceCommission
                }


                debugger
                $.ajax({
                    url: "/Registration/ItemRegistrationNew.aspx/Save",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ItemModel: ItemModel }),
                    success: function (BaseModel)
                    {
                        debugger
                        if (BaseModel.d.Success)
                        {
                            $(".empty1").val("");
                            swal("Registered", BaseModel.d.Message, "success");
                        }
                        else
                        {

                            swal("Error", BaseModel.d.Message, "error");
                        }


                    }

                });

            }
        }
        $(document).ready(function ()
        {
            GetModelList();
            GetGoDownList();
            GetManufacturerList();
            GetCategoryList();
            GetPackingList();
            GetClassList();
            GetItemNatureList();
            GetColorList();
            GetBarCodeCategoryList();
            HeightList();
            WidthList();

            AccountUnitList();
            ItemTypeList();
            COGSList();
            Income_List();
            GetBrandList2();
            GetGroupList();

        });

    </script>


</asp:Content>
