﻿<%@  Control Language="C#" AutoEventWireup="true" CodeBehind="_OtherManagment.ascx.cs" Inherits="WebPOS.Registration._OtherManagment" %>

<div class="otherManagements-pages" >
      <h4 class="otherManagements-heading">Other Management</h4>
            <div  class="row">
                
                <div data-page="220" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/UserManagement.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon green">
                                <i class="fas fa-user-cog"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>User Management</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="2" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/BrandManagement.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fab fa-bimobject"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Brand Management</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="3" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/ClassManagment.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon red">
                                <i class="fas fa-copyright"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Class Management</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="4" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/CategoryManagment.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon green">
                                <i class="fas fa-list-alt"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Category Management</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="5" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/GoDownManagment.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon blue">
                                <i class="fas fa-warehouse"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>GoDown Management</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="6" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/ManufacturerManagment.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon red">
                                <i class="fas fa-industry"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Manufacturer Management</p>
                            </div>
                        </div>
                    </a>
                </div>


                <div data-page="7" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/ColorManagment.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon bluecorel">
                                <i class="fas fa-paint-brush"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Color Management</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="8" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/PackingManagment.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon purplish">
                                <i class="fas fa-box-open"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Packing Management</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="17" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/SaleManManagment.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon green">
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Saleman Management</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="18" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/DepartmentManagment.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon red">
                                <i class="fas fa-building"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Department Management</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="215" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/UserPermissionManagement.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon green">
                                <i class="fas fa-user-lock"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>User Permission Management</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

    </div>