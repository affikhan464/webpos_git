﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ChartOfAccounts.aspx.cs" Inherits="WebPOS.Registration.ChartOfAccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="/css/tree.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="title AddItems">
        <h2><i class="fas fa-list-ul" aria-hidden="true"></i> Chart Of Accounts</h2>
    </div>

    <!-- the demo root element -->
    <div id="chartOfAccounts">
    </div>
    <div class="modal fade" id="AddChildModal" tabindex="-1" role="dialog" aria-labelledby="AddChildModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Child to <span class="parentAccountTitle"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form" data-saveattribute="">
                            <div class="row">

                                <div class="col-12">
                                    <div class="top-row parent">
                                        <span>Parent</span>
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <span class="input input--hoshi">
                                                    <input id="parentAccountTitle" required="required" disabled="disabled" class="parentAccountTitle empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                                    <label class="input__label input__label--hoshi">
                                                        <span class="input__label-content input__label-content--hoshi">Title<span class="req">*</span> </span>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                                <span class="input input--hoshi">
                                                    <input id="parentAccountCode" name="parentAccountCode" disabled="disabled" required="required" class="parentAccountCode empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                                    <label class="input__label input__label--hoshi">
                                                        <span class="input__label-content input__label-content--hoshi">Code<span class="req">*</span> </span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="col-12">
                                    <span class="input input--hoshi">
                                        <input name="AccountTitle" required="required" class="AccountTitle empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Title </span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi  input--filled">
                                        <select class="TypeDD round input__field--hoshi">
                                            <option selected  value="Header">Header</option>
                                            <option value="Detail">Detail</option>
                                        </select>
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Type </span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi  input--filled">
                                        <select class="GroupDD round input__field--hoshi">
                                            <option selected value="Balance Sheet" >Balance Sheet</option>
                                            <option value="Profit and Loss">Profit & Loss</option>
                                        </select>
                                       
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Group </span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                   <div class="level-wrapper">
                                        <label class="lvl">
                                            Level
                                        </label>
                                        <span class='upperCircle  ml-3'><span class='innerCircle accountLevel'>3</span></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi  input--filled">
                                        <select class="NatureDD round input__field--hoshi">
                                          <option value="1" selected>Dr.</option>
                                            <option value="2">Cr.</option>
                                        </select>
                                       
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Nature </span>
                                        </label>
                                    </span>
                                </div>
                            </div>


                            <hr />
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a class=" btn btn-3 btn-bm btn-3e fa-times w-100" data-dismiss="modal">Close</a>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a onclick="saveAccount()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="EditChildModal" tabindex="-1" role="dialog" aria-labelledby="EditChildModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form" data-saveattribute="">
                            <div class="row">


                                  <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input  name="AccountTitle" required="required" class="AccountTitle empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Title </span>
                                        </label>
                                    </span>
                                </div>
                                
                                 <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input  name="AccountCode"  required="required" class="AccountCode empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Code </span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi  input--filled">
                                        <select class="TypeDD round input__field--hoshi">
                                            <option selected  value="Header">Header</option>
                                            <option value="Detail">Detail</option>
                                        </select>
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Type </span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi  input--filled">
                                        <select class="GroupDD round input__field--hoshi">
                                            <option selected value="Balance Sheet" >Balance Sheet</option>
                                            <option value="Profit and Loss">Profit & Loss</option>
                                        </select>
                                       
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Group </span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                   <div class="level-wrapper">
                                        <label class="lvl">
                                            Level
                                        </label>
                                        <span class='upperCircle  ml-3'><span class='innerCircle accountLevel'>3</span></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi  input--filled">
                                        <select class="NatureDD round input__field--hoshi">
                                          <option value="1" selected>Dr.</option>
                                            <option value="2">Cr.</option>
                                        </select>
                                       
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Nature </span>
                                        </label>
                                    </span>
                                </div>
                                
                                 <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input  name="OpeningBalance"  required="required" class="OpeningBalance empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Opening Balance</span>
                                        </label>
                                    </span>
                                </div>
                            </div>


                            <hr />
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a class=" btn btn-3 btn-bm btn-3e fa-times w-100" data-dismiss="modal">Close</a>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a onclick="updateAccount()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Update</a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="/Script/ChartOfAccount/Tree.js"></script>
    <script src="/Script/ChartOfAccount/ChartOfAccount.js"></script>
    


</asp:Content>
