﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="BrandWiseDiscount.aspx.cs" Inherits="WebPOS.Registration.BrandWiseDiscount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Brand Wise Discounts</h2>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtPartyName" class="PartyBox PartyName autocomplete  empty1 input__field input__field--hoshi" data-type="Party" data-id="txtPartyName" data-function="GetParty" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyCode" disabled="disabled" class="PartyCode input__field input__field--hoshi empty1" disabled="disabled" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Part Code</span>
                            </label>
                        </span>
                    </div>

                </div>
            </section>
        </div>

    </div>

    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Brand </h3>
                </div>
            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-title">
                            <div>
                                <span>Name</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Rate</span>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <div class="container-fluid mb-3">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <span class="input input--hoshi">
                    <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" id="btnReset" onclick="reset()">Reset </a>
                </span>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <span class="input input--hoshi">
                    <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                </span>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
       <script src="/Script/Autocomplete.js"></script> 
    <script type="text/javascript">
        $(document).ready(function () {

            GetAllBrandList();



        });

        function GetAllBrandList() {

            $.ajax({
                url: '/WebPOSService.asmx/Brand_List',
                type: "Post",

                success: function (BrandList) {

                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        $(".item-list").append('<li class="item item-data"><div class="item-row"><div class="item-col item-col-sales"><div class="item-heading">Sr. No.</div><div>' + (i + 1) + '</div></div> <div class="item-col item-col-sales"><div class="item-heading">Code</div><div>' + code + '</div></div><div class="item-col item-col-title no-overflow"> <div><a href="" class=""><h4 class="item-title no-wrap">' + name + ' </h4></a></div></div><div class="item-col item-col-sales"><div>  <input data-code="' + code + '" name="rate" class="' + i + '_Rate form-control" value="0" />  </div></div></div></li>');
                    }

                },
                fail: function (jqXhr, exception) {

                }
            });



        }
        function GetBrand_PartyWiseDisc_Rate(PartyCode) {
            $("[name=rate]").val(0);
            $.ajax({
                url: '/WebPOSService.asmx/Brand_PartyWiseDisc_Rate',
                type: "Post",

                data: { "PartyCode": PartyCode },


                success: function (BrandList) {

                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].BrandCode;
                        var rate = BrandList[i].DiscountRate;
                        $("[data-code=" + code + "]").val(rate);
                    }

                },
                fail: function (jqXhr, exception) {

                }
            });



        }


        function save() {
            var listBrands = [];
            var BrandTxtBox = $("[data-code]");
            var PartyCode = $(".PartyCode").val();
            for (var i = 0; i < BrandTxtBox.length; i++) {

                var Rate = $(BrandTxtBox[i]).val();
                var BrandCode = $(BrandTxtBox[i]).attr("data-code");
                var Brand = {
                    Code: BrandCode,
                    Rate: Rate
                }
                listBrands.push(Brand);
            }

            var SaveBrandWiseDiscountModel = {
                Brands: listBrands,
                PartyCode: PartyCode

            };
            
            $.ajax({
                url: "/Registration/BrandWiseDiscount.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ SaveBrandWiseDiscountModel: SaveBrandWiseDiscountModel }),
                success: function (BaseModel) {
                    
                    if (BaseModel.d.Success) {
                        $("[name=rate]").val(0);
                        $(".PartyName").val("");
                        $(".PartyCode").val("");

                        updateInputStyle();
                        smallSwal("Updated!!", BaseModel.d.Message, "success");
                      
                    }
                    else {
                        smallSwal("Failed!!", BaseModel.d.Message, "error");
                    }


                }

            });

        }


    </script>


</asp:Content>
