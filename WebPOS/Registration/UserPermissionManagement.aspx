﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="UserPermissionManagement.aspx.cs" Inherits="WebPOS.Registration.UserPermissionManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid mb-3">
        <div class="mt-4">
            <section class="form">
                <h2 class="form-header ">User Permission Managment</h2>
               
                <div class="card sameheight-item items" data-exclude="xs,sm,lg">
                    
                    <div class="card-header bordered">
                        <div class="header-block">
                            <h3 class="title">Select User </h3>
                        </div>
                    </div>
                    <%@ Register Src="~/Registration/_UsersList.ascx" TagName="_UsersList" TagPrefix="uc" %>
                    <uc:_UsersList ID="_UsersList1" runat="server" />

                </div>

                <hr />

                <div class="row">

                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">

        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Pages</h3>
                </div>
            </div>
            <ul class="item-list pages-list striped ">
                <li class="item item-list-header">
                    <div class="item-row">
                    </div>
                </li>

            </ul>
        </div>

        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Dropdown.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            AppendLevelParents();
            GetAllUsersList();

            var rows = $("ul.pages-list .item-row");
            var uls = $("ul.pages-list ul");
            uls.slideUp("slow");
            rows.each(function (i, element) {
                if ($(element).find("~ ul").length) {
                    $(element).addClass("isParent");
                }

            });
        });

        $(document).on("click", ".isParent", function (e) {

            if (!$(this).hasClass("show")) {
                $(this).addClass("show");
                $(this).find("~ ul").slideDown("fast");
            } else {
                $(this).removeClass("show");
                $(this).find("~ ul").slideUp("fast");
            }
            e.stopPropagation();
        })
        function selectedChanged(selectedValue) {
            GetAllUsersList(selectedValue.value);
        }


        function AppendLevelParents() {
            var level1Parents = $("[data-parent=1]");
            $(".pages-list .item-data").remove();
            for (var i = 0; i < level1Parents.length; i++) {
                var name = $(level1Parents[i]).children("a").text().trim();
                var level2parents = $(`[data-parent=2][data-parentname='${name}']`);
                var uls = `<ul>`;
                level2parents.each(function (i, element) {
                    var level2parentName = $(element).children("a").text().trim();

                    var level3parents = $(`[data-parent=3][data-parentname='${level2parentName}']`);
                    var ul3s = `<ul>`;
                    level3parents.each(function (i3, element3) {
                        var level3parentName = $(element3).children("a").text().trim();
                        ul3s += `
                            <li class="item item-data level3" data-parentitem="${level3parentName}">
                                <div class="item-row">
                                    <div class="item-col flex-1">
                                        <div class="item-heading"></div>
                                        <strong> ${level3parentName}</strong>
                                    </div>
                                </div>
                                <label>
                                    <input onchange="parentCheckChange(this)" data-name="${level3parentName}" type="checkbox" class="checkbox" />
                                    <span></span>
                                </label>
                                <ul></ul>
                            </li>`;
                    });
                    ul3s += `</ul>`;

                    uls += `
                        <li class="item item-data level2" data-parentitem="${level2parentName}">
                            <div class="item-row">
                                <div class="item-col flex-1"><div class="item-heading"></div><strong>${level2parentName}</strong></div></div>
                            <label>
                                <input onchange="parentCheckChange(this)" data-name="${level2parentName}" type="checkbox" class="checkbox" />
                                <span></span>
                            </label>
                            ${ul3s}
                        </li>`;
                });
                uls += `</ul>`;
                $(".pages-list").append(`
                        <li class="item item-data level1"  data-parentitem="${name}">
                            <div class="item-row"><div class="item-col flex-1"><div class="item-heading"></div><strong>${name}</strong></div></div>
                            <label>
                                <input onchange="parentCheckChange(this)" data-name="${name}" type="checkbox" class="checkbox " /><span></span>
                            </label>
                            ${uls}
                        </li>`);
            }


            AppendPages();
        }
        function parentCheckChange(checkbox) {
            var parentLevel = $(checkbox).parents("li").length;
            var childCheckboxes = $(checkbox).parents("li.item.item-data.level" + parentLevel).find("li .checkbox");

            if (checkbox.checked)
                checkUnCheck(childCheckboxes, true);

            else
                checkUnCheck(childCheckboxes, false);



        }
        function AppendPages() {
            var pages = $("[data-page]");
            pages.each(function (index, element) {
                var id = element.dataset.page;
                var name = $(element).children("a").text().trim();
                var parentitem = $(element).parent().parent().children("a").text().trim();
                var level = $(element).parents("li").length + 1;
                $(`[data-parentitem='${parentitem}'] > ul`).append(`
                    <li class="item item-data level${level}">
                        <div class="item-row">
                            <div class="item-col flex-1">
                                <div class="item-heading"></div>
                                <label>
                                    <span class="mr-2 badge badge-warning" >${id}</span> 
                                    ${name}
                                </label> 
                                
                            </div>
                        </div>
                         <label>
                             <input data-pageid="${id}" type="checkbox" name="PageSelected" class="checkbox rounded" />
                              <span></span>
                         </label>
                    </li>`);

            })
            addNewPagesIfAddedToView();

        }
        function SelectUser(user) {
            $("#UserIdTextBox").remove();
            $('body').append(`<input type='hidden' id='UserIdTextBox' value='${user.dataset.userid}' />`);
            $(".item.item-data.selected").removeClass("selected");
            $(user).addClass("selected");

            checkPermittedPages(user.dataset.userid);
        }
        function checkPermittedPages(userId) {

            
            $.ajax({
                url: '/WebPOSService.asmx/GetUserPermittedPagesList',
                type: "Post",

                data: { "userId": userId },
                success: function (pageList) {

                    var checkboxes = $("[type=checkbox]:checked");
                    checkUnCheck(checkboxes, false);
                    var chkbxsids = "";
                    for (var i = 0; i < pageList.length; i++) {

                        var id = pageList[i].Id;
                        if (i != pageList.length - 1) {
                            chkbxsids += "[type=checkbox][data-pageid=" + id + "],"

                        } else {
                            chkbxsids += "[type=checkbox][data-pageid=" + id + "]"
                        }

                    }
                    var checkableCheckboxes = $(chkbxsids);
                    checkUnCheck(checkableCheckboxes, true);
                },
                fail: function (jqXhr, exception) {

                }
            });
        }

        function checkUnCheck(checkboxes, checked) {


            checkboxes.each(function (i, chkbx) {
                chkbx.checked = checked;
            })
        }
        function save() {
            if ($("#UserIdTextBox").length > 0) {
                var model = [];
                var pages = $("[data-pageid][name=PageSelected]:checked");
                pages.each(function (index, element) {
                    var id = element.dataset.pageid;
                    var userId = $("#UserIdTextBox").val();;
                    

                    var pagesModel = {
                        Id: id,
                        UserId: userId
                    };

                    model.push(pagesModel);
                });
                if (model.length > 0) {
                    var newModel = JSON.stringify({ model: model });
                    $.ajax({
                        url: "/Registration/UserPermissionManagement.aspx/Save",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: newModel,
                        success: function (BaseModel) {

                            if (BaseModel.d.Success) {
                                smallSwal("Saved", BaseModel.d.Message, "success");
                            }
                            else {
                                smallSwal("Failed", BaseModel.d.Message, "error");
                            }


                        }

                    });

                }
            } else
                swal("Select User First", "", "warning")
        }
        function addNewPagesIfAddedToView() {
            var model = [];
            var pages = $("li[data-page]");
            pages.each(function (index, element) {
                var id = element.dataset.page;
                var href = $(element).children("a").attr("href");
                var name = $(element).children("a").text().trim();

                var pagesModel = {
                    Id: id,
                    Name: name,
                    Url: href
                };

                model.push(pagesModel);
            });
            if (model.length > 0) {
                var newModel = JSON.stringify({ model: model });
                $.ajax({
                    url: "/Registration/UserPermissionManagement.aspx/AddPages",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: newModel,
                    success: function (BaseModel) {

                        if (!BaseModel.d.Success) {
                            smallSwal("Failed", BaseModel.d.Message, "error");
                        }


                    }

                });

            }

        }
    </script>

</asp:Content>
