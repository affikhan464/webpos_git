﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemRegistration.aspx.cs" Inherits="WebPOS.ItemRegistration" %>

<%--  <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %> --%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
                       <h2 style="text-align:center; font-size:30px; " >Item Registration</h2>  
    <br />
                        

    <asp:Label Text="Item Code" ID="Label2" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <input type="text" id="txtItemCode"  style="width: 174px; height: 20px;margin-left:36px;" />
                                    <br />
    <asp:Label Text="Bar Code" ID="Label1" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <input type="text" id="txtBarCode" class="BarCode empty1" style="width: 250px; height: 20px;margin-left:42px;" />
     <asp:Label Text="BarCode Categ" ID="Label23" runat="server"  Style="margin-left:2px;"></asp:Label>
                                     <select id="lstBarCodeCategory" style="list-style-type:disc;width:91px; margin-left:5px;">
                                    </select>
                                    <br />
   <asp:Label Text="Item Name" ID="Label18" runat="server"  Style="margin-left:290px;"></asp:Label>   
                                    <input type="text" id="txtItemName" class="ItemName empty1" style="width: 454px; height: 20px;margin-left:31px;margin-top:2px;" />
                                    <input type="button" id="btnSave1"  value="reset" onclick="empty1()" />
                                    <br />
    <asp:Label Text="Refernce-1" ID="Label15" runat="server"  Style="margin-left:290px;"></asp:Label> 
                                    <input type="text" id="txtReference1" class="Reference1 empty1" style="width: 150px; height: 20px;margin-left:30px;margin-top:2px;" />
    <asp:Label Text="Refernce-2:" ID="Label16" runat="server"  Style="margin-left:0px;"></asp:Label>                     
                                    <input type="text" id="txtReference2" class="Reference2 empty1" style="width: 150px; height: 20px;" />
   
    <asp:Label Text="Unit" ID="Label19" runat="server"  Style="margin-left:0px;"></asp:Label>  
                                    <select id="lstUnit" style="width: 68px; height: 18px;margin-left:8px;">    
                                    </select> 
                                    <a  target="_blank" href="" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="">Add</a>
                                    <button type="button" value="ref"  onclick="AccountUnitList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
<br />
        <asp:Label Text="Sale Price/Unit" ID="Label20" runat="server"  Style="margin-left:290px;"></asp:Label>  
                                    <input type="text" id="txtSellingPrice" class="SellingPrice empty1" style="width: 90px; height: 20px;margin-left:5px;margin-top:2px;" />
                       Min Limit
                                    <input type="text" value="0" id="txtMinLimit" class="MinLimit empty1" style="width: 90px; height: 20px;" />
                       Max Limit
                                    <input type="text" value="0" id="txtMaxLimit" class="MaxLimit empty1" style="width: 98px; height: 20px;" />
                       Pieces in Packing
                                <input type="text" value="1" id="txtPiecesInPacking" class="PiecesInPacking empty1" style="width: 90px; height: 20px;" />
    <br />
    <asp:Label Text="Brand Name" ID="Label21" runat="server"  Style="margin-left:290px;"></asp:Label>  
                      <select id="lstBrand"  style="list-style-type:disc;width:128px; margin-left:21px;">    
                                    </select>
                                    
                                    <a  target="_blank" href="/Registration/BrandManagement.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn4">Add</a>
                                    <button type="button" value="ref"  onclick="GetBrandList2()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                            
                                    
                       Reorder Point
                                    <input type="text"  id="txtReOrdLevel" class="ReOrdLevel empty1" style="width: 90px; height: 20px;margin-top:2px;" />
                       Reorder Quantity
                                    <input type="text" value="1" id="txtReorderQty" class="ReorderQty empty1" style="width: 90px; height: 20px;" />
                                    
                                    
                            <br />
    <asp:Label Text="Item Type" ID="Label22" runat="server"  Style="margin-left:290px;"></asp:Label>  
                       <select id="lstItemType" style="width: 250px; height: 20px;margin-left:39px;">    
                                    </select> 
                                    
                                    <label><input type="checkbox" id="chkItemIsActive" checked/>Item is Active</label>
                                  <label>  <input type="checkbox" id="ChkVisiable" checked   /> Visiable</label>
                               <br />
    <asp:Label Text="Other Info." ID="Label26" runat="server"  Style="margin-left:290px;"></asp:Label>   
                                    <input type="text" id="txtOtherInFo" class="OtherInFo empty1" style="width: 454px; height: 20px;margin-left:34px;margin-top:2px;" />
    <hr />    
  
    <asp:Label Text="Manufacturer" ID="Label3" runat="server"  Style="margin-left:290px;"></asp:Label>
                                     <select id="lstManufacturer" style="list-style-type:disc;width:300px; margin-left:16px; height:20px;">
                                    </select>
                                    <a  target="_blank" href="/Registration/ManufacturerManagment.aspx"  style="height: 30px; border-radius: 5px;" id="ManfBtn5">Add</a>
                                    <button type="button" value="ref"  onclick="GetManufacturerList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
    <asp:Label Text="Packing" ID="Label4" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstPacking" style="list-style-type:disc;width:300px; margin-left:54px;height:20px;margin-top:2px;">
                                    </select>                         
                                     <a  target="_blank" href="" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn4">Add</a>
                                    <button type="button" value="ref"  onclick="GetPackingList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
     <asp:Label Text="Category" ID="Label5" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstCategory" style="list-style-type:disc;width:300px; margin-left:45px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/CategoryManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn1">Add</a>
                                    <button type="button" value="ref"  onclick="GetCategoryList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
     <asp:Label Text=" Class/Warranty" ID="Label6" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstClass" style="list-style-type:disc;width:300px; margin-left:1px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/ClassManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn2">Add</a>
                                    <button type="button" value="ref"  onclick="GetClassList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
     <asp:Label Text="Godown" ID="Label7" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstGoDown" style="list-style-type:disc;width:300px; margin-left:48px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/GoDownManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn3">Add</a>
                                    <button type="button" value="ref"  onclick="GetGoDownList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
    <asp:Label Text="Order Quantity" ID="Label8" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <input type="text" value="0" id="txtOrderQTY" class="OrderQTY empty1" style="width: 100px; height: 20px;margin-left:6px;margin-top:2px;" />
    <asp:Label Text="Bonus Quantity" ID="Label9" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <input type="text" value="0" id="txtBonusQTY" class="BonusQTY empty1" style="width: 100px; height: 20px;"  />
    <asp:Label Text="GST %" ID="Label10" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <input type="text" value="0" id="txtGST_Rate" class="GST_Rate empty1" style="width: 100px; height: 20px;" />
                                    <input type="checkbox" id="chkGSTApply"  />Apply GST
                                    <br />
    <asp:Label Text="Item Nature" ID="Label11" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstItemNature" style="list-style-type:disc;width:300px; margin-left:26px;height:20px;margin-top:2px;">    
                                    </select>

                            
    <asp:Label Text="Income Account" ID="Label12" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <select id="lstIncomeAccount" style="width: 174px; height: 20px;">    
                                    </select>
                                    
                                    <br />
    <asp:Label Text="COGS Account" ID="Label13" runat="server"  Style="margin-left:290px;"></asp:Label>
    <select id="lstCOGS" style="width: 174px; height: 20px;margin-left:2px;">    
                                    </select>
                                    
    <asp:Label Text="Asset Account" ID="Label14" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <select id="lstAssetAccount1" style="width: 174px; height: 20px;">    
                                    </select>
                                    <br />
    <asp:Label Text="Height" ID="Label24" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstHeight" style="list-style-type:disc;width:70px; margin-left:59px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href=""  style="height: 30px; border-radius: 5px;" id="ManfBtn5">Add</a>
                                    <button type="button" value="ref"  onclick=" HeightList()" style="border:none;" ><img src="/Images/arrow.png" /></button>

    <asp:Label Text="Width" ID="Label25" runat="server"  Style="margin-left:24px;"></asp:Label>
                                    <select id="lstWidth" style="list-style-type:disc;width:70px; margin-left:26px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href=""  style="height: 30px; border-radius: 5px;" id="ManfBtn5">Add</a>
                                    <button type="button" value="ref"  onclick="WidthList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
    <asp:Label Text="Color" ID="Label17" runat="server"  Style="margin-left:0px;"></asp:Label>  
                                    <select id="lstColor" style="list-style-type:disc;width:80px; margin-left:0px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/ColorManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn2">Add</a>
                                    <button type="button"   onclick="GetColorList()" style="border:none;" ><img src="/Images/arrow.png" /></button>

    <br />
    <br />
     <center>                       <input type="button" id="btnSave" class="btnSave1" value="save" onclick="save()" />
                                    <input type="button" id="btnEdit" class="btnEdit1" value="Edit"  />
     </center>
   
    
    <script src="../Script/jquery.min.js"></script>
    <script src="../Script/jquery-ui.min.js"></script>
    

    <script type="text/javascript">
        function empty1() {
            $(".empty1").val("");
        }
        function save() {
            var BarCode = $(".BarCode").val();
            var Description = $(".ItemName").val();
            var Reference1 = $(".Reference1").val();
            var Reference2 = $(".Reference2").val();
            var PiecesInPacking = $(".PiecesInPacking").val();
            var Brand = $("#lstBrand").val();
            var chkVisiable = document.getElementById("ChkVisiable").checked ? "1" : "0";
            var ReOrdLevel = $(".ReOrdLevel").val();
            var Manufacturer = $("#lstManufacturer").val();
            var Packing = $("#lstPacking").val();
            var Category = $("#lstCategory").val();
            var Class = $("#lstClass").val();
            var GoDown = $("#lstGoDown").val();
            var ReOrdQuantity = $(".ReorderQty").val();
            var OrderQty = $(".OrderQTY").val();
            var BonusQty = $(".BonusQTY").val();
            var ItemNature = $("#lstItemNature").val();
            var AccountUnit = $("#lstUnit").val();
            var SellingCost = $(".SellingPrice").val();
            var Revenue_Code = $("#lstIncomeAccount").val();
            var chkActive = document.getElementById("chkItemIsActive").checked ? "1" : "0";
            var GSTRate = $(".GST_Rate").val();
            var chkGSTApply = document.getElementById("chkGSTApply").checked ? "1" : "0";
            var MinLimit = $(".MinLimit").val();
            var MaxLimit = $(".MaxLimit").val();
            var lstCGS_Code = $("#lstCOGS").val();
            var lstItemType=$("#lstItemType").val();
            var lstWidth=$("#lstWidth").val();
            var lstHeight=$("#lstHeight").val();
            var lstBarCodeCategory=$("#lstBarCodeCategory").val();
            var lstColor=$("#lstColor").val();
            var OtherInFo = $(".OtherInFo").val();
            if (PiecesInPacking == "") { PiecesInPacking = 1 }
            if (ReOrdLevel == "") { ReOrdLevel = 1 }
            if (ReOrdQuantity == "") { ReOrdQuantity = 1 }
            if (OrderQty == "") { OrderQty = 0 }
            if (BonusQty == "") { BonusQty = 0 }
            if (SellingCost == "") { SellingCost = 0 }
            if (GSTRate == "") { GSTRate = 0 }
            if (MinLimit == "") { MinLimit = 0 }
            if (MaxLimit == "") { MaxLimit = 0 }

            PiecesInPacking = isNaN(PiecesInPacking) ? 1 : PiecesInPacking;
            ReOrdLevel = isNaN(ReOrdLevel) ? 1 : ReOrdLevel;
            ReOrdQuantity = isNaN(ReOrdQuantity) ? 1 : ReOrdQuantity;
            OrderQty = isNaN(OrderQty) ? 0 : OrderQty;
            BonusQty = isNaN(BonusQty) ? 0 : BonusQty;
            SellingCost = isNaN(SellingCost) ? 0 : SellingCost;
            GSTRate = isNaN(GSTRate) ? 0 : GSTRate;
            MinLimit = isNaN(MinLimit) ? 0 : MinLimit;
            MaxLimit = isNaN(MaxLimit) ? 0 : MaxLimit;
            
            var ItemModel = {
                BarCode:BarCode,
                Description: Description,
                Reference: Reference1,
                Reference2: Reference2,
                PiecesInPacking:PiecesInPacking,
                Brand:Brand,
                Visiable: chkVisiable,
                ReorderLevel: ReOrdLevel,
                Manufacturer:Manufacturer,
                Packing:Packing,
                Category:Category,
                Class:Class,
                Godown:GoDown,
                ReorderQty:ReOrdQuantity,
                OrderQTY:OrderQty,
                BonusQTY:BonusQty,
                Nature:ItemNature,
                AccountUnit:AccountUnit,
                SellingCost1: SellingCost,
                Revenue_Code:Revenue_Code,
                Active:chkActive,
                GST_Rate:GSTRate,
                GST_Apply:chkGSTApply,
                MinLimit:MinLimit,
                MaxLimit:MaxLimit,
                CGS_Code:lstCGS_Code,
                ItemType:lstItemType,
                Length:lstWidth,
                Height:lstHeight,
                BarCodeCategory: lstBarCodeCategory,
                Color: lstColor,
                OtherInFo:OtherInFo
            }


            debugger
            $.ajax({
                url: "/Registration/ItemRegistration.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ ItemModel: ItemModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        $(".empty1").val("");
                        alert(BaseModel.d.Message);
                    }
                    else {
                        alert(BaseModel.d.Message);
                    }


                }

            });

        }
        $(document).ready(function () {
            GetGoDownList();
            GetManufacturerList();
            GetCategoryList();
            GetPackingList();
            GetClassList();
            GetItemNatureList();
            GetColorList();
            GetBarCodeCategoryList();
            HeightList();
            WidthList();
            
            AccountUnitList();
            ItemTypeList();
            COGSList();
            Income_List();
            GetBrandList2();
            
        });
        function Income_List() {
            $("#lstIncomeAccount").empty();
            $.ajax({
                url: '/WebPOSService.asmx/InComeList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.InComeAccount.length; i++) {

                        var name = AllComboList.InComeAccount[i].Name.split("-")[0];
                        var code = AllComboList.InComeAccount[i].Name.split("-")[1];
                        if (code == "0103010100001") {
                            $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstIncomeAccount");
                        }
                        else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstIncomeAccount");
                        }

                    }


                },
                fail: function (jqXhr, exception) {

                }
            });

        }


        function COGSList() {
            $("#lstCOGS").empty();
            $.ajax({
                url: '/WebPOSService.asmx/COGSList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.COGSAccount.length; i++) {

                        var name = AllComboList.COGSAccount[i].Name.split("*")[0] ;
                        var code = AllComboList.COGSAccount[i].Name.split("*")[1];
                        if (code == "0104010100001") {
                            $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstCOGS");
                        }
                        else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstCOGS");
                        }

                    }


                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function ItemTypeList() {
            $("#lstItemType").empty();
            $.ajax({
                url: '/WebPOSService.asmx/ItemTypeList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.ItemType.length; i++) {

                        var name = AllComboList.ItemType[i].Name.split("*")[0];
                        var id = AllComboList.ItemType[i].Name.split("*")[1] ;
                        if (id == "1") {
                            $(' <option value="' + id + '"selected>' + name + '</option>').appendTo("#lstItemType");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstItemType");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function AccountUnitList() {
            $("#lstUnit").empty();
            $.ajax({
                url: '/WebPOSService.asmx/AccountUnitList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.Unit.length; i++) {

                        var name = AllComboList.Unit[i].Name.split("-")[0];
                        var id = AllComboList.Unit[i].Name.split("-")[1];
                        if (id == "1") {
                            $(' <option value="' + id + '" selected>' + name + '</option>').appendTo("#lstUnit");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstUnit");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function GetBrandList2() {
            $("#lstBrand").empty();
            $.ajax({
                url: '/WebPOSService.asmx/Brand_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {

                    for (var i = 0; i < BrandList.length; i++) {

                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        if (code == "1") {
                            $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstBrand");
                        } else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstBrand");
                        }
                    }
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        
        function WidthList() {
            $("#lstWidth").empty();
            $.ajax({
                url: '/WebPOSService.asmx/WidthList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.Width.length; i++) {

                        var name = AllComboList.Width[i].Name.split("*")[0];
                        var id = AllComboList.Width[i].Name.split("*")[1];
                        if (id == "1") {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstWidth");
                        }
                        else
                        {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstWidth");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function HeightList() {
            $("#lstHeight").empty();
            $.ajax({
                url: '/WebPOSService.asmx/HeightList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.Height.length; i++) {

                        var name = AllComboList.Height[i].Name;
                        var id = AllComboList.Height[i].Code;
                        if (id == "1") {
                            $(' <option value="' + id + '" selected>' + name + '</option>').appendTo("#lstHeight");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstHeight");
                        }
                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function GetBarCodeCategoryList() {
            $("#lstBarCodeCategory").empty();
            $.ajax({
                url: '/WebPOSService.asmx/BarCodeCategoryList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.BarCodeCategory.length; i++) {

                        var name = AllComboList.BarCodeCategory[i].Name.split("*")[0];
                        var id = AllComboList.BarCodeCategory[i].Name.split("*")[1];
                        if (id == 1) {
                            $(' <option value="' + id + '" selected>' + name + '</option>').appendTo("#lstBarCodeCategory");
                        }
                        else {
                            $(' <option value="' + id + '" >' + name + '</option>').appendTo("#lstBarCodeCategory");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }

        function GetColorList() {
            $("#lstColor").empty();
            $.ajax({
                url: '/WebPOSService.asmx/Color_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList)  {

                    for (var i = 0; i < BrandList.length; i++) {

                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        if (code == 1) {
                            $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstColor");
                        }
                        else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstColor");
                        }
                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }

        function GetItemNatureList() {
            $("#lstItemNature").empty();
            $.ajax({
                url: '/WebPOSService.asmx/ItemNatureList',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (AllComboList) {

                    for (var i = 0; i < AllComboList.ItemNature.length; i++) {

                        var name = AllComboList.ItemNature[i].Name.split("-")[0] ;
                        var id = AllComboList.ItemNature[i].Name.split("-")[1] ;
                        if (id == "1") {
                            $(' <option value="' + id + '" selected>' + name + '</option>').appendTo("#lstItemNature");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstItemNature");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }

        function GetGoDownList() {
            $("#lstGoDown").empty();
            $.ajax({
                url: '/WebPOSService.asmx/GoDown_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {

                    for (var i = 0; i < BrandList.length; i++) {

                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        if (code == "1") {
                            $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstGoDown");
                        }
                        else{
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstGoDown");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function GetClassList() {
            $("#lstClass").empty();
            $.ajax({
                url: '/WebPOSService.asmx/Class_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {

                    for (var i = 0; i < BrandList.length; i++) {

                        var name = BrandList[i].Name;
                        var id = BrandList[i].Code;
                        if (id == 1) {
                            $(' <option value="' + id + '"selected>' + name + '</option>').appendTo("#lstClass");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstClass");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }

        function GetPackingList() {
            $("#lstPacking").empty();
            $.ajax({
                url: '/WebPOSService.asmx/Packing_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {
                   
                    for (var i = 0; i < BrandList.length; i++) {

                        var name = BrandList[i].Name;
                        var id = BrandList[i].Code;
                        if (id == "1") {
                            $(' <option value="' + id + '"selected>' + name + '</option>').appendTo("#lstPacking");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstPacking");
                        }

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function GetCategoryList() {
            $("#Category_List").empty();
            $.ajax({
                url: '/WebPOSService.asmx/Category_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {
                   
                    for (var i = 0; i < BrandList.length; i++) {
                       
                        var name = BrandList[i].Name;
                        var id = BrandList[i].Code;
                        if (id == "1") {
                            $(' <option value="' + id + '"selected>' + name + '</option>').appendTo("#lstCategory");
                        }
                        else {
                            $(' <option value="' + id + '">' + name + '</option>').appendTo("#lstCategory");
                        }
                        
                    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       

                },
                fail: function (jqXhr, exception) {

                }
            });

        }
        function GetManufacturerList() {
            $("#lstManufacturer").empty();
            $.ajax({
                url: '/WebPOSService.asmx/Manufacturer_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {
                    ; 
                    
                    for (var i = 0; i <  BrandList.length; i++) {

                        
                        
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        if (code == "1") {
                            $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstManufacturer");
                        }
                        else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstManufacturer");
                        }
                    }

                },
                fail: function (jqXhr, exception) {

                }
            });

        }

        


    </script>
    
                                     
</asp:Content>
