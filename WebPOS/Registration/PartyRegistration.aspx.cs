﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;
namespace WebPOS
{
    public partial class PartyRegistration : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        ModGLCode objModGLCode = new ModGLCode();
        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        Module7 objModule7 = new Module7();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {





            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(PartyModel PartyModel)
        {

            var CompID = "01";
            try
            {
                var isAdmin = HttpContext.Current.Session["UserId"].ToString() == "0";
                var listPages = HttpContext.Current.Session["UserPermittedPages"] as List<WebPOS.Model.MenuPage>;
                if (isAdmin || listPages.Any(a => a.Url.Contains("PartyRegistration")))
                {
                    Module1 objModule1 = new Module1();
                    Module7 objModule7 = new Module7();
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    int NorBalance = Convert.ToInt16(PartyModel.NorBalance);
                    string PartyCode = objModule1.MaxPartyCode(NorBalance);
                    var PartyName = PartyModel.PartyName;
                    var ContactPerson = PartyModel.ContactPerson;
                    var MobileNo = PartyModel.MobileNo;
                    var FaxNo = PartyModel.FaxNo;
                    var LandLineNo = PartyModel.LandLineNo;
                    var Address = PartyModel.Address;
                    var Email = PartyModel.Email;
                    var lstSellingPriceNo = PartyModel.lstSellingPriceNo;
                    int lstDealApplyNo = Convert.ToInt16(PartyModel.lstDealApplyNo);
                    decimal CreditLimit = Convert.ToDecimal(PartyModel.CreditLimit);
                    int chkCreditLimitApply = Convert.ToInt16(PartyModel.CreditLimitApply);
                    var OtherInfo = PartyModel.OtherInfo;
                    var lstSaleMan = PartyModel.lstSaleMan;
                    int chkDeal = Convert.ToInt16(PartyModel.chkDeal);
                    int chkPerDiscount = Convert.ToInt16(PartyModel.chkPerDiscount);

                    var message = "Party Registered Successfully.";

                    SqlCommand cmd = new SqlCommand("insert into PartyCode (code,name,OwnerName,Mobile,Fax,Phone,Address,Email,NorBalance,CreditLimit,LimitApplicable,SellingPrice,OtherInfo,SalManCode,CompID,DealRs,DisPer,DealApplyNo)values('" + PartyCode + "','" + PartyName + "','" + ContactPerson + "','" + MobileNo + "','" + FaxNo + "','" + ContactPerson + "','" + Address + "','" + Email + "'," + NorBalance + "," + CreditLimit + "," + chkCreditLimitApply + "," + lstSellingPriceNo + ",'" + OtherInfo + "'," + lstSaleMan + ",'" + CompID + "'," + chkDeal + "," + chkPerDiscount + "," + lstDealApplyNo + ")", con, tran);
                    cmd.ExecuteNonQuery();

                    SqlCommand cmd2 = new SqlCommand("insert into PartyOpBalance(code,Dat,NorBalance,CompID) values('" + PartyCode + "','" + objModule7.StartDate() + "'," + NorBalance + ",'" + CompID + "')", con, tran);
                    cmd2.ExecuteNonQuery();

                    SqlCommand cmd3 = new SqlCommand("insert into GLCode(code, Title, NorBalance, Lvl, GroupDet, Header_Detail, CompID) values('" + PartyCode + "','" + PartyName + "'," + NorBalance + "," + 5 + ",'" + "Balance Sheet" + "','" + "Detail" + "', '" + CompID + "')", con, tran);
                    cmd3.ExecuteNonQuery();

                    SqlCommand cmd4 = new SqlCommand("insert into GLOpeningBalance(Code, Date1, CompID) values('" + PartyCode + "', '" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
                    cmd4.ExecuteNonQuery();



                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = message };


                }
                else
                {
                    return new BaseModel() { Success = false, Message = "You are not permitted to register any party." };

                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }






        ////////void SaveData()
        ////////{

        ////////    SqlTransaction tran;
        ////////    if (con.State==ConnectionState.Closed) { con.Open(); }
        ////////    tran = con.BeginTransaction();
        ////////    try
        ////////    {

        ////////    decimal OrderQTY = 0;
        ////////    decimal BonusQTY = 0;
        ////////    decimal GSTRate = 0;
        ////////    Int32 Visiable = 0;
        ////////    decimal ReOrdLevel = 0;
        ////////    decimal ReorderQty = 0;
        ////////    decimal MinLimit = 0;
        ////////    decimal MaxLimit = 0;
        ////////    decimal Height = 0;
        ////////    decimal Length = 0;
        ////////    decimal PiecesInPacking = 0;
        ////////    decimal SellingPrice = 0;
        ////////    Int32 ItemType=1;
        ////////    Int32 ColorID=1;    
        ////////    Int32 Active = 1;
        ////////    Int32 GST_Apply=1;

        ////////    ///if (txtReorderQty.Text != "") { ReorderQty =Convert.ToDecimal(txtReorderQty.Text);}
        ////////    //if (txtReOrdLevel.Text != "") { ReOrdLevel = Convert.ToDecimal(txtReOrdLevel.Text);}
        ////////    //if (txtPiecesInPacking.Text != "") { PiecesInPacking = Convert.ToDecimal(txtPiecesInPacking.Text);}
        ////////    //if (txtBonusQTY.Text != "") { BonusQTY = Convert.ToDecimal(txtBonusQTY.Text); }
        ////////    //if (txtSellingPrice.Text != "") { SellingPrice = Convert.ToDecimal(txtSellingPrice.Text); }
        ////////    //if (chkItemIsActive.Checked == false ) { Active = 0; }
        ////////    //if (txtOrderQTY.Text != "") { OrderQTY = Convert.ToDecimal(txtOrderQTY.Text); }


        ////////        //if (txtGST_Rate.Text != "") {  GSTRate = Convert.ToDecimal(txtGST_Rate.Text); }
        ////////        //if (chkGST.Checked ==false ) {  GST_Apply =0; }
        ////////        //if (txtMinLimit.Text!= "") {MinLimit = Convert.ToDecimal(txtMinLimit.Text); }
        ////////        //if (txtMaxLimit.Text!= "") {MaxLimit = Convert.ToDecimal(txtMaxLimit.Text); }

        ////////        string ManufacturerCode = "1"; // Convert.ToString(objModItem.ManufacturerCodeAgainstName(lstManufacturer.Text));
        ////////        Int32 PackingCode = 1; // objModItem.PackingCodeAgainstName(lstPacking.Text);
        ////////        Int32 CategoryCode = 1; // objModItem.CategoryCodeAgainstName(lstCategory.Text);
        ////////        Int32 ClassCode = 1; // objModItem.ClassCodeAgainstName(lstClass.Text);
        ////////        Int32 brandcode = 1;// objModItem.BrandCodeAgainstBrandName(lstBrand.Text);
        ////////        Int32 GodownCode = 1; // objModItem.GodownCodeAgainstName(lstGoDown.Text);
        ////////        Int32 AccountUnit = 1; // objModItem.AccUnitCodeAgainstName(lstUnit.Text);
        ////////        Int32 ItemNatureCode = 1; //objModItem.ItemNatureCodeAgainstName(lstItemNature.Text);
        ////////        string RevenueHead = "1111";// objModGLCode.GLCodeAgainstTitle2(lstIncomeAccount.Text);
        ////////    //Int32 Length = objModItem.LengthCodeAgainstName(lstLength.Text)
        ////////    if (ChkVisiable.Checked == true) { Visiable = 1; }   


        ////////       //txtItemCode.Text = Convert.ToString(objModule1.MaxInvCode());
        ////////       //if (txtBarCode.Text == "") { txtBarCode.Text = txtItemCode.Text; }


        ////////        DateTime SysTime = DateTime.Now;


        ////////        SqlCommand cmd = new SqlCommand("insert into InvCode (code,Description,Reference2,PiecesInPacking,Reference,Brand,Visiable,ReorderLevel,Manufacturer,Packing,Category,Class,Godown,ReorderQty,OrderQTY,BonusQTY,Nature,AccountUnit,SellingCost,RegNo,Revenue_Code,Active,GST_Rate,GST_Apply,MinLimit,MaxLimit,Color,CompID) values( " + Convert.ToDecimal(1) + ",'" + "txtItemName.Text" + "','" + "txtReference2.Text" + "'," + PiecesInPacking + ",'" + "txtReference1.Text" + "','" + brandcode + "'," + Visiable + "," + ReOrdLevel + ",'" + ManufacturerCode + "'," + PackingCode + "," + CategoryCode + "," + ClassCode + "," + GodownCode + "," + ReorderQty + "," + OrderQTY + "," + BonusQTY + "," + ItemNatureCode + "," + AccountUnit + "," + SellingPrice + ",'" + "txtRegNo.Text" + "','" + RevenueHead + "'," + Active + "," + GSTRate + "," + GST_Apply + "," + MinLimit + "," + MaxLimit + "," + ColorID + ",'" +  CompID + "')", con,tran);
        ////////        cmd.ExecuteNonQuery();

        ////////        SqlCommand cmd2 = new SqlCommand("Insert into InvBarCode(ItemCode,BarCode,CompID) values(" + Convert.ToDecimal(222) + ",'" + "txtBarCode.Text" + "','" + CompID + "')", con, tran);
        ////////        cmd2.ExecuteNonQuery();






        ////////        SqlCommand cmd4 = new SqlCommand("insert into InventoryOpeningBalance (ICode,Dat,CompID) values( " + Convert.ToDecimal(2222) + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
        ////////        cmd4.ExecuteNonQuery();
        ////////        SqlCommand cmd5 = new SqlCommand("insert into WarrantyOpeningBalance (ICode,Dat,CompID) values( " + Convert.ToDecimal(2222) + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
        ////////        cmd5.ExecuteNonQuery();

        ////////        SqlCommand cmd6 = new SqlCommand("insert into WorkshopOpeningBalance (ICode,Dat,CompId) values( " + Convert.ToDecimal(33333) + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
        ////////        cmd6.ExecuteNonQuery();





        ////////        //txtItemName.Text = "";
        ////////        //txtReference1.Text ="";
        ////////        //txtReference2.Text ="";
        ////////        //txtReOrdLevel.Text =""; 







        ////////        tran.Commit();
        ////////        con.Close();

        ////////    }
        ////////    catch (Exception ex)
        ////////    {
        ////////        tran.Rollback();
        ////////        ///txtItemName.Text = ex.ToString();
        ////////    }

        ////////    con.Close();

        ////////}



    }
}