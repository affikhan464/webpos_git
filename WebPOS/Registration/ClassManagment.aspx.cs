﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class ClassManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var ClassID = Brand.Code;
                var ClassName = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(ClassID) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Class set Name='" + ClassName + "' where id=" + ClassID, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Class UpDated";

                }
                else
                {
                    var MaxClassID = objModule1.MaxClassCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Class(ID,Name,CompID) Values(" + MaxClassID + ",'" + ClassName + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Class Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteClass(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var ClassCode = Brand.Code;

                Module1 objModule1 = new Module1();
                var AlreadyAsignedToItem = objModItem.ClassAssignedToItem(ClassCode);
                var message = "";
                if (!AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from Class where id=" + ClassCode + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    message = "Class Deleted";
                }
                else
                {
                    message = "Class can not be deleted assigned";
                    return new BaseModel() { Success = false, Message = message };
                }



                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }


    }

}