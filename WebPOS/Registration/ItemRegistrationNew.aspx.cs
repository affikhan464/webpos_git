﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;
namespace WebPOS
{
    public partial class ItemRegistrationNew : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        ModGLCode objModGLCode = new ModGLCode();
        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        Module7 objModule7 = new Module7();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ItemModel ItemModel)
        {
            Module1 objModule1 = new Module1();
            ModItem objModItem = new ModItem();
            ModGLCode objModGLCode = new ModGLCode();
            Module7 objModule7 = new Module7();
            var CompID = "01";
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                var ItemCode = objModule1.MaxInvCode();
                var BarCode = ItemModel.BarCode;
                var Description = ItemModel.Description;
                var Reference = ItemModel.Reference == string.Empty ? "" : ItemModel.Reference;
                var Reference2 = ItemModel.Reference2 == string.Empty ? "" : ItemModel.Reference2;
                var PiecesInPacking = ItemModel.PiecesInPacking;
                var Brand = ItemModel.Brand;
                var chkVisiable = ItemModel.Visiable;
                var ReOrdLevel = ItemModel.ReorderLevel;
                int Manufacturer = Convert.ToInt16(ItemModel.Manufacturer);//........
                int PackingID = Convert.ToInt16(ItemModel.Packing);//........
                int CategoryID = Convert.ToInt16(ItemModel.Category);//........
                int ClassID = Convert.ToInt16(ItemModel.Class);//........
                int GoDownID = Convert.ToInt16(ItemModel.Godown);//........
                decimal ReOrdQuantity = Convert.ToDecimal(ItemModel.ReorderQty);
                decimal OrderQty = Convert.ToDecimal(ItemModel.OrderQTY);
                decimal BonusQty = Convert.ToDecimal(ItemModel.BonusQTY);
                int ItemNatureID = Convert.ToInt16(ItemModel.Nature);//........
                int AccountUnitID = Convert.ToInt16(ItemModel.AccountUnit);//........
                decimal SellingPrice = Convert.ToDecimal(ItemModel.SellingCost1);
                var Revenue_Code = ItemModel.Revenue_Code;
                int chkActive = Convert.ToInt16(ItemModel.Active);
                decimal GSTRate = Convert.ToDecimal(ItemModel.GST_Rate);
                int chkGSTApply = Convert.ToInt16(ItemModel.GST_Apply);
                decimal MinLimit = Convert.ToDecimal(ItemModel.MinLimit);
                decimal MaxLimit = Convert.ToDecimal(ItemModel.MaxLimit);
                var CGS_Code = ItemModel.CGS_Code;
                int lstItemType = Convert.ToInt16(ItemModel.ItemType);
                int lstWidth = Convert.ToInt16(ItemModel.Length);
                int lstHeight = Convert.ToInt16(ItemModel.Height);
                int lstBarCodeCategory = Convert.ToInt16(ItemModel.BarCodeCategory);
                int lstColor = Convert.ToInt16(ItemModel.Color);
                string otherInFo = ItemModel.OtherInFo;
                string PerPieceCommission = ItemModel.PerPieceCommission==null || ItemModel.PerPieceCommission == string.Empty ? "0" : ItemModel.PerPieceCommission;
                string Year1 = ItemModel.Year1;
                string group = ItemModel.Group;
                string LP = ItemModel.LP == null || ItemModel.LP == string.Empty ? "0" : ItemModel.LP;
                
                string mSP = ItemModel.MSP == null || ItemModel.MSP == string.Empty ? "0" : ItemModel.MSP;


                SqlCommand cmd = new SqlCommand("insert into InvCode (code,LP,Description,Reference2,PiecesInPacking,Reference,Brand,Visiable,ReorderLevel,Manufacturer,Packing,Category,Class,Godown,ReorderQty,OrderQTY,BonusQTY,Nature,AccountUnit,SellingCost,RegNo,Revenue_Code,Active,GST_Rate,GST_Apply,MinLimit,MaxLimit,CompID,Qty,CGS_Code,Ingridients,ItemType,Length,Height,BarCodeCategory,Color,OtherInFo,PerPieceCommission,Year1,GroupId,MSP) values(" + ItemCode + "," + LP + ", '" + Description + "', '" + Reference2 + "', '" + PiecesInPacking + "', '" + Reference + "', '" + Brand + "', " + chkVisiable + ", " + Convert.ToDecimal(ReOrdLevel) + ", " + Manufacturer + "," + PackingID + ", " + CategoryID + ", " + ClassID + ", " + GoDownID + "," + ReOrdQuantity + "," + OrderQty + "," + BonusQty + "," + ItemNatureID + "," + AccountUnitID + "," + SellingPrice + ", '" + "RegNo" + "','" + Revenue_Code + "'," + chkActive + "," + GSTRate + "," + chkGSTApply + "," + MinLimit + "," + MaxLimit + ",'" + CompID + "'," + 0 + ", '" + CGS_Code + "', " + 0 + ", " + lstItemType + ", " + lstWidth + ", " + lstHeight + ", " + lstBarCodeCategory + ", " + lstColor + ", '" + otherInFo + "', " + PerPieceCommission + ", '" + Year1 + "', " + group + ", " + mSP + ")", con, tran);

                cmd.ExecuteNonQuery();
                if (BarCode == "") { BarCode = Convert.ToString(ItemCode); }
                SqlCommand cmd2 = new SqlCommand("Insert into InvBarCode(ItemCode,BarCode,CompID) values(" + ItemCode + ",'" + BarCode + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();

                
                if (con.State == ConnectionState.Closed) { con.Open(); }
                
                

               
                    SqlCommand cmd4 = new SqlCommand("insert into InventoryOpeningBalance (ICode,Dat,CompID) values( " + ItemCode + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
                    cmd4.ExecuteNonQuery();
                    SqlCommand cmd5 = new SqlCommand("insert into WarrantyOpeningBalance (ICode,Dat,CompID) values( " + ItemCode + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
                    cmd5.ExecuteNonQuery();
                    SqlCommand cmd6 = new SqlCommand("insert into WorkshopOpeningBalance (ICode,Dat,CompId) values( " + ItemCode + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
                    cmd6.ExecuteNonQuery();
                




                tran.Commit();
                con.Close();
                return   new BaseModel() { Success = true, Message = "Item Created successfully" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }






        ////////void SaveData()
        ////////{

        ////////    SqlTransaction tran;
        ////////    if (con.State==ConnectionState.Closed) { con.Open(); }
        ////////    tran = con.BeginTransaction();
        ////////    try
        ////////    {

        ////////    decimal OrderQTY = 0;
        ////////    decimal BonusQTY = 0;
        ////////    decimal GSTRate = 0;
        ////////    Int32 Visiable = 0;
        ////////    decimal ReOrdLevel = 0;
        ////////    decimal ReorderQty = 0;
        ////////    decimal MinLimit = 0;
        ////////    decimal MaxLimit = 0;
        ////////    decimal Height = 0;
        ////////    decimal Length = 0;
        ////////    decimal PiecesInPacking = 0;
        ////////    decimal SellingPrice = 0;
        ////////    Int32 ItemType=1;
        ////////    Int32 ColorID=1;    
        ////////    Int32 Active = 1;
        ////////    Int32 GST_Apply=1;

        ////////    ///if (txtReorderQty.Text != "") { ReorderQty =Convert.ToDecimal(txtReorderQty.Text);}
        ////////    //if (txtReOrdLevel.Text != "") { ReOrdLevel = Convert.ToDecimal(txtReOrdLevel.Text);}
        ////////    //if (txtPiecesInPacking.Text != "") { PiecesInPacking = Convert.ToDecimal(txtPiecesInPacking.Text);}
        ////////    //if (txtBonusQTY.Text != "") { BonusQTY = Convert.ToDecimal(txtBonusQTY.Text); }
        ////////    //if (txtSellingPrice.Text != "") { SellingPrice = Convert.ToDecimal(txtSellingPrice.Text); }
        ////////    //if (chkItemIsActive.Checked == false ) { Active = 0; }
        ////////    //if (txtOrderQTY.Text != "") { OrderQTY = Convert.ToDecimal(txtOrderQTY.Text); }


        ////////        //if (txtGST_Rate.Text != "") {  GSTRate = Convert.ToDecimal(txtGST_Rate.Text); }
        ////////        //if (chkGST.Checked ==false ) {  GST_Apply =0; }
        ////////        //if (txtMinLimit.Text!= "") {MinLimit = Convert.ToDecimal(txtMinLimit.Text); }
        ////////        //if (txtMaxLimit.Text!= "") {MaxLimit = Convert.ToDecimal(txtMaxLimit.Text); }

        ////////        string ManufacturerCode = "1"; // Convert.ToString(objModItem.ManufacturerCodeAgainstName(lstManufacturer.Text));
        ////////        Int32 PackingCode = 1; // objModItem.PackingCodeAgainstName(lstPacking.Text);
        ////////        Int32 CategoryCode = 1; // objModItem.CategoryCodeAgainstName(lstCategory.Text);
        ////////        Int32 ClassCode = 1; // objModItem.ClassCodeAgainstName(lstClass.Text);
        ////////        Int32 brandcode = 1;// objModItem.BrandCodeAgainstBrandName(lstBrand.Text);
        ////////        Int32 GodownCode = 1; // objModItem.GodownCodeAgainstName(lstGoDown.Text);
        ////////        Int32 AccountUnit = 1; // objModItem.AccUnitCodeAgainstName(lstUnit.Text);
        ////////        Int32 ItemNatureCode = 1; //objModItem.ItemNatureCodeAgainstName(lstItemNature.Text);
        ////////        string RevenueHead = "1111";// objModGLCode.GLCodeAgainstTitle2(lstIncomeAccount.Text);
        ////////    //Int32 Length = objModItem.LengthCodeAgainstName(lstLength.Text)
        ////////    if (ChkVisiable.Checked == true) { Visiable = 1; }   


        ////////       //txtItemCode.Text = Convert.ToString(objModule1.MaxInvCode());
        ////////       //if (txtBarCode.Text == "") { txtBarCode.Text = txtItemCode.Text; }


        ////////        DateTime SysTime = DateTime.Now;


        ////////        SqlCommand cmd = new SqlCommand("insert into InvCode (code,Description,Reference2,PiecesInPacking,Reference,Brand,Visiable,ReorderLevel,Manufacturer,Packing,Category,Class,Godown,ReorderQty,OrderQTY,BonusQTY,Nature,AccountUnit,SellingCost,RegNo,Revenue_Code,Active,GST_Rate,GST_Apply,MinLimit,MaxLimit,Color,CompID) values( " + Convert.ToDecimal(1) + ",'" + "txtItemName.Text" + "','" + "txtReference2.Text" + "'," + PiecesInPacking + ",'" + "txtReference1.Text" + "','" + brandcode + "'," + Visiable + "," + ReOrdLevel + ",'" + ManufacturerCode + "'," + PackingCode + "," + CategoryCode + "," + ClassCode + "," + GodownCode + "," + ReorderQty + "," + OrderQTY + "," + BonusQTY + "," + ItemNatureCode + "," + AccountUnit + "," + SellingPrice + ",'" + "txtRegNo.Text" + "','" + RevenueHead + "'," + Active + "," + GSTRate + "," + GST_Apply + "," + MinLimit + "," + MaxLimit + "," + ColorID + ",'" +  CompID + "')", con,tran);
        ////////        cmd.ExecuteNonQuery();

        ////////        SqlCommand cmd2 = new SqlCommand("Insert into InvBarCode(ItemCode,BarCode,CompID) values(" + Convert.ToDecimal(222) + ",'" + "txtBarCode.Text" + "','" + CompID + "')", con, tran);
        ////////        cmd2.ExecuteNonQuery();






        ////////        SqlCommand cmd4 = new SqlCommand("insert into InventoryOpeningBalance (ICode,Dat,CompID) values( " + Convert.ToDecimal(2222) + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
        ////////        cmd4.ExecuteNonQuery();
        ////////        SqlCommand cmd5 = new SqlCommand("insert into WarrantyOpeningBalance (ICode,Dat,CompID) values( " + Convert.ToDecimal(2222) + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
        ////////        cmd5.ExecuteNonQuery();

        ////////        SqlCommand cmd6 = new SqlCommand("insert into WorkshopOpeningBalance (ICode,Dat,CompId) values( " + Convert.ToDecimal(33333) + ",'" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
        ////////        cmd6.ExecuteNonQuery();





        ////////        //txtItemName.Text = "";
        ////////        //txtReference1.Text ="";
        ////////        //txtReference2.Text ="";
        ////////        //txtReOrdLevel.Text =""; 







        ////////        tran.Commit();
        ////////        con.Close();

        ////////    }
        ////////    catch (Exception ex)
        ////////    {
        ////////        tran.Rollback();
        ////////        ///txtItemName.Text = ex.ToString();
        ////////    }

        ////////    con.Close();

        ////////}



    }
}
