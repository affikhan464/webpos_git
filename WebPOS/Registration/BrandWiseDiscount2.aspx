﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="BrandWiseDiscount2.aspx.cs" Inherits="WebPOS.Registration.BrandWiseDiscount2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Brand Wise Discount 2</h2>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi  input--filled">
                            <select id="lstBrand" class="round input__field--hoshi">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Brand Name </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Brand"><i class="fas fa-plus-circle"></i> Add Brand </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtDiscount" class="Discount input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Discount (%)</span>
                            </label>
                        </span>
                    </div>

                </div>

                 <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" id="btnReset" onclick="reset()"><span>Reset </span></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100"><span>Set To All Clients</span></a>
                        </span>
                    </div>
                </div>
            </section>
        </div>

    </div>

      <%@ Register Src="~/Registration/_AttributeModal.ascx" TagName="_AttributeModal" TagPrefix="uc" %>
     <uc:_AttributeModal ID="_AttributeModal1" runat="server" />
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
       <script src="/Script/Autocomplete.js"></script> 
    <script src="/Script/Attribute.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            GetBrandList2();



        });

       
      
        function save() {
          
            var SaveBrandWiseDiscountModel = {
                BrandCode: $("#lstBrand").val(),
                Discount: $(".Discount").val()

            };
            $("#btnSave span").html("<i class='fas fa-spinner fa-spin'></i> Wait Setting Discounts...")
            $("#btnSave").addClass("disableClick");
            $.ajax({
                url: "/Registration/BrandWiseDiscount2.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ SaveBrandWiseDiscountModel: SaveBrandWiseDiscountModel }),
                success: function (BaseModel) {
                    
                    if (BaseModel.d.Success) {
                        $("#lstBrand").val(1);
                        $(".Discount").val('');
                        updateInputStyle();
                        $("#btnSave span").html("Set to All Clients");
                        $("#btnSave").removeClass("disableClick");

                        smallSwal("Updated!!", BaseModel.d.Message, "success");
                      
                    }
                    else {
                        $("#btnSave").removeClass("disableClick");
                        $("#btnSave span").html("Set to All Clients");
                        smallSwal("Failed!!", BaseModel.d.Message, "error");
                    }


                }

            });

        }


    </script>


</asp:Content>
