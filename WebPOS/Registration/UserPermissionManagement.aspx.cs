﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;
namespace WebPOS.Registration
{
    public partial class UserPermissionManagement : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel AddPages(List<MenuPage> model)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var cmd = new SqlCommand(@"Select Id,Name from MenuPages", con, tran);

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;

                dAdapter.Fill(objDs);

                var data = objDs.Tables[0];

                var menuPages = new List<MenuPage>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string id = data.Rows[i]["Id"].ToString();
                    string name = data.Rows[i]["Name"].ToString();

                    var menuPage = new MenuPage()
                    {
                        Id = id,
                        Name = name

                    };
                    menuPages.Add(menuPage);
                }

                foreach (var page in model)
                {
                    var Id = page.Id;
                    var Name = page.Name;
                    var Url = page.Url;

                    var isMenuExist = menuPages.Any(a => a.Id == Id);
                    var isMenuNameExist = menuPages.Any(a => a.Name == Name && a.Id == Id);
                    var isMenuUrlExist = menuPages.Any(a => a.Url == Url && a.Id == Id);

                    if (isMenuExist && (!isMenuNameExist || !isMenuUrlExist))
                    {
                        SqlCommand cmdUpdate = new SqlCommand("Update MenuPages set Name='" + Name + "',Url='" + Url + "' where Id=" + Id + " and CompId='" + CompID + "'", con, tran);
                        cmdUpdate.ExecuteNonQuery();
                    }
                    else if (!isMenuExist && !isMenuNameExist)
                    {
                        SqlCommand cmdInsert = new SqlCommand("Insert into MenuPages(Id,Name,Url,CompId) Values(" + Id + ",'" + Name + "','" + Url + "','" + CompID + "')", con, tran);
                        cmdInsert.ExecuteNonQuery();
                    }

                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(List<MenuPage> model)
        {

            var isAdmin = HttpContext.Current.Session["UserId"].ToString() == "0";  //0 mean admin
            if (isAdmin)
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    
                    SqlCommand cmdUpdate = new SqlCommand("Delete from UserMenuPages where UserId=" + model.FirstOrDefault().UserId + " and CompId='" + CompID + "'", con, tran);
                    cmdUpdate.ExecuteNonQuery();

                    foreach (var page in model)
                    {
                        var Id = page.Id;
                        var UserId = page.UserId;

                        SqlCommand cmdInsert = new SqlCommand("Insert into UserMenuPages(UserId,PageId,CompId) Values(" + UserId + "," + Id + ",'" + CompID + "')", con, tran);
                        cmdInsert.ExecuteNonQuery();
                    }

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Pages Permitted Successfully" };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "You do not have access to do this." };

            }
        }
    }

}