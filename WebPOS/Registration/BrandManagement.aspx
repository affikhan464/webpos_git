﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="BrandManagement.aspx.cs" Inherits="WebPOS.Registration.BrandManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header ">Brand Managment</h2>
                <div class="row">


                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-6">
                        <span class="input input--hoshi">
                            <input class="BrandName empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtSellingPrice1" class="BrandCode input__field input__field--hoshi empty1" disabled="disabled" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input class="blocked checkbox" id="chkBlocked" type="checkbox" />
                                <span></span>
                                Blocked
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="chkVerify" class="checkbox" type="checkbox" />
                                <span></span>
                                Verify IME
                            </label>
                        </span>
                    </div>
                </div>

                <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" id="btnReset" onclick="reset()">Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Brands </h3>
                </div>

            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-4">
                            <div>
                                <span>Name</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-1">
                            <div>
                                <span>Is Blocked?</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-1">
                            <div>
                                <span>IME Verify?</span>
                            </div>
                        </div>

                        <div class="item-col item-col-header  flex-3">
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script type="text/javascript">

        $(document).ready(function () {

            GetAllBrandList();

        });

        function GetAllBrandList() {

            $.ajax({
                url: '/WebPOSService.asmx/Brand_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {
                    $(".item-list .item-data").remove();
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        var Blocked = Number(BrandList[i].Blocked);
                        var VerifyIME = Number(BrandList[i].VerifyIME);
                        var deleteAttr = '';
                        if (code != 1) {
                            deleteAttr = ' <i data-code="' + code + '"  onclick="deleteItem(this)"  class="fas fa-trash mr-3"></i> ';
                        }
                        var BlockedIcon = ' <i class="fas fa-times"></i> ';
                        var VerifyIMEIcon = ' <i class="fas fa-times"></i> '

                        if (Boolean(Blocked))
                            BlockedIcon = ' <i class="fas fa-check"></i> '


                        if (Boolean(VerifyIME))
                            VerifyIMEIcon = ' <i class="fas fa-check"></i> '


                        $(".item-list").append('<li class="item item-data"><div class="item-row"><div class="item-col flex-1"><div class="item-heading">Sr. No.</div><div>' + (i + 1) + '</div></div>   <div class="item-col flex-2"><div class="item-heading">Code</div><div>' + code + '</div></div>    <div class="item-col flex-4 no-overflow"> <div><a class=""><h4 class="item-title no-wrap">' + name + ' </h4></a></div></div>    <div class="item-col flex-1"><div class="item-heading">Blocked?</div><div>' + BlockedIcon + '</div></div>         <div class="item-col flex-1"><div class="item-heading">Verifyied IME?</div><div>' + VerifyIMEIcon + '</div></div>         <div class="item-col item-col-date flex-3"><div><i  data-brandverify="' + VerifyIME + '" data-brandBlocked="' + Blocked + '" data-brandcode="' + code + '" data-brandname="' + name + '" onclick="edit(this)" class="fas fa-edit mr-3"></i> ' + deleteAttr + ' </div></div></div></li>');

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });



        }
        function empty1() {
            $(".BrandName").val("");
            $(".BrandCode").val("");
            document.getElementById("chkBlocked").checked = false;
            document.getElementById("chkVerify").checked = false;


        }
        function edit(element) {
            empty1();
            var code = element.dataset.brandcode;
            var name = element.dataset.brandname;
            var Blocked = element.dataset.brandblocked;
            var Verify = element.dataset.brandverify;

            $(".BrandName").val(name);
            $(".BrandCode").val(code);

            if (Blocked == 1) {
                document.getElementById("chkBlocked").checked = true;
            } else {
                document.getElementById("chkBlocked").checked = false;
            }
            if (Verify == 1) {
                document.getElementById("chkVerify").checked = true;
            } else {
                document.getElementById("chkVerify").checked = false;
            }

            updateInputStyle();

        }




        function save() {
            var brandName = $(".BrandName").val();
            var brandCode = $(".BrandCode").val();
            var chkBlocked = document.getElementById("chkBlocked").checked ? "1" : "0";
            var chkVerity = document.getElementById("chkVerify").checked ? "1" : "0";
            if (brandCode == "") { brandCode = 0; }
            var Brand = {
                Code: brandCode,
                Name: brandName,
                Blocked: chkBlocked,
                VerifyIME: chkVerity
            }


            debugger
            $.ajax({
                url: "/Registration/BrandManagement.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetAllBrandList();
                        $(".BrandName").val("");
                        $(".BrandCode").val("");
                        document.getElementById("chkBlocked").checked = false;
                        document.getElementById("chkVerify").checked = false;
                        smallSwal(BaseModel.d.Message, '', "success");
                    }
                    else {
                        smallSwal("Failed", BaseModel.d.Message, "error");
                    }


                }

            });

        }

        function deleteItem(element) {
            var code = element.dataset.code;

            swal({
                title: 'Are you sure to delete?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes Delete It',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {

                        $.ajax({
                            url: "/Registration/BrandManagement.aspx/DeleteBrand",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: { "Code": code },
                            success: function (BaseModel) {
                                debugger
                                if (BaseModel.d.Success) {
                                    GetAllBrandList();
                                    smallSwal(BaseModel.d.Message, '', "success");
                                }
                                else {
                                    smallSwal("Failed", BaseModel.d.Message, "error");
                                }
                                $(".BrandName").val("");
                                $(".BrandCode").val("");
                                document.getElementById("chkBlocked").checked = false;
                                document.getElementById("chkVerify").checked = false;
                            }

                        });

                        resolve();
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            })






        }


    </script>

</asp:Content>
