﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="FindIME.aspx.cs" Inherits="WebPOS.Reports.SaleReports.FindIME" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="searchAppSection reports">
        <div class="contentContainer">
            <h2>Find IME</h2>
            <div class="BMSearchWrapper row">
               
                <div class="col col-12 col-sm-12 col-md-3">
                    <span class="userlLabel">IME</span>
                    <input id="IME" class="IME" name="IME" type="text" placeholder="Enter IME here..." />
                </div>

                <div class="col col-12 col-sm-12 col-md-3">

                    <select id="PageSizeDD" class="dropdown">
                        <option value="100">    Page Size: 100</option>
                        <option value="250">    Page Size: 250</option>
                        <option value="500">    Page Size: 500</option>
                        <option value="1000">   Page Size: 1000</option>
                        <option value="2000">   Page Size: 2000</option>
                    </select>
                </div>

                <div class="col col-12 col-sm-12 col-md-3">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
                </div>

                <div class="col col-12 col-sm-12 col-md-3">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i> Print</a>
                </div>
                <div class="col col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center pt-1">
                    <label class="processLabel active">
                        <input type="radio" name="MatchIME" checked="checked" value="Left" />
                        Left</label>
                </div>
                <div class="col col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center pt-1">

                    <label class="processLabel">
                        <input type="radio" name="MatchIME" value="AnyWhere" />
                        Any Where</label>
                </div>
                <div class="col col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center pt-1">
                    <label class="processLabel">
                        <input type="radio" name="MatchIME" value="ExactMatch" />
                        Exact Match</label>
                </div>
                <div class="col col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center pt-1">

                    <label class="processLabel">
                        <input type="radio" name="MatchIME" value="Right" />
                        Right</label>
                </div>

                <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="container-fluid">

            <div class="DaySaleGroup2 reports recommendedSection allItemsView">


                <div class="itemsHeader">
                    <div class="five phCol">
                        <a href="javascript:void(0)">Sr.
                        </a>
                    </div>
                    <div class="five phCol">
                        <a href="javascript:void(0)">Code
                        </a>
                    </div>
                    <div class="eleven  phCol">
                        <a href="javascript:void(0)">Item
                        </a>
                    </div>
                    <div class="eight  phCol">
                        <a href="javascript:void(0)">Brand
                        </a>
                    </div>
                    <div class="seven  phCol">
                        <a href="javascript:void(0)">Qty In Hand 
                        </a>
                    </div>
                    <div class=" eight phCol">
                        <a href="javascript:void(0)">Date
                        </a>
                    </div>
                    <div class="seven phCol">
                        <a href="javascript:void(0)">Trans.
                        </a>
                    </div>
                    <div class="eleven phCol">
                        <a href="javascript:void(0)">Party
                        </a>
                    </div>
                    <div class="seven phCol">
                        <a href="javascript:void(0)">Voucher#
                        </a>
                    </div>
                    <div class="ten phCol">
                        <a href="javascript:void(0)">Price
                        </a>
                    </div>
                    <div class="sixteen phCol">
                        <a href="javascript:void(0)">IME
                        </a>
                    </div>
                    <div class="five phCol">
                        
                    </div>
                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow'>
                        <div class='five'><span></span></div>
                        <div class='five'><span></span></div>

                        <div class='eleven '><span></span></div>
                        <div class='eight '><span></span></div>
                        <div class='seven'>
                            <input disabled="disabled" name='totalQuantity' />
                        </div>
                        <div class='eight'><span></span></div>
                        <div class='seven'><span></span></div>
                        <div class='eleven'><span></span></div>
                        <div class='seven'><span></span></div>
                        <div class='ten'>
                            <input disabled="disabled" name='totalPrice' />
                        </div>
                        <div class='sixteen'><span></span></div>
                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->
        <div class="container-fluid"><a class="loadMoreBtn disableClick redBtn w-100 mt-2 mb-2 float-left"><i class="fas fa-spinner"></i> Load More <span class="pageSizeText">100</span> Records</a></div>
    </div>

    </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>"></script>

    <script type="text/javascript">

        var counter = 0;
        $(document).on("click", ".processLabel ", function () {
            $(".processLabel ").removeClass("active");
            $(this).addClass("active");
            clearInvoice();
        });
        $(".loadMoreBtn").on("click", function (e) {
            $(".loadMoreBtn i").addClass("fa-spin");
            var pageSize = $("#PageSizeDD .dd-selected-value").val();
            var currentPage = Number($("#currentPage").val()) + 1;
            counter = pageSize * currentPage;

            $("#currentPage").val(currentPage);


            getReport();

        });

        $(document).on("click", ".openIMEBtn ", function () {
            var ime = this.dataset.ime;
            $("#IME").val(ime);
            //$(".itemsSection .itemRow:not([data-ime=" + ime + "])").remove();
           
            $("[name=MatchIME][value=ExactMatch]").prop('checked', true);
            $("[name=MatchIME]").parent().removeClass("active")
            $("[name=MatchIME][value=ExactMatch]").parent().addClass("active");
            clearInvoice();
            getReport();
            rerenderSerialNumber();
            calculateData();
        });

        $(document).on("click", ".deleteIMEBtn ", function () {

            var ime = this.dataset.ime;
            var invoicenumber = this.dataset.invoicenumber;
          
            var row = $(this).parents(".itemRow");
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this record!",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {

                        $.ajax({
                            url: '/Reports/SerialNo/FindIME.aspx/DeleteIME',
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify({ "IME": ime, "invoiceNumber": invoicenumber }),
                            success: function (response) {


                                if (response.d.Success) {

                                    swal("IME Record Releted", 'IME record deleted successfully!', "success");
                                    $(row).remove();
                                    rerenderSerialNumber();
                                    calculateData();
                                }
                                else {
                                    $(".loader").hide();
                                    swal("there is some error", response.d.Message, "error");
                                }

                            },
                            error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                            }
                        });

                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            });

         
         
        });
       
        $(document).ready(function () {
            appendTooltip();
            $("#PageSizeDD").ddslick({
                width: "100%",
                onSelected: function (data) {
                    $(".pageSizeText").text(data.selectedData.value);
                    clearInvoice();
                }
            });
            initializeDatePicker();
            resizeTable();
        });

        $(window).resize(function () {
            resizeTable();

        });

     
        $("#toTxbx").on("keypress", function (e) {

            if (e.key == 13) {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                counter = 0;
                getReport();

            }
        });
        $("#searchBtn").on("click", function (e) {
            counter = 0;
            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='0' />").appendTo("body");
            clearInvoice();
            $(".loader").show();
            getReport();

        });
        function rerenderSerialNumber() {
            var rows = $(".itemsSection .itemRow .SNo");
            var sno = 0;
            rows.each(function (index, element) {
                sno += 1;
                $(element).text(sno);
            })
        }
        window.smoothScroll = function (target) {
            var scrollContainer = target;
            do { //find scroll container
                scrollContainer = scrollContainer.parentNode;
                if (!scrollContainer) return;
                scrollContainer.scrollTop += 1;
            } while (scrollContainer.scrollTop == 0);

            var targetY = 0;
            do { //find the top of target relatively to the container
                if (target == scrollContainer) break;
                targetY += target.offsetTop;
            } while (target = target.offsetParent);

            scroll = function (c, a, b, i) {
                i++; if (i > 30) return;
                c.scrollTop = (a + (b - a) / 30 * i) - 261;
                setTimeout(function () { scroll(c, a, b, i); }, 20);
            }
            // start scrolling
            scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
        }
        function getReport() {
            var pageSize = $("#PageSizeDD .dd-selected-value").val() == null || $("#PageSizeDD .dd-selected-value").val() == undefined ? 100 : $("#PageSizeDD .dd-selected-value").val();


            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
            $.ajax({
                url: '/Reports/SerialNo/FindIME.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "MatchIME": $('[name=MatchIME]:checked').val(), "IME": $(".IME").val(), "pageSize": pageSize, "page": currentPage }),
                success: function (response) {


                    if (response.d.Success) {


                        if (response.d.ListofInvoices.length > 0) {
                            var firstItem = counter + 1;

                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                var deleteIME = "";
                                if (response.d.ListofInvoices[i].Transaction == "Sale") {
                                    deleteIME = "<div class='five'> <a class='deleteIMEBtn'  data-invoicenumber='" + response.d.ListofInvoices[i].InvoiceNumber + "' data-ime='" + response.d.ListofInvoices[i].IME + "' ><i class='fas fa-trash   ml-4 mt-2'></i></a></div>";

                                }
                                $("<div  id='" + counter + "' data-ime='" + response.d.ListofInvoices[i].IME + "' class='itemRow newRow'>" +
                                    "<div class='five'><span class='SNo'>" + counter + "</span></div>" +
                                    "<div class='five'><span title='" + response.d.ListofInvoices[i].ItemCode + "'>" + response.d.ListofInvoices[i].ItemCode + "</span></div>" +
                                    "<div class='eleven  name'><span title='" + response.d.ListofInvoices[i].Description + "'>" + response.d.ListofInvoices[i].Description + "</span></div>" +
                                    "<div class='eight  name'><span title='" + response.d.ListofInvoices[i].BrandName + "'>" + response.d.ListofInvoices[i].BrandName + "</span></div>" +
                                    "<div class='seven '> <input name='Quantity' value='" + Number(response.d.ListofInvoices[i].Quantity).toFixed(2) + "' disabled /></div>" +
                                    "<div class='eight name'> <input value='" + response.d.ListofInvoices[i].Date + "' disabled /></div>" +
                                    "<div class='seven name'> <input value='" + response.d.ListofInvoices[i].Transaction + "' disabled /></div>" +
                                    "<div class='eleven name' title='" + response.d.ListofInvoices[i].PartyName + "'> <input title='" + response.d.ListofInvoices[i].PartyName + "' value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    "<div class='seven'><input title='" + response.d.ListofInvoices[i].InvoiceNumber + "'  value='" + response.d.ListofInvoices[i].InvoiceNumber + "' disabled /></div>" +
                                    "<div class='ten'> <input name='Price' value='" + Number(response.d.ListofInvoices[i].Price).toFixed(2) + "' disabled /></div>" +
                                    "<div class='sixteen'> <span class='float-right'><a class='openIMEBtn' data-ime='" + response.d.ListofInvoices[i].IME + "' >" + response.d.ListofInvoices[i].IME + "</a> </span></div>" +
                                    deleteIME +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                            $(".loadMoreBtn i").removeClass("fa-spin");

                            if (response.d.ListofInvoices.length == pageSize)
                                $(".loadMoreBtn").removeClass("disableClick");
                            else
                                $(".loadMoreBtn").addClass("disableClick");

                            
                            
                            smoothScroll(document.getElementById(firstItem));
                            appendTooltip();

                        } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                            $(".loader").hide(); swal({
                                title: "No Result Found ", type:'error',
                                text: "No Result Found!"
                            });
                        } else {
                            $(".loadMoreBtn i").removeClass("fa-spin");
                            $(".loader").hide();
                            swal("No More Record Found", "No More Record Found against this IME", "info");

                            $(".loadMoreBtn").addClass("disableClick");

                        }

                    }
                    else {
                        $(".loader").hide();
                        swal("there is some error", response.d.Message, "error");
                    }

                },
                error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }
        function clearInvoice() {
            $(".loadMoreBtn").addClass("disableClick");
            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='0' />").appendTo("body");
            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {
            var sum;
            var inputs = ["Quantity", 'Price'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (Number(sum) + Number(0));
                    } else {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


            }
        }
    </script>
</asp:Content>
