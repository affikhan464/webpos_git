﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemWiseSoldSerialNo.aspx.cs" Inherits="WebPOS.Reports.SaleReports.ItemWiseSoldSerialNo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Item Wise Sold Serial No</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                   
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Name</span>
								<input id="SearchBox"  data-id="itemTextbox" data-type="Item" data-function="GetRecords"  class="SearchBox autocomplete"  name="ItemDescription" type="text" />
							</div>
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Code</span>
                                <asp:TextBox ID="ItemCode" ClientIDMode="Static" CssClass="ItemCode"  name="ItemCode"  runat="server"></asp:TextBox>

							</div>
                          
                       
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i> Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="sixteen phCol" style="width:40%!important">
										<a href="javascript:void(0)"> 
										S. No.
										</a>
									</div>
									<div class="sixteen phCol" style="width:30%!important">
										<a href="javascript:void(0)"> 
										Code
										</a>
									</div>
                                    <div class="sixteen phCol" style="width:150%!important">
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="sixteen phCol" style="width:30%!important">                                                                    
										<a href="javascript:void(0)"> 
											Brand Name
										</a>
									</div>
									<div class="sixteen phCol" style="width:50%!important">
										<a href="javascript:void(0)"> 
											Date
										</a>
									</div>
								    <div class="sixteen phCol" style="width:70%!important">
										<a href="javascript:void(0)"> 
											Transaction
										</a>
									</div>
                                    <div class="sixteen phCol" style="width:150%!important">
										<a href="javascript:void(0)"> 
											Party Name
										</a>
									</div>
                                    <div class="sixteen phCol" style="width:50%!important">
										<a href="javascript:void(0)"> 
											Voucher No.
										</a>
									</div>
								<div class="sixteen phCol" style="width:50%!important">
										<a href="javascript:void(0)"> 
											Price
										</a>
									</div>
                                    <div class="sixteen phCol">
										<a href="javascript:void(0)"> 
											IME
										</a>
									</div>
						
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='sixteen' style='width:40%!important'><span></span></div>
								<div class='sixteen' style='width:30%!important'><span></span></div>
								<div class='sixteen' style='width:150%!important'><span></span></div>
								<div class='sixteen' style='width:30%!important'><span></span></div>
                                <div class='sixteen' style='width:50%!important'><span></span></div>
                                <div class='sixteen' style='width:70%!important'><span></span></div>
								<div class='sixteen' style='width:150%!important'><span></span></div>
								<div class='sixteen' style='width:50%!important'><span></span></div>
								<div class='sixteen' style='width:50%!important'><span></span></div>
                                <div class='sixteen text-white'><input  disabled="disabled"  name='totalQuantity'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
      </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
     <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/SerialNo/ItemWiseSoldSerialNo.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "itemCode": $(".ItemCode").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='sixteen' style='width:40%!important'><span>" + counter + "</span></div>" +
                                    "<div class='sixteen' style='width:30%!important'><span>" + response.d.ListofInvoices[i].ItemCode + "</span></div>" +
                                    "<div class='sixteen' style='width:150%!important'><span>" + response.d.ListofInvoices[i].Description + "</span></div>" +
                                    "<div class='sixteen' style='width:30%!important'><span>" + response.d.ListofInvoices[i].BrandName + "</span></div>" +
                                   "<div class='sixteen' style='width:50%!important'><span>" + response.d.ListofInvoices[i].Date + "</span></div>" +
                                    "<div class='sixteen' style='width:70%!important'><span>" + response.d.ListofInvoices[i].DepartmentName + "</span></div>" +
                                   
                                     "<div class='sixteen' style='width:150%!important'><span>" + response.d.ListofInvoices[i].PartyName + "</span></div>" +
                                      "<div class='sixteen' style='width:50%!important'><span>" + response.d.ListofInvoices[i].InvoiceNumber + "</span></div>" +
                                       "<div class='sixteen' style='width:50%!important'><span>" + response.d.ListofInvoices[i].Rate + "</span></div>" +
                                        "<div class='sixteen'>      <input value=" + response.d.ListofInvoices[i].IME + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {
                var itemRows = $(".itemsSection .itemRow");
                var sum = 0;

                itemRows.each(function (index, element) {
                    sum = index + 1;
                });
                
                $("[name=totalQuantity]").val('');
                $("[name=totalQuantity]").val('Total Sno: ' + sum);

            }
   </script>
</asp:Content>
