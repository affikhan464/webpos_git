﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class AllIME : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(int page, int pageSize, int soldType)
        {
            try
            {

                var model = GetTheReport(page, pageSize, soldType);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(int page, int pageSize, int soldType)
        {
            page = page + 1;
            SqlCommand cmd = new SqlCommand(@"
                            SELECT * FROM
                            (
                                SELECT TOP  " + pageSize + @" * FROM
                                (
                                  SELECT TOP " + (page * pageSize) + @" t.Date1,VoucherNo,PartyCode.Name as 'PartyName',InvCode.Description, IME
                                    
                                  FROM  InventorySerialNoFinal as t
                                  
                                  JOIN PartyCode on t.PartyCode=PartyCode.Code 
                                  JOIN InvCode on t.ItemCode=InvCode.Code 
                                  WHERE  
                                  t.CompID='01' 
                                  and  t.Date1 = (SELECT MAX(Date1) FROM InventorySerialNoFinal as n where t.IME=n.IME  and sold=" + soldType + @")
                                  and sold = " + soldType + @"
                                  order by IME asc 
                                   ) AS t1
                                ORDER BY t1.IME DESC
                            ) AS t2
                            ORDER BY t2.IME ASC", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToShortDateString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.IME = Convert.ToString(dt.Rows[i]["IME"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["VoucherNo"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //var itemCode = ItemCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}