﻿<%@ page title="" language="C#" masterpagefile="~/masterpage.Master" autoeventwireup="true" codebehind="DateWiseCashBookOld1.aspx.cs" inherits="WebPOS.Reports.CashBook.DateWiseCashBookOld1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">



    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2>Date Wise Cash Book Old 1</h2>
            <div class="BMSearchWrapper row align-items-end">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                <div class="col col-12 col-sm-3 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-3 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>

                <div class="col col-12 col-sm-3 col-md-3">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i>Get Record</a>
                </div>

                <div class="col col-12 col-sm-3 col-md-3">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                </div>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="reports recommendedSection allItemsView">


                <div class="itemsHeader">
                    <div class="seven phCol" style="width:20% !important" >
                        <a href="javascript:void(0)">S #
                        </a>
                    </div>
                    <div class="seven phCol" style="width:70% !important" >
                        <a href="javascript:void(0)">Description
                        </a>
                    </div>
                    <div class="seven phCol" style="width:50% !important">
                        <a href="javascript:void(0)">One
                        </a>
                    </div>
                    <div class="thirteen  phCol" style="width:300% !important">
                        <a href="javascript:void(0)">Two
                        </a>
                    </div>
                    <div class="thirteen  phCol" style="width:300% !important">
                        <a href="javascript:void(0)">Three
                        </a>
                    </div>
                    
                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow'>
                        <div class='seven' style='width:20% !important'><span></span></div>
                        <div class='seven' style='width:70% !important'><span></span></div>
                        <div class='seven' style='width:50% !important'><span></span></div>
                         <div class='seven' style='width:50% !important'><span></span></div>
                        <div class='thirteen' style='width:300% !important'><span></span></div>
                        

                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>
                   <%@ Register Src="~/Reports/Modals/_CashSaleModal.ascx" TagName="_CashSaleModal" TagPrefix="uc" %>
                    <uc:_CashSaleModal  runat="server" />
</asp:content>
<asp:content runat="server" id="content4" contentplaceholderid="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            initializeDatePicker();

            resizeTable();
        });

        $(window).resize(function () {
            resizeTable();
        });


        $("#toTxbx").on("keypress", function (e) {
            if (e.key == 13) {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                clearInvoice();
                getReport();

            }
        });

        $("#searchBtn").on("click", function (e) {

            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
            clearInvoice();
            getReport();

        });

        //Modal Start---------------------------------------------------------
        $(document).on("click", ".CashSale", function (e) {
            smallSwal('Loading...',"",'info')
            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val()
            }

            $.ajax({
                url: '/Reports/SaleReports/DayCashAndCreditSale.aspx/getReportModel',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    "<td><span>" + response.d.ListofInvoices[i].Date + "</span></td>" +
                                    "<td><span>" + response.d.ListofInvoices[i].InvoiceNumber + "</span></td>" +
                                    "<td>" + response.d.ListofInvoices[i].PartyName + "</td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Total).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].NetDiscount).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + "</td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Balance).toFixed(2) + "</td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].CurrentBalance).toFixed(2) + "</td>" +
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal").modal();
                           
                        } else if (response.d.ListofInvoices.length == 0 ) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });

        $(document).on("click", ".ReceiptfromParties", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val()
            }

            $.ajax({
                url: '/Reports/ReceiptandCapitalReports/DateWiseReceiptSummary.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    "<td><span>" + response.d.ListofInvoices[i].Code + "</span></td>" +
                                    "<td><span>" + response.d.ListofInvoices[i].PartyName + "</span></td>" +
                                    "<td>" + response.d.ListofInvoices[i].NumberOfInvoices + "</td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Receipt");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });

        $(document).on("click", ".CashExpence", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                BankCode:"12345"
            }

            $.ajax({
                url: '/Reports/PaymentExpenceDrawings/DateWiseExpenceSummary.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    
                                   
                                    "<td>" + response.d.ListofInvoices[i].Particular + "</td>" +
                                   
                                    "<td>" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Expences");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });

         $(document).on("click", ".PaidtoParties", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                
            }

            $.ajax({
               url: '/Reports/PaymentExpenceDrawings/DateWisePaymentSummary.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    
                                   
                                    "<td>" + response.d.ListofInvoices[i].PartyName + "</td>" +
                                   
                                    "<td>" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Paymentsss");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });


         $(document).on("click", ".Cashincentivereceived", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                
            }

            $.ajax({
              url: '/Reports/Incentive/DateWiseIncentiveReceivedCashSummary.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    
                                   
                                    "<td>" + response.d.ListofInvoices[i].PartyCode + "</td>" +
                                    "<td>" +  response.d.ListofInvoices[i].PartyName + "</td>" +
                                    
                                    "<td>" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Incentive Received Summary.");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });

         $(document).on("click", ".Cashincentivegiven", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                
            }

            $.ajax({
             url: '/Reports/Incentive/DateWiseIncentiveGivenCashSummary.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    "<td>" + response.d.ListofInvoices[i].PartyCode + "</td>" +
                                    "<td>" +  response.d.ListofInvoices[i].PartyName + "</td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Incentive Given Summary.");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });
        
        $(document).on("click", ".Cashpurchasereturn", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                
            }

            $.ajax({
             url: '/Reports/PurchaseReturnReports/DateWisePurchaseReturnSummary.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    "<td>" + response.d.ListofInvoices[i].Date + "</td>" +
                                    "<td>" + response.d.ListofInvoices[i].PartyName + "</td>" +
                                   
                                    "<td>" + Number(response.d.ListofInvoices[i].Total).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].NetDiscount).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Balance).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Incentive Given Summary.");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });


         $(document).on("click", ".CashSaleReturn", function (e) {
            swal('Loading...', "", 'info');
                swal.showLoading();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                
            }

            $.ajax({
             url: '/Reports/SaleReturnReports/DayCashAndCreditSaleReturn.aspx/getReportModal',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        
                        if (response.d.ListofInvoices.length > 0) {
                            var trs = "";
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                trs+="<tr>" +
                                    "<td><span>" + count + "</span></td>" +
                                    "<td>" + response.d.ListofInvoices[i].Date + "</td>" +
                                    "<td>" + response.d.ListofInvoices[i].InvoiceNumber + "</td>" +
                                    "<td>" + response.d.ListofInvoices[i].PartyName + "</td>" +
                                   
                                    "<td>" + Number(response.d.ListofInvoices[i].Total).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].NetDiscount).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + " </td>" +
                                    "<td>" + Number(response.d.ListofInvoices[i].Balance).toFixed(2) + " </td>" +
                                    
                                    "</tr>";
                            }
                            $("#DayCashSaleModal .soldItems").html(trs);
                            $("#DayCashSaleModal .head span").text("Cash Incentive Given Summary.");
                             swal.close();
                            $("#DayCashSaleModal").modal();

                        } else if (response.d.ListofInvoices.length == 0) {
                             swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });

        });

        //Modal End----------------------------------------------------------------
        
        function getReport() {
            $(".loader").show();
            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val()
            }

            $.ajax({
                url: '/Reports/CashBook/DateWiseCashBookOld1.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        if (response.d.ListofInvoices.length > 0) {

                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                var count = i + 1;
                                var SaleType = response.d.ListofInvoices[i].Description.replace(' ', "");
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven' style='width:20% !important'><span>" + count + "</span></div>" +
                                    "<div class='seven' style='width:70% !important'><a class='"+response.d.ListofInvoices[i].ClassType+"'> <span>" + response.d.ListofInvoices[i].Description + "</span></a></div>" +
                                    "<div class='seven' style='width:70% !important'><span>" + response.d.ListofInvoices[i].PartyName + "</span></div>" +


                                    "<div class='eleven '><input name='ReturnAmount'  value=" + Number(response.d.ListofInvoices[i].Paid).toFixed(2) + " disabled /></div>" +
                                    "<div class='eleven '><input name='ReturnAmount'  value=" + Number(response.d.ListofInvoices[i].OpeningBalance).toFixed(2) + " disabled /></div>" +

                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");
                            }
                            $(".loader").hide();
                            calculateData();

                        } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                            $(".loader").hide(); swal({
                                title: "No Result Found ",
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }

        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Total", 'Discount', 'Paid', 'Balance', 'CBalance'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (Number(sum) + Number(0));
                    } else {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


            }

        }
    </script>
</asp:content>
