﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports
{
    public partial class frmSaleInvoiceReport : System.Web.UI.Page
    {
      
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);

        protected void Page_Load(object sender, EventArgs e)
        {
            Module1 objModule1 = new Module1();

            if (!IsPostBack)
            {
                txtInvoiceNo.Text = Convert.ToString(objModule1.MaxInvNoVerify());
            }
            

        }

        void LoadInvoice()
        {

            SqlDataAdapter cmd = new SqlDataAdapter("Select Invoice1.Date1,Invoice1.PartyCode,Invoice1.Name,Invoice1.Particular,Invoice1.address ,Invoice1.Manual_InvNo,Invoice1.InvNo,Invoice1.SalesMan,Invoice1.Phone,Invoice3.Total as Total, Invoice3.Flat_Discount_Per as Flat_Discount_Per , Invoice3.CashPaid as CashPaid , Invoice3.FlatDiscount as FlatDiscount , Invoice3.Balance as Balance from (Invoice1 inner join invoice3 on invoice1.invno=invoice3.invno) where invoice1.CompID='" + CompID + "' And invoice1.InvNo = " + txtInvoiceNo.Text, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                txtAddress.Text = Convert.ToString(dt.Rows[0]["address"]);
                txtCustomerName.Text = Convert.ToString(dt.Rows[0]["Name"]);
                txtCustomerCode.Text = Convert.ToString(dt.Rows[0]["PartyCode"]);
                txtDate.Text = Convert.ToDateTime(dt.Rows[0]["Date1"]).ToString("dd MMM yyyy");
                txtParticular.Text = Convert.ToString(dt.Rows[0]["Particular"]);

                txtTotal.Text = Convert.ToString(dt.Rows[0]["Total"]);
                txtPaid.Text = Convert.ToString(dt.Rows[0]["CashPaid"]);

                txtPerDisRate.Text = Convert.ToString(dt.Rows[0]["Flat_Discount_Per"]) + " %";
                txtFlatDisc.Text = Convert.ToString(dt.Rows[0]["FlatDiscount"]);

            }





            SqlCommand cmd1 = new SqlCommand("Select SN0,Code,Description,Qty,Rate,Amount,Purchase_Amount,Remarks,Bonus,Item_Discount,PerDiscount,CTN,Pcs,DealRs from Invoice2 where CompID='" + CompID + "' and InvNo=" + txtInvoiceNo.Text + " order by SN0", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd1);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();





        }
        protected void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {


                LoadInvoice();
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
        }



        decimal TotQty = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal TotAmount = 0;
        decimal TotNetAmount = 0;
        decimal FlatDiscount = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables

                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                TotAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotNetAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount")) - Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[2].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotQty.ToString();
                e.Row.Cells[5].Text = TotAmount.ToString();
                e.Row.Cells[6].Text = TotItem_Discount.ToString();
                //txtItemDiscount.Text = TotItem_Discount.ToString();
                e.Row.Cells[7].Text = TotDealRs.ToString();
                //txtDealDiscount.Text = TotDealRs.ToString();
                e.Row.Cells[8].Text = TotNetAmount.ToString();
                FlatDiscount = Convert.ToDecimal(txtFlatDisc.Text);
                txtBillTotal.Text = (TotNetAmount - FlatDiscount).ToString();
                e.Row.Cells[3].HorizontalAlign = e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                txtTotal.Font.Bold = true;
                e.Row.Font.Bold = true;
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtInvoiceNo.Text != "" && Convert.ToDecimal(txtInvoiceNo.Text) > 1)
                {
                    txtInvoiceNo.Text = (Convert.ToDecimal(txtInvoiceNo.Text) - 1).ToString();
                    LoadInvoice();
                }

            }
            catch (Exception ex)
            { Response.Write("error  = " + ex.Message); }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtInvoiceNo.Text != "")
                {
                    txtInvoiceNo.Text = (Convert.ToDecimal(txtInvoiceNo.Text) + 1).ToString();
                    LoadInvoice();
                }

            }
            catch (Exception ex)
            { Response.Write("error  = " + ex.Message); }
        }

    }
}