﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="AllCategoryPurchaseReturnSummary.aspx.cs" Inherits="WebPOS.Reports.PurchaseReturnReports.AllCategoryPurchaseReturnSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="brandWiseGroupByPartySaleSummary brandWiseSaleSummary reports searchAppSection">
				<div class="contentContainer">
					<h2>All Category Purchase Return Summary</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-4">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-4">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                    <div class="col col-12 col-sm-6 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-2">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
                          
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
										Serial #
										</a>
									</div>
                                 
									<div class="thirteen phCol" >                                                                    
										<a href="javascript:void(0)"> 
											Category Name
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Quantity
										</a>
									</div>
								
									
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Amount
										</a>
									</div>
                              
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Return Units
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
									Return	Amount
											
										</a>
									</div>
								<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Net qty
											
										</a>
									</div>
								<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Net Amount
											
										</a>
									</div>
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalQuantity'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalAmount'/></div>  
                                <div class='thirteen'><input  disabled="disabled"  name='totalReturnQty'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalReturnAmount'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalNetQtySold'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalNetAmountSold'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

   <script type="text/javascript">

       var counter = 0;
       //$(".itemsSection").scroll(function () {
       //   

       //    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
       //        var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? 0 : $("#currentPage").val();
       //        currentPage = parseInt(currentPage) + 1;
       //        $("#currentPage").remove();
       //        $("<input hidden id='currentPage' value='" + currentPage + "' />").appendTo("body");
       //        getReport();
       //    }
       //});
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/PurchaseReturnReports/AllCategoryPurchaseReturnSummary.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='seven'><span>" + counter + "</span></div>" +
                                    "<div class='thirteen name'> <input name='PartyName'  id='newDescriptionTextbox_" + i + "' value='" + response.d.ListofInvoices[i].BrandName + "' disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Quantity' type='text' id='newQtyTextbox_" + i + "' value=" + parseFloat(response.d.ListofInvoices[i].Quantity).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Amount'  value=" + parseFloat(response.d.ListofInvoices[i].Amount).toFixed(2) + " disabled /></div>" +
                                      "<div class='thirteen'><input name='ReturnQty'  value=" + parseFloat(response.d.ListofInvoices[i].ReturnQty).toFixed(2) + " disabled /></div>" +
                                      "<div class='thirteen'><input name='ReturnAmount'  value=" + parseFloat(response.d.ListofInvoices[i].ReturnAmount).toFixed(2) + " disabled /></div>" +
                                      "<div class='thirteen'><input name='NetQtySold'  value=" + parseFloat(response.d.ListofInvoices[i].NetQtySold).toFixed(2) + " disabled /></div>" +
                                      "<div class='thirteen'><input name='NetAmountSold'  value=" + parseFloat(response.d.ListofInvoices[i].NetAmountSold).toFixed(2) + " disabled /></div>" +
                                 "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function print() {
                $.ajax({
                    url: '/Reports/PurchaseReturnReports/AllCategoryPurchaseReturnSummary.aspx/print',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Quantity", 'Amount', 'ReturnQty', 'ReturnAmount', 'NetQtySold', 'NetAmountSold'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (parseFloat(sum) + parseFloat(0));
                        } else {
                            sum = (parseFloat(sum) + parseFloat(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
