﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class BrandAndAllPartyPurchaseReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, brandCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string brandCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                           Select 
                PartyCode.Name as PartyName , 
                Sum(PurchaseReturn2.Qty) as Qty ,
                Sum(PurchaseReturn2.Amount) as Amount,
                          
                        (ISNULL((
                        Select
                            Sum(Purchase2.Amount)
                            from Purchase2
                            inner join Purchase1 on Purchase2.InvNo=Purchase1.InvNo 
							inner join InvCode as item on Purchase2.Code=item.Code
                            inner join PartyCode as party on Purchase1.PartyCode=party.Code 
                            where 
                            party.Code=PartyCode.Code
                            and  item.Brand='" + brandCode + @"'
							
							and Purchase1.Dat>='" + StartDate + @"' 
							and Purchase1.Dat<='" + EndDate + @"'
                            group by party.Code),0))
                        as ReturnAmount,
                       
                        (ISNULL((Select Sum(Purchase2.Qty) 
                         from Purchase2
                            inner join Purchase1 on Purchase2.InvNo=Purchase1.InvNo 
							inner join InvCode as item on Purchase2.Code=item.Code
                            inner join PartyCode as party on Purchase1.PartyCode=party.Code 
                            where 
                            party.Code=PartyCode.Code
                            and  item.Brand='" + brandCode + @"'
							
							and Purchase1.Dat>='" + StartDate + @"' 
							and Purchase1.Dat<='" + EndDate + @"'
                            group by party.Code),0))
                as ReturnQty 


                From PurchaseReturn1
                inner join PurchaseReturn2 on PurchaseReturn1.InvNo= PurchaseReturn2.InvNo
                inner join InvCode on PurchaseReturn2.Code=InvCode.Code 
                inner join PartyCode on PurchaseReturn1.PartyCode=PartyCode.Code 

                where InvCode.Brand= '" + brandCode + @"' 
                and PurchaseReturn1.CompID='" + CompID + @"' 
                and   InvCode.Nature=1 
                and PurchaseReturn1.Date1>='" + StartDate + @"' 
                and PurchaseReturn1.Date1<='" + EndDate + @"' 
                GROUP BY PartyCode.Name,PartyCode.Code                  
                order by PartyCode.Name     ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();


                var Quantity = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);

                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = "";//brandCodeTxbx.Text;
                              // Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}