﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class Brand_BrandWisePurchaseReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(brandCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String brandCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int BrandID = Convert.ToInt32(brandCode);

            SqlCommand cmd = new SqlCommand(@"SELECT 
                                                    PurchaseReturn1.InvNo,
                                                    PurchaseReturn1.Name,
                                                    PR2.Code, 
                                                    PR2.Description, 
                                                    PR2.QTY as PurReturnQty, 
                                                    PR2.amount as PurReturnAmount,
                                                    isnull(PR2.Item_Discount,0) as Item_Discount, 
                                                    isnull(PR2.DealRs,0) as DealRs
                                            FROM 
                                                    PurchaseReturn2 PR2 inner join PurchaseReturn1  on PurchaseReturn1.invno = PR2.invno
                                                    inner join invcode on PR2.code = invcode.code
                                                    inner join brandname on invcode.brand = brandname.code
                                            WHERE
                                                    PR2.date1 >= '" + StartDate + @"'  and
                                                    PR2.date1 <= '" + EndDate + @"'  and
                                                    invcode.Brand = '" + brandCode + @"'
                                            
                                            ORDER BY PR2.Description", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["PurReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["PurReturnAmount"]);


                invoice.VoucherNo = Convert.ToString(dt.Rows[i]["InvNo"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);

                invoice.ReturnQty = ReturnQty.ToString();
                invoice.Item_Disc = Convert.ToString(dt.Rows[i]["Item_Discount"]);
                invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                invoice.ReturnAmount = ReturnAmount.ToString();

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}