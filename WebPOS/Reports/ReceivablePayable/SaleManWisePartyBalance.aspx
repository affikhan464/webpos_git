﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="SaleManWisePartyBalance.aspx.cs" Inherits="WebPOS.Reports.StockReport.SaleManWisePartyBalance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Sale Man Wise Party Balance Summary</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                        <div class="col col-12 col-sm-6 col-md-2">
                    <select id="PartyGroupDD" class="dropdown">
                        <option value="" class="selectCl" onclick="manInvSelection(this)">Select Party Group</option>
                    </select>
                </div>
						   <div class="col col-12 col-sm-6 col-md-2">
							<label class="processLabel text-center active"> 
                                <input type="radio" name="PartyRadioBtn" checked="checked" value="All"/> 
                                All</label>

						   </div>
                         <div class="col col-12 col-sm-6 col-md-2">
							<label class="processLabel text-center"> 
                                <input type="radio" name="PartyRadioBtn" value="Client"/> 
                                Client</label>

						   </div>
                         <div class="col col-12 col-sm-6 col-md-2">
							<label class="processLabel text-center"> 
                                <input type="radio" name="PartyRadioBtn" value="Supplier"/> 
                                Supplier</label>

						   </div>
						
                       
						 <div class="col col-12 col-sm-6 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-2">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="five phCol" style="width:20% !important">
										<a href="javascript:void(0)"> 
										SNo.
										</a>
									</div>
                                     <div class="ten phCol" style="width:100% !important">
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    <div class="sixteen phCol" style="width:200% !important">
										<a href="javascript:void(0)"> 
											Party Name
										</a>
									</div>
									<div class="sixteen phCol" style="width:200% !important">                                                                    
										<a href="javascript:void(0)"> 
											Address
										</a>
									</div>
									<div class="ten phCol" style="width:50% !important">
										<a href="javascript:void(0)"> 
											Nature
										</a>
									</div>
								

									<div class="ten phCol">
										<a href="javascript:void(0)"> 
											Debit
										</a>
									</div>
								
									<div class="ten phCol">
										<a href="javascript:void(0)"> 
											Credit
										</a>
									</div>
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='five' style='width:20% !important'><span></span></div>
                                <div class='ten' style='width:100% !important'><span></span></div>
								<div class='sixteen' style='width:200% !important'><span></span></div>
                                <div class='sixteen' style='width:200% !important'><span></span></div>
								<div class='ten' style='width:50% !important'><span></span></div>
							    <div class='ten'><input  disabled="disabled"  name='totalDebit'/></div>
								<div class='ten'><input  disabled="disabled"  name='totalCredit'/></div>
                                
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery.min.js")%>"></script>
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery-ui.min.js")%>"></script>
    
    <script src="<%=ResolveClientUrl("/Script/Vendor/ddslick.js")%>"></script>

    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
                //getReport();
                appendAttribute("PartyGroupDD", "SaleManList");
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                  
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
               
                clearInvoice();
                getReport();
             
            });

            $(document).on("click", ".processLabel ", function () {
                $(".processLabel ").removeClass("active");
                $(this).addClass("active");
            });
            function getReport() { $(".loader").show();
                var SaleMID = $("#PartyGroupDD .dd-selected-value").val() == undefined ? "1" : $("#PartyGroupDD .dd-selected-value").val();
                $.ajax({
                    url: '/reports/ReceivablePayable/SaleManWisePartyBalance.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "PartyType": $('[name=PartyRadioBtn]:checked').val(), "SaleManID": SaleMID }),
                    success: function (response) {
                      
                        if (response.d.Success) {
                            if (response.d.PrintReport.length > 0) {
                                
                            
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='five' style='width:20% !important'><span>" + counter + "</span></div>" +
                                    "<div class='ten name' style='width:100% !important'> <input value='" + response.d.PrintReport[i].Text_1 + "' disabled /></div>" +
                                    "<div class='sixteen name' style='width:200% !important'>     <input type='text' value='" + response.d.PrintReport[i].Text_2 + "' disabled /></div>" +
                                    "<div class='sixteen name' style='width:200% !important'>     <input type='text' value='" + response.d.PrintReport[i].Text_3 + "' disabled /></div>" +
                                    "<div class='ten name' style='width:50% !important'>      <input  value='" + response.d.PrintReport[i].Text_4 + "' disabled /></div>" +
                                    "<div class='ten'>     <input name='Debit' type='text' value='" + Number(response.d.PrintReport[i].Text_5).toFixed(2) + "' disabled /></div>" +
                                    "<div class='ten'>     <input name='Credit' value='" + Number(response.d.PrintReport[i].Text_6).toFixed(2) + "' disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                                calculateData();
                                 var closingBalance = $(".itemsSection .itemRow:last").find("[name=Balance]").val();
                                $(".itemsFooter [name=closingBalance]").val(closingBalance);
                            $(".loader").hide();
                        
                            } else if (response.d.PrintReport.length == 0) {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found in the selected Date Range"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Debit", 'Credit'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }
                  
            }
   </script>
</asp:Content>
