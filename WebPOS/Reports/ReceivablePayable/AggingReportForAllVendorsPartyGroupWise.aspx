﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="AggingReportForAllVendorsPartyGroupWise.aspx.cs" Inherits="WebPOS.Reports.ReceivablePayable.AggingReportForAllVendorsPartyGroupWise" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Party Group Wise Agging Report For All Suppliers </h2>
					<div class="BMSearchWrapper row">
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="five phCol">
										<a href="javascript:void(0)"> 
										SNo.
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                     <div class="eleven phCol">
										<a href="javascript:void(0)"> 
											Supplier Name
										</a>
									</div>
									<div class="eight phCol">                                                                    
										<a href="javascript:void(0)"> 
											1 to 15
										</a>
									</div>
									<div class="eight phCol">
										<a href="javascript:void(0)"> 
											16 to 30
										</a>
									</div>
								
									<div class="eight phCol">
										<a href="javascript:void(0)"> 
											31 to 45
										</a>
									</div>
                                    <div class="eight phCol">
										<a href="javascript:void(0)"> 
											46 to 60
										</a>
									</div>
                                    <div class="eight phCol">
										<a href="javascript:void(0)"> 
											61 to 75
										</a>
									</div>
                                    <div class="eight phCol">
										<a href="javascript:void(0)"> 
											76 to 90
										</a>
									</div>
                                    <div class="eight phCol">
										<a href="javascript:void(0)"> 
											90++
										</a>
									</div>
                                    <div class="eight phCol">
										<a href="javascript:void(0)"> 
											Net Balance
										</a>
									</div>
								
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='five'><span></span></div>
								<div class='nine'><span></span></div>
								<div class='eleven'><span></span></div>
                                <div class='eight'><input  disabled="disabled"  name='total1-15'/></div>
                                <div class='eight'><input  disabled="disabled"  name='total16-30'/></div>
								<div class='eight'><input  disabled="disabled"  name='total31-45'/></div>
								<div class='eight'><input  disabled="disabled"  name='total46-60'/></div>
								<div class='eight'><input  disabled="disabled"  name='total61-75'/></div>
								<div class='eight'><input  disabled="disabled"  name='total76-90'/></div>
								<div class='eight'><input  disabled="disabled"  name='total90Upward'/></div>
								<div class='eight'><input  disabled="disabled"  name='totalNetBalance'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery.min.js")%>"></script>
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery-ui.min.js")%>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.datetimepicker.js")%>"></script>
    <script src="<%=ResolveClientUrl("/Script/InitializeDateTimePicker.js")%>"></script>
	<script src="<%=ResolveClientUrl("/Script/Vendor/ddslick.js")%>"></script>
    
    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                  
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
               
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/ReceivablePayable/AggingReportForAllVendorsPartyGroupWise.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "PGCode": $("#PartyGroupDD .dd-selected-value").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.AggingReports.length > 0) {
                                
                            
                                for (var i = 0; i < response.d.AggingReports.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                    "<div class='five'><span>" + counter + "</span></div>" +
                                    "<div class='nine name'> <input value='" + response.d.AggingReports[i].Code + "' disabled /></div>" +
                                    "<div class='eleven name'> <input value='" + response.d.AggingReports[i].Name + "' disabled /></div>" +
                                    "<div class='eight'>     <input name='1-15' type='text' value=" + Number(response.d.AggingReports[i].FirstRange) + " disabled /></div>" +
                                    "<div class='eight'>      <input name='16-30' value=" + Number(response.d.AggingReports[i].SecondRange) + " disabled /></div>" +
                                    "<div class='eight'>      <input name='31-45'  value=" + Number(response.d.AggingReports[i].ThirdRange) + " disabled /></div>" +
                                    "<div class='eight'>      <input name='46-60'  value=" + Number(response.d.AggingReports[i].FourthRange) + " disabled /></div>" +
                                    "<div class='eight'>      <input name='61-75'  value=" + Number(response.d.AggingReports[i].FifthRange) + " disabled /></div>" +
                                    "<div class='eight'>     <input name='76-90' type='text' value=" + Number(response.d.AggingReports[i].SixthRange) + " disabled /></div>" +
                                    "<div class='eight'>     <input name='90Upward' value=" + Number(response.d.AggingReports[i].SeventhRange) + " disabled /></div>" +
                                    "<div class='eight'>      <input name='NetBalance'  value=" + Number(response.d.AggingReports[i].NetBalance) + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");
                            }
                                calculateData();
                                 var closingBalance = $(".itemsSection .itemRow:last").find("[name=Balance]").val();
                                $(".itemsFooter [name=closingBalance]").val(closingBalance);
                            $(".loader").hide();
                        
                            } else if (response.d.AggingReports.length == 0) {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found in the selected Date Range"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["NetBalance", "1-15", '16-30', '31-45', '46-60', '61-75', '76-90', '90Upward'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }
                  
            }
   </script>
</asp:Content>
