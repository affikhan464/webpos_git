﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.ReceivablePayable.Prints
{
    public partial class PartyBalanceSummaryCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            ModGL objModGL = new ModGL();
            ModItem objModItem = new ModItem();
            var cr = new PartyBalanceSummary();
            var PartyType = Request.QueryString["PartyType"];
            
            var andNorBalance = string.Empty;
            
            if (PartyType == Party.Client)
            {
                andNorBalance = "and NorBalance = 1";
            }
            else if (PartyType == Party.Supplier)
            {
                andNorBalance = "and NorBalance = 2";
            }
            SqlCommand cmd = new SqlCommand(@"Select 
                Code,
                Name,
                Balance,
                Address,
                Norbalance,Phone,Mobile
                from PartyCode
                where CompID = '" + CompID + @"'
                " + andNorBalance + @"
                and  Balance <> 0
                order by name", con);

            //SqlCommand cmd = new SqlCommand(@"select Code,
            //    Name,
            //    Balance,
            //    Address,
            //    Norbalance,Phone,Mobile
            //    from PartyCode order by name", con);


            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed)
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
            }

            dAdapter.Fill(dt);
            
            con.Close();

            var dset = new DataSet();
            var CompanyName = objModGL.CompanyName();
            var CompanyAddress1 = objModGL.CompanyAddress1();
            var CompanyAddress2 = objModGL.CompanyAddress2();
            var CompanyAddress3 = objModGL.CompanyAddress3();
            var CompanyAddress4 = objModGL.CompanyAddress4();
            var CompanyPhone = objModGL.CompanyPhone();
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                decimal Debit = 0;
                decimal Credit = 0;
                string partyCode = Convert.ToString(dt.Rows[i]["Code"]);
                var partyName = dt.Rows[i]["Name"].ToString();
                var Address = dt.Rows[i]["Address"].ToString() + "/" + dt.Rows[i]["Phone"].ToString() + "/" + dt.Rows[i]["Mobile"].ToString();
                var norBalance = Convert.ToDecimal(dt.Rows[i]["Norbalance"]);
                var balance = Convert.ToDecimal(dt.Rows[i]["Balance"]);
                if (norBalance == 1 && balance > 0)
                {
                    Debit = balance;
                }
                else if (norBalance == 2 && balance < 0)
                {
                    Debit = (balance * -1);
                }
                else if (norBalance == 2 && balance > 0)
                {
                    Credit = balance;
                }
                else if (norBalance == 1 && balance < 0)
                {
                    Credit = (balance * -1);
                }


                var invoice = new
                {
                    SNo = i + 1,
                    PartyCode = partyCode.ToString(),
                    PartyName = partyName,
                    Address = Address,
                    PartyType = norBalance == 1 ? "Client" : "Supplier",
                    Debit = Convert.ToDecimal( Debit),
                    Credit =Convert.ToDecimal(Credit),

                    CompanyName = CompanyName,
                    CompanyAddress1= CompanyAddress1,
                    CompanyAddress2 = CompanyAddress2,
                    CompanyAddress3 = CompanyAddress3,
                    CompanyAddress4 = CompanyAddress4,
                    CompanyPhone= CompanyPhone,
                    Heading1="Parties Balance Summary",
                    Heading2= "All"
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);



            System.Drawing.Printing.PrintDocument localPrinter = new PrintDocument();
            cr.PrintOptions.PrinterName = localPrinter.PrinterSettings.PrinterName;
            //cr.PrintToPrinter(1, false, 0, 0);


            //var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            ////if (InstalledPrinters > 4)
            ////var a= cr.PrintOptions.PrinterName.ToString();
            ////  cr.PrintToPrinter(1, false, 0, 0);
            ////else
            //cr.PrintToPrinter(1, false, 0, 0);
            //cr.PrintOptions.PrinterName = "";
            CRViewer.ReportSource = cr;

        }
        
    }
}