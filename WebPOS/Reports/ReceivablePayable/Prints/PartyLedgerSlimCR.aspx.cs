﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.ReceivablePayable.Prints
{
    public partial class PartyLedgerSlimCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            Module2 objModule2 = new Module2();
            Module7 objModule7 = new Module7();
            ModGL objModGL = new ModGL();
            ModItem objModItem = new ModItem();
            ModPartyCodeAgainstName objModPArty = new ModPartyCodeAgainstName();
            var rpt = new PartyLedgerSlim();
            var PartyCode = Request.QueryString["partyCode"];
            var StartDate =getDate(Request.QueryString["from"]);
            var EndDate = getDate( Request.QueryString["to"]);
            var PartyName = objModGL.GLTitleAgainstCode(PartyCode);
            var Address = objModPArty.PartyAddressAgainstPartyCode(PartyCode)+ "/" + objModPArty.PartyPhoneAgainstPartyCode (PartyCode);
            decimal OpB = 0;
           
            
            Int32 NorBalance = objModule2.NormalBalanceParties(PartyCode);
            SqlCommand cmd = new SqlCommand("select OpBalance from PartyOpBalance where CompID = '" + CompID + "' and Code = '" + PartyCode + "' and Dat = '" + objModule7.StartDateTwo(StartDate) + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToDecimal(dt.Rows[0]["OpBalance"]);

            }

            decimal Dr = 0;
            decimal Cr = 0;
           
            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt1.Rows.Count > 0)
            {
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
               if (NorBalance == 2)
            {
                OpB = OpB + Cr - Dr;
            }


           
            var Date = Convert.ToString(StartDate);
            var Opening = Convert.ToString(OpB);
            
            var dset = new DataSet();
            var CompanyName = objModGL.CompanyName();
            var CompanyAddress1 = objModGL.CompanyAddress1();
            var CompanyAddress2 = objModGL.CompanyAddress2();
            var CompanyAddress3 = objModGL.CompanyAddress3();
            var CompanyAddress4 = objModGL.CompanyAddress4();
            var CompanyPhone = objModGL.CompanyPhone();
            var model = new List<Object>();
           


                var invoice = new
                {
                    
                    Date = Convert.ToDateTime(Date).ToString("dd-MMM-yyyy"),
                    StartDate = Convert.ToDateTime(StartDate).ToString("dd-MMM-yyyy"),
                    EndDate = Convert.ToDateTime(EndDate).ToString("dd-MMM-yyyy"),
                    PartyName = PartyName,
                    Address = Address,
                    Description = "Opening Balance",
                    Debit = 0,//Convert.ToDecimal(Debit),
                    Credit = 0,//Convert.ToDecimal(Credit),
                    Balance = OpB.ToString(),
                    Qty = 0,
                    Rate =0,
                    DisRs = 0,
                    DisPer = 0,
                    DealRs = 0,
                    Amount = 0,
                    CompanyName = CompanyName,
                    CompanyAddress1 = CompanyAddress1,
                    CompanyAddress2 = CompanyAddress2,
                    CompanyAddress3 = CompanyAddress3,
                    CompanyAddress4 = CompanyAddress4,
                    CompanyPhone = CompanyPhone,
                    Heading1 = "Partiy Wise Ledger Medium",
                    Heading2 = PartyName
                };
                model.Add(invoice);
            

                //---------------------------------END OF Opening Balance---------------------------------
                Decimal ClosingBalance = 0;
                Int32 j = 1;

                SqlCommand cmd2 = new SqlCommand("select dateDr, DescriptionDr, Convert(numeric(18, 2), AmountDr) as AmountDr, Convert(numeric(18, 2), AmountCr) as AmountCr, IsNull(Qty, 0) as Qty, IsNull(Rate, 0) as Rate, IsNull(Amount, 0) as Amount, DescriptionOfBillNo, S_No, BillNo, IsNull(DisRs, 0) as DisRs, IsNull(DisPer, 0) as DisPer, IsNull(DealRs, 0) as DealRs from PartiesLedger where (AmountDr>0 or AmountCr>0) and CompID = '" + CompID + "' and code = '" + PartyCode + "'  and datedr >= '" + StartDate + "' and datedr <= '" + EndDate + "' order by System_Date_Time", con);
                SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
                var dt2 = new DataTable();
                adpt2.Fill(dt2);

            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                decimal AmountDr = Convert.ToDecimal(dt2.Rows[i]["AmountDr"]);
                decimal AmountCr = Convert.ToDecimal(dt2.Rows[i]["AmountCr"]);



                    if (i == 0 && NorBalance == 1)
                {
                    ClosingBalance = (Convert.ToDecimal(Opening) + Convert.ToDecimal(AmountDr)) - Convert.ToDecimal(AmountCr);
                }
                if (i > 0 && NorBalance == 1)
                {
                    ClosingBalance = (Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(AmountDr)) - Convert.ToDecimal(AmountCr);
                }

                if (i == 0 && NorBalance == 2)
                {
                    ClosingBalance = (Convert.ToDecimal(Opening) + Convert.ToDecimal(AmountCr)) - Convert.ToDecimal(AmountDr);
                }
                if (i > 0 && NorBalance == 2)
                {
                    ClosingBalance = (Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(AmountCr)) - Convert.ToDecimal(AmountDr);
                }


                var invoice2 = new


                
                {
                    StartDate = Convert.ToDateTime(StartDate).ToString("dd-MMM-yyyy"),
                    EndDate = Convert.ToDateTime(EndDate).ToString("dd-MMM-yyyy"),
                    Address = Address,
                    Date = Convert.ToDateTime(dt2.Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"), // Convert.ToDateTime(dset.Tables[0].Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"),
                Description = Convert.ToString(dt2.Rows[i]["DescriptionDr"]),
                Qty = Convert.ToString(dt2.Rows[i]["Qty"]),
                Rate = Convert.ToString(dt2.Rows[i]["Rate"]),
                DisRs = Convert.ToString(dt2.Rows[i]["DisRs"]),
                DisPer = Convert.ToString(dt2.Rows[i]["DisPer"]),
                DealRs = Convert.ToString(dt2.Rows[i]["DealRs"]),
                Amount = Convert.ToString(dt2.Rows[i]["Amount"]),
                Debit = Convert.ToDecimal(dt2.Rows[i]["AmountDr"]),
                Credit = Convert.ToDecimal(dt2.Rows[i]["AmountCr"]),
                Balance = ClosingBalance,






                    CompanyName = CompanyName,
                    CompanyAddress1 = CompanyAddress1,
                    CompanyAddress2 = CompanyAddress2,
                    CompanyAddress3 = CompanyAddress3,
                    CompanyAddress4 = CompanyAddress4,
                    CompanyPhone = CompanyPhone,
                    Heading1 = "Partiy Wise Ledger Medium",
                    Heading2 = PartyName




                };
                model.Add(invoice2);
            }

                //----------------------------------------------------------------------------------------

                rpt.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = rpt;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }


}