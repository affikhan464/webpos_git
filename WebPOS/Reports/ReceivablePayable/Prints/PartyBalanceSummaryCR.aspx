﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartyBalanceSummaryCR.aspx.cs" Inherits="WebPOS.Reports.ReceivablePayable.Prints.PartyBalanceSummaryCR" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <CR:CrystalReportViewer ID="CRViewer" runat="server" AutoDataBind="true" HasPrintButton="True" PrintMode="ActiveX" GroupTreeStyle-ShowLines="True" />
    </div>
    </form>
</body>
</html>
