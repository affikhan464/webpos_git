﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyWiseItemReturnSummary.aspx.cs" Inherits="WebPOS.Reports.SaleReturnReports.PartyWiseItemReturnSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Party Wise Item Return Summary</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                       
                            <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div  class="col col-12 col-sm-6 col-md-3">
                             <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                            <div  class="col col-12 col-sm-6 col-md-3">
								
								<input id="PartySearchBox" data-id="PartyTextbox" data-type="Party" data-function="GetParty" class="clientInput clearable PartySearchBox autocomplete form-control" name="PartyName" type="text" placeholder="Enter a party name" />
							</div>
                            <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Party Code</span>
                              <asp:TextBox id="txtPartyCode" disabled="disabled" ClientIDMode="Static" CssClass="PartyCode"  name="PartyCode"  runat="server"></asp:TextBox>
							</div>
                        
                            
                          
							
						<div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						 <%--<asp:Button ID="Button2" Cssclass="btn btn-bm"  runat="server" Text="Print" />--%>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class=" reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Serial #
										</a>
									</div>
									<div class="thirteen phCol" style="width:300%!important">
										<a href="javascript:void(0)"> 
										Item Name
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Quantity
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Amount
										</a>
									</div>
								
									
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='thirteen' style='width:300%!important'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalQuantity'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalAmount'/></div>
                              
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
        </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
	
    <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
       //$(".itemsSection").scroll(function () {
       //   

       //    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
       //        var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? 0 : $("#currentPage").val();
       //        currentPage = Number(currentPage) + 1;
       //        $("#currentPage").remove();
       //        $("<input hidden id='currentPage' value='" + currentPage + "' />").appendTo("body");
       //        getReport();
       //    }
       //});
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });
            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/SaleReturnReports/PartyWiseItemReturnSummary.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "partyCode": $(".PartyCode").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                    "<div class='seven'><span>" + counter + "</span></div>" +
                                    "<div class='thirteen name' style='width:300%!important'> <input name='Description'  id='newDescriptionTextbox_" + i + "' value='" + response.d.ListofInvoices[i].Description + "' disabled /></div>" +
                                    "<div class='thirteen'> <input name='Quantity' type='text' id='newQtyTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Quantity).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'> <input name='Amount'  id='newGAmountTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            } $(".loader").hide();
                            calculateData();
                         
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Amount", 'Quantity'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
