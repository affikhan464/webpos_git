﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class DayCashAndCreditSaleReturn : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport( fromDate,  toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName();   
            SqlCommand cmd = new SqlCommand(@"Select SaleReturn1.Date1 as [Date],SaleReturn1.InvNo as Invoice , SaleReturn1.Name as Name,SaleReturn1.PartyCode,
                                                    SaleReturn1.Total as Total,SaleReturn1.Discount as Discount,SaleReturn1.Paid as Paid,
                                                    SaleReturn1.Total-(SaleReturn1.Discount+SaleReturn1.Paid) as Balance 
                                                    from 
                                                        SaleReturn1 
                                                    where 
                                                        SaleReturn1.CompID='" + CompID + @"' and  
                                                        SaleReturn1.Date1>='" + StartDate + @"' and 
                                                        SaleReturn1.Date1<='" + EndDate + @"'
                                                        order by SaleReturn1.InvNo ", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);


            var model = new List<InvoiceViewModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd-MMM-yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                invoice.Balance = Convert.ToString(dt.Rows[i]["Balance"]);
                invoice.CurrentBalance =Convert.ToString( objParty.PartyBalance(Convert.ToString(dt.Rows[i]["PartyCode"])));
                model.Add(invoice);
            }

            return model;



        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReportModal(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReportModal(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        public static List<InvoiceViewModel> GetTheReportModal(DateTime StartDate, DateTime EndDate)
        {
            ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName();
            SqlCommand cmd = new SqlCommand(@"Select SaleReturn1.Date1 as [Date],SaleReturn1.InvNo as Invoice , SaleReturn1.Name as Name,SaleReturn1.PartyCode,
                                                    SaleReturn1.Total as Total,SaleReturn1.Discount as Discount,SaleReturn1.Paid as Paid,
                                                    SaleReturn1.Total-(SaleReturn1.Discount+SaleReturn1.Paid) as Balance 
                                                    from 
                                                        SaleReturn1 
                                                    where 
                                                        SaleReturn1.Paid>0 and
                                                        SaleReturn1.CompID='" + CompID + @"' and  
                                                        SaleReturn1.Date1>='" + StartDate + @"' and 
                                                        SaleReturn1.Date1<='" + EndDate + @"'
                                                        order by SaleReturn1.InvNo ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            decimal TotalPaid = 0;
            decimal TotalTotal = 0;
            decimal TotalDiscount = 0;
            decimal TotalBalance = 0;
            var model = new List<InvoiceViewModel>();

            var invoiceHeading = new InvoiceViewModel();
            invoiceHeading.PartyName = "Party Name";
            invoiceHeading.Total ="Total";
            invoiceHeading.NetDiscount = "NetDiscount";
            invoiceHeading.Paid = "CashBook Paid";
            invoiceHeading.Balance = "Balance";
            model.Add(invoiceHeading);



            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd-MMM-yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                invoice.Balance = Convert.ToString(dt.Rows[i]["Balance"]);
                invoice.CurrentBalance = Convert.ToString(objParty.PartyBalance(Convert.ToString(dt.Rows[i]["PartyCode"])));

                TotalTotal += Convert.ToDecimal(invoice.Total);
                TotalDiscount += Convert.ToDecimal(invoice.NetDiscount);
                TotalPaid += Convert.ToDecimal(invoice.Paid);
                TotalBalance += Convert.ToDecimal(invoice.Balance);
                model.Add(invoice);
            }
            var invoiceTotal = new InvoiceViewModel();
            invoiceTotal.PartyName = "Tota";
            invoiceTotal.Total = Convert.ToString(TotalTotal);
            invoiceTotal.NetDiscount = Convert.ToString(TotalDiscount);
            invoiceTotal.Paid = Convert.ToString(TotalPaid);
            invoiceTotal.Balance = Convert.ToString(TotalBalance);
            model.Add(invoiceTotal);



            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}