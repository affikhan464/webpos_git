﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class ClassWiseSaleReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string ClassID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(ClassID, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String ClasID, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int ClassID = Convert.ToInt32(ClasID);
            

            SqlCommand cmd = new SqlCommand(@"SELECT SaleReturn2.Code, SaleReturn2.Description, sum(SaleReturn2.QTY) as SaleReturnQty , sum(SaleReturn2.amount) as SaleReturnAmount,sum(SaleReturn2.ItemDis) as ItemDis , sum(SaleReturn2.DealRs) as DealRs 
                                            FROM SaleReturn2  inner join SaleReturn1  on SaleReturn1.invno = SaleReturn2.invno
                                                inner join invcode on SaleReturn2.code = invcode.code
                                                 inner join Class on invcode.Class = Class.ID

                                                        WHERE
                                                            SaleReturn2.date1 >= '" + StartDate + @"'  and
                                                            SaleReturn2.date1 <= '" + EndDate + @"'  and
                                                            invcode.Class = '" + ClassID + @"' 
                                                            
                                                             GROUP BY SaleReturn2.Code, SaleReturn2.Description 
                                                            ORDER BY SaleReturn2.Description ", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

               
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
               

                invoice.BrandName = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.Item_Disc = Convert.ToString(dt.Rows[i]["ItemDis"]);
                invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                invoice.ReturnAmount = ReturnAmount.ToString();
               

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}