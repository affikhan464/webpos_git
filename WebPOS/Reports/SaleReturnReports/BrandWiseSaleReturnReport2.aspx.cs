﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class BrandWiseSaleReturnReport2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(brandCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String brandCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int BrandID = Convert.ToInt32(brandCode);
            SqlCommand cmd = new SqlCommand(@"Select	  SaleReturn1.Name ,
	         SaleReturn2.Description as ItemName,
             Sum(SaleReturn2.Amount) as Amount,
             Sum(SaleReturn2.Qty) as Qty,
				      
                        (ISNULL((Select Sum(Invoice2.Amount)
                        from Invoice2
                        inner join InvCode as item on Invoice2.Code=item.Code 
                        inner join Invoice1 on Invoice2.InvNo=Invoice1.InvNo 
                        where item.Brand='" + BrandID + @"'
                        
	                    and Invoice1.Date1>='" + StartDate + @"' 
						and Invoice1.Date1<='" + EndDate + @"'
                        group by item.Brand),0))
                as ReturnAmount,
                       
                        (ISNULL((Select Sum(Invoice2.Qty) from Invoice2 
                        inner join InvCode as item on Invoice2.Code=item.Code 
                        inner join Invoice1 on Invoice2.InvNo=Invoice1.InvNo                         
                        where item.Brand='" + BrandID + @"'   
                       
	                    and Invoice1.Date1>='" + StartDate + @"' 
						and Invoice1.Date1<='" + EndDate + @"'
                        group by item.Brand),0))
                as ReturnQty

             FROM SaleReturn2       as SaleReturn2   
             INNER JOIN SaleReturn1 as SaleReturn1 ON SaleReturn2.InvNo = SaleReturn1.InvNo   
            inner join InvCode on SaleReturn2.Code=InvCode.Code   
             where SaleReturn1.CompID = '" + CompID + "'   and InvCode.Brand='" + brandCode + "' and SaleReturn1.Date1 >= '" + StartDate + "'   and SaleReturn1.Date1 <= '" + EndDate + "' GROUP BY  SaleReturn1.Name, SaleReturn2.Description order by SaleReturn1.Name", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();



                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["ItemName"]);


                var Quantity = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);

                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}