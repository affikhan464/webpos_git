﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class DaySaleReturnGroupByItem : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            
            SqlCommand cmd = new SqlCommand(@"SELECT  
                SaleReturn2.Code as Code , 
                SaleReturn2.Description as Description , 
                Sum(SaleReturn2.Qty) as Qty1 ,
                Sum(SaleReturn2.Amount) as Amount , 
                Sum(SaleReturn2.DealRs) as DealRs , 
                sum(SaleReturn2.ItemDis) as Item_Discount1 
                FROM 
                    SaleReturn2 
                where 
                    SaleReturn2.CompID='" + CompID + @"' and   
                    SaleReturn2.Date1>='" + StartDate + @"' and
                    SaleReturn2.Date1<='" + EndDate + @"'                 
                    GROUP BY SaleReturn2.Code, SaleReturn2.Description
                    order by SaleReturn2.Description     ", con);
            
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode    = (dt.Rows[i]["Code"]).ToString();
                invoice.Description   = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity     = Convert.ToString(dt.Rows[i]["Qty1"]);
                invoice.NetDiscount     = Convert.ToString(dt.Rows[i]["Item_Discount1"]);
                invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                invoice.Amount        = Convert.ToString(dt.Rows[i]["Amount"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItemCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}