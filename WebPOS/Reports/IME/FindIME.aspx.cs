﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.IME
{
    public partial class FindIME : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string IME)
        {
            try
            {
                
                var model = GetTheReport(IME);
                return new BaseModel { Success = true, ListofIME  = model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<ItemModel> GetTheReport(string IME)
        {

            //If OptLft.Value = True Then str = "Select Date1 , Description  ,  PartyCode  , VoucherNo , Price_Cost,ItemCode,IME,isnull(DisRs,0) as DisRs,isnull(DisPer,0) as DisPer,isnull(DealRs,0) as DealRs from InventorySerialNoFinal  where CompID='" & logon.CompID & "' and  IME Like '" & IME & "&' order by System_Date_Time"
            //If OptAnyWhere.Value = True Then str = "Select Date1 , Description  ,  PartyCode  , VoucherNo , Price_Cost,ItemCode,IME,isnull(DisRs,0) as DisRs,isnull(DisPer,0) as DisPer,isnull(DealRs,0) as DealRs from InventorySerialNoFinal  where  CompID='" & logon.CompID & "' and IME Like '%" & IME & "%' order by System_Date_Time"
            //If OptRight.Value = True Then str = "Select Date1 , Description  ,  PartyCode  , VoucherNo , Price_Cost,ItemCode,IME,isnull(DisRs,0) as DisRs,isnull(DisPer,0) as DisPer,isnull(DealRs,0) as DealRs from InventorySerialNoFinal  where  CompID='" & logon.CompID & "' and IME Like '%" & IME & "' order by System_Date_Time"
            //If OptExectMatch.Value = True Then str = "Select Date1 , Description  ,  PartyCode  , VoucherNo , Price_Cost,ItemCode,IME,isnull(DisRs,0) as DisRs,isnull(DisPer,0) as DisPer,isnull(DealRs,0) as DealRs from InventorySerialNoFinal  where  CompID='" & logon.CompID & "' and IME ='" & IME & "' order by System_Date_Time"


            SqlCommand cmd = new SqlCommand("Select Date1 , Description  ,  PartyCode  , VoucherNo , Price_Cost,ItemCode,IME,isnull(DisRs,0) as DisRs,isnull(DisPer,0) as DisPer,isnull(DealRs,0) as DealRs from InventorySerialNoFinal  where CompID='" + CompID + "' and  IME Like '%" + IME + "%' order by System_Date_Time", con);
               SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<ItemModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var IMIE = new ItemModel();

                IMIE.Code = Convert.ToString(dt.Rows[i]["ItemCode"]);
                IMIE.Description = Convert.ToString(dt.Rows[i]["Description"]);
                //IMIE.Brand = Convert.ToString(dt.Rows[i]["Payable"]);
                //IMIE.qty = Convert.ToString(dt.Rows[i]["Discount"]);
                //IMIE.Date = Convert.ToString(dt.Rows[i]["CashPaid"]);
                //IMIE.Color = Convert.ToString(dt.Rows[i]["Balance"]);
                //IMIE.PartyName = Convert.ToString(dt.Rows[i]["Balance"]);
                //IMIE.BillNo = Convert.ToString(dt.Rows[i]["Balance"]);
                //IMIE.IME = Convert.ToString(dt.Rows[i]["Balance"]);
                //IMIE.Price = Convert.ToString(dt.Rows[i]["Balance"]);
                //IMIE.Price = Convert.ToString(dt.Rows[i]["Balance"]);

                model.Add(IMIE);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
           // var partyCode = PartyCode.Text;
           // Response.Redirect("~/Reports/SaleReports/CReports/PartyWiseItemSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode);
        }
    }
}