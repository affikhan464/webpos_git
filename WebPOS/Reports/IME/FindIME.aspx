﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="FindIME.aspx.cs" Inherits="WebPOS.Reports.IME.FindIME" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Find IME</h2>
					<div class="BMSearchWrapper row">
						
                            <div  class="col col-12 col-sm-6 col-md-1">
                                    <Label">Party Code</label>
                            </div>
                            <div  class="col col-12 col-sm-6 col-md-3">
								
								<input  data-id="IME"   class="IME clientInput clearable PartySearchBox autocomplete form-control"  type="text" placeholder="Enter a party name" />
							</div>
                            
						<div class="col col-12 col-sm-6 col-md-3">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-3">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						 
					    </div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Date
										</a>
									</div>
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
										Bill #
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Payable
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Discount
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Paid
											
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Balance
										</a>
									</div>
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='seven'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalPayable'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalNetDiscount'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalPaid'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalBalance'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
        </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
       
            $(document).ready(function () {
                
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });
            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/IME/FindIME.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "IME": $(".IME").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofIME.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofIME.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                    "<div class='seven'><span>" + response.d.ListofIME[i].Code + "</span></div>" +
                                    "<div class='seven'><span>" + response.d.ListofIME[i].Description + "</span></div>" +
                                    
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.ListofIME.length == 0 && currentPage == "0") {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Payable", 'NetDiscount', 'Paid', 'Balance' ];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
