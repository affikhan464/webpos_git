﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="TrialBalance.aspx.cs" Inherits="WebPOS.Reports.TrialBalance.TrialBalance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Trial Balance</h2>
					<div class="BMSearchWrapper row">
                         <div class="col col-12 col-sm-6 col-md-1">
						    <label class="processLabel text-center active"> 
                                <input type="radio" name="Lvl"  value="0"/> 
                                Level-0</label>
						   </div>
                         <div class="col col-12 col-sm-6 col-md-1">
							<label class="processLabel text-center"> 
                                <input type="radio" name="Lvl" value="1"/> 
                                Level-1</label>
						   </div>
                         <div class="col col-12 col-sm-6 col-md-1">
							<label class="processLabel text-center"> 
                                <input type="radio" name="Lvl" value="2"/> 
                                Level-2</label>
						   </div>
                           <div class="col col-12 col-sm-6 col-md-1">
							<label class="processLabel text-center"> 
                                <input type="radio" name="Lvl" value="3"/> 
                                Level-3</label>
						   </div>
                        <div class="col col-12 col-sm-6 col-md-1">
							<label class="processLabel text-center"> 
                                <input type="radio" name="Lvl" checked="checked" value="4"/> 
                                Level-4</label>
						   </div>
                       
						 <div class="col col-12 col-sm-6 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i> Get Report</a>
						  </div>
						
                        <div class="col col-12 col-sm-6 col-md-2">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						 
					    </div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	</div>
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Serial No.
										</a>
									</div>
									<div class="five phCol">
										<a href="javascript:void(0)"> 
										Code
										</a>
									</div>
									<div class="eleven  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
									<div class="eleven  phCol">
										<a href="javascript:void(0)"> 
											Debit
										</a>
									</div>
								
									<div class="eleven  phCol">
										<a href="javascript:void(0)"> 
										Credit
											
										</a>
									</div>
									                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='five'><span></span></div>
								<div class='eleven '><span></span></div>
								<div class='eleven '><input  disabled="disabled"  name='totalDr'/></div>
								<div class='eleven '><input  disabled="disabled"  name='totalCr'/></div>
							    
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
 </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
   <script type="text/javascript">

       var counter = 0;
       //$(".itemsSection").scroll(function () {
       //   

       //    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
       //        var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? 0 : $("#currentPage").val();
       //        currentPage = Number(currentPage) + 1;
       //        $("#currentPage").remove();
       //        $("<input hidden id='currentPage' value='" + currentPage + "' />").appendTo("body");
       //        getReport();
       //    }
       //});
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

      

           
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/TrialBalance/TrialBalance.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    //data: JSON.stringify({ "from": "", "to": "" }),
                    data: JSON.stringify({ "TBType": $('[name=Lvl]:checked').val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                     "<div class='seven'><span>" + counter + "</span></div>" +
                                    "<div class='five'><span>" + response.d.ListofInvoices[i].Code + "</span></div>" +
                                    "<div class='eleven  name'><input  id='newDescriptionTextbox_" + i + "' value='" + response.d.ListofInvoices[i].GLTitle + "' disabled /></div>" +
                                    "<div class='eleven '><input name='Dr' type='text' id='newQtyTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Debit).toFixed(2) + " disabled /></div>" +
                                    "<div class='eleven '><input name='Cr'  id='newGAmountTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Credit).toFixed(2) + " disabled /></div>" +

                                      
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Dr", 'Cr', ];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

       }
       $(document).on('click', '#print', function () {
          
            window.open(
                '/Reports/SaleReports/CReports/AllBrandSaleSummaryCR.aspx?startDate='+ $(".fromTxbx").val() +'&endDate=' + $(".toTxbx").val(),
                '_blank'
            );
        });
   </script>
</asp:Content>
