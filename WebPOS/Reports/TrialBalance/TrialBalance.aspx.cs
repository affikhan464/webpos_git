﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.TrialBalance
{
    public partial class TrialBalance : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string TBType)
        {
            try
            {
                //var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                //                      System.Globalization.CultureInfo.InvariantCulture);
                //var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                //                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(TBType);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(string TBType)
        {
            var objModGLCode = new ModGLCode();
            SqlCommand cmd = new SqlCommand(@"Select Code,Title,NorBalance,Balance from GLCode where compID='" + CompID + "' and  lvl=5 order by code", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();
            decimal Dr = 0;
            decimal Cr = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Dr = 0;
                Cr = 0;
                Int32 NorBalance= Convert.ToInt32(dt.Rows[i]["NorBalance"]);
                string GLCode = Convert.ToString(dt.Rows[i]["Code"]);
                decimal CloBal = objModGLCode.GLClosingBalance(GLCode, NorBalance, DateTime.Today, DateTime.Today);
                if (CloBal!=0) {
                    var invoice = new InvoiceViewModel();

                    invoice.Code = Convert.ToString(dt.Rows[i]["Code"]);
                    invoice.GLTitle = Convert.ToString(dt.Rows[i]["Title"]);


                    if (NorBalance == 1 && CloBal>0)
                    {
                        Dr = CloBal;
                    }
                    else
                        if (NorBalance == 1 && CloBal<0)
                    {
                        Cr = CloBal;
                    }
                    else
                    if (NorBalance == 2 && CloBal > 0)
                    {
                        Cr = CloBal;
                    }
                    else
                        if (NorBalance == 2 && CloBal < 0)
                    {
                        Dr = CloBal;
                    }


                    invoice.Debit = Convert.ToString(Math.Abs(Dr));
                    invoice.Credit = Convert.ToString(Math.Abs(Cr));



                    model.Add(invoice);
                }
            }

            return model;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //var itemCode = itemCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleGroupByPartyCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}