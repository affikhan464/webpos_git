﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class StockAtReorderPointBrand : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string brandId)
        {
            try
            {

                var model = GetTheReport(brandId);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string brandId)
        {
            ModItem objModItem = new ModItem();
            SqlCommand cmd = new SqlCommand("Select InvCode.Code,Description,qty,ReorderLevel,ReorderQty,isnull(Ave_Cost,0) as Ave_Cost,BrandName.Name as Brand from invCode inner join BrandName on BrandName.Code=InvCode.Brand where  InvCode.CompID='" + CompID + "' and  InvCode.Brand=" + brandId + "  and qty<ReorderLevel and Visiable=1 order by description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintRow = new PrintingTable();
                var InHandQty = Convert.ToInt32(dt.Rows[i]["qty"]);
                var ReorderQty = Convert.ToInt32(dt.Rows[i]["ReorderQty"]);
                var ReorderLevel = Convert.ToInt32(dt.Rows[i]["ReorderLevel"]);
                var requiredQty = (ReorderLevel - InHandQty) + ReorderQty;

                var unitPrice = dt.Rows[i]["Ave_Cost"] != null ? Convert.ToInt32(dt.Rows[i]["Ave_Cost"]) : 0;
                var amountRequired = unitPrice * requiredQty;

                PrintRow.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                PrintRow.ItemName = Convert.ToString(dt.Rows[i]["Description"]);
                PrintRow.ReorderLevel = ReorderLevel.ToString();
                PrintRow.InHandQty = InHandQty.ToString();
                PrintRow.ShortQty = Convert.ToString(ReorderLevel - InHandQty);
                PrintRow.ReOrderQty = ReorderQty.ToString();
                PrintRow.RequiredQty = requiredQty.ToString(); // 
                PrintRow.LastPurchasePrice = Convert.ToString(objModItem.LastPurchasePrice(PrintRow.ItemCode));
                PrintRow.AmountRequired = Convert.ToString(Convert.ToDecimal(PrintRow.LastPurchasePrice) * Convert.ToDecimal(PrintRow.RequiredQty));
                PrintRow.BrandNAme = Convert.ToString(dt.Rows[i]["Brand"]);
                model.Add(PrintRow);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}