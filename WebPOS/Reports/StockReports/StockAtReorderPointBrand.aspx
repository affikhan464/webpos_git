﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StockAtReorderPointBrand.aspx.cs" Inherits="WebPOS.Reports.SaleReports.StockAtReorderPointBrand" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Stock At Reorder Point, Brand Wise</h2>
					<div class="BMSearchWrapper clearfix row">
                       <div class="DDWrapper col col-12 col-sm-6 col-md-6">
                            <select id="BrandNameDD" class="dropdown">
                                <option value="0">Select Brand</option>
                            </select>
                        </div>
						<div class="col col-12 col-sm-3 col-md-3">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-3">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						 <%--<asp:Button ID="Button2" Cssclass="btn btn-bm"  runat="server" Text="Print" />--%>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol" style="width:30% !important">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="seven phCol" style="width:40% !important">
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                   <div class="seven phCol" style="width:80% !important">
										<a href="javascript:void(0)"> 
											Brand
										</a>
									</div>
									<div class="fourteen  phCol" style="width:300% !important">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="fourteen phCol">
										<a href="javascript:void(0)"> 
											R. O. Point
										</a>
									</div>
									<div class="fourteen phCol">
										<a href="javascript:void(0)"> 
											In Hand Qty
											
										</a>
									</div>
                                    <div class="fourteen phCol">
										<a href="javascript:void(0)"> 
											Short Qty
											
										</a>
									</div>
                                    <div class="fourteen phCol">
										<a href="javascript:void(0)"> 
										Set Order Qty 
											
										</a>
									</div>
									<div class="fourteen phCol">
										<a href="javascript:void(0)"> 
										Qty Required
											
										</a>
									</div>
                                    <div class="fourteen phCol">
										<a href="javascript:void(0)"> 
										Last Purchased
											
										</a>
									</div>
                                     <div class="fourteen phCol">
										<a href="javascript:void(0)"> 
										Amount Required
											
										</a>
									</div>
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven' style="width:30% !important"><span></span></div>
								<div class='seven' style="width:40% !important"><span></span></div>
                                <div class='seven' style="width:80% !important"><span></span></div>
								<div class='fourteen' style="width:300% !important"><span></span></div>
								<div class='fourteen'><input  disabled="disabled"  name='totalQuantity'/></div> 
								<div class='fourteen'><input  disabled="disabled"  name='totalInHand'/></div>
                                <div class='fourteen'><span></span></div>
                                <div class='fourteen'><span></span></div>
								<div class='fourteen'><input  disabled="disabled"  name='totalReorderQty'/></div>
								<div class='fourteen'><span></span></div>
								<div class='fourteen'><input  disabled="disabled"  name='totalAmount'/></div>
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	 </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
	<script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>"></script>
   <script type="text/javascript">

       $(document).ready(function () {
           appendAttribute("BrandNameDD", "BrandName");
           setTimeout(function() {
               //getReport();
           },1000) 
                resizeTable();

                //appendAttribute takes two value one is the DropdownId and Table name from which data is collected.
             
            });

            $(window).resize(function () {
                resizeTable();
              
            });

          
            

            $("#searchBtn").on("click", function (e) {
                
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                    clearInvoice();
                    getReport();
             
            });
            function getReport() { $(".loader").show();

                var currentPage=$("#currentPage").val()=="" || $("#currentPage").val()==undefined ?"0":$("#currentPage").val();
                $(".loader").show();
                $.ajax({
                    url: '/Reports/StockReports/StockAtReorderPointBrand.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "brandId": $("#BrandNameDD .dd-selected-value").val() }),
                   
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.PrintReport.length > 0) {
                               
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                var count = i + 1;
                                $("<div class='itemRow newRow'>" +
                                     "<div class='seven' style='width:30% !important'><span>" + count + "</span></div>" +
                                    "<div class='seven' style='width:40% !important'><span>" + response.d.PrintReport[i].ItemCode + "</span></div>" +
                                    "<div class='fourteen  name' style='width:80% !important' ><span class='font-size-12' >" + response.d.PrintReport[i].BrandNAme + "</span></div>" +
                                    "<div class='fourteen name' style='width:300% !important' ><span class='font-size-12' >" + response.d.PrintReport[i].ItemName + "</span></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].ReorderLevel).toFixed(2) + " name='Quantity' disabled /></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].InHandQty).toFixed(2) + " name='ReorderLevel' disabled /></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].ShortQty).toFixed(2) + " name='ReorderQty' disabled /></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].ReOrderQty).toFixed(2) + " disabled /></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].RequiredQty).toFixed(2) + "  name='Amount' disabled /></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].LastPurchasePrice).toFixed(2) + "  name='Amount' disabled /></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].AmountRequired).toFixed(2) + "  name='Amount' disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            $(".loader").hide();
                            calculateData();
                            appendTooltip();
                        
                            } else if (response.d.PrintReport.length == 0 && currentPage == "0") {
                               
                                $(".loader").hide();
                                swal({
                                    title: "No Data Found ",
                                    //text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ['ReorderQty', 'Quantity', 'Amount'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
