﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.StockReports.Prints
{
    public partial class AllStockCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            ModGL objModGL = new ModGL();
            ModItem objModItem = new ModItem();
            var cr = new BrandWiseStockSummery();
            var brandId = Request.QueryString["brandId"];
           
            string  BrandName = objModItem.BrandNameAgainstBrandCode(Convert.ToInt32(brandId));
            SqlCommand cmd = new SqlCommand(@"select BrandName.Name as BrandName, InvCode.Code as ItemCode,Description,qty, isnull(InvCode.Ave_Cost,0) as UnitPrice,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Value from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where  InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by  Description", con);

            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed)
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
            }

            dAdapter.Fill(dt);
            con.Close();

            var dset = new DataSet();
            var CompanyName = objModGL.CompanyName();
            var CompanyAddress1 = objModGL.CompanyAddress1();
            var CompanyAddress2 = objModGL.CompanyAddress2();
            var CompanyAddress3 = objModGL.CompanyAddress3();
            var CompanyAddress4 = objModGL.CompanyAddress4();
            var CompanyPhone = objModGL.CompanyPhone();
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
               var Quantity = Convert.ToString(dt.Rows[i]["qty"]);
               var Rate = Convert.ToString(dt.Rows[i]["UnitPrice"]);
                var invoice = new
                {
                    SNo = i + 1,
                    ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]),
                    BrandName = Convert.ToString(dt.Rows[i]["BrandName"]),
                    Description = Convert.ToString(dt.Rows[i]["Description"]),
                    Quantity = Convert.ToDecimal(Quantity),
                    Rate = Convert.ToDecimal(Rate),
                    Amount = Math.Round(Convert.ToDecimal(Convert.ToString(dt.Rows[i]["Value"])), 2),

                   


                CompanyName = CompanyName,
                    CompanyAddress1= CompanyAddress1,
                    CompanyAddress2 = CompanyAddress2,
                    CompanyAddress3 = CompanyAddress3,
                    CompanyAddress4 = CompanyAddress4,
                    CompanyPhone= CompanyPhone,
                    Heading1="All Stock",
                    Heading2= ""
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }
        
    }
}