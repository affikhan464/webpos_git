﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="TotalStockWithOutAmount.aspx.cs" Inherits="WebPOS.Reports.SaleReports.TotalStockWithOutAmount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="dayCashAndCreditSale searchAppSection">
				<div class="contentContainer">
					<h2>Total Stock With Out Amount</h2>
					<div class="BMSearchWrapper clearfix row">
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>

					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol" style="width:20% !important">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="seven phCol" style="width:30% !important">
										<a href="javascript:void(0)"> 
											Item Code
										</a>
									</div>
                                    <div class="thirteen phCol" style="width:60% !important">
										<a href="javascript:void(0)"> 
											Brand Name
										</a>
									</div>
									<div class="thirteen  phCol" style="width:350% !important">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven' style='width:20% !important'><span></span></div>
								<div class='seven' style='width:30% !important'><span></span></div>
								<div class='thirteen' style='width:60% !important'><span></span></div>
								<div class='thirteen' style='width:350% !important'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalQuantity'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	  
   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

   <script type="text/javascript">

       
            $(document).ready(function () {
                getReport();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

         

            $("#searchBtn").on("click", function (e) {
                
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                    clearInvoice();
                    getReport();
             
            });
            function getReport() { $(".loader").show();

                var currentPage=$("#currentPage").val()=="" || $("#currentPage").val()==undefined ?"0":$("#currentPage").val();
                $(".loader").show();
                $.ajax({
                    url: '/Reports/StockReports/TotalStockWithOutAmount.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                   
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.Reports.length > 0) {
                            
                                for (var i = 0; i < response.d.Reports.length; i++) {
                                var count = i + 1;
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven' name='sno' style='width:20% !important'><span>" + count + "</span></div>" +
                                    "<div class='seven' style='width:30% !important'><span>" + response.d.Reports[i].ItemCode + "</span></div>" +
                                    "<div class='thirteen name' style='width:60% !important'><input value='" + response.d.Reports[i].BrandName + "' disabled /></div>" +
                                    "<div class='thirteen name' style='width:350% !important' title='" + response.d.Reports[i].Description + "'><span class='font-size-12' >" + response.d.Reports[i].Description + "</span></div>" +
                                    "<div class='thirteen'><input name='Quantity' type='text' id='newQuantityTextbox_" + i + "' value=" + Number(response.d.Reports[i].Quantity).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            $(".loader").hide();
                            appendTooltip();
                            calculateData();
                        
                            } else if (response.d.Reports.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Quantity"];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

       }
        
       //For CTRL+P
        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printThisTransaction();
            }
        });
        function printThisTransaction() {
             window.open(
                 '/Reports/StockReports/Prints/StockWithOutAmountCR.aspx',
                "DescriptiveWindowName",
                "resizable,scrollbars,status"
            );
        }
        //For Print Button
        //$(document).on('click', '#printInvoice', function () {
        //    printThisTransaction();
        //})



       $(document).on('click', '#print', function () {
          
        printThisTransaction();
            //window.open(
            //    '/Reports/StockReports/Prints/StockWithOutAmountCR.aspx',
            //    '_blank'
            //);
        });
   </script>
</asp:Content>
