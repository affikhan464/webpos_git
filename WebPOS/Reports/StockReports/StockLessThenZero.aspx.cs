﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebPOS.Model;
using System.Web.Services;
using System.Web.Script.Services;

namespace WebPOS.Reports.StockReports
{
    public partial class StockLessThenZero : System.Web.UI.Page
    {
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport()
        {
            try
            {

                var model = GetTheReport();
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport()
        {

            SqlCommand cmd = new SqlCommand("select InvCode.Code,InvCode.Description,InvCode.qty,BrandName.Name as BrandName from (InvCode inner join BrandName on BrandName.Code=InvCode.Brand)  where  InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty<0 order by InvCode.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintRow = new PrintingTable();
                PrintRow.BrandNAme = Convert.ToString(dt.Rows[i]["BrandName"]);
                PrintRow.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                PrintRow.ItemName = Convert.ToString(dt.Rows[i]["Description"]);
                PrintRow.InHandQty = Convert.ToString(dt.Rows[i]["qty"]);
                model.Add(PrintRow);
            }

            return model;



        }




    }
}