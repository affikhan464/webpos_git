﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Reports.SaleReports
{
    public partial class frmDaySaleGroupByItem : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadStationList();
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }

           
        }


        void LoadStationList()
        {

            SqlCommand cmd = new SqlCommand("select Name from Station where CompID='01' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstStation.DataSource = dset;
            lstStation.DataBind();
            lstStation.DataTextField = "Name";
            lstStation.DataBind();

        }

        void DaySaleGroupByItemCashCreditSale(DateTime StartDate, DateTime EndDate)
        {




            if (chkStation.Checked == false)
            {
                SqlCommand cmd = new SqlCommand("SELECT  Invoice2.Code as Code , Invoice2.Description as Description , Sum(Invoice2.Qty) as Qty1 ,Sum(Invoice2.Amount) as Amount , Sum(DealRs) as DealRs , sum(Item_Discount) as Item_Discount1 FROM (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice1.CompID='" + CompID + "' and   Invoice2.ItemNature = 1 and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Code,Invoice2.Description order by Invoice2.Description", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            else
                if(chkStation.Checked==true)
                {
                    int StationID = 1;
                    ModViewInventory objModViewInventory = new ModViewInventory();
                    

                    SqlCommand cmd = new SqlCommand("SELECT  Invoice2.Code as Code , Invoice2.Description as Description , Sum(Invoice2.Qty) as Qty1 ,Sum(Invoice2.Amount) as Amount , Sum(DealRs) as DealRs , sum(Item_Discount) as Item_Discount1 FROM (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice1.CompID='" + CompID + "' and Invoice1.StationID=" + StationID + " and Invoice2.ItemNature = 1 and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Code,Invoice2.Description order by Invoice2.Description", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);

                    GridView1.DataSource = dset;
                    GridView1.DataBind();
                }

        }






        protected void Button1_Click(object sender, EventArgs e)
        {
            
            DaySaleGroupByItemCashCreditSale(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
        }


        decimal TotQty = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal TotAmount = 0;
        decimal TotNetAmount = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty1"));
                TotAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount1"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                
                TotNetAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount")) - (Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount1")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs")));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotQty.ToString();
                e.Row.Cells[4].Text = TotAmount.ToString();
                e.Row.Cells[5].Text = TotItem_Discount.ToString();
                e.Row.Cells[6].Text = TotDealRs.ToString();
                e.Row.Cells[7].Text = TotNetAmount.ToString();


                e.Row.Cells[3].HorizontalAlign = e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        
    }
}