﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class AllStock : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport()
        {
            try
            {

                var model = GetTheReport();
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport()
        {

            SqlCommand cmd = new SqlCommand("select BrandName.Name as BrandName, InvCode.Code as ItemCode,Description,qty, isnull(InvCode.Ave_Cost,0) as 'UnitPrice',(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Value from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where  InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrinTab = new PrintingTable();

                PrinTab.Text_1 = Convert.ToString(dt.Rows[i]["ItemCode"]);
                PrinTab.Text_2 = Convert.ToString(dt.Rows[i]["BrandName"]);
                PrinTab.Text_3 = Convert.ToString(dt.Rows[i]["Description"]);
                PrinTab.Text_4 = Convert.ToString(dt.Rows[i]["qty"]);
                PrinTab.Text_5 = Convert.ToString(dt.Rows[i]["UnitPrice"]);
                PrinTab.Text_6 = Convert.ToString(dt.Rows[i]["Value"]);
                model.Add(PrinTab);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}