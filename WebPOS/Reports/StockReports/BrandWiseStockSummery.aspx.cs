﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class BrandWiseStockSummery : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string brandId)
        {
            try
            {

                var model = GetTheReport(brandId);
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport(string brandId)
        {
            var module7 = new Module7();
           
            
          
            SqlCommand cmd = new SqlCommand(@"select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description," +
              
                "qty," +
                "isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where BrandName.Code=" + brandId + " and InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Brand ,Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                
                invoice.Amount = (Convert.ToDecimal(invoice.Quantity) * Convert.ToDecimal(invoice.Rate)).ToString();
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}