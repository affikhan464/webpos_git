﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class AggingStockReport : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string brandId, string days)
        {
            try
            {

                var model = GetTheReport(Convert.ToInt32(brandId), Convert.ToInt32(days));
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport(int brandId = 0, int days = 0)
        {
            Module7 objModule7 = new Module7();

            var brandQuery = string.Empty;
            if (brandId != 0)
            {
                brandQuery = " and BrandName.Code=" + brandId;
            }

            SqlCommand cmd = new SqlCommand(@"
                select
                BrandName.Name,
                InvCode.Code,
                InvCode.Description,
                InvCode.qty,
                DATEDIFF(day,  (select MAX(Invoice1.Date1) from Invoice2 join Invoice1 on Invoice2.InvNo=Invoice1.InvNo where Invoice2.Code=InvCode.Code), GETDATE())-1 as DaysAfterSale
                from InvCode
                join Invoice2 on InvCode.Code = Invoice2.Code
                join Invoice1 on Invoice2.InvNo = Invoice1.InvNo
                join BrandName on InvCode.Brand = BrandName.Code
                where (DATEDIFF(day,  (select MAX(Invoice1.Date1) from Invoice2 join Invoice1 on Invoice2.InvNo=Invoice1.InvNo where Invoice2.Code=InvCode.Code) , GETDATE())-1 > " + days + @") 
                and InvCode.qty > 0 
                and InvCode.Nature = 1  
                " + brandQuery + @" 
                 Group by BrandName.Name, InvCode.Code, InvCode.Description, InvCode.qty
                order by InvCode.Description ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();
                var Quantity = dt.Rows[i]["qty"].ToString();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Quantity;
                invoice.DaysAfterSale = (dt.Rows[i]["DaysAfterSale"]).ToString();
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}