﻿<%@ page title="" language="C#" masterpagefile="~/masterpage.Master" autoeventwireup="true" codebehind="BrandWiseStockSummery.aspx.cs" inherits="WebPOS.Reports.SaleReports.BrandWiseStockSummery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Brand Wise Stock Summary</h2>
					<div class="BMSearchWrapper row align-items-end">
                       <div class="col col-12 col-sm-6 col-md-4">
                           <label>Select Brand</label>
                            <select id="BrandNameDD" class="dropdown">
                                <option value="0">Select Brand</option>
                            </select>
                        </div>
						 <div class="col col-12 col-sm-3 col-md-4">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-4">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>

					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">

								<div class="itemsHeader d-flex">
									<div class="five flex-1 phCol">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="five flex-1 phCol">
										<a href="javascript:void(0)"> 
											Item Code
										</a>
									</div>
									<div class="five flex-1 phCol">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
                                   
									<div class="five flex-1 phCol">                                                                    
										<a href="javascript:void(0)"> 
											Quantity
										</a>
									</div>
									<div class="five flex-1 phCol">
										<a href="javascript:void(0)"> 
											Rate
										</a>
									</div>
									<div class="five flex-1 phCol">
										<a href="javascript:void(0)"> 
										    Amount
										</a>
									</div>
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow d-flex'>
								<div class='five flex-1'><span></span></div>
								<div class='five flex-1'><span></span></div>
								<div class='five flex-1'><span></span></div>
								<div class='five flex-1'><input  disabled="disabled"  name='totalQuantity'/></div>
								<div class='five flex-1'><span></span></div>
								<div class='five flex-1'><input  disabled="disabled"  name='totalAmount'/></div>
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
   </asp:content>
<asp:content runat="server" id="content4" contentplaceholderid="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>"></script>
   <script type="text/javascript">


       $(document).ready(function () {
           appendAttribute("BrandNameDD", "BrandName");
           setTimeout(function () {
               //getReport();
           }, 1000)
           resizeTable();

           //appendAttribute takes two value one is the DropdownId and Table name from which data is collected.

       });

       $(window).resize(function () {
           resizeTable();

       });



       
        $(document).on('click', '#print', function () {
          
            window.open(
                '/Reports/StockReports/Prints/BrandWiseStockSummeryCR.aspx?brandId='+ $("#BrandNameDD .dd-selected-value").val(),
                '_blank'
            );
        });
       $("#searchBtn").on("click", function (e) {

           $("#currentPage").remove();
           $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
           clearInvoice();
           getReport();

       });
       function getReport() {
           $(".loader").show();

           var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
           $(".loader").show();
           $.ajax({
               url: '/Reports/StockReports/BrandWiseStockSummery.aspx/getReport',
               type: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               data: JSON.stringify({ "brandId": $("#BrandNameDD .dd-selected-value").val() }),

               success: function (response) {


                   if (response.d.Success) {
                       if (response.d.Reports.length > 0) {
                           var header = "";
                           
                           for (var i = 0; i < response.d.Reports.length; i++) {
                               var count = i + 1;
                              
                               $("<div class='itemRow newRow d-flex'>" +
                                   "<div class='five flex-1'><span>" + count + "</span></div>" +
                                   "<div class='five flex-1'><span>" + response.d.Reports[i].ItemCode + "</span></div>" +
                                   "<div class='five  flex-1 name'><span class='font-size-12' >" + response.d.Reports[i].Description + "</span></div>" +
                                   "<div class='five flex-1'><input name='Quantity' type='text' value='"+ Number(response.d.Reports[i].Quantity).toFixed(2) +"' disabled /></div>" +
                                   "<div class='five flex-1'><input value=" + Number(response.d.Reports[i].Rate).toFixed(2) + " disabled /></div>" +
                                   "<div class='five flex-1'><input name='Amount'  id='newAmountTextbox_" + i + "' value=" + Number(response.d.Reports[i].Amount).toFixed(2) + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                           }
                           $(".loader").hide();
                           calculateData();
                           resizeTable();

                       } else if (response.d.Reports.length == 0 && currentPage == "0") {
                           $(".loader").hide();
                           swal({
                               title: "No Result Found ",
                               text: "No Result Found!"
                           });
                       }

                   }
                   else {
                       $(".loader").hide();
                       swal({
                           title: "there is some error",
                           text: response.d.Message
                       });
                   }

               },
               error: function (error) {
                   swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

               }
           });
       }

       function clearInvoice() {
           $(".itemsSection .itemRow").remove();
       }
       function calculateData() {

           var sum;
           var inputs = ['Amount','Quantity'];
          
           for (var i = 0; i < inputs.length; i++) {

               var currentInput = inputs[i];
               sum = 0;
               $("input[name = '" + currentInput + "']").each(function () {

                   if (this.value.trim() === "") {
                       sum = (Number(sum) + Number(0));
                   } else {
                       sum = (Number(sum) + Number(this.value));
                   }

               });

               $("[name=total" + currentInput + "]").val('');
               $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


           }

       }




   </script>
</asp:content>
