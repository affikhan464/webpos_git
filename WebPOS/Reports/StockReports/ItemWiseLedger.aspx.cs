﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.StockReport
{
    public partial class ItemWiseLedger : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string itemCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, itemCode);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(DateTime StartDate, DateTime EndDate, string ItemCode)
        {
    

            ModItem objModItem = new ModItem();


            //decimal ItemCode = objModItem.ItemCodeAgainstItemName(txtItemName.Text);


            decimal OpBalance = 0;

            DateTime abc = Convert.ToDateTime(StartDate);
            OpBalance = ItemOpBal(Convert.ToDecimal(ItemCode), Convert.ToDateTime(abc));




            SqlCommand cmd = new SqlCommand(@"
                select 
                Dat,
                Description,
                Convert(numeric(18,2), Received) as AmountDr,
                Convert(numeric(18,2),Issued) as AmountCr,
                DescriptionOfBillNo,
                BillNo 
                from Inventory 
                where CompID='" + CompID + @"' and 
                ICode='" + ItemCode + @"' and 
                Dat>='" + StartDate + @"' and 
                Dat<='" + EndDate + @"' 
                order by System_Date_Time", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            
            int j = 1;
            decimal ClosingBalance = 0;
            decimal Debit = 0;
            decimal Credit = 0;
            var model = new List<PrintingTable>();

            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                var report = new PrintingTable();
                Debit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountDr"]);
                Credit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountCr"]);
                if (j == 1)
                {
                    var openingBalanceRow = new PrintingTable() {
                        Text_1=StartDate.ToString("dd-MMM-yyyy"),
                        Text_2= "Opening Balance",
                        Text_3= "0",
                        Text_4 = "0",
                        Text_5 = OpBalance.ToString()
                    };
                    model.Add(openingBalanceRow);

                    ClosingBalance = (OpBalance + Debit) - Credit;
                }
                else
                    if (j > 1)
                {
                    ClosingBalance = (ClosingBalance + Debit) - Credit;
                }

                
                report.Text_1 = Convert.ToDateTime(dset.Tables[0].Rows[i]["Dat"]).ToString("dd-MMM-yyyy");
                report.Text_2 = dset.Tables[0].Rows[i]["Description"].ToString();
                report.Text_3 = dset.Tables[0].Rows[i]["AmountDr"].ToString();
                report.Text_4 = dset.Tables[0].Rows[i]["AmountCr"].ToString();
                report.Text_5 = ClosingBalance.ToString();

                j++;
                model.Add(report);
            }

            
            //-------------------------------------------
            
          
            return model;
        }

       public static Decimal ItemOpBal(decimal ItemCode, DateTime StartDate)
        {
            Module7 obj = new Module7();
            DateTime dat = obj.StartDateTwo(StartDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(Opening),0) as Opening from InventoryOpeningBalance where  CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat='" + dat + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["Opening"]);

            }

            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(Received),0) as D,isnull(sum(Issued),0) as C from Inventory where  CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat>='" + dat + "' and Dat<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            OpB = OpB + Dr - Cr;

            return OpB;
        }


        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = ItemCode.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}