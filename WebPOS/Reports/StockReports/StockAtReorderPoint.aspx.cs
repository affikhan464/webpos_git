﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class StockAtReorderPoint : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport()
        {
            try
            {

                var model = GetTheReport();
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport()
        {

            SqlCommand cmd = new SqlCommand("Select InvCode.Code,Description,qty,ReorderLevel,ReorderQty,isnull(Ave_Cost,0) as Ave_Cost,BrandName.Name as Brand from invCode inner join BrandName on BrandName.Code=InvCode.Brand where  InvCode.CompID='" + CompID + "' and  qty<ReorderLevel and Visiable=1 order by description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();

            ModItem objModItem = new ModItem();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintRow = new PrintingTable();
                var InHandQty = Convert.ToInt32(dt.Rows[i]["qty"]);
                var ReorderQty = Convert.ToInt32(dt.Rows[i]["ReorderQty"]);
                var ReorderLevel = Convert.ToInt32(dt.Rows[i]["ReorderLevel"]);
                var requiredQty = (ReorderLevel - InHandQty) + ReorderQty;

                var unitPrice = dt.Rows[i]["Ave_Cost"] != null ? Convert.ToInt32(dt.Rows[i]["Ave_Cost"]) : 0;
              
                var amountRequired = unitPrice * requiredQty;

                PrintRow.Text_1 = Convert.ToString(dt.Rows[i]["Code"]);
                PrintRow.Text_2 = Convert.ToString(dt.Rows[i]["Description"]);
                PrintRow.Text_3 = ReorderLevel.ToString();
                PrintRow.Text_4 = InHandQty.ToString();
                PrintRow.Text_5 = Convert.ToString(ReorderLevel - InHandQty);
                PrintRow.Text_6 = ReorderQty.ToString();
                PrintRow.Text_7 = requiredQty.ToString(); // 
                PrintRow.Text_8 = Convert.ToString(objModItem.LastPurchasePrice(PrintRow.Text_1));
                PrintRow.Text_9 = Convert.ToString(Convert.ToDecimal(PrintRow.Text_7) * Convert.ToDecimal(PrintRow.Text_8));
                PrintRow.Text_10 = Convert.ToString(dt.Rows[i]["Brand"]);
                model.Add(PrintRow);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}