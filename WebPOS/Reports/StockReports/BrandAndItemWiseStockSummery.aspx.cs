﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class BrandAndItemWiseStockSummery : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(int brandId, string searchText)
        {
            try
            {

                var model = GetTheReport(brandId, searchText);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(int brandId, string searchText)
        {
            var module7 = new Module7();
         
            var brandWhereClause = string.Empty;
            if (brandId != 0)
            {
                brandWhereClause = " and BrandName.Code = " + brandId + " ";
            }

            var searchTextWhereClause = string.Empty;
            if (searchText.Trim() != string.Empty)
            {
                searchTextWhereClause = " and Description like '%" + searchText + "%' ";
            }
           


            //////SqlCommand cmd = new SqlCommand(@"select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description," +
            //////    "qty," +
            //////    "isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  " +
            //////    "where  InvCode.CompID='" + CompID + "' "+brandWhereClause + searchTextWhereClause +"  and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Description", con);

            SqlCommand cmd = new SqlCommand("select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description,qty,isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code where  InvCode.CompID='" + CompID + "' " + brandWhereClause + searchTextWhereClause + "  and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Description", con);




            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);






            var model = new List<PrintingTable >();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PTRow = new PrintingTable();

                PTRow.Text_1 = Convert.ToString(dt.Rows[i]["ItemCode"]);
                PTRow.Text_2 = Convert.ToString(dt.Rows[i]["BrandName"]);
                PTRow.Text_3 = Convert.ToString(dt.Rows[i]["Description"]);
                PTRow.Text_4 = Convert.ToString(dt.Rows[i]["qty"]);
                PTRow.Text_5 = Convert.ToString(dt.Rows[i]["Rate"]);

                PTRow.Text_6 = (Convert.ToDecimal(PTRow.Text_4) * Convert.ToDecimal(PTRow.Text_5)).ToString();
                model.Add(PTRow);
            }

            return model.OrderBy(a=>a.Text_3).ToList();



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}