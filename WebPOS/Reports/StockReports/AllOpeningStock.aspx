﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="AllOpeningStock.aspx.cs" Inherits="WebPOS.Reports.SaleReports.AllOpeningStock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="dayCashAndCreditSale searchAppSection">
				<div class="contentContainer">
					<h2>All Opening Stock</h2>
                                 
					<div class="BMSearchWrapper clearfix row">
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>

					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol" style="width:20% !important">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="seven phCol" style="width:30% !important">
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    
									<div class="thirteen  phCol" style="width:60% !important">                                                                    
										<a href="javascript:void(0)"> 
											Brand Name
										</a>
									</div>
                                    <div class="thirteen  phCol" style="width:300% !important">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
										<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Cost
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Amount
										</a>
									</div>
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven' style='width:20% !important'><span></span></div>
								<div class='seven' style='width:30% !important'><span></span></div>
								
								<div class='thirteen' style='width:60% !important'><span></span></div>
								<div class='thirteen' style='width:300% !important'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalQuantity'/></div>
								<div class='thirteen'><input  disabled="disabled" /></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalAmount'/></div>
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

   <script type="text/javascript">

       
            $(document).ready(function () {
                getReport();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            $("#searchBtn").on("click", function (e) {
                
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                    clearInvoice();
                    getReport();
             
            });
            function getReport() { $(".loader").show(); $(".loader").show();

                var currentPage=$("#currentPage").val()=="" || $("#currentPage").val()==undefined ?"0":$("#currentPage").val();
                $(".loader").show();
                $.ajax({
                    url: '/Reports/StockReports/AllOpeningStock.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                   
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.PrintReport.length > 0) {
                            
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                var count = i + 1;
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven' style='width:20% !important'><span>" + count + "</span></div>" +
                                    "<div class='seven' style='width:30% !important'><span>" + response.d.PrintReport[i].Text_1 + "</span></div>" +
                                    
                                    "<div class='thirteen name' style='width:60% !important'><span class='font-size-12' >" + response.d.PrintReport[i].Text_2 + "</span></div>" +
                                    "<div class='thirteen name' style='width:300% !important'><input value='" + response.d.PrintReport[i].Text_3 + "' disabled /></div>" +
                                    "<div class='thirteen'><input name='Quantity' type='text' value=" + Number(response.d.PrintReport[i].Text_4).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'><input type='text'value=" + Number(response.d.PrintReport[i].Text_5).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'><input name='Amount' type='text'  value=" + Number(response.d.PrintReport[i].Text_6).toFixed(2) + " disabled /></div>" +
                                     "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            $(".loader").hide();
                            calculateData();
                        
                            } else if (response.d.PrintReport.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Quantity","Amount"];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
