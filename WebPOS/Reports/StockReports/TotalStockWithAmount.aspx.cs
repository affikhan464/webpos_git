﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class TotalStockWithAmount : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        private static SqlCommand cmd;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string PartyType)
        {
            try
            {

                var model = GetTheReport(PartyType);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string PartyType)
        {


            ModItem objModItem = new ModItem();

            SqlCommand cmd = new SqlCommand("select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description,qty, isnull(InvCode.Ave_Cost,0) as  AvgCost ,isnull(InvCode.SellingCost,0) as  SellingCost ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where  InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by BrandName.Name, Description", con);
                

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();


            decimal Rate = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintingT = new PrintingTable();

                if (PartyType == "Avg") { 
                    Rate = Convert.ToDecimal(dt.Rows[i]["AvgCost"]);
                        }
                else
                if (PartyType == "Sell") {
                    Rate = Convert.ToDecimal(dt.Rows[i]["SellingCost"]);
                }
                else
                    if (PartyType == "LstP") {
                    Rate = objModItem.LastPurchasePrice(Convert.ToString(dt.Rows[i]["ItemCode"]));
                }
                PrintingT.Text_1 = Convert.ToString(dt.Rows[i]["ItemCode"]);
                PrintingT.Text_2 = Convert.ToString(dt.Rows[i]["BrandName"]);
                PrintingT.Text_3 = Convert.ToString(dt.Rows[i]["Description"]);
                PrintingT.Text_4 = Convert.ToString(dt.Rows[i]["qty"]);
                PrintingT.Text_5 = Convert.ToString(Rate);
                PrintingT.Text_6 = Convert.ToString(Convert.ToDecimal(dt.Rows[i]["qty"])* Convert.ToDecimal( Rate));
                model.Add(PrintingT);
            }
            model = model.OrderBy(a => a.Text_2).ThenBy(x => x.Text_3).ToList();
            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}