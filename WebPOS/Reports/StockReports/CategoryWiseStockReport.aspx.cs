﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class CategoryWiseStockReport : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string attributeId, string attribute)
        {
            try
            {

                var model = GetTheReport(attributeId, attribute);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string attributeId, string attribute)
        {
            var selectedAttributeType = attribute;
            var selectedAttributeTypeId = attributeId;

            SqlCommand cmd = new SqlCommand("select InvCode.Code,InvCode.Description,BrandName.Name as BrandName,InvCode.qty,isnull(InvCode.Ave_Cost,0) as Ave_Cost, (InvCode.qty*isnull(InvCode.Ave_Cost,0)) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code where  InvCode.CompID='" + CompID + "' and  qty>0 and " + selectedAttributeType + "=" + selectedAttributeTypeId + " order by BrandName.Name", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PTRow = new PrintingTable();

                PTRow.Text_1 = Convert.ToString(dt.Rows[i]["Code"]);
                PTRow.Text_2 = Convert.ToString(dt.Rows[i]["BrandName"]);
                PTRow.Text_3 = Convert.ToString(dt.Rows[i]["Description"]);
                PTRow.Text_4 = Convert.ToString(dt.Rows[i]["qty"]);
                PTRow.Text_5 = Convert.ToString(dt.Rows[i]["Ave_Cost"]);
                PTRow.Text_6 = Convert.ToString(dt.Rows[i]["Amount"]);
                PTRow.Attribute  = attribute;
                PTRow.AttributeType = attribute;
                model.Add(PTRow);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}