﻿<%@ page title="" language="C#" masterpagefile="~/masterpage.Master" autoeventwireup="true" codebehind="ProcessedItemReceivingDetail.aspx.cs" inherits="WebPOS.Reports.SaleReports.ProcessedItemReceivingDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Processed Item Received Detail</h2>
                    <div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->

                        <div class="col col-12 col-sm-6 col-md-3">
                          
                             <div class="d-inline-flex w-100">
                                <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                                    <label>
                                        <input id="brandCheck" class="checkbox" type="checkbox" />
                                        <span></span>
                                    </label>
                                </span>
                                <select id="BrandNameDD" class="dropdown" data-type="BrandName">
                                    <option value="0">Select Brand</option>
                                </select>
                            </div>
                        </div>

                         <div class="col col-12 col-sm-6 col-md-3">
                            <span class="userlLabel">From</span>
                            <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                            <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                        </div>
                       <div class="col col-12 col-sm-6 col-md-3">
                            <span class="userlLabel">To</span>
                            <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                            <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                        </div>
                       
                         
                        
                       
                        
						 <div class="col col-12 col-sm-3 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-1">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
					</div><!-- searchAppSection -->


					
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">

						<div class="itemsHeader d-flex">
									<div class="five flex-1 phCol " style="width:20% !important">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="five flex-1 phCol" style="width:70% !important">
										<a href="javascript:void(0)"> 
											Date
										</a>
									</div>
                                    <div class="five flex-1 phCol" style="width:50% !important">
										<a href="javascript:void(0)"> 
											Voucher #
										</a>
									</div>
									<div class="five flex-1  phCol" style="width:50% !important">                                                                    
										<a href="javascript:void(0)"> 
											Item Code
										</a>
									</div>
									<div class="five flex-1  phCol" style="width:300% !important"">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
                                    <div class="five flex-1  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Quantity
										</a>
									</div>
                                    <div class="five flex-1  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Rate
										</a>
									</div>
                                     <div class="five flex-1  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Amount
										</a>
									</div>
									
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow d-flex footerQuantities'>
								<div class='five flex-1' style='width:20% !important'><span></span></div>
								<div class='five flex-1' style='width:70% !important'><span></span></div>
								<div class='five flex-1' style='width:50% !important'><span></span></div>
                                <div class='five' style='width:50% !important'><span></span></div>
								<div class='five' style='width:300% !important'><span></span></div>
								<div class='five flex-1' ><input name="totalQuantity" class="totalQuantity" disabled/></div>
								<div class='five' ><span></span></div>
                               <div class='five flex-1' ><input name="totalAmount" class="totalAmount" disabled/></div>
                               
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
   </asp:content>
<asp:content runat="server" id="content4" contentplaceholderid="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>"></script>
   <script type="text/javascript">


       $(document).ready(function () {
           appendAttribute("BrandNameDD", "BrandName");
           setTimeout(function () {
               //getReport();
           }, 1000)
           resizeTable();

           //appendAttribute takes two value one is the DropdownId and Table name from which data is collected.

       });

       $(window).resize(function () {
           resizeTable();

       });
       
       
       $(document).on('click', '#print', function () {

           var brandId = 0;
           var searchText = "";

           if ($("#searchBoxCheck").prop("checked")) {
               searchText = $("#searchBox").val();
           }
           if ($("#brandCheck").prop("checked")) {
               brandId = $("#BrandNameDD .dd-selected-value").val();
           }
           window.open(
               '/Reports/StockReports/Prints/ProcessedItemReceivingDetail.aspx?type=individual&BrandID=' + BrandID + '&searchText=' + searchText,
               '_blank'
           );
       });
       $("#searchBtn").on("click", function (e) {

           $("#currentPage").remove();
           $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
           clearInvoice();
           getReport();

       });
       function getReport() {
           $(".loader").show();

           var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
           $(".loader").show();
            var data = {
               brandId: 0,
               searchText: ""
           };
           if ($("#searchBoxCheck").prop("checked")) {
               data.searchText = $("#searchBox").val();
           }
           if ($("#brandCheck").prop("checked")) {
               data.brandId = $("#BrandNameDD .dd-selected-value").val();
           }

           $.ajax({
               url: '/Reports/StockReports/ProcessedItemReceivingDetail.aspx/getReport',
               type: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               data: JSON.stringify(data),
               data: JSON.stringify({ "StartDate": $(".fromTxbx").val(), "EndDate": $(".toTxbx").val(), "BrandID": $("#BrandNameDD .dd-selected-value").val() }),
               success: function (response) {


                   if (response.d.Success) {
                       
                       if (response.d.PrintReport.length > 0) {
                          
                           QuantityCount = response.d.PrintReport[0].QuantityCount;
                         
                           
                           
                           for (var i = 0; i < response.d.PrintReport.length; i++) {
                               var count = i + 1;
                               $("<div class='itemRow newRow d-flex'>" +
                                   "<div class='five flex-1' style='width:20% !important'><span>" + count + "</span></div>" +
                                   "<div class='five flex-1' style='width:70% !important'><span>" + response.d.PrintReport[i].Date1 + "</span></div>" +
                                   "<div class='five  flex-1' style='width:50% !important'><span>" + response.d.PrintReport[i].VoucherNo + "</span></div>" +
                                   "<div class='five flex-3' style='width:50% !important'><input name='' type='text' value='" + response.d.PrintReport[i].ItemCode + "' disabled /></div>" +
                                   "<div class='five flex-1' style='width:300% !important'><input name='' type='text' value='" + response.d.PrintReport[i].ItemName + "' disabled /></div>" +
                                   "<div class='five flex-1'><input name='Quantity' type='text' value='" + Number(response.d.PrintReport[i].Received).toFixed(2) + "' disabled /></div>" +
                                   "<div class='five flex-1'><input name='Rate' type='text' value='" + Number(response.d.PrintReport[i].Rate).toFixed(2) + "' disabled /></div>" +
                                   "<div class='five flex-1'><input name='Amount' type='text' value='" + Number(response.d.PrintReport[i].Amount).toFixed(2) + "' disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                           }
                           $(".loader").hide();
                           calculateData();
                           resizeTable();

                       } else if (response.d.PrintReport.length == 0 && currentPage == "0") {
                           $(".loader").hide();
                           swal({
                               title: "No Result Found ",
                               text: "No Result Found!"
                           });
                       }

                   }
                   else {
                       $(".loader").hide();
                       swal({
                           title: "there is some error",
                           text: response.d.Message
                       });
                   }

               },
               error: function (error) {
                   swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

               }
           });
       }

       function clearInvoice() {


           $(".itemsSection .itemRow").remove();
       }
       function calculateData() {

           var sum;
           var inputs = ['Quantity', 'Amount'];
         
           for (var i = 0; i < inputs.length; i++) {

               var currentInput = inputs[i];
               sum = 0;
               $("input[name = '" + currentInput + "']").each(function () {

                   if (this.value.trim() === "") {
                       sum = (Number(sum) + Number(0));
                   } else {
                       sum = (Number(sum) + Number(this.value));

                   }

               });

               $("[name=total" + currentInput + "]").val('');
               $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


           }

       }
   </script>
</asp:content>
