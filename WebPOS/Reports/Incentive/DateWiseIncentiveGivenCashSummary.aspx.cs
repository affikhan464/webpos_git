﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.Incentive
{
    public partial class DateWiseIncentiveGivenCashSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {


            ModGL objModGL = new ModGL();
            string IncentiveIncomeGLCode ="01"+ objModGL.GLCodeAgainstGLLockName("IncentiveIncome");
            

            SqlCommand cmd = new SqlCommand(@"Select CashIncentive.PartyCode, sum(Amount) as Amount,PartyCode.Name 
                                                from 
                                                    CashIncentive 
                                                    inner join partycode on partycode.code= CashIncentive.PartyCode
                                                where 
                                                    Nature = 'CashIncentiveGiven' and 
                                                    Date1 >= '" + StartDate + @"' and
                                                    date1 <= '" + EndDate + @"' 
                                                    group by CashIncentive.partyCode,PartyCode.Name
                                                    order by PartyCode.Name", con);
            






            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();


                invoice.PartyCode = Convert.ToString(dt.Rows[i]["PartyCode"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);





                model.Add(invoice);
            }

            return model;

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReportModal(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReportModal(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        public static List<InvoiceViewModel> GetTheReportModal(DateTime StartDate, DateTime EndDate)
        {
            ModGL objModGL = new ModGL();
            string IncentiveIncomeGLCode = "01" + objModGL.GLCodeAgainstGLLockName("IncentiveIncome");
            SqlCommand cmd = new SqlCommand(@"Select CashIncentive.PartyCode, sum(Amount) as Amount,PartyCode.Name 
                                                from 
                                                    CashIncentive 
                                                    inner join partycode on partycode.code= CashIncentive.PartyCode
                                                where 
                                                    Nature = 'CashIncentiveGiven' and 
                                                    Date1 >= '" + StartDate + @"' and
                                                    date1 <= '" + EndDate + @"' 
                                                    group by CashIncentive.partyCode,PartyCode.Name
                                                    order by PartyCode.Name", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();
            decimal Total = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                invoice.PartyCode = Convert.ToString(dt.Rows[i]["PartyCode"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                Total += Convert.ToDecimal(invoice.Amount);
                model.Add(invoice);
            }
            var invoiceTot = new InvoiceViewModel();          
            invoiceTot.PartyName = "Total";
            invoiceTot.Amount = Convert.ToString(Total);
           
            model.Add(invoiceTot);

            return model;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = itemCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleGroupByPartyCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}