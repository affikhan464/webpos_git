﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ProftiAndLoss1.aspx.cs" Inherits="WebPOS.Reports.ProfitLoss.ProftiAndLoss1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2>Profit & Loss 1</h2>
            <div class="BMSearchWrapper row">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                <div class="col col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3  ">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>
                <div class="col col-6 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-left pt-1">

                    <label>
                                <input id="DetailRequired" class="checkbox" type="checkbox" />
                                <span></span>
                                Detail ?
                            </label>
                </div>
                <div class="col col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2 ">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
                </div>

                <div class="col col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2 ">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i> Print</a>
                </div>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="reports recommendedSection allItemsView">


                <div class="itemsHeader">
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Sr.#
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Date
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Invoice
                        </a>
                    </div>
                    <div class="thirteen  phCol">
                        <a href="javascript:void(0)">Sale
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Cost
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Profit
											
                        </a>
                    </div>
                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow'>
                        <div class='thirteen'><span class="color-white">Totals</span></div>
                        <div class='thirteen'><span></span></div>
                        <div class='thirteen'><span></span></div>
                        <div class='thirteen'>
                            <input disabled="disabled" name='totalSale' /></div>
                        <div class='thirteen'>
                            <input disabled="disabled" name='totalCost' /></div>
                        <div class='thirteen'>
                            <input disabled="disabled" name='totalProfit' />
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script type="text/javascript">


        $(document).ready(function ()
        {
            initializeDatePicker();
            resizeTable();
        });

        $(window).resize(function ()
        {
            resizeTable();

        });


        $("#toTxbx").on("keypress", function (e)
        {
            if (e.key == 13)
            {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                clearInvoice();
                getReport();

            }
        });

        $("#searchBtn").on("click", function (e)
        {

            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
            clearInvoice();
            getReport();

        });
        function getReport()
        {
            $(".loader").show();
            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
            $.ajax({
                url: '/Reports/ProfitLoss/ProftiAndLoss1.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "isDetailRequired": $("#DetailRequired").prop('checked') }),                
                success: function (response)
                {
                    if (response.d.Success)
                    {
                        if (response.d.ListofInvoices.length > 0)
                        {
                            for (var i = 0; i < response.d.ListofInvoices.length; i++)
                            {
                                $(`<div class='itemRow newRow'>
                                    <div class='thirteen'><span>${i+1 }</span></div>
                                    <div class='thirteen'><span>${ response.d.ListofInvoices[i].Date }</span></div>
                                    <div class='thirteen'><span>${ response.d.ListofInvoices[i].InvoiceNumber }</span></div>
                                    <div class='thirteen '><input  name='Sale' value='${ response.d.ListofInvoices[i].Sale }' disabled /></div>
                                    <div class='thirteen'><input name='Cost' type='text' value='${ Number(response.d.ListofInvoices[i].Cost).toFixed(2) }' disabled /></div>
                                    <div class='thirteen'><input name='Profit'    value='${ Number(response.d.ListofInvoices[i].Profit).toFixed(2) }' disabled /></div>
                                    </div>`).appendTo(".dayCashAndCreditSale .itemsSection");
                            }
                            $(".loader").hide();
                            calculateData();

                        } else if (response.d.ListofInvoices.length == 0 && currentPage == "0")
                        {
                            $(".loader").hide(); swal({
                                title: "No Result Found ", type:'error',
                                text: "No Result Found!"
                            });
                        }

                    }
                    else
                    {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error)
                {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }

        function clearInvoice()
        {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData()
        {

            var sum;
            var inputs = ["Sale", 'Cost', 'Profit'];
            for (var i = 0; i < inputs.length; i++)
            {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function ()
                {

                    if (this.value.trim() === "")
                    {
                        sum = (Number(sum) + Number(0));
                    } else
                    {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


            }

        }
    </script>
</asp:Content>
