﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.StockReport
{
    public partial class IncomeStatement2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        public static List<PrintingTable> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            var model = new List<PrintingTable>();
            Module7 objModule7 = new Module7();
            ModItem objModItem = new ModItem();
            ModGLCode objModGLCode = new ModGLCode();
            ModGL objModGL = new ModGL();
            Module2 objModule2 = new Module2();
            decimal Sale = 0;
            decimal DiscountOnSale = 0;
            decimal NetSale = 0;
            //string PartyCode = "0101010300038";
            
    
            SqlDataAdapter cmd1 = new SqlDataAdapter("Select Code,Title from GLCode where Lvl=5 and code like '" + CompID + "0301%' order by title", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);
            var openingBalanceRow1 = new PrintingTable();
            openingBalanceRow1.Text_1 = "Income";
            openingBalanceRow1.Text_2 = "";
            openingBalanceRow1.Text_3 = "";
            openingBalanceRow1.Text_4 = "";
            openingBalanceRow1.Text_5 = "";
            model.Add(openingBalanceRow1);
            decimal TotSale = 0;
            for (int i = 0; i < dt1.Rows.Count; i++)
            {

                var openingBalanceRow = new PrintingTable();
                Sale = objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dt1.Rows[i]["Code"]), 2, StartDate, EndDate);
                TotSale = TotSale + Sale;
                openingBalanceRow.Text_1 = "";
                openingBalanceRow.Text_2 = Convert.ToString(dt1.Rows[i]["Title"]);
                openingBalanceRow.Text_3 =  Convert.ToString(Sale);
                openingBalanceRow.Text_4 = "";
                openingBalanceRow.Text_5 = "";
                if (Sale > 0) { model.Add(openingBalanceRow); }
            }

            //----------------------Discount on Sale
            var DisOnSale = new PrintingTable();
            DisOnSale.Text_1 = "";
            DisOnSale.Text_2 = "Less Discount on Sale";
            DiscountOnSale = objModGL.UpToDateClosingBalanceGL_3("0103020300001", 1, StartDate, EndDate);
            DisOnSale.Text_3 = Convert.ToString(DiscountOnSale);
            DisOnSale.Text_4 = "";
            DisOnSale.Text_5 = "";
            model.Add(DisOnSale);

            //----------------------Sale Return
            //var SaleReturn = new PrintingTable();
            //SaleReturn.Text_2 = "Less Sale Return";
            //SaleReturn.Text_3 = Convert.ToString(objModGL.UpToDateClosingBalanceGL_3("0103010100001", 1, StartDate, EndDate));
            //model.Add(SaleReturn);
            //----------------------Net Sale
            decimal netSal = 0;
            var NetSale1 = new PrintingTable();
            NetSale1.Text_1 = "";
            NetSale1.Text_2 = "Net Sale";
            NetSale1.Text_3 = "";
            netSal = TotSale - DiscountOnSale;
            NetSale1.Text_4 = Convert.ToString(netSal);
            NetSale1.Text_5 = "";
            model.Add(NetSale1);
            //-----------------------CGS
            var cgs = new PrintingTable();
            cgs.Text_1 = "Cost Of Goods Sold:";
            cgs.Text_2 = "";
            cgs.Text_3 = "";
            cgs.Text_4 = "";
            cgs.Text_5 = "";
            model.Add(cgs);
            SqlDataAdapter cmd2 = new SqlDataAdapter("Select Code,Title from GLCode where Lvl=5 and code like '" + CompID + "0401%' order by title", con);
            DataTable dt2 = new DataTable();
            cmd2.Fill(dt2);

            decimal CGS = 0;
            decimal TotCGS = 0;
            for (int i = 0; i < dt2.Rows.Count; i++)
            {

                var CGSRow = new PrintingTable();
                CGS =  objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dt2.Rows[i]["Code"]), 1, StartDate, EndDate);
                TotCGS = TotCGS + CGS;
                CGSRow.Text_1 = "";
                CGSRow.Text_2 = Convert.ToString(dt2.Rows[i]["Title"]);
                CGSRow.Text_3 = Convert.ToString(objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dt2.Rows[i]["Code"]), 1, StartDate, EndDate));
                CGSRow.Text_4 = "";
                CGSRow.Text_5 = "";
                if (CGS > 0) { model.Add(CGSRow); }
            }

            var NetCGS = new PrintingTable();
            NetCGS.Text_1 = "";
            NetCGS.Text_2 = "Total Cost";
            NetCGS.Text_3 = "";
            NetCGS.Text_4 = Convert.ToString(TotCGS);
            NetCGS.Text_5 = "";
            model.Add(NetCGS);
            //---------------------------------------------------------------------Gross Profit
            var GP = new PrintingTable();
            GP.Text_1 = "Gross Profit:";
            GP.Text_2 = "";
            GP.Text_3 = "";
            GP.Text_4 = "";
            decimal GrossProfit =netSal- TotCGS;
            GP.Text_5 = Convert.ToString(GrossProfit);
            model.Add(GP);
            //--------------------------------------------------------------------Selling Expence
            var sell = new PrintingTable();
            sell.Text_1 = "Selling Exp:";
            sell.Text_2 = "";
            sell.Text_3 = "";
            sell.Text_4 = "";
            sell.Text_5 = "";
            model.Add(sell);
            SqlDataAdapter cmdSelling = new SqlDataAdapter("Select Code,Title from GLCode where Lvl=5 and code like '" + CompID + "040202%' order by title", con);
            DataTable dtSelling = new DataTable();
            cmdSelling.Fill(dtSelling);
            decimal SellingExp = 0;
            decimal TotalSellingExp = 0;
            for (int i = 0; i < dtSelling.Rows.Count; i++)
            {
                var SellingRow = new PrintingTable();
                SellingExp = SellingExp + objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dtSelling.Rows[i]["Code"]), 1, StartDate, EndDate);
                SellingRow.Text_1 = "";
                SellingRow.Text_2 = Convert.ToString(dtSelling.Rows[i]["Title"]);
                SellingExp= Convert.ToDecimal(objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dtSelling.Rows[i]["Code"]), 1, StartDate, EndDate));
                TotalSellingExp = TotalSellingExp + SellingExp;
                SellingRow.Text_3 = Convert.ToString(SellingExp);
                SellingRow.Text_4 = "";
                SellingRow.Text_5 = "";
                if (SellingExp > 0)
                {
                    model.Add(SellingRow);
                }
            }
            var TotSelExpRow = new PrintingTable();
            TotSelExpRow.Text_1 = "";
            TotSelExpRow.Text_2 = "Total Selling Exp";
            TotSelExpRow.Text_3 = "";
            TotSelExpRow.Text_4 = Convert.ToString(TotalSellingExp);
            TotSelExpRow.Text_5 = "";
            model.Add(TotSelExpRow);
            //--------------------------------------------------------------------Admin Expence
            var admi = new PrintingTable();
            admi.Text_1 = "Admin Exp:";
            admi.Text_2 = "";
            admi.Text_3 = "";
            admi.Text_4 = "";
            admi.Text_5 = "";
            model.Add(admi);
            SqlDataAdapter cmdAdminExp = new SqlDataAdapter("Select Code,Title from GLCode where Lvl=5 and code like '" + CompID + "040201%' order by title", con);
            DataTable dtAdminExp = new DataTable();
            cmdAdminExp.Fill(dtAdminExp);
            decimal AdminExp = 0;
            decimal TotalAdminExp = 0;
            for (int i = 0; i < dtAdminExp.Rows.Count; i++)
            {
                var AdminRow = new PrintingTable();
                AdminExp = SellingExp + objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dtAdminExp.Rows[i]["Code"]), 1, StartDate, EndDate);
                AdminRow.Text_1 = "";
                AdminRow.Text_2 = Convert.ToString(dtAdminExp.Rows[i]["Title"]);
                AdminExp = Convert.ToDecimal(objModGL.UpToDateClosingBalanceGL_3(Convert.ToString(dtAdminExp.Rows[i]["Code"]), 1, StartDate, EndDate));
                TotalAdminExp = TotalAdminExp + AdminExp;
                AdminRow.Text_3 = Convert.ToString(AdminExp);
                AdminRow.Text_4 = "";
                AdminRow.Text_5 = "";
                if (AdminExp > 0)
                {
                    model.Add(AdminRow);
                }
            }
            var TotAdminExpRow = new PrintingTable();
            TotAdminExpRow.Text_1 = "";
            TotAdminExpRow.Text_2 = "Total Admin Exp";
            TotAdminExpRow.Text_3 = "";
            TotAdminExpRow.Text_4 = Convert.ToString(TotalAdminExp);
            TotAdminExpRow.Text_5 = "";
            model.Add(TotAdminExpRow);
            //------------------------------------------------------------Total Selling /Admin

            var TotOperaExpRow = new PrintingTable();
            TotOperaExpRow.Text_1 = "";
            TotOperaExpRow.Text_2 = "Total Operational Exp:";
            TotOperaExpRow.Text_3 = "";
            TotOperaExpRow.Text_4 = "";
            TotOperaExpRow.Text_5 = Convert.ToString(TotalSellingExp+ TotalAdminExp);
            model.Add(TotOperaExpRow);
            //------------------------------------------------------------Net Operational Profit

            var NetOprProftRow = new PrintingTable();
            //NetOprProftRow.Text_1 = "";
            NetOprProftRow.Text_1 = "Net Operational Profit:";
            NetOprProftRow.Text_2 = "";
            NetOprProftRow.Text_3 = "";
            NetOprProftRow.Text_4 = "";
            NetOprProftRow.Text_5 = Convert.ToString(GrossProfit-(TotalSellingExp + TotalAdminExp));
            model.Add(NetOprProftRow);



            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = ItemCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}