﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_CashSaleModal.ascx.cs" Inherits="WebPOS.Reports.Modals._CashSaleModal" %>

<div class="modal fade" id="DayCashSaleModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close position-absolute rt-10" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-0">
                <div id="generic_price_table" class="m-0 ">
                    <section>
                        <div class="row">


                            <div class="col-12">

                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">

                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix m-0 pb-3">

                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">

                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Cash Sale</span>
                                            </div>
                                            <!--//HEAD END-->

                                        </div>
                                        <!--//HEAD CONTENT END-->


                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list pb-2">
                                        <table class="w-100">
                                            <thead>
                                                <tr class="p-1">
                                                    <th>Date</th>
                                                    <th>Item Name</th>
                                                    <th>Rate</th>
                                                    <th>Per Dis.</th>
                                                    <th>Deal Dis.</th>
                                                </tr>
                                            </thead>
                                            <tbody class="soldItems">
                                               
                                            </tbody>

                                        </table>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <%-- <div class="generic_price_btn clearfix">
                                        <a class="" href="">Sign up</a>
                                    </div>--%>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

