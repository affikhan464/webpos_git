﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.ReceiptandCapitalReports
{
    public partial class DateWiseReceiptDetail : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport( fromDate,  toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            string CashAccountGLCode = "0101010100001";
            SqlCommand cmd = new SqlCommand(@"Select  GeneralLedger.datedr,GeneralLedger.VenderCode,GeneralLedger.V_No,GeneralLedger.AmountDr,GeneralLedger.Narration,
                                                        PartyCode.Name from GeneralLedger
                                                        inner join partycode on partycode.code=GeneralLedger.VenderCode
                                                        where   partycode.CompID='" + CompID + @"' and  
                                                                GeneralLedger.V_Type='CRV' and 
                                                                GeneralLedger.Code='" + CashAccountGLCode + @"' and 
                                                                GeneralLedger.SecondDescription ='FromParties' and 
                                                                GeneralLedger.datedr>='" + StartDate + @"' and 
                                                                GeneralLedger.datedr<='" + EndDate + @"' 
                                                                order by GeneralLedger.System_Date_Time", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["datedr"]).ToString("dd-MMM-yyyy");
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"] + " - Nar:" + dt.Rows[i]["Narration"]);
                invoice.NumberOfInvoices = Convert.ToString(dt.Rows[i]["V_No"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["AmountDr"]);
                
                model.Add(invoice);
            }

            return model;



        }

        //---------
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReportReceiptDetail(string PartyCode)
        {
            try
            {
                
                var model = GetTheReportReceiptDetail(PartyCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReportReceiptDetail(string PartyCode)
        {
            string CashAccountGLCode = "0101010100001";
            SqlCommand cmd = new SqlCommand();
            if (PartyCode == "")
            {
                cmd = new SqlCommand(@"Select  top 100 Date1,PartyCode,VoucherNo,Amount,Narration,
                                                        PartyName from CashReceived
                                                        where   CompID='" + CompID + @"' 
                                                        order by System_Date_Time desc", con);
            }
            else
            {
                 cmd = new SqlCommand(@"Select  top 100 Date1,PartyCode,VoucherNo,Amount,Narration,
                                                        PartyName from CashReceived
                                                        where   CompID='" + CompID + @"' and
                                                        PartyCode='" + PartyCode + @"'
                                                        order by System_Date_Time desc", con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToString("dd-MMM-yyyy");
                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"] + " - Nar:" + dt.Rows[i]["Narration"]);
                invoice.NumberOfInvoices = Convert.ToString(dt.Rows[i]["VoucherNo"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Amount"]);

                model.Add(invoice);
            }

            return model;



        }



        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DateWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}