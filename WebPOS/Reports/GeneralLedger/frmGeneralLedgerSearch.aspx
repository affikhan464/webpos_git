﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmGeneralLedgerSearch.aspx.cs" Inherits="WebPOS.Reports.GeneralLedger.frmGeneralLedgerSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
        <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br /><h2 class="heading">Double Entry Verification</h2><br />
        
        
           <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">

        
            <table border="0" style="width:100%" id="top">
            <tr style="background-color:aliceblue;align-items:center">
                <td  >
                    <asp:Label ID="Label1" runat="server" Text="Start Date"></asp:Label>
                    <asp:TextBox ID="StartDate"  CssClass="datetimepicker"  runat="server"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="End Date"></asp:Label>
                    <asp:TextBox ID="txtEndDate"  CssClass="datetimepicker"   runat="server"></asp:TextBox>
                    
                    <asp:CheckBox ID="chkGrouping" runat="server" text="Grouping"/>
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
</div>
        <div style="width:100%">
        
               
        
  

            <br /><br />




        <asp:GridView ID="GridView1" CssClass="grid" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      
            <Columns>

        <asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:d MMM yyyy}" ReadOnly="true"  >  
                <ItemStyle Width="160px" HorizontalAlign="Center" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Description" DataField="Description" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Debit" DataField="Debit" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Credit" DataField="Credit" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>

        <asp:BoundField HeaderText="Balance" DataField="Balance" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        
                
                
                <asp:BoundField HeaderText="DescriptionOfBill" DataField="DescriptionOfBill" ReadOnly="true" Visible="False"  />  
        <asp:BoundField HeaderText="BillNo" DataField="BillNo" ReadOnly="true" Visible="False"  />  
      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>


    </div>
 




</asp:Content>
