﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="JournalVoucher.aspx.cs" Inherits="WebPOS.Reports.GeneralLedger.JournalVoucher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Journal Voucher</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                   
                            <%--<div  class="col col-12 col-sm-6 col-md-3">
                             <input id="expenceTextbox" data-id="expenceTextbox" data-type="ExpenceHead" data-function="GLList" data-glcode="01" class="autocomplete ExpenceHeadTitle BankTitle empty" name="PartyName" type="text" placeholder="Enter GL Titale" autocomplete="off"/>
							</div>
                            <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Party Code</span>
                              <asp:TextBox id="GLCode" disabled="disabled" ClientIDMode="Static" CssClass="input__field input__field--hoshi ExpenceHeadCode empt GLCode"  name="PartyCode"  runat="server" autocomplete="off"></asp:TextBox>
                                
							</div>--%>
                        
                       
						 <div class="col col-12 col-sm-3 col-md-3">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-3">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class=" phCol" style="width:80% !important">
										<a href="javascript:void(0)"> 
										Date
										</a>
									</div>
                                    <div class=" phCol" style="width:70% !important">
										<a href="javascript:void(0)"> 
											GLCode
										</a>
									</div>
                                    <div class="phCol" style="width:100% !important">
										<a href="javascript:void(0)"> 
											GL Title
										</a>
									</div>
                                    <div class="phCol" style="width:200% !important">
										<a href="javascript:void(0)"> 
											Narration
										</a>
									</div>
                                    <div class="sixteen phCol" style="width:50% !important">
										<a href="javascript:void(0)"> 
											V. No.
										</a>
									</div>
                                    <div class="sixteen phCol" style="width:30% !important">
										<a href="javascript:void(0)"> 
											V. Type
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Debit
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Credit
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Balance
											
										</a>
									</div>
								
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='sixteen' style='width:80% !important'><span></span></div>
								<div  style='width:70% !important'><span></span></div>
                                <div  style='width:100% !important'><span></span></div>
                                <div  style='width:200% !important'><span></span></div>
                                <div class='sixteen' style='width:50% !important'><span></span></div>
                                <div class='sixteen' style='width:30% !important'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalDebit'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalCredit'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='closingBalance'/></div>
                                
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery.min.js")%>"></script>
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery-ui.min.js")%>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.datetimepicker.js")%>"></script>
    <script src="<%=ResolveClientUrl("/Script/InitializeDateTimePicker.js")%>"></script>
     <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                  
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
               
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/GeneralLedger/JournalVoucher.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.PrintReport.length > 0) {
                                
                            
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>" +
                                   "<div class='eight' style='width:80% !important'><span>" + response.d.PrintReport[i].Text_1 + "</span></div>" +
                                    "<div  style='width:70% !important'><span>" + response.d.PrintReport[i].Text_2 + "</span></div>" +
                                    "<div  style='width:100% !important'><span>" + response.d.PrintReport[i].Text_3 + "</span></div>" +
                                    "<div  style='width:200% !important'><span>" + response.d.PrintReport[i].Text_4 + "</span></div>" +
                                   "<div class='thirteen' style='width:50% !important'><span>" + response.d.PrintReport[i].Text_5 + "</span></div>" +
                                   "<div class='thirteen' style='width:30% !important'><span>" + response.d.PrintReport[i].Text_6 + "</span></div>" +
                                   "<div class='thirteen' ><input  value=" + Number(response.d.PrintReport[i].Text_9).toFixed(2) + " disabled name='Debit'/></div>" +
                                   "<div class='thirteen' ><input  value=" + Number(response.d.PrintReport[i].Text_10).toFixed(2) + " disabled name='Credit'/></div>" +
                                   "<div class='thirteen' ><input  value=" + Number(response.d.PrintReport[i].Text_11).toFixed(2) + " disabled name='ClosingB'/></div>" +
                                   "</div>").appendTo(".itemsSection");


                            }
                                calculateData();
                              var closingBalance = $(".itemsSection .itemRow:last").find("[name=Balance]").val();
                                $(".itemsFooter [name=closingBalance]").val(closingBalance);
                            $(".loader").hide();
                        
                            } else if (response.d.PrintReport.length == 0) {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found in the selected Date Range"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Debit", 'Credit'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }
                  

            }
   </script>
</asp:Content>
