﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.GeneralLedger
{
    public partial class JournalVoucher : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        public static List<PrintingTable> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            var model = new List<PrintingTable>();
        Module7 objModule7 = new Module7();
        ModItem objModItem = new ModItem();
        ModGL objModGL = new ModGL();
        Module2 objModule2 = new Module2();
        
            
            Decimal ClosingBalance = 0;
Int32 j = 1;

SqlCommand cmd2 = new SqlCommand("select Code,dateDr, Description, Convert(numeric(18, 2), AmountDr) as AmountDr, Convert(numeric(18, 2), AmountCr) as AmountCr,V_No,V_Type from GeneralLedger where v_type='JV' and descriptionofbillno='JVThroughVoucher' and datedr >= '" + StartDate + "' and datedr <= '" + EndDate + "' order by s_no", con);
SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
var dt2 = new DataTable();
adpt2.Fill(dt2);
           
            decimal TotDr = 0;
            decimal TotCr = 0;


            for (int i = 0; i<dt2.Rows.Count; i++)
            {
                var PrintingT = new PrintingTable();

                

                var Date1 = Convert.ToDateTime(dt2.Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"); // Convert.ToDateTime(dset.Tables[0].Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"),
                var GLCode = Convert.ToString(dt2.Rows[i]["Code"]);
                var GLTitle = objModGL.GLTitleAgainstCode(Convert.ToString(dt2.Rows[i]["Code"]));
                var Narration= Convert.ToString(dt2.Rows[i]["Description"]);
                var V_No= Convert.ToString(dt2.Rows[i]["V_No"]);
                var V_Type = Convert.ToString(dt2.Rows[i]["V_Type"]);
                var Description = Convert.ToString(dt2.Rows[i]["Code"]) + "-" + objModGL.GLTitleAgainstCode(Convert.ToString(dt2.Rows[i]["Code"])) + "Nar:" + Convert.ToString(dt2.Rows[i]["Description"]);
                var AmountDr = Convert.ToString(dt2.Rows[i]["AmountDr"]);
                var AmountCr = Convert.ToString(dt2.Rows[i]["AmountCr"]);


                
                    PrintingT.Text_1 = Date1.ToString();
                    PrintingT.Text_2 = GLCode.ToString();
                    PrintingT.Text_3 = GLTitle.ToString();
                    PrintingT.Text_4 = Narration.ToString();
                    PrintingT.Text_5 = V_No.ToString();
                    PrintingT.Text_6 = V_Type.ToString();
                    PrintingT.Text_9 = AmountDr.ToString();
                    PrintingT.Text_10 = AmountCr.ToString();
                    PrintingT.Text_11 = ClosingBalance.ToString();
                    TotDr = TotDr + Convert.ToDecimal(AmountDr);
                    TotCr = TotCr + Convert.ToDecimal(AmountCr);
                model.Add(PrintingT);
                if (TotDr == TotCr) {
                    var PrintingT2 = new PrintingTable();
                    PrintingT2.Text_1 = "";
                    PrintingT2.Text_2 = "Total";
                    PrintingT2.Text_3 = "";
                    PrintingT2.Text_4 = "";
                    PrintingT2.Text_5 = "";
                    PrintingT2.Text_6 = "";
                    PrintingT2.Text_9 = ""; Convert.ToString(TotDr);
                    PrintingT2.Text_10 = ""; Convert.ToString(TotCr);
                    PrintingT2.Text_11 = Convert.ToString(TotCr);
                    TotDr = 0;
                    TotCr = 0;
                    model.Add(PrintingT2);
                }
                //OldVNo = V_No;




                

               

            }

            return model;
        }


       
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = ItemCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}