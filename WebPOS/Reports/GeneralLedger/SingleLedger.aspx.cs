﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS.Reports.GeneralLedger
{
    public partial class SingleLedger : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string code)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, code);
                return new BaseModel { Success = true, Reports = model };
            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static List<ReportViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string code)
        {

            ModPartyCodeAgainstName objPC = new ModPartyCodeAgainstName();
            Module2 objModule2 = new Module2();

            int NorBalance = 1;
            NorBalance = objModule2.NormalBalanceParties(code);
            decimal OpBalance = 0;

            OpBalance = PartyOpBal(code, StartDate, NorBalance);
            
            SqlCommand cmd = new SqlCommand(@"
                select
                dateDr,
                Description as DescriptionDr,
                Convert(numeric(18, 2), AmountDr) as AmountDr,
                Convert(numeric(18, 2), AmountCr) as AmountCr,
                DescriptionOfBillNo,
                BillNo,
                V_Type,
                V_No

                from GeneralLedger
                where (AmountDr > 0 or AmountCr > 0)
                and CompID = '" + CompID + @"'
                and code = '" + code + @"'
                and datedr>= '" + StartDate + @"'
                and datedr<= '" + EndDate + @"'
                order by dateDr", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            int j = 1;
            decimal ClosingBalance = 0;
            decimal Debit = 0;
            decimal Credit = 0;
            var reports = new List<ReportViewModel>();

            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                var report = new ReportViewModel();
                Debit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountDr"]);
                Credit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountCr"]);
                var V_Type =dset.Tables[0].Rows[i]["V_Type"].ToString();
                var V_No = dset.Tables[0].Rows[i]["V_No"].ToString();

                if (j == 1 && NorBalance == 1)
                {
                    var openingBalanceRow = new ReportViewModel()
                    {
                        Date = StartDate.ToString("dd-MMM-yyyy"),
                        VoucherNumber="--",
                        VoucherType="--",
                        Description = "Opening Balance",
                        Debit = "0",
                        Credit = "0",
                        Balance = OpBalance.ToString()
                    };
                    reports.Add(openingBalanceRow);
                    ClosingBalance = (OpBalance + Debit) - Credit;
                }
                else
                   if (j > 1 && NorBalance == 1)
                {
                    ClosingBalance = (ClosingBalance + Debit) - Credit;
                }

                if (j == 1 && NorBalance == 2)
                {
                    ClosingBalance = (OpBalance + Credit) - Debit;
                }
                else
                    if (j > 1 && NorBalance == 2)
                {
                    ClosingBalance = (ClosingBalance + Credit) - Debit;
                }


                report.Date = Convert.ToDateTime(dset.Tables[0].Rows[i]["datedr"]).ToString("dd-MMM-yyyy");
                report.Description = dset.Tables[0].Rows[i]["DescriptionDr"].ToString();
                report.Debit = Debit.ToString();
                report.Credit = Credit.ToString();
                report.Balance = ClosingBalance.ToString();
                report.VoucherNumber = V_No;
                report.VoucherType = V_Type;

                j++;
                reports.Add(report);
            }


            //-------------------------------------------


            return reports;



        }

        public static decimal PartyOpBal(string PartyCode, DateTime StartDate, int NorBalance)
        {
            Module7 obj = new Module7();
            DateTime dat = obj.StartDateTwo(StartDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + dat + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["OpBalance"]);

            }

            //'''''''''''''''''''''''''''''''''''''
            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + dat + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
                if (NorBalance == 2)
            {
                OpB = OpB + Cr - Dr;
            }
            return OpB;
        }
        protected void BtnClick(object sender, EventArgs e)
        {

        //    var startDate = fromTxbx.Text;
          //  var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/PartyWiseItemSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode);

        }
    }
}