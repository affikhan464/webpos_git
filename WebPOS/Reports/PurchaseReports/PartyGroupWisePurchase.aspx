﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyGroupWisePurchase.aspx.cs" Inherits="WebPOS.Reports.PurchaseReports.PartyGroupWisePurchase" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Party Group Wise Purchase</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                    <div class="col col-12 col-sm-6 col-md-3">
								
								<select id="PartyGroupDD" class="dropdown">
									<option value="" class="selectCl" onclick="manInvSelection(this)">Select Party Group</option>
						        </select>
								
							</div>
                       
                      
						 <div class="col col-12 col-sm-6 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-1">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
                                    	<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Date
										</a>
									</div>
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
										Bill #
										</a>
									</div>
                                 
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Party Name
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Total
										</a>
									</div>
								
									
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Discount
										</a>
									</div>
                              <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Recieved
										</a>
									</div>
								<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Current Balance
										</a>
									</div>
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='thirteen'><span></span></div>
                                <div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalTotal'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalNetDiscount'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalPaid'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalCurrentBalance'/></div>
							
                            </div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	
    
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
       
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
                appendAttribute("PartyGroupDD", "PartyGroup");
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();
                $(".loader").show();
                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/PurchaseReports/PartyGroupWisePurchase.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "PGCode":  $("#PartyGroupDD .dd-selected-value").val()  }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='thirteen'><span>" + response.d.ListofInvoices[i].Date + "</span></div>" +
                                    "<div class='seven'><span>" + response.d.ListofInvoices[i].InvoiceNumber + "</span></div>" +
                                    "<div class='thirteen name'> <input name='PartyName'  id='newDescriptionTextbox_" + i + "' value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    "<div class='thirteen'>             <input name='Total' type='text' id='newQtyTextbox_" + i + "' value=" + parseFloat(response.d.ListofInvoices[i].Total).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='NetDiscount'  value=" + parseFloat(response.d.ListofInvoices[i].NetDiscount).toFixed(2) + " disabled/></div>" +
                                    "<div class='thirteen'>      <input name='Paid'  value=" + parseFloat(response.d.ListofInvoices[i].Paid).toFixed(2) + " disabled/></div>" +
                                    "<div class='thirteen'>      <input name='CurrentBalance'  value=" + parseFloat(response.d.ListofInvoices[i].CurrentBalance).toFixed(2) + " disabled/></div>" +

                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            } $(".loader").hide();
                            calculateData();
                         
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ['Total', 'Paid', 'CurrentBalance', 'NetDiscount'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (parseFloat(sum) + parseFloat(0));
                        } else {
                            sum = (parseFloat(sum) + parseFloat(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
