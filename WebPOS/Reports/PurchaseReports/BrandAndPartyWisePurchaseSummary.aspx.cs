﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class BrandAndPartyWisePurchaseSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode, brandCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode, string brandCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                           Select 
                              InvCode.Code  as Code , 
                Purchase2.Description as Description , 
                Sum(Purchase2.Qty) as Qty ,
                Sum(Purchase2.Amount) as Amount,

				Isnull((
                        Select Sum(PurchaseReturn2.Qty) from PurchaseReturn2
				        where Date1>='" + StartDate + @"'  
                        and Date1 <= '" + EndDate + @"' 
                        and PurchaseReturn2.Code=InvCode.Code 
                        group by PurchaseReturn2.Code,PurchaseReturn2.Description				
                ),0) as ReturnQty,				
                Isnull((
                        Select Sum(PurchaseReturn2.Amount) from PurchaseReturn2				
                        where Date1 >= '" + StartDate + @"'  
                        and Date1 <= '" + EndDate + @"' 
                        and PurchaseReturn2.Code=InvCode.Code  			
                        group by PurchaseReturn2.Code,PurchaseReturn2.Description				
                ),0) as ReturnAmount,		
                Isnull((
                        Select Sum(item.qty) from InvCode item			
                        where  item.Code=InvCode.Code  			
                ),0) as InHandQty 
                
                FROM Purchase1                 
                INNER JOIN Purchase2 ON Purchase1.InvNo = Purchase2.InvNo          
                inner join InvCode on Purchase2.Code=InvCode.Code 
                where  Purchase1.VenderCode = '" + partyCode + @"' 
                and InvCode.Brand= '" + brandCode + @"' 
                and Purchase1.CompID='" + CompID + @"' 
                and Purchase1.Dat>='" + StartDate + @"' 
                and Purchase1.Dat<='" + EndDate + @"' 
                GROUP BY InvCode.Code ,Purchase2.Description       
                Order by Purchase2.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                var Quantity = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);


                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
                invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var partyCode = txtPartyCode.Text;
            var itemCode = ""; //brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode + "&itemCode=" + itemCode);
        }
    }
}