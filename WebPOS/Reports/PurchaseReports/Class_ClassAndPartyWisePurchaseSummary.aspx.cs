﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class Class_ClassAndPartyWisePurchaseSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode, string ClassID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode, ClassID);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode, string ClassID)
        {


            SqlCommand cmd = new SqlCommand(@"select A.code,A.Description,
                                                    sum(A.pq) as [PurQty],
		                                            sum(A.pa) as [PurAmount],
		                                            sum(A.prq) as [PurReturnQty],
		                                            sum(A.pra) as [PurReturnAmount]
                                                        
                         FROM(SELECT Pur2.Code, Pur2.Description, Pur2.QTY[pq], Pur2.amount[pa], 0 as [prq], 0 as [pra]
                         FROM Purchase1 Pur1 inner JOIN Purchase2 Pur2 ON Pur1.invno = Pur2.invno
                            
                   where
                           Pur1.dat >= '" + StartDate + @"'
                           and Pur1.dat <='" + EndDate + @"' 
                           and Pur1.VenderCode = '" + partyCode + @"' 
                           
			   UNION ALL

		           SELECT PR2.Code, PR2.Description, 0 as [pq], 0 as [pa], PR2.QTY[prq], PR2.amount as [pra]
                           FROM PurchaseReturn2 PR2 inner join PurchaseReturn1 on PR2.invno=PurchaseReturn1.invno
                           inner join invcode on PR2.code=invcode.code
                 WHERE
                           PR2.date1 >='" + StartDate + @"'  
                           and PR2.date1 <='" + EndDate + @"'
                           and invcode.Class ='" + ClassID + @"' 
                           and PurchaseReturn1.PartyCode = '" + partyCode + @"'
                           ) AS A JOIN invcode InvC ON InvC.code = A.Code
                           and InvC.Class ='" + ClassID + @"' 
                           GROUP BY A.Code, A.Description , InvC.Qty ORDER BY A.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                var Quantity = Convert.ToInt32(dt.Rows[i]["PurQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["PurReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["PurReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["PurAmount"]);


                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
               // invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }

            return model;




        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            var itemCode = ""; //brandCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode + "&itemCode=" + itemCode);
        }
    }
}