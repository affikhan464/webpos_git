﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class ItemWisePurchaseSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string itemCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, itemCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string itemCode)
        {

            SqlCommand cmd = new SqlCommand(@"
					 Select 
						Purchase1.Dat as date,
						Purchase2.Code,
						Purchase2.Description,
					  
						Sum(Purchase2.Qty) as qty,
						Sum(Purchase2.Amount ) as Amount
						
						from Purchase1 
						inner join Purchase2 on Purchase1.InvNo=Purchase2.InvNo

						where Purchase2.Code = '" + itemCode + @"' and 
						Purchase1.CompID='" + CompID + @"' and 
						Purchase1.Dat>='" + StartDate + @"' and 
						Purchase1.Dat<='" + EndDate + @"' 
						GROUP BY Purchase1.Dat, Purchase2.Description,Purchase2.Code    
						order by Purchase2.Description         ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                invoice.Date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("dd/MMM/yyyy");
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = ItemCode.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}