﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class CustomerPhoneNumbersCR_A4 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string from, string to)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                                  
								  
								  
								  
								  
					var cmd = new SqlCommand(@"
                        Select   
                       
                        PartyCode.Mobile,
                        Invoice1.Phone,
                        Invoice1.Address,
                        PartyCode.Name

                        from Invoice1
                        inner join PartyCode on Invoice1.PartyCode=PartyCode.Code 
                        where  Invoice1.CompID='" + CompID + @"' 
                        and Invoice1.Date1>='" + fromDate + @"' 
                        and invoice1.Date1<='" + toDate + @"' 
                       
                        group by PartyCode.Name,PartyCode.Mobile,
                        Invoice1.Phone,
                        Invoice1.Address
                        order by PartyCode.Name", con);

                DataTable dt = new DataTable();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                dAdapter.Fill(dt);
                con.Close();
                var model = new PrintViewModel();
                var rows = new List<object>();

                
                model.DateFrom = fromDate.ToString("dd/MM/yyyy");
                model.DateTo = toDate.ToString("dd/MM/yyyy");
              

                var TotalQty = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    var Mobile = dt.Rows[i]["Mobile"].ToString();
                    var Name = dt.Rows[i]["Name"].ToString();
                    var Address = dt.Rows[i]["Address"].ToString();
                    var Phone = dt.Rows[i]["Phone"].ToString();

                    var row = new 
                    {
                        SNo = i + 1,
                        Name = Name,
                        Address = string.IsNullOrEmpty(Address) ?"--": Address,
                        Mobile = string.IsNullOrEmpty(Mobile)?"--":Mobile,
                        Phone = string.IsNullOrEmpty(Phone) ?"--": Phone,
                    };
                    rows.Add(row);
                }
                model.Rows = rows;
                model.TotalQty = TotalQty;
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}