﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class DaySaleGroupByItem2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);

        public static object SELECT { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            //        SqlCommand cmd = new SqlCommand(@"SELECT
            //            Invoice2.Code as Code , 
            //            Invoice2.Description as Description , 
            //            Sum(Invoice2.Qty) as Qty1 ,
            //            Sum(Invoice2.Amount) as Amount,

            //Isnull((Select Sum(SaleReturn2.Qty) from SaleReturn2
            //where  Date1>='" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description                group by SaleReturn2.Code,SaleReturn2.Description				),0) as ReturnQty,				Isnull((Select Sum(SaleReturn2.Amount) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0) as ReturnAmount,				(Sum(Invoice2.Qty)-Isnull((Select Sum(SaleReturn2.Qty) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0)) as NetQtySold				,				(Sum(Invoice2.Amount)-Isnull((Select Sum(SaleReturn2.Amount) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0)) as NetAmountSold,				Isnull((Select Sum(InvCode.qty) from InvCode				where InvCode.Code=Invoice2.Code and InvCode.Description=Invoice2.Description				),0) as InHandQty                              FROM Invoice1                 INNER JOIN invoice2 ON Invoice1.InvNo = Invoice2.InvNo                where Invoice1.CompID='" + CompID + "' and   Invoice2.ItemNature = 1  and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Code,Invoice2.Description                  order by Invoice2.Description     ", con);

            //DataTable dt = new DataTable();
            //using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ConnectionString))
                ////using (var cmd1 = new SqlCommand("ABC", con))
                ////using (var da = new SqlDataAdapter(cmd1))
                ////{
                ////    cmd1.CommandType = CommandType.StoredProcedure;
                ////    cmd1.Parameters.AddWithValue("@D1", StartDate);
                ////    cmd1.Parameters.AddWithValue("@D2", EndDate);
                ////    da.Fill(dt);
                ////}

                SqlCommand cmd = new SqlCommand("SELECT A.Code,C.Description,C.Qty as InHand,SUM(A.sq) AS[Qty1],SUM(A.sm) AS[Amount],SUM(A.rq) as [ReturnQty],   	SUM(A.rm) AS[ReturnAmount]     FROM(  SELECT V2.Code, V2.QTY[sq], V2.amount[sm], 0 as [rq], 0 as [rm]  FROM invoice1 V1 JOIN invoice2 V2 ON V1.invno = V2.invno  where v1.date1 between '" + StartDate + "' and '" + EndDate + "' UNION ALL  SELECT S2.Code, 0 as [sq], 0 as [sm], S2.QTY[r1], S2.amount as [rm] FROM salereturn2 S2  WHERE S2.date1 between '" + StartDate + "' and '" + EndDate + "' ) AS A JOIN invcode C ON C.code = A.Code GROUP BY A.Code, C.Description , C.Qty ORDER BY C.Description", con);

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Convert.ToString(dt.Rows[i]["Qty1"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                invoice.ReturnQty = Convert.ToString(dt.Rows[i]["ReturnQty"]);
                invoice.ReturnAmount = Convert.ToString(dt.Rows[i]["ReturnAmount"]);
                invoice.NetQtySold = Convert.ToString(Convert.ToDecimal( invoice.Quantity)- Convert.ToDecimal( invoice.ReturnQty));
                invoice.NetAmountSold = Convert.ToString(Convert.ToDecimal(invoice.Amount) - Convert.ToDecimal(invoice.ReturnAmount));
                invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHand"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItem2CR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}