﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class CustomerPhoneNumbers : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
         
            SqlCommand cmd = new SqlCommand(@"
                        Select   
                        PartyCode.Mobile,
                        Invoice1.Phone,
                        Invoice1.Address,
                        PartyCode.Name
                        
                        from Invoice1
                       
                        inner join PartyCode on Invoice1.PartyCode=PartyCode.Code 
                     
                        where  Invoice1.CompID='" + CompID + @"' 
                        and Invoice1.Date1>='" + StartDate + @"' 
                        and invoice1.Date1<='" + EndDate + @"' 
                     
                        group by PartyCode.Name,PartyCode.Mobile,
                        Invoice1.Phone,
                        Invoice1.Address
                        order by PartyCode.Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var Name = dt.Rows[i]["Name"].ToString();
                var Mobile = dt.Rows[i]["Mobile"].ToString();
                var Address = dt.Rows[i]["Address"].ToString();
                var Phone = dt.Rows[i]["Phone"].ToString();
                var invoice = new InvoiceViewModel();

                invoice.PartyName = Name;
                invoice.PhoneNumber = Phone;
                invoice.Address = Address;
                invoice.MobileNumber = Mobile;

                model.Add(invoice);
            }

            return model;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/AllCategorySaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate);

        }
    }
}