﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class BrandWiseSaleGroupByParty : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string PGCode, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, brandCode, PGCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string brandCode, string PGCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                        Select 
                          
                          InvCode.Description,
                          Sum(invoice2.Qty) as qty,
                          Sum(invoice2.Amount ) as Amount,
                          
                        (ISNULL((
                        Select
                            Sum(SaleReturn2.Amount)
                            from SaleReturn2
                            inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo 
                            inner join PartyCode as party on SaleReturn1.PartyCode=party.Code 
							inner join InvCode as item on SaleReturn2.Code=item.Code
                            where item.Code=InvCode.Code
							and   party.GroupID='" + PGCode + @"' 
                            and  item.Brand='" + brandCode + @"'
							
							and SaleReturn1.Date1>='" + StartDate + @"' 
							and SaleReturn1.Date1<='" + EndDate + @"'
                            group by item.Code),0))
                        as ReturnAmount,
                       
                        (ISNULL((Select Sum(SaleReturn2.Qty) 
                         from SaleReturn2
                            inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo 
                            inner join PartyCode as party on SaleReturn1.PartyCode=party.Code 
							inner join InvCode as item on SaleReturn2.Code=item.Code
                            where item.Code=InvCode.Code
							and   party.GroupID='" + PGCode + @"' 
                            and  item.Brand='" + brandCode + @"'
							
							and SaleReturn1.Date1>='" + StartDate + @"' 
							and SaleReturn1.Date1<='" + EndDate + @"'
                            group by item.Code),0))
                as ReturnQty 

                from Invoice2 
                inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo
                inner join InvCode on Invoice2.Code=InvCode.Code
                inner join PartyCode on Invoice1.PartyCode=PartyCode.Code
                where 
                
                InvCode.Brand='" + brandCode + @"'  
                and PartyCode.GroupID='" + PGCode + @"' 
                and Invoice1.CompID='" + CompID + @"' 
                and Invoice1.Date1>='" + StartDate + @"'  and Invoice1.Date1<='" + EndDate + @"' 
                Group by InvCode.Code, InvCode.Description  ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);
                var invoice = new InvoiceViewModel();
                var Quantity = Convert.ToInt32(dt.Rows[i]["qty"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);


                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
                model.Add(invoice);
            }

            return model;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = itemCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleGroupByPartyCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}