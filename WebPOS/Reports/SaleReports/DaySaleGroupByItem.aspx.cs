﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class DaySaleGroupByItem : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            
            SqlCommand cmd = new SqlCommand(@"SELECT  
                Invoice2.Code as Code , 
                Invoice2.Description as Description , 
                Sum(Invoice2.Qty) as Qty1 ,
                Sum(Invoice2.Amount) as Amount , 
                Sum(DealRs) as DealRs , 
                sum(Item_Discount) as Item_Discount1 
                FROM (Invoice1 
                INNER JOIN invoice2 ON Invoice1.InvNo = Invoice2.InvNo) 
                where Invoice1.CompID='" + CompID + "' and   Invoice2.ItemNature = 1  and Invoice1.Date1>='" + StartDate + "'                 and Invoice1.Date1<='" + EndDate + "'                 GROUP BY Invoice2.Code,Invoice2.Description                 order by Invoice2.Description     ", con);
            //SqlCommand cmd = new SqlCommand("Select Invoice1.Date1 as [Date],Invoice1.InvNo as Invoice , Invoice1.Name as Name,Invoice3.Total as Total,Invoice3.Discount1 as Discount,Invoice3.CashPaid as Paid,Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) where Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' order by Invoice1.InvNo", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode    = (dt.Rows[i]["Code"]).ToString();
                invoice.Description   = Convert.ToString(dt.Rows[i]["Description"]);
                invoice.Quantity     = Convert.ToString(dt.Rows[i]["Qty1"]);
                invoice.NetDiscount     = Convert.ToString(dt.Rows[i]["Item_Discount1"]);
                invoice.Amount        = Convert.ToString(dt.Rows[i]["Amount"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItemCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}