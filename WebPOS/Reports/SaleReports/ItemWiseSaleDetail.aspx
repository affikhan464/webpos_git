﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemWiseSaleDetail.aspx.cs" Inherits="WebPOS.Reports.SaleReports.ItemWiseSaleDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Item Wise Sale Detail</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" ClientIDMode="Static" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" ClientIDMode="Static" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                   
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Name</span>
								<input id="SearchBox"  data-id="itemTextbox" data-type="Item" data-function="GetRecords"  class="SearchBox autocomplete"  name="ItemDescription" type="text" />
							</div>
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Code</span>
                                <asp:TextBox ID="ItemCode" ClientIDMode="Static" CssClass="ItemCode"  name="ItemCode"  runat="server"></asp:TextBox>

							</div>
                          
                       
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
										Bill #
										</a>
									</div>
                                    <div class="seven phCol">
										<a href="javascript:void(0)"> 
											Date
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Client Name
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Quantity
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Rate
											
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Amount
										</a>
									</div>
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalQuantity'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalRate'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalAmount'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	  
    </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
     <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/SaleReports/ItemWiseSaleDetail.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "itemCode": $(".ItemCode").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='seven'><span>" + response.d.ListofInvoices[i].InvoiceNumber + "</span></div>" +
                                    "<div class='seven'><span>" + response.d.ListofInvoices[i].Date + "</span></div>" +
                                    "<div class='thirteen name'> <input name='PartyName'  id='newDescriptionTextbox_" + i + "' value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    "<div class='thirteen'>             <input name='Quantity' type='text' id='newQtyTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Quantity).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Rate'  id='newGAmountTextbox_" + i + "' value=" + Number(response.d.ListofInvoices[i].Rate).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Amount'  value=" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Quantity", 'Rate', 'Amount'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

       }
       
        $(document).on("click", "#print", function (e) {
            var url = "/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + $("#fromTxbx").val() + "&endDate=" + $("#toTxbx").val() + "&itemCode=" + $("#ItemCode").val();
            window.open(url, '_blank');
        });
   </script>
</asp:Content>
