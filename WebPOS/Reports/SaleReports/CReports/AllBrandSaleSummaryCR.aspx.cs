﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class AllBrandSaleSummaryCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            ModGL objModGL = new ModGL();
            var cr = new AllBrandSaleSummary();
            var StartDate = getDate(Request.QueryString["startDate"]);
            var EndDate = getDate(Request.QueryString["endDate"]);

            SqlCommand cmd = new SqlCommand(@"select A.Code,A.Name,
                                                            sum(A.sq) as [SaleQty],
		                                                    sum(A.sa) as [SaleAmount],
		                                                    sum(A.srq) as [SaleReturnQty],
		                                                    sum(A.sra) as [SaleReturnAmount]
                                                        
                                        FROM(SELECT brandname.Code, brandname.Name, Inv2.QTY[sq], Inv2.amount[sa], 0 as [srq], 0 as [sra]
                                        FROM invoice1 Inv1 inner JOIN invoice2 Inv2 ON Inv1.invno = Inv2.invno
                                            inner join invcode on inv2.code=invcode.code
                                            inner join brandname on invcode.brand=brandname.code
        
                                                        where

                                                            Inv1.date1 >= '" + StartDate + @"' 
                                                            and Inv1.date1 <='" + EndDate + @"' 
                                                            
                                                            

                                                        UNION ALL

                                                             SELECT brandname.Code, BrandName.Name, 0 as [sq], 0 as [sa], SR2.QTY[srq], SR2.amount as [sra]
                                                            FROM salereturn2 SR2 inner join SaleReturn1 on SR2.invno=salereturn1.invno
                                                            inner join invcode on SR2.code=invcode.code
                                                            inner join brandname on invcode.brand=brandname.code

                                                        WHERE

                                                            SR2.date1 >= '" + StartDate + @"'  
                                                            and SR2.date1 <='" + EndDate + @"'
                                                            

                                                            ) AS A JOIN invcode InvC ON InvC.code = A.Code

                                         

                                                             GROUP BY A.Code, A.Name  ORDER BY A.Name", con);



            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed)
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
            }

            dAdapter.Fill(dt);
            con.Close();

            var dset = new DataSet();

            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var Quantity = Convert.ToInt32(dt.Rows[i]["SaleQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["SaleAmount"]);
                var CompanyName = objModGL.CompanyName();
                var CompanyAddress1 = objModGL.CompanyAddress1();
                var CompanyAddress2 = objModGL.CompanyAddress2();
                var CompanyAddress3 = objModGL.CompanyAddress3();
                var CompanyAddress4 = objModGL.CompanyAddress4();
                var CompanyPhone = objModGL.CompanyPhone();


                var invoice = new
                {
                    SNo = i + 1,
                    ItemCode = (dt.Rows[i]["Code"]).ToString(),
                    Description = Convert.ToString(dt.Rows[i]["Name"]),
                    SoldQty = Quantity.ToString(),
                    SoldRs = Amount.ToString(),
                    ReturnedQty = ReturnQty.ToString(),


                    NetSoldQty = (Quantity - ReturnQty).ToString(),
                    NetSoldRs = (Amount - ReturnAmount).ToString(),
                    CompanyName = CompanyName,
                    CompanyAddress1 = CompanyAddress1,
                    CompanyAddress2 = CompanyAddress2,
                    CompanyAddress3 = CompanyAddress3,
                    CompanyAddress4 = CompanyAddress4,
                    CompanyPhone = CompanyPhone,
                    Heading1 = "Parties Balance Summary",
                    Heading2 = "All"
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}