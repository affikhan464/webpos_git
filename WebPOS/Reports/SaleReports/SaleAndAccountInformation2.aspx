﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="SaleAndAccountInformation2.aspx.cs" Inherits="WebPOS.Reports.SaleReports.SaleAndAccountInformation2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="brandWiseGroupByPartySaleSummary brandWiseSaleSummary reports searchAppSection">
				<div class="contentContainer">
					<h2>Sale And Account Information 2</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						    <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							  <div class="col col-12 col-sm-6 col-md-3">
                                  <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                        <div class="col col-12 col-sm-6 col-md-2">
								
								<select id="PartyGroupDD" class="dropdown ">
									<option value="" class="selectCl" onclick="manInvSelection(this)">Select Party Group</option>
									
								</select>
								
							</div>
                       
                       <div class="col col-12 col-sm-6 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-2">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="serialNum  phCol">
										<a href="javascript:void(0)"> 
										S.No.
										</a>
									</div>
                                 
									<div class="nine phCol" >                                                                    
										<a href="javascript:void(0)"> 
											Customer
										</a>
									</div>
									<div class="nine phCol">
										<a href="javascript:void(0)"> 
											Opening Balance
										</a>
									</div>
									<div class="nine phCol">
										<a href="javascript:void(0)"> 
										Total Sale
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
										Total Return
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
										Net Sale
										</a>
									</div>
								    <div class="nine phCol">
										<a href="javascript:void(0)"> 
										Total Received
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
										Total Payment
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
										Debit
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
										Credit
										</a>
									</div>
                                    <div class="nine phCol">
										<a href="javascript:void(0)"> 
											Closing Balance
										</a>
									</div>

								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='serialNumData'><span></span></div>
								<div class='nine'><span></span></div>
								<div class='nine'><input  disabled="disabled"  name='totalOpeningBalance'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalTotalSale'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalTotalReturn'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalNetSale'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalTotalReceived'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalTotalPayment'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalDebit'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalCredit'/></div>
                                <div class='nine'><input  disabled="disabled"  name='totalClosingBalance'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
       </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>"></script>
   
   <script type="text/javascript">

       var counter = 0;
       //$(".itemsSection").scroll(function () {
       //   

       //    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
       //        var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? 0 : $("#currentPage").val();
       //        currentPage = Number(currentPage) + 1;
       //        $("#currentPage").remove();
       //        $("<input hidden id='currentPage' value='" + currentPage + "' />").appendTo("body");
       //        getReport();
       //    }
       //});
            $(document).ready(function () {
                initializeDatePicker();
                appendAttribute("PartyGroupDD", "PartyGroup");
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                //clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();


               
                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();

               $.ajax({
                   url: '/Reports/SaleReports/SaleAndAccountInformation2.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "PGCode": $(".dd-selected-value").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='serialNumData'><span>" + counter + "</span></div>" +
                                    "<div class='nine name'><input name='PartyName'   value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    "<div class='nine'><input name='OpeningBalance'    value=" + Number(response.d.ListofInvoices[i].OpeningBalance).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='TotalSale'      value=" + Number(response.d.ListofInvoices[i].TotalSale).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='TotalReturn'      value=" + Number(response.d.ListofInvoices[i].TotalReturn).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='NetSale'    value=" + Number(response.d.ListofInvoices[i].NetSale).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='TotalReceived'      value=" + Number(response.d.ListofInvoices[i].TotalReceived).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='TotalPayment'      value=" + Number(response.d.ListofInvoices[i].TotalPayment).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='Debit'    value=" + Number(response.d.ListofInvoices[i].Debit).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='Credit'      value=" + Number(response.d.ListofInvoices[i].Credit).toFixed(2) + " disabled /></div>" +
                                    "<div class='nine'><input name='ClosingBalance'      value=" + Number(response.d.ListofInvoices[i].ClosingBalance).toFixed(2) + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            } $(".loader").hide();
                            calculateData();
                         
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
                $(".itemsSection .descriptionData.name").mouseenter(function (e) {

                    var text = $($(this).children()[0]).val();
                    $(".descriptionText").text(text);
                    $(".descriptionText").show();
                });
                $(" .itemsSection .descriptionData.name").mouseleave(function (e) {

                    $(".descriptionText").text("");
                    $(".descriptionText").hide();
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ['OpeningBalance', 'TotalSale', 'TotalReturn', 'NetSale', 'TotalReceived', 'TotalPayment', 'Debit', 'Credit', 'ClosingBalance'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
            
   </script>
</asp:Content>
