﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class DayCashAndCreditSale : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport( fromDate,  toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReportModel(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReportModal(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName();   
            SqlCommand cmd = new SqlCommand(
                "Select Invoice1.Date1 as [Date],Invoice1.InvNo as Invoice , Invoice1.Name as Name,Invoice1.PartyCode,Invoice3.Total as Total," +
                    "Invoice3.Discount1 as Discount,Invoice3.CashPaid as Paid,Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance " +
                "from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) " +
                "where Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' " +
                " order by Invoice1.InvNo ", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);


            var model = new List<InvoiceViewModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd-MMM-yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                invoice.Balance = Convert.ToString(dt.Rows[i]["Balance"]);
                invoice.CurrentBalance =Convert.ToString( objParty.PartyBalance(Convert.ToString(dt.Rows[i]["PartyCode"])));
                model.Add(invoice);
            }

            return model;



        }
        public static List<InvoiceViewModel> GetTheReportModal(DateTime StartDate, DateTime EndDate)
        {
            ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName();
            SqlCommand cmd = new SqlCommand(
                "Select Invoice1.Date1 as [Date],Invoice1.InvNo as Invoice , Invoice1.Name as Name,Invoice1.PartyCode,Invoice3.Total as Total," +
                    "Invoice3.Discount1 as Discount,Invoice3.CashPaid as Paid,Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance " +
                "from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) " +
                "where Invoice1.CompID='" + CompID + "' and Invoice3.CashPaid>0 and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' " +
                " order by Invoice1.InvNo ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            decimal TotalSale = 0;
            decimal NetDis = 0;
            decimal Paid = 0;
            decimal Balance = 0;
            var model = new List<InvoiceViewModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd-MMM-yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                invoice.Balance = Convert.ToString(dt.Rows[i]["Balance"]);
                invoice.CurrentBalance = Convert.ToString(objParty.PartyBalance(Convert.ToString(dt.Rows[i]["PartyCode"])));
                TotalSale += Convert.ToDecimal(invoice.Total);
                NetDis+= Convert.ToDecimal(invoice.NetDiscount);
                Paid+= Convert.ToDecimal(invoice.Paid);
                Balance+= Convert.ToDecimal(invoice.Balance);
                model.Add(invoice);
            }
            if (model.Any())
            {
                var Total = new InvoiceViewModel();

                //invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd-MMM-yyyy");
                //invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                Total.PartyName = "Total";
                Total.Total = Convert.ToString(TotalSale);
                Total.NetDiscount = Convert.ToString(NetDis);
                Total.Paid = Convert.ToString(Paid);
                Total.Balance = Convert.ToString(Balance);
                //invoice.CurrentBalance = Convert.ToString(objParty.PartyBalance(Convert.ToString(dt.Rows[i]["PartyCode"])));
                model.Add(Total);

            }



            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}