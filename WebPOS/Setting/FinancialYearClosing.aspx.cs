﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Setting
{
    public partial class FinancialYearClosing : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlTransaction tran;



        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SavePartiesClosing(string date ,string pin)
        {
            var convertedDate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
            ModYearClosing objModYearClosing = new ModYearClosing();
            if (pin == "101700862949411")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    objModYearClosing.PartiesClosing(convertedDate, con, tran);
                    // objModYearClosing.UpdateNextYearPartiesClosingBalance(convertedDate, con, tran);
                    //objModYearClosing.InventoryClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearQty(convertedDate, con, tran);
                    //objModYearClosing.GLClosing(convertedDate, con, tran);
                    //objModYearClosing.GLClosingPLS(convertedDate, con, tran);

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Year Close successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "invalid pin" };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveUpdateNextYearPartiesClosingBalance(string date, string pin)
        {
            var convertedDate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
            ModYearClosing objModYearClosing = new ModYearClosing();
            if (pin == "101700862949411")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    //objModYearClosing.PartiesClosing(convertedDate, con, tran);
                     objModYearClosing.UpdateNextYearPartiesClosingBalance(convertedDate, con, tran);
                    //objModYearClosing.InventoryClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearQty(convertedDate, con, tran);
                    //objModYearClosing.GLClosing(convertedDate, con, tran);
                    //objModYearClosing.GLClosingPLS(convertedDate, con, tran);

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Year Close successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "invalid pin" };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveInventoryClosing(string date, string pin)
        {
            var convertedDate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
            ModYearClosing objModYearClosing = new ModYearClosing();
            if (pin == "101700862949411")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    //objModYearClosing.PartiesClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearPartiesClosingBalance(convertedDate, con, tran);
                    objModYearClosing.InventoryClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearQty(convertedDate, con, tran);
                    //objModYearClosing.GLClosing(convertedDate, con, tran);
                    //objModYearClosing.GLClosingPLS(convertedDate, con, tran);

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Year Close successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "invalid pin" };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveUpdateNextYearQty(string date, string pin)
        {
            var convertedDate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
            ModYearClosing objModYearClosing = new ModYearClosing();
            if (pin == "101700862949411")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    //objModYearClosing.PartiesClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearPartiesClosingBalance(convertedDate, con, tran);
                    //objModYearClosing.InventoryClosing(convertedDate, con, tran);
                    objModYearClosing.UpdateNextYearQty(convertedDate, con, tran);
                    //objModYearClosing.GLClosing(convertedDate, con, tran);
                    //objModYearClosing.GLClosingPLS(convertedDate, con, tran);

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Year Close successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "invalid pin" };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveGLClosing(string date, string pin)
        {
            var convertedDate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
            ModYearClosing objModYearClosing = new ModYearClosing();
            if (pin == "101700862949411")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    //objModYearClosing.PartiesClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearPartiesClosingBalance(convertedDate, con, tran);
                    //objModYearClosing.InventoryClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearQty(convertedDate, con, tran);
                    objModYearClosing.GLClosing(convertedDate, con, tran);
                    //objModYearClosing.GLClosingPLS(convertedDate, con, tran);

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Year Close successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "invalid pin" };
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveGLClosingPLS(string date, string pin)
        {
            var convertedDate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
            ModYearClosing objModYearClosing = new ModYearClosing();
            if (pin == "101700862949411")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    //objModYearClosing.PartiesClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearPartiesClosingBalance(convertedDate, con, tran);
                    //objModYearClosing.InventoryClosing(convertedDate, con, tran);
                    //objModYearClosing.UpdateNextYearQty(convertedDate, con, tran);
                    //objModYearClosing.GLClosing(convertedDate, con, tran);
                    objModYearClosing.GLClosingPLS(convertedDate, con, tran);

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Year Close successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "invalid pin" };
            }

        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(Brand Brand)
        {

            try
            {
                var Id = Brand.Code;
                ModItem objModItem = new ModItem();
                var message = "";
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                SqlCommand cmdDelete1 = new SqlCommand("delete from Dept where id=" + Id + " and CompId = " + CompID + "", con, tran);
                cmdDelete1.ExecuteNonQuery();

                message = "Department Deleted!!";

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }

        }
    }

}