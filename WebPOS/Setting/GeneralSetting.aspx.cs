﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.ModelBinding;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;


namespace WebPOS
{
    public partial class GeneralSetting : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        



        Module4 objModule4 = new Module4();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //txtCurrentDate.Text = DateTime.Now.ToString("d");
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveCostingMethod(ModelSetting ModelSetting)
        {
            try
            {


                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string CGSMethod=ModelSetting.CGSMethod;
                


               
                if (CGSMethod == "FIFO")
                {
                    CGSMethod = "1";

                }
                else if (CGSMethod == "LIFO")
                {
                    CGSMethod = "2";

                }
                else if (CGSMethod == "Average")
                {
                    CGSMethod = "3";

                }
                else if (CGSMethod == "Fixed")
                {
                    CGSMethod = "4";

                }
                else if (CGSMethod == "IME")
                {
                    CGSMethod = "5";

                }
                else if (CGSMethod == "SP")
                {
                    CGSMethod = "6";

                }
                else if (CGSMethod == "LP")
                {
                    CGSMethod = "7";

                }






                SqlCommand cmd1 = new SqlCommand("UpDate Configuration set Valueno = '" + CGSMethod + "' where  CompID = '" + CompID + "' and Description = 'CGS_Method'", con, tran);
                cmd1.ExecuteNonQuery();
                

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "CGS Method Updated Successfully.", LastInvoiceNumber = "1" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveIMELEngth(ModelSetting ModelSetting)
        {
            try
            {


                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string IMEL1 = ModelSetting.IMEL1;
                string IMEL2 = ModelSetting.IMEL2;
                string IMEL3 = ModelSetting.IMEL3;
               

                SqlCommand cmd1 = new SqlCommand("UpDate Configuration set Valueno='" + IMEL1 + "' where  CompID='" + CompID + "' and Description='LenthOfSerialNo1'", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("UpDate Configuration set Valueno = '" + IMEL2 + "' where  CompID = '" + CompID + "' and Description = 'LenthOfSerialNo2'", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("UpDate Configuration set Valueno = '" + IMEL3 + "' where  CompID = '" + CompID + "' and Description = 'LenthOfSerialNo3'", con, tran);
                cmd3.ExecuteNonQuery();
                

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "IME Length Updated Successfully.", LastInvoiceNumber = "1" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelSetting ModelSetting)
        {
            try
            {

                
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string CompanyName = ModelSetting.CompanyName;
                string Address1 = ModelSetting.CompanyAddress1;
                string Address2 = ModelSetting.CompanyAddress2;
                string Address3 = ModelSetting.CompanyAddress3;
                string Address4 = ModelSetting.CompanyAddress4;
                string CompanyPhone = ModelSetting.CompanyPhone;

                SqlCommand cmd1 = new SqlCommand("Update Configuration set valueNo='" + CompanyName + "' where Description='CompanyName'", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("Update Configuration set valueNo='" + Address1 + "' where Description='Address_1'", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("Update Configuration set valueNo='" + Address2 + "' where Description='Address_2'", con, tran);
                cmd3.ExecuteNonQuery();

                SqlCommand cmd4 = new SqlCommand("Update Configuration set valueNo='" + Address3 + "' where Description='Address_3'", con, tran);
                cmd4.ExecuteNonQuery();
                SqlCommand cmd5 = new SqlCommand("Update Configuration set valueNo='" + Address4 + "' where Description='Address_4'", con, tran);
                cmd5.ExecuteNonQuery();
                SqlCommand cmd6 = new SqlCommand("Update Configuration set valueNo='" + CompanyPhone + "' where Description='Phone'", con, tran);
                cmd6.ExecuteNonQuery();



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Company Registered Successfully.", LastInvoiceNumber = "1" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveInvoiceFormat(ModelSetting ModelSetting)
        {
            try
            {


                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string InvoiceFormat = ModelSetting.InvoiceFormat;
                
                SqlCommand cmd1 = new SqlCommand("Update Configuration set valueNo='" + InvoiceFormat + "' where Description='InvoiceFormat'", con, tran);
                cmd1.ExecuteNonQuery();

                


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Invoice format updated Successfully.", LastInvoiceNumber = "1" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }







    }
}
