﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="GeneralSetting.aspx.cs" Inherits="WebPOS.GeneralSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="container">
        <div class="mt-5">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Company Information</h2>
                <div class="row">


                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtCompanyName" class="input__field input__field--hoshi PartyBox CompanyName  empt" autocomplete="off" data-type="Party" data-id="txtPartyName" data-function="GetParty" data-nextfocus=".CashPaid" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Company Name </span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">
                            <input id="txtAddress1" class=" CompanyAddress1 empt input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address1 </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">
                            <input id="txtAddress2" class=" CompanyAddress2 empt input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address2 </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">
                            <input id="txtAddress3" class=" CompanyAddress3 empt input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address3 </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">
                            <input id="txtAddress4" class=" CompanyAddress4 empt input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address4 </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtPhone" class=" Phone empt input__field input__field--hoshi" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Phone</span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class="btnSave btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>


    <div class="container">
        <div class="mt-5">
            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Invoice format</h2>
                <div class="row">
                    <div class="col-12">
                            <label>Invoice Format</label>

                        <span class="input input--hoshi">
                            <select id="InvoiceFormat" class="InvoiceFormat w-100 input__field input__field--hoshi">
                                <%for (int i = 1; i <= 30; i++)
                                { %>
                                    <option value="<%=i %>"><%=i %></option>
                                <% } %>
                            </select>
                            
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi"> </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBeforeInvoiceFormat()" class="btnSave btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

     <div class="container">
        <div class="mt-5">
            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Costing Format</h2>
                <div class="row">
                       <div class="col-2">
                               <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" ID="FIFO" value="FIFO"/> 
                                FIFO</label>
                        </div>
                        <div class="col-2">
                                <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" ID="LIFO" value="LIFO"/> 
                                LIFO</label>
                        </div>
                        <div class="col-2">
                                <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" id="Average" value="Average"/> 
                                Average</label>
                        </div>
                        <div class="col-2">
                                <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" ID="Fixed" value="Fixed"/> 
                                Fixed</label>
                        </div>
                        <div class="col-2">
                                <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" ID="IME" value="IME"/> 
                                IME</label>
                        </div>
                        <div class="col-2">
                                <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" ID="SP" value="SP"/> 
                                Selling Price</label>
                        </div>
                        <div class="col-2">
                                <label class="processLabel text-center"> 
                                <input type="radio" name="CostingMethod" ID="LP" value="LP"/> 
                                Last Purchase</label>
					    </div>

                        <div>
                            
                            
                        
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                            <a id="btnSave" onclick="saveCostingMethod()" class="btnSave btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                            </span>
 
                       </div>
                </div>
                </section>
            </div>
         </div>

 <div class="container">
        <div class="mt-5">
            <section class="form">
                <h2 class="form-header fa-money-bill-alt">IME Length</h2>
                <div class="row">
                    

                            <div class="col-2">
                                <label style="margin-top:1px;">Length 1</label>
                                </div>
                            <div class="col-2">
                                <input type="text"  class="form-control PartyBalance IMEL1 " id="txtIMELength1"  Style="text-align:right;"/>
                           </div>

                           <div class="col-2">
                                <label style="margin-top:1px;">Length 2</label>
                           </div>
                            <div class="col-2">
                                <input type="text"  class="form-control PartyBalance IMEL2 " id="txtIMELength2"  Style="text-align:right;"/>
                           </div>
                           <div class="col-2">
                                <label style="margin-top:1px;">Length 3</label>
                           </div>
                            <div class="col-2">
                                <input type="text"  class="form-control PartyBalance IMEL3 " id="txtIMELength3"  Style="text-align:right;"/>
                           </div>

                </div>
                            
                            
                        
                    
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="saveIMELength()" class="btnSave btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                 </section>
                </div>
     </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="Server">

    <script src="../Script/Voucher/SettingCompany.js"></script>

    <script>

        $(document).ready(function () {
            GetCompanyName()
            GetCompanyAddress1()
            GetCompanyAddress2()
            GetCompanyAddress3()
            GetCompanyAddress4()
            GetCompanyPhone()
            GetInvoiceFormat()
            GetImeLengthType()
            GetCostingMethod()

        });

         
         function GetCompanyName() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCompanyName',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                     $(".CompanyName").val(ModelSetting.CompanyName);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }

        function GetCompanyAddress1() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCompanyAddress1',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                     $(".CompanyAddress1").val(ModelSetting.CompanyAddress1);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function GetCompanyAddress2() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCompanyAddress2',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                     $(".CompanyAddress2").val(ModelSetting.CompanyAddress2);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function GetCompanyAddress3() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCompanyAddress3',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                     $(".CompanyAddress3").val(ModelSetting.CompanyAddress3);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
         function GetCompanyAddress4() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCompanyAddress4',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                    
                     $(".CompanyAddress4").val(ModelSetting.CompanyAddress4);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function GetCompanyPhone() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCompanyPhone',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                     $(".Phone").val(ModelSetting.CompanyPhone);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }


        
        function GetInvoiceFormat() {
               
            $.ajax({
                url: '/WebPOSService.asmx/GetInvoiceFormat',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                    $("#InvoiceFormat").val(ModelSetting.InvoiceFormat);
                   
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function GetImeLengthType() {
            
           
            $.ajax({
                url: '/WebPOSService.asmx/GetIMELengthSetting',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                    
                   
                    $("#txtIMELength1").val(ModelSetting.IMEL1);
                    $("#txtIMELength2").val(ModelSetting.IMEL2);
                    $("#txtIMELength3").val(ModelSetting.IMEL3);
                   
                      //updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
         function GetCostingMethod() {
            $.ajax({
                url: '/WebPOSService.asmx/GetCostingMethod',
                type: "Post",
                //data: { "ItemCode": ItemCode },
                success: function (ModelSetting) {
                   
                    
                    $("#" + ModelSetting.CGSMethod + "").attr('checked', true);
                      updateInputStyle()
                },
                fail: function (jqXhr, exception) {
                }
            });
        }


    </script>
</asp:Content>
