﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="FinancialYearClosing.aspx.cs" Inherits="WebPOS.Setting.FinancialYearClosing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-database abc">Financial Year Closing</h2>
                <div class="row">

                     <div class="col-12 col-lg-4">
                        <span class="input input--hoshi">
                            <input id="txtCurrentDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-lg-4">
                        <span class="input input--hoshi">
                            <input class="pinCode empty1 input__field input__field--hoshi" type="password" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Enter Pin</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-lg-4">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SavePartiesClosing()" class=" btn btn-3 btn-bm btn-3e fa-database w-100"><span>Close Year</span></a>
                        </span>
                    </div>
                </div>

                <hr />

                <div class="row">
                </div>

            </section>
        </div>

    </div>
    <div class="container">
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>

<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">


    <script type="text/javascript">

        function SavePartiesClosing() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                swal({
                    title: 'Are you sure to Close Year?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Yes Do It!!',
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            $.ajax({
                                url: "/Setting/FinancialYearClosing.aspx/SavePartiesClosing",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({date:$(".Date").val(), pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        //$(".pinCode").val("1");
                                        //updateInputStyle();
                                        //resolve();
                                        $(".abc").text("PartiesClosing=Ok"); 
                                        //swal("", BaseModel.d.Message, "success");
                                        
                                        SaveUpdateNextYearPartiesClosingBalance();
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })

            } else {
                swal("Enter Pin Please.", '', 'error');
            }
        }



        ////===============================================================================================================
        function SaveUpdateNextYearPartiesClosingBalance() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                
                            $.ajax({
                                url: "/Setting/FinancialYearClosing.aspx/SaveUpdateNextYearPartiesClosingBalance",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({date:$(".Date").val(), pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        //$(".pinCode").val("1");
                                        //updateInputStyle();
                                        //resolve();
                                        $(".abc").text("PartiesClosing=Ok/Update Ok.")
                                        //swal("", BaseModel.d.Message, "success");
                                        SaveInventoryClosing();
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }
        ////===============================================================================================================
        function SaveInventoryClosing() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                
                            $.ajax({
                                url: "/Setting/FinancialYearClosing.aspx/SaveInventoryClosing",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({date:$(".Date").val(), pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        //$(".pinCode").val("1");
                                        //updateInputStyle();
                                        //resolve();
                                        $(".abc").text("PartiesClosing=Ok/Update Ok./Inventory Closing Ok.")
                                        //swal("", BaseModel.d.Message, "success");
                                        SaveUpdateNextYearQty();
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                       

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }
        ////===============================================================================================================
        function SaveUpdateNextYearQty() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                
                            $.ajax({
                                url: "/Setting/FinancialYearClosing.aspx/SaveUpdateNextYearQty",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({date:$(".Date").val(), pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        //$(".pinCode").val("1");
                                        //updateInputStyle();
                                        //resolve();
                                        $(".abc").text("PartiesClosing=Ok/Update Ok./Inventory Closing Ok./Update Next Year Qty Ok")
                                        //swal("", BaseModel.d.Message, "success");
                                        SaveGLClosing();
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }
        ////===============================================================================================================
        function SaveGLClosing() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                
                            $.ajax({
                                url: "/Setting/FinancialYearClosing.aspx/SaveGLClosing",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({date:$(".Date").val(), pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        //$(".pinCode").val("1");
                                        //updateInputStyle();
                                        //resolve();
                                        $(".abc").text("PartiesClosing=Ok/Update Ok./Inventory Closing Ok./UpdateNext Year Qty Ok/Gl Closing Ok.")
                                        //swal("", BaseModel.d.Message, "success");
                                        SaveGLClosingPLS();
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }
        ////===============================================================================================================
        function SaveGLClosingPLS() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                
                            $.ajax({
                                url: "/Setting/FinancialYearClosing.aspx/SaveGLClosingPLS",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({date:$(".Date").val(), pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        //$(".pinCode").val("1");
                                        //updateInputStyle();
                                        //resolve();
                                        $(".abc").text("PartiesClosing=Ok/Update Ok./Inventory Closing Ok./UpdateNext Year Qty Ok/Gl Closing Ok./PLS Closing Ok")
                                        swal("", BaseModel.d.Message, "success");
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }
        ////===============================================================================================================



    </script>
</asp:Content>
