﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="EmptyDatabase.aspx.cs" Inherits="WebPOS.Setting.EmptyDBAll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-database">Empty Database</h2>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input class="pinCode empty1 input__field input__field--hoshi" type="password" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Enter Pin</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-database w-100"><span>Empty Database</span></a>
                        </span>
                    </div>
                </div>

                <hr />

                <div class="row">
                </div>

            </section>
        </div>

    </div>
    <div class="container">
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>

<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">


    <script type="text/javascript">

        function save() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                swal({
                    title: 'Are you sure to empty database?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Yes Do It!!',
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            $.ajax({
                                url: "/Setting/EmptyDBAll.aspx/Save",
                                async: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({ pin: pinCode }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        $(".pinCode").val("");
                                        //updateInputStyle();
                                        //resolve();
                                        swal("", BaseModel.d.Message, "success");
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }

    </script>
</asp:Content>
