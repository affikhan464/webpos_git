﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Setting
{
    public partial class EmptyDBAll : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlTransaction tran;



        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(string pin)
        {

            ModEmptyDB objModEmptyDB = new ModEmptyDB();
            if (pin == "10170086294786")
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();


                    objModEmptyDB.EmptyDBAlll(con, tran);



                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Databases has been emptied successfully." };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }else
                {
                    return new BaseModel() { Success = false, Message = "invalid pin" };
            }

            ////////if (con.State == ConnectionState.Closed) { con.Open(); }
            ////////tran = con.BeginTransaction();


            ////////objModEmptyDB.EmptyDBAlll(con, tran);



            ////////tran.Commit();
            ////////con.Close();
            ////////return new BaseModel() { Success = true, Message = "Database has been emptied successfully." };










        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(Brand Brand)
        {

            try
            {
                var Id = Brand.Code;
                ModItem objModItem = new ModItem();
                var message = "";
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                SqlCommand cmdDelete1 = new SqlCommand("delete from Dept where id=" + Id + " and CompId = " + CompID + "", con, tran);
                cmdDelete1.ExecuteNonQuery();

                message = "Department Deleted!!";

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }

        }
    }

}