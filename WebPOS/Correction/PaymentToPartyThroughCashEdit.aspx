﻿<%@ page title="" language="C#" masterpagefile="~/masterpage.Master" autoeventwireup="true" codebehind="PaymentToPartyThroughCashEdit.aspx.cs" inherits="WebPOS.PaymentToPartyThroughCashEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>" />
    <link href="../../css/allitems.css" rel="stylesheet" />
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="Server">
      <input type="hidden"  id="isLastVoucher"/>
        <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-edit">Edit Cash Payment</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" autocomplete="off" type="text" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Voucher</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 d-flex" >
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PreviousBtn" onclick="GetPreviousVoucherData()" data-toggle="tooltip" title="Previous Voucher"><i class="fas fa-chevron-circle-left"></i></a>
                        </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  data-toggle="tooltip" title="Get Voucher" onclick="GetVoucherData()"><i class="fas fa-spinner"></i></a>
                        </span>
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="NextBtn" onclick="GetNextVoucherData()" data-toggle="tooltip" title="Next Voucher"><i class="fas fa-chevron-circle-right"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                    
                        <span class="input input--hoshi">
                            <input id="txtCurrentDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 d-flex" >
                        <span class="input input--hoshi">
                            <a onclick="deleteVoucher()" class=" btn btn-3 btn-bm btn-3e fa-trash w-100">Delete</a>
                        </span>
                        
                        
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyName" data-type="Party" data-nextfocus=".CashPaid" data-id="txtPartyName" data-function="GetParty" name="PartyName" class=" PartyBox PartyName autocomplete empt input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyCode" disabled="disabled" name="PartyCode" class="input__field input__field--hoshi PartyCode empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtAddress" class="input__field input__field--hoshi PartyAddress empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="Client_RadioButton" checked="checked" class="radio" name="PartiesRB" type="radio" />
                                <span></span>
                                Client
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="Supplier_RadioButton" class="radio" name="PartiesRB" type="radio" />
                                <span></span>
                                Vender
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Cash Paid</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="OldBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Old Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>
                    
                </div>

                <hr />

                <div class="row  ">
                    
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-2">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-8 justify-content-end">
                      
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-2 justify-content-end">
                       </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  data-toggle="tooltip" title="Get Last 100 payments detail" onclick="LoadPaymentDetail()"><i class="fas fa-spinner"></i></a>
                        </span>
                    </div>

                </div>
               

            </section>
        </div>
    </div>
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                    <div class="seven phCol" style="width:30%!important">
										<a href="javascript:void(0)"> 
											S. No.
										</a>
									</div>
									<div class="seven phCol" style="width:50%!important">
										<a href="javascript:void(0)"> 
											Date
										</a>
									</div>
									<div class="thirteen phCol" style="width:300%!important">
										<a href="javascript:void(0)"> 
											Party Name
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Voucher No.
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Amount
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											
											
										</a>
									</div>
									
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven' style='width:30%!important'><span></span></div>
								<div class='seven' style='width:50%!important'><span></span></div>
								<div class='thirteen' style='width:300%!important'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='a'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalPaid'/></div>
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->
	
				</div> 
    </asp:content>



<asp:content runat="server" id="content4" contentplaceholderid="Scripts">

    <script src="../Script/Autocomplete.js"></script> 
    <script src="../Script/Voucher/PaymentToPartyCashEdit_Save.js"></script>
   
  
</asp:content>
