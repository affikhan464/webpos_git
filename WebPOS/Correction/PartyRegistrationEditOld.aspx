﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyRegistrationEditOld.aspx.cs" Inherits="WebPOS.PartyRegistrationEditOld" %>

<%--  <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %> --%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/allitems.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <br />
                       <h2 style="text-align:center; font-size:30px; " > Edit Party Registration </h2>  
    <br />
                       
                                                                              
    <br /> 
           <asp:Label Text="Party Code" ID="Label2" runat="server"  Style="margin-left:290px;"></asp:Label>           
                                    <input type="text" id="txtPartyCode" disabled="disabled" class="PartyCode empty1" style="width: 174px; height: 15px;margin-left:25px;" />
    <br />
                                    <input type="radio" id="Client"   name ="NorBalance" value="1" style="margin-left:392px;" checked="checked"/> Client
                                    <input type="radio" id="Supplyer"   name ="NorBalance" value="2" /> Supplyer
                                    
    <br />
           <asp:Label Text="Name" ID="Label1" runat="server"  Style="margin-left:290px;"></asp:Label>               
                                    <input type="text" id="txtPartyName" class="PartyBox PartyName autocomplete empty1" data-type="Party" data-id="txtPartyName" data-function="GetParty" style="width: 500px; height: 20px;margin-left:59px;margin-top:2px;"/>
                                    <input type="button" id="btnEdit" class="btnReset" value="reset" onclick="empty1()"  />
    <br />      
          <asp:Label Text="Contact Person" ID="Label3" runat="server"  Style="margin-left:290px;"></asp:Label>           
                                    <input type="text" id="txtContactPerson" class="ContactPerson empty1" style="width: 500px; height: 20px;margin-left:0px; margin-top:2px;" />

    <br />      
          <asp:Label Text="Mobile No." ID="Label4" runat="server"  Style="margin-left:290px;"></asp:Label>        
                                    <input type="text" id="txtMobileNo" class="MobileNo empty1" style="width: 400px; height: 20px;margin-left:23px;margin-top:2px;" />
    <br />
        <asp:Label Text="Fax No" ID="Label5" runat="server"  Style="margin-left:290px;"></asp:Label>      
                               <input type="text" id="txtFaxNo" class="FaxNo empty1" style="width: 400px; height: 20px;margin-left:49px;margin-top:2px;" /> 
    <br />
        <asp:Label Text="Land Line No." ID="Label6" runat="server"  Style="margin-left:290px;"></asp:Label>              
                            <input type="text" id="txtLandLineNo" class="LandLineNo empty1" style="width: 400px; height: 20px;margin-left:3px;margin-top:2px;" />
    <br />
    <asp:Label Text="Address" ID="Label7" runat="server"  Style="margin-left:290px;"></asp:Label>                 
                            <input type="text" id="txtAddress" class="Address empty1" style="width: 500px; height: 20px;margin-left:45px;margin-top:2px;" />
    <br />
    <asp:Label Text="E-mail" ID="Label8" runat="server"  Style="margin-left:290px;"></asp:Label>                 
                            <input type="text" id="txtEmail" class="Email empty1" style="width: 300px; height: 20px;margin-left:53px;margin-top:2px;" />
    <br />
    <asp:Label Text="Selling Price" ID="Label9" runat="server"  Style="margin-left:290px;"></asp:Label>  
                            <select id="lstSellingPriceNo" style="list-style-type:disc;width:50px; margin-left:15px;margin-top:2px;">
                                    </select>
    <br />
    <asp:Label Text="Deal Apply #" ID="Label14" runat="server"  Style="margin-left:290px;"></asp:Label>  
                            <select id="lstDealApplyNo" style="list-style-type:disc;width:50px; margin-left:11px;margin-top:2px;">
                                    </select>
    <br />
    <asp:Label Text="Credit Limit" ID="Label10" runat="server"  Style="margin-left:290px;"></asp:Label> 
                            <input type="text" id="txtCreditLimit" class="CreditLimit empty1" style="width: 175px; height: 20px;margin-left:17px;margin-top:2px;" />                    
                           
    <label><input type="checkbox" id="chkCreditLimitApply" style="margin-left:10px;"/>Credit Limit Apply</label>
    <br />
    
    <asp:Label Text="Other Info" ID="Label12" runat="server"  Style="margin-left:290px;"></asp:Label>                     
                            <input type="text" id="txtOtherInfo" class="OtherInfo empty1" style="width: 500px; height: 20px;margin-left:30px;margin-top:2px;" />
    <br />
    <asp:Label Text="SaleMan" ID="Label13" runat="server"  Style="margin-left:290px;"></asp:Label>   
                            <select id="lstSaleMan" style="list-style-type:disc;width:181px; margin-left:40px;margin-top:2px;"></select>                  
       <br />   
    <label><input type="checkbox" id="chkDeal" style="margin-left:390px;"/>Deal</label>
    <br />
    <label><input type="checkbox" id="chkPerDiscount" style="margin-left:390px;"/>Per. Discount</label>
     <br />
     <center>                       <input type="button" id="btnSave" class="btnSave" value="save" onclick="SaveBefore()" />
                                    <input type="button" id="btnDelete" class="btnDelete" value="delete" onclick="deleteParty()" />
                                    
     </center>
   
    
    <script src="../Script/jquery.min.js"></script>
    <script src="../Script/jquery-ui.min.js"></script>
    
    <script src="../Script/Autocomplete.js"></script> 

    <script type="text/javascript">
        function SaveBefore() {
            var NorBalance = 0;
            NorBalance = document.getElementById("Client").checked ? "1" : "2";
            var PartyName = $(".PartyName").val();
            if (NorBalance>0 && PartyName!=""){
                save();
                if (NorBalance = 0) { alert("Define Nature"); }
            } 


            


        }
        function empty1() {
            $(".empty1").val("");
            document.getElementById("chkCreditLimitApply").checked = false;
            document.getElementById("chkPerDiscount").checked = false;
            document.getElementById("Supplyer").checked = false;
            document.getElementById("Client").checked = false;
            document.getElementById("chkDeal").checked = false;
        }
        function save() {
            var NorBalance = document.getElementById("Client").checked ? "1" : "2";
            var PartyCode=$(".PartyCode").val();
            var PartyName = $(".PartyName").val();
            var ContactPerson = $(".ContactPerson").val();
            var MobileNo = $(".MobileNo").val();
            var FaxNo = $(".FaxNo").val();
            var LandLineNo = $(".LandLineNo").val();
            var Address = $(".Address").val();
            var Email = $(".Email").val();
            var lstSellingPriceNo = $("#lstSellingPriceNo").val();
            var lstDealApplyNo = $("#lstDealApplyNo").val();
            var CreditLimit = $("#txtCreditLimit").val();
            var chkCreditLimitApply = document.getElementById("chkCreditLimitApply").checked ? "1" : "0";
            

            var OtherInfo = $(".OtherInfo").val();
            var lstSaleMan = $("#lstSaleMan").val();
            
            var chkDeal = document.getElementById("chkDeal").checked ? "1" : "0";
            var chkPerDiscount = document.getElementById("chkPerDiscount").checked ? "1" : "0";


            if (CreditLimit == "") { CreditLimit = 0 }
            CreditLimit = isNaN(CreditLimit) ? 0 : CreditLimit;
            
            var PartyModel = {
                NorBalance: NorBalance,
                PartyCode:PartyCode,
                 PartyName: PartyName,
                 ContactPerson: ContactPerson,
                 MobileNo: MobileNo,
                 FaxNo: FaxNo,
                 LandLineNo: LandLineNo,
                 Address: Address,
                 Email: Email,
                 lstSellingPriceNo: lstSellingPriceNo,
                 lstDealApplyNo: lstDealApplyNo,
                 CreditLimit: CreditLimit,
                 CreditLimitApply: chkCreditLimitApply,
                 OtherInfo: OtherInfo,
                 lstSaleMan: lstSaleMan,
                 chkDeal: chkDeal,
                 chkPerDiscount: chkPerDiscount 
            }
            debugger
            $.ajax({
                url: "/Correction/PartyRegistrationEdit.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ PartyModel: PartyModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        empty1();
                        alert(BaseModel.d.Message);
                    }
                    else {
                        alert(BaseModel.d.Message);
                    }


                }

            });

        }
        function save() {
            var NorBalance = document.getElementById("Client").checked ? "1" : "2";
            var PartyCode = $(".PartyCode").val();
            var PartyName = $(".PartyName").val();
            var ContactPerson = $(".ContactPerson").val();
            var MobileNo = $(".MobileNo").val();
            var FaxNo = $(".FaxNo").val();
            var LandLineNo = $(".LandLineNo").val();
            var Address = $(".Address").val();
            var Email = $(".Email").val();
            var lstSellingPriceNo = $("#lstSellingPriceNo").val();
            var lstDealApplyNo = $("#lstDealApplyNo").val();
            var CreditLimit = $("#txtCreditLimit").val();
            var chkCreditLimitApply = document.getElementById("chkCreditLimitApply").checked ? "1" : "0";


            var OtherInfo = $(".OtherInfo").val();
            var lstSaleMan = $("#lstSaleMan").val();

            var chkDeal = document.getElementById("chkDeal").checked ? "1" : "0";
            var chkPerDiscount = document.getElementById("chkPerDiscount").checked ? "1" : "0";


            if (CreditLimit == "") { CreditLimit = 0 }
            CreditLimit = isNaN(CreditLimit) ? 0 : CreditLimit;

            var PartyModel = {
                NorBalance: NorBalance,
                PartyCode: PartyCode,
                PartyName: PartyName,
                ContactPerson: ContactPerson,
                MobileNo: MobileNo,
                FaxNo: FaxNo,
                LandLineNo: LandLineNo,
                Address: Address,
                Email: Email,
                lstSellingPriceNo: lstSellingPriceNo,
                lstDealApplyNo: lstDealApplyNo,
                CreditLimit: CreditLimit,
                CreditLimitApply: chkCreditLimitApply,
                OtherInfo: OtherInfo,
                lstSaleMan: lstSaleMan,
                chkDeal: chkDeal,
                chkPerDiscount: chkPerDiscount
            }
            debugger
            $.ajax({
                url: "/Correction/PartyRegistrationEdit.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ PartyModel: PartyModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        empty1();
                        alert(BaseModel.d.Message);
                    }
                    else {
                        alert(BaseModel.d.Message);
                    }


                }

            });

        }
        function deleteParty() {
            
            var PartyCode = $(".PartyCode").val();
            

            var PartyModel = {
                PartyCode: PartyCode
                     }
            debugger
            $.ajax({
                url: "/Correction/PartyRegistrationEdit.aspx/deleteParty",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ PartyModel: PartyModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        empty1();
                        alert(BaseModel.d.Message);
                    }
                    else {
                        alert(BaseModel.d.Message);
                    }


                }

            });

        }
        $(document).ready(function () {
            GetSaleManList();
            GetSellingPriceList();
            GetDealApplyNoList();
        });
        function GetPartyData() {
            var PartyCode = $(".PartyCode").val();
            $.ajax({
                url: '/WebPOSService.asmx/GetPartyDataAgainstPartyCode',
                type: "Post",
                data: { "PartyCode": PartyCode },
                success: function (PartyModel) {
                    var PCode = $(".PartyCode").val();
                    empty1();
                    $(".PartyCode").val(PartyCode);

                    document.getElementById("Client").checked = true;
                    var NorBalance = PartyModel.NorBalance;
                    if (NorBalance == 2) { document.getElementById("Supplyer").checked = true; }
                    
                    
                    document.getElementById("chkCreditLimitApply").checked = false;
                    if (PartyModel.CreditLimitApply == 1) { document.getElementById("chkCreditLimitApply").checked = true;}
                    
                    document.getElementById("chkDeal").checked = false;
                    if (PartyModel.chkDeal == 1) { document.getElementById("chkDeal").checked = true; }

                    document.getElementById("chkPerDiscount").checked=false;
                    
                    if (PartyModel.chkPerDiscount == 1) { document.getElementById("chkPerDiscount").checked = true; }

                    


                    $(".PartyName").val(PartyModel.PartyName);
                    $(".ContactPerson").val(PartyModel.ContactPerson);
                    $("#txtMobileNo").val(PartyModel.MobileNo);
                    
                    $(".FaxNo").val(PartyModel.FaxNo);
                    $(".LandLineNo").val(PartyModel.LandLineNo);
                    $(".Address").val(PartyModel.Address);
                    $(".Email").val(PartyModel.Email);
                    $("#lstSellingPriceNo").val(PartyModel.lstSellingPriceNo);
                    $("#lstDealApplyNo").val(PartyModel.lstDealApplyNo);
                    $("#txtCreditLimit").val(PartyModel.CreditLimit);
                    
                    $(".OtherInfo").val(PartyModel.OtherInfo);
                    //$("#lstSaleMan").sele\ .val(PartyModel.lstSaleMan);
                    //alert(PartyModel.lstSaleMan);
                    
                    
                },
                fail: function (jqXhr, exception) {
                }
            });
        }

        function GetSaleManList() {
            $.ajax({
                url: '/WebPOSService.asmx/SaleMan_List',
                type: "Post",
                success: function (BrandList) {
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        if (code == 1) {
                            $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstSaleMan");
                        } else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstSaleMan");
                        }
                    }
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function GetSellingPriceList() {

            for (var i = 1; i < 6; i++) {
                var code = i;
                var name = i;
                $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstSellingPriceNo");

            }
        }
        function GetDealApplyNoList() {

            for (var i = 1; i < 4; i++) {
                var code = i;
                var name = i;
                $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstDealApplyNo");
            }
        }

    </script>
    
                                     
</asp:Content>
