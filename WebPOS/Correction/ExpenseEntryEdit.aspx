﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ExpenseEntryEdit.aspx.cs" Inherits="WebPOS.ExpenseEntryEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
   <input type="hidden"  id="isLastVoucher"/>
        
        <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-edit">Edit - Expence Entry</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" autocomplete="off" type="text" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Voucher</span>
                            </label>
                        </span>
                    </div>
                   <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 d-flex" >
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PreviousBtn" onclick="GetPreviousVoucherData()" data-toggle="tooltip" title="Previous Voucher"><i class="fas fa-chevron-circle-left"></i></a>
                        </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  data-toggle="tooltip" title="Get Voucher" onclick="GetVoucherData()"><i class="fas fa-spinner"></i></a>
                        </span>
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="NextBtn" onclick="GetNextVoucherData()" data-toggle="tooltip" title="Next Voucher"><i class="fas fa-chevron-circle-right"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="txtDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 d-flex" >
                        <span class="input input--hoshi">
                            <a onclick="deleteVoucher()" class=" btn btn-3 btn-bm btn-3e fa-trash w-100">Delete</a>
                        </span>
                        
                        
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">

                            <input  data-nextfocus=".CashPaid" data-id="expenceTextbox" data-type="ExpenceHead" data-function="GLList" data-glcode="0104" class="autocomplete ExpenceHeadTitle empt input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Expence Head</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">

                            <input id="txtCode" class="input__field input__field--hoshi ExpenceHeadCode clearable empt BankCode" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Expence Head Code</span>
                            </label>
                        </span>
                    </div>
                   
                    
                     <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Cash</span>
                            </label>
                        </span>
                    </div>

                  
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">

                            <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>
                </div>
               

                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-2">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-8 justify-content-end">
                      
                    </div>
                     <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-2 justify-content-end">
                       </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  data-toggle="tooltip" title="Get Last 100 voucher detail" onclick="LoadPaymentDetail()"><i class="fas fa-spinner"></i></a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

      <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                    <div class="seven phCol" style="width:30%!important">
										<a href="javascript:void(0)"> 
											S. No.
										</a>
									</div>
									<div class="seven phCol" style="width:70%!important">
										<a href="javascript:void(0)"> 
											Date
										</a>
									</div>
									<div class="thirteen phCol" style="width:200%!important">
										<a href="javascript:void(0)"> 
											Expence GL Title
										</a>
									</div>
									<div class="thirteen phCol" style="width:250%!important">                                                                    
										<a href="javascript:void(0)"> 
											Narration
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Voucher #
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Amount
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											
											
										</a>
									</div>
									
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven' style='width:30%!important'><span></span></div>
								<div class='seven' style='width:150%!important'><span></span></div>
								<div class='thirteen' style='width:250%!important'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='a'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalPaid'/></div>
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->
	
				</div> 
  
    </asp:Content>
    <asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
     <script src="../Script/Autocomplete.js"></script> 
     <%--<script src="../Script/SearchExpHeadAutoComplete.js"></script>--%> 
    
    <script src="../Script/Voucher/ExpenceCashEdit_Save.js"></script>
    
     
    <script type="text/javascript">
    

      

       
        

     </script>


</asp:Content>
