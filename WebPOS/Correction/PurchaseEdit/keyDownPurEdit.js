﻿$(document).on('keydown', ".Recieved", function (e) {
    //Ctrl + end
    if (e.which == 13) {
        SaveBefore();
        e.preventDefault();
    }
});



$(document).on("keypress", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();
    if (e.which == 13) {
        e.preventDefault();
        if (txbxName == "Qty") {
            $("#myTable ." + sno + "_Rate").focus();
            $("#myTable ." + sno + "_Rate").select();
        }
        else if (txbxName == "Rate") {
            $("#myTable ." + sno + "_PerDis").focus();
            $("#myTable ." + sno + "_PerDis").select();
        }
        else if (txbxName == "PerDis") {
            $("#myTable ." + sno + "_DealDis").focus();
            $("#myTable ." + sno + "_DealDis").select();
        }
        else if (txbxName == "DealDis") {

            $(".SearchBox").focus();
            $(".SearchBox").val("");
        }
        else if (txbxName == "ImeiTxbx") {
            
            
            appendRowInIMEI(this);

            $(`#myTable .${sno}_ScannedItem`).val("").focus();
   
        }
        calculationSale();
        updateIMETable(ItemCode, sno);
        calculationSale();
    }
    else {
        return true;
    }
});

$(document).on("input", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();

    calculationSale();
    updateIMETable(ItemCode, sno);
    calculationSale();
    updateRowRateDisDeal(sno);
});

$(document).on("click", "#myTable .fas.fa-times", function () {

    swal({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Remove',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                this.parentElement.parentElement.remove();
                var itemCode = this.dataset.itemcode;
                var itemRowNumber = $(this).parents("tr").data("rownumber");
                removeIMEs(itemCode, itemRowNumber);
                calculationSale();
                rerenderIMEITableSerialNumber();
                rerenderSerialNumber();
                
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })


});

$(document).on("click", "#IMEITable .fas.fa-times", function () {


    swal({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Remove',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                
                var itemCode = $(this).parents("tr").data("itemcode");
                var rate = $(this).parents("tr").data("rate");
                var perdis = $(this).parents("tr").data("perdis");
                var dealrs = $(this).parents("tr").data("dealrs");
                var itemRowNumber = $(this).parents("tr").data("rownumber");
                this.parentElement.parentElement.remove();

                rerenderIMEITableSerialNumber();
                ScanedQtyTotal(itemCode, rate, perdis, dealrs, itemRowNumber)

                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })

});
$("#myTable").keydown(function (e) {

    var A_Key = 65;
    if (e.which === A_Key && e.ctrlKey) {
        e.preventDefault();

        var rowNumber = $(e.target).parents("tr").data().rownumber;
        copyItem(rowNumber);
    }
});


function copyItem(rowNumber) {

    var lastRowNumber = $("#myTable tr:last-child").data().rownumber;
    var selectedRow = $("#myTable tr[data-rownumber=" + rowNumber + "]");
    var ItemCode = selectedRow.find("[name=Code]").val();
    var ItemName = selectedRow.find("[name=ItemName]").val();
    var Rate = selectedRow.find("[name=Rate]").val();
    var avrgCost = selectedRow.find("." + rowNumber + "_AverageCost").val();
    var revCode = selectedRow.find("." + rowNumber + "_RevCode").val();
    var cgsCode = selectedRow.find("." + rowNumber + "_CgsCode").val();
    var actualSellingPrice = selectedRow.find("." + rowNumber + "_ActualSellingPrice").val();
    var Qty = selectedRow.find("[name=Qty]").val();
    var Amount = selectedRow.find("[name=Amount]").val();
    var NetAmount = selectedRow.find("[name=NetAmount]").val();
    var PurAmount = selectedRow.find("[name=PurAmount]").val();
    var PerDis = selectedRow.find("[name=PerDis]").val();
    var ItemDis = selectedRow.find("[name=ItemDis]").val();
    var DealDis = selectedRow.find("[name=DealDis]").val();

    var Sno = Number(lastRowNumber) + 1;
    insertPRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, actualSellingPrice)

    $("#myTable tr:last-child").find("[name=Qty]").select().focus();
    rerenderSerialNumber();

}


function removeIMEs(itemCode, itemRowNumber) {
    $(".imei-table table tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + itemCode + "]").remove();
}
function itemAlreadyExist(code, rate, disPer, dealDis) {
    var rowNo = $("#myTable [data-itemcode=" + code + "][data-itemrate=" + rate + "][data-itemdisper=" + disPer + "][data-itemdealdis=" + dealDis + "]").data("rownumber");
    return rowNo;
}
function appendRowInIMEI(element, insertIntoTableAsWell) {
   
    insertIntoTableAsWell = insertIntoTableAsWell == undefined ? false : insertIntoTableAsWell;
    var ItemCode;
    if (insertIntoTableAsWell == false) {
        $("#IMEIRow [name=ImeiTxbx]").val("").select().focus();
        var ItemCode = $(element).parents("tr").find("input[name=Code]").val();
        
    } else {
        var ItemCode = element.dataset.itemcode;
    }

    $(".IMEPortion").fadeIn();

    $.ajax({
        url: '/WebPOSService.asmx/IsItemIMEVerifiedAgainstItemCode',
        async: false,
        type: "Post",
        data: { "itemCode": ItemCode },
        success: function (isChecked) {

            $('[name=isIMEVerifiedChkbx]').prop('checked', isChecked);
            var rowNumber = element.dataset.rownumber;
            var rowInputs = $("#myTable #Row_" + rowNumber + " input");
            rowInputs.each(function (index, input) {

                var name = $(input).attr("name");
                var value = $(input).val();
                if (name == "Qty") {
                    $("#IMEIRow [name=" + name + "]").val(1);
                } else {
                    $("#IMEIRow [name=" + name + "]").val(value);
                }


            });
            $("#IMEIRow  [name=ItemRowNumber]").val(rowNumber);
            var ime = $(`#mainTable  .${rowNumber}_ScannedItem`).val();
            $("#IMEIRow  [name=ImeiTxbx]").val(ime);

            
            
                inserIMEItem();
               
        },
        fail: function (jqXhr, exception) {
        }
    });



}
function rerenderIMEITableSerialNumber() {
    var rows = $("#IMEITable tbody tr .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html("<input type='text' value='" + sno + "'  />");

    })
}

function rerenderSerialNumber() {
    var rows = $("#myTable tbody tr .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html("<input type='text' value='" + sno + "'  />");
    })
}
(document).on("keydown", "#IMEIRow [name=ImeiTxbx]", function (e) {

    if (e.keyCode == 13) {
        inserIMEItem();
        var itemCode = $("#IMEIRow [name=Code]").val();
        isQuantityAvailableLocal(itemCode);
    }

});


function inserIMEItem() {
    var imei = $("#IMEIRow [name=ImeiTxbx]").val();
    var itemCode = $("#IMEIRow [name=Code]").val();
    var itemRowNumber = $("#IMEIRow [name=ItemRowNumber]").val();

    if (!isDuplicateIMEI(imei) && isQuantityAvailableLocal(itemCode)) {

        $.ajax({
            async: false,
            url: '/WebPOSService.asmx/IsIMELengthLegalForPurchase',
            type: "Post",
            data: { "imeCode": imei },
            success: function (model) {

                if (model.isLengthCorrect) {

                    insertIMEDataInGrid(itemRowNumber, $("#IMEIRow [name=Code]").val(), $("#IMEIRow [name=ItemName]").val(), imei, $("#IMEIRow [name=Qty]").val()
                        , $("#IMEIRow [name=Rate]").val(), $("#IMEIRow [name=Amount]").val(),
                        $("#IMEIRow [name=ItemDis]").val(), $("#IMEIRow [name=PerDis]").val(), $("#IMEIRow [name=DealDis]").val(),
                        $("#IMEIRow [name=NetAmount]").val(), $("#IMEIRow [name=PurAmount]").val());
                   
                    ScanedQtyTotal($("#IMEIRow [name=Code]").val(), $("#IMEIRow [name=Rate]").val(), $("#IMEIRow [name=PerDis]").val(), $("#IMEIRow [name=DealDis]").val(), itemRowNumber)
                } else {
                    swal("Error", "IME length is not correct !!", "error");
                    $("#IMEIRow [name=ImeiTxbx]").val("");

                    playError();

                    return false;
                }


            },
            fail: function (jqXhr, exception) {
                debugger;
            }
        });
        

    }
    else {
        playError();
        swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
        //$("#IMEIRow input").val("");


    }

}

function ScanedQtyTotal(code, rate, perDis, dealRs, itemRowNumber) {

    var qtysInIMETable = $("#IMEITable [data-itemcode=" + code + "][data-rate=" + rate + "][data-perdis=" + perDis + "][data-dealrs=" + dealRs + "]").length;

    $(`#myTable .${itemRowNumber}_SQ`).val(qtysInIMETable);

}
function isQuantityAvailableLocal(code) {
    var rows = $("#myTable  [value=" + code + "][name=Code]");
    
    var qtyInMainTable = 0;
    rows.each(function (index, element) {
        var rownumber = $(element).parents("tr").data().rownumber;
        qtyInMainTable += Number($("#myTable  ." + rownumber + "_Qty").val());

    });

    var qtysInIMETable = $("#IMEITable [data-code=" + code + "]");
    var sumqtys = 0;
    qtysInIMETable.each(function (index, element) {
        var qty = Number(element.value);
        sumqtys += qty;
    });
    if (sumqtys == qtyInMainTable) {
        $("#IMEIRow input").val("");
    }
    return sumqtys < qtyInMainTable;

}

function insertIMEDataInGrid(itemRowNumber, Code, ItemName, imei, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount) {

    var rowCount = $("#IMEITable td").closest("tr").length == 0 ? 1 : $("#IMEITable td").closest("tr").length + 1;
    var Sno = rowCount;

    var rows = createIMERow(itemRowNumber, Sno, Code, ItemName, imei, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount);

    var lastChildofTableCode = $("#IMEITable tbody tr:last-child").length > 0 ? $("#IMEITable tbody tr:last-child").data().itemcode : null;
    var lastChildofTableItemRowNumber = $("#IMEITable tbody tr:last-child").length > 0 ? $("#IMEITable tbody tr:last-child").data().itemrownumber : null;

    if ((lastChildofTableCode == Code && lastChildofTableItemRowNumber == itemRowNumber) || lastChildofTableCode == null) {
        $(rows).appendTo("#IMEITable tbody");
    } else {
        var rowOfSpecifiedItemCode = $("#IMEITable tbody tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + Code + "]");
        var isChildOfSpecifiedCodeAvailable = rowOfSpecifiedItemCode.length > 0;
        if (isChildOfSpecifiedCodeAvailable) {
            var lastChildOfSpecificCodeId = "#IMEITable #" + $("#IMEITable tbody tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + Code + "]").last().attr("id");
            $(rows).insertAfter(lastChildOfSpecificCodeId);
        } else {
            $(rows).appendTo("#IMEITable tbody");
        }


    }
    
    $("#IMEIRow [name=ImeiTxbx]").val("");
    //$("#IMEIRow input").val("");
    $("#txtBarCode").val("").select().focus();
    //calculationSale(); //29-12-2020 closed````````````````````````
    rerenderIMEITableSerialNumber();
    playSuccess();
    return true;
}

function isDuplicateIMEI(IMEI) {
    var isImeiAlreardyExist = $("#IMEITable [value=" + IMEI + "].IMEI").length > 0;

    return isImeiAlreardyExist;
}

function isQuantityAvailable(code) {
    var rows = $("#myTable  [value=" + code + "][name=Code]");
    var qtyInMainTable = 0;
    rows.each(function (index, element) {
        var rownumber = $(element).parents("tr").data().rownumber;
        qtyInMainTable += Number($("#myTable  ." + rownumber + "_Qty").val());

    });

    var qtysInIMETable = $("#IMEITable [data-code=" + code + "]");
    var sumqtys = 0;
    qtysInIMETable.each(function (index, element) {
        var qty = Number(element.value);
        sumqtys += qty;
    });

    return sumqtys < qtyInMainTable;

}


function updateIMETable(itemCode, rowNumber) {

    var Rate = $("#myTable ." + rowNumber + "_Rate").val();
    var PerDis = $("#myTable ." + rowNumber + "_PerDis").val();
    var DealDis = $("#myTable ." + rowNumber + "_DealDis").val();
    var ItemDis = $("#myTable ." + rowNumber + "_ItemDis").val();
    var Amount = $("#myTable ." + rowNumber + "_Amount").val();
    var NetAmount = $("#myTable ." + rowNumber + "_NetAmount").val();
    var PurAmount = 0;

    if ($("#myTable ." + rowNumber + "_ActualCost").length > 0)
        PurAmount = $("#myTable ." + rowNumber + "_ActualCost").val();
    else
        PurAmount = $("#myTable ." + rowNumber + "_PurAmount").val();

    var rows = $("#IMEITable [data-itemrownumber=" + rowNumber + "][data-itemcode=" + itemCode + "]");

    rows.each(function (index, element) {

        $(element).find("[name=Rate]").val(Rate);
        $(element).find("[name=PerDis]").val(PerDis);
        $(element).find("[name=DealDis]").val(DealDis);
        $(element).find("[name=ItemDis]").val(ItemDis);
        $(element).find("[name=Amount]").val(Amount);
        $(element).find("[name=NetAmount]").val(NetAmount);
        $(element).find("[name=PurAmount]").val(PurAmount);

    });

}

function updateRowRateDisDeal(rowNumber) {
    var Rate = $("#myTable ." + rowNumber + "_Rate").val();
    var PerDis = $("#myTable ." + rowNumber + "_PerDis").val();
    var DealDis = $("#myTable ." + rowNumber + "_DealDis").val();
    $("#myTable [data-rownumber=" + rowNumber + "]").data('itemrate', Rate)
        .data('itemdisper', PerDis).data('itemdealdis', DealDis)
        .attr('data-itemrate', Rate).attr('data-itemdisper', PerDis).attr('data-itemdealdis', DealDis);
}

function reArrangeIMETabeRows() {
    var items = $("#myTable tr");
    var newArrangedRows;
    items.each(function (index, element) {
        var rowNumber = element.dataset.rownumber;
        var itemCode = element.dataset.itemcode;
        var IMEIRows = $(`#IMEITable tr[data-itemrownumber=${rowNumber}][data-itemcode=${itemCode}]`);
        IMEIRows.each(function (i, e) {
            newArrangedRows += e.outerHTML
        });

    });

    $(`#IMEITable tbody`).html(newArrangedRows);
    calculationSale();
    rerenderIMEITableSerialNumber();
}

function createIMERow(itemRowNumber, Sno, ItemCode, ItemName, IME, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount) {
    var row = `
    <tr  data-itemrownumber='${itemRowNumber}' data-itemcode='${ItemCode}'  data-rate='${Rate}' data-perdis='${PerDis}' data-dealrs='${DealDis}' id='Row_${Sno}' data-rownumber='${Sno}'>
        <td class='SNo three  '>
            ${Sno}
        </td>
        <td class='four name'>
            <input class='${Sno}_Code' name='Code' type='text' value='${ItemCode}' />
        </td>
        <td class='name fifteen '>
            <input class='${Sno}_ItemName' name='ItemName' type='text' value='${ItemName}' />
        </td>
        <td class='thirteen'>
            <input class='${Sno}_IMEI IMEI' name='IMEI' type='text' value='${IME}' />
        </td>
        <td class='five'>
            <input class='${Sno}_Qty calcu' data-code='${ItemCode}' name='Qty' type='text' value='${Qty}' />
        </td>
        <td class='eight'>
            <input name='Rate' class='${Sno}_Rate calcu' type='text' value='${Rate}' /></td>
        <td class='eight'>
            <input class='${Sno}_Amount' name='Amount' type='text' value='${Amount}' />
        </td>
        <td class='eight'>
            <input class='${Sno}_ItemDis' type='text' value='${ItemDis}' name='ItemDis' />
        </td>
        <td class='six'>
            <input name='PerDis' class='${Sno}_PerDis calcu' type='text' value='${PerDis}' />
        </td>
        <td class='eight'>
            <input name='DealDis' class='${Sno}_DealDis' type='text' value='${DealDis}' />
        </td>
        <td class='eight'>
            <input class='${Sno}_NetAmount' type='text' value='${NetAmount}' name='NetAmount' />
        </td>
        <td class='eight'>
            <input class='${Sno}_PurAmount' type='text' value='${PurAmount}' name='PurAmount' />
        </td>
        <td class='six actionBtn text-center'><i class='fas fa-times'></i> </td>
    </tr>`;
    return row;
}

