﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using WebPOS.Model;

namespace WebPOS
{
    public partial class WithDrawFromBankEdit : System.Web.UI.Page
    {
        static string CashAccountGLCode = "0101010100001";
        Int32 OperatorID = 0;
        static string CompID = "01";
        Module1 objModule1 = new Module1();
        Module7 objModule7 = new Module7();
        Module4 objModule4 = new Module4();
        ModGLCode objModGLCode = new ModGLCode();
        static SqlTransaction tran;
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {


            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveWidthDrawFromBankEdit(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

                
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashWithDrawForPettyCash";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;

                SqlCommand cmdDeleteOld2 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and  V_Type='CRV' and SecondDescription='" + SecondDescription + "'", con, tran);
                cmdDeleteOld2.ExecuteNonQuery();



                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string GLCode = ModelPaymentVoucher.PartyCode;
                string GLTitle = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;



                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,bankcode,datedr,Narration,SecondDescription,System_Date_Time,Code1,CompId) values ('" + CashAccountGLCode + "','" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "CRV" + "','" + GLCode + "','" + Convert.ToDateTime(VoucherDate) + "','" + Narration + "','" + SecondDescription + "','" + SysTime + "','" + CashAccountGLCode + "','" + CompID + "')", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger(Code, Description, AmountCr, V_No, V_Type, bankcode, datedr, Narration, SecondDescription, System_Date_Time, Code1,CompId) values('" + GLCode + "', '" + Narration + "', " + Convert.ToDecimal(CashPaid) + ", " + VoucherNo + ", '" + "CRV" + "', '" + GLCode + "', '" + Convert.ToDateTime(VoucherDate) + "', '" + Narration + "', '" + SecondDescription + "', '" + SysTime + "', '" + CashAccountGLCode + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Cash WidthDraw from Bank Edit Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

                
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashWithDrawForPettyCash";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;

                SqlCommand cmdDeleteOld2 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and  V_Type='CRV' and SecondDescription='" + SecondDescription + "'", con, tran);
                cmdDeleteOld2.ExecuteNonQuery();



                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string GLCode = ModelPaymentVoucher.PartyCode;
                string GLTitle = ModelPaymentVoucher.PartyName;
                

                objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }



    }
}