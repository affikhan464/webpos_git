﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public partial class ReceiptFromPartyThroughBankEdit : System.Web.UI.Page
    {
        static string CompID = "01";
        static string CashAccountGLCode = "0101010100001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);


        protected void Page_Load(object sender, EventArgs e)
        {

        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {
            Module4 objModule4 = new Module4();
            var objModule1 = new Module1();
            var objModGLCode = new ModGLCode();
            var objModGL = new ModGL();
            var objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";
            PartyCode = ModelPaymentVoucher.PartyCode;
            try
            {
                
                SqlCommand cmdDelOld1 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_NO=" + ModelPaymentVoucher.VoucherNo + " and V_type='BRV' and SecondDescription='FromPartiesThroughBank'", con, tran);
                cmdDelOld1.ExecuteNonQuery();
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID ='" + CompID + "' and V_NO=" + ModelPaymentVoucher.VoucherNo + " and V_type='BRV' and DescriptionOfBillNo='FromPartiesThroughBank'", con, tran);
                cmdDelOld2.ExecuteNonQuery();

                string SysTime = Convert.ToString(DateTime.Now);
                decimal CashPaid = 0;
                if (string.IsNullOrEmpty(ModelPaymentVoucher.CashPaid)) { CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid); }
                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountDr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId) values('" + ModelPaymentVoucher.BankCode + "','" + VoucherDate + "','" + ModelPaymentVoucher.Narration + "-" + ModelPaymentVoucher.PartyName + "','" + ModelPaymentVoucher.ChqNo + "','" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "BRV" + "','" + SysTime + "','" + ModelPaymentVoucher.BankCode + "','" + "FromPartiesThroughBank" + "','" + PartyCode + "','" + CompID + "')", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountCr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId) values('" + PartyCode + "','" + VoucherDate + "','" + ModelPaymentVoucher.Narration + "','" + ModelPaymentVoucher.ChqNo + "','" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "BRV" + "','" + SysTime + "','" + ModelPaymentVoucher.BankCode + "','" + "FromPartiesThroughBank" + "','" + PartyCode + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountCr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,VenderCode,CompId) values('" + PartyCode + "','" + ModelPaymentVoucher.Narration + ",BRV.No.=" + ModelPaymentVoucher.VoucherNo + "(" + ModelPaymentVoucher.BankName + ")" + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "BRV" + "','" + VoucherDate + "','" + SysTime + "','" + "FromPartiesThroughBank" + "','" + ModelPaymentVoucher.BankCode + "','" + CompID + "')", con, tran);
                cmd3.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Updated Successfully!!" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();

                return new BaseModel() { Success = false, Message = ex.Message };

            }


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(ModelPaymentVoucher ModelPaymentVoucher)
        {
            Module4 objModule4 = new Module4();
            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";
            PartyCode = ModelPaymentVoucher.BankCode;
            try
            {
                SqlCommand cmdDelOld1 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_NO=" + ModelPaymentVoucher.VoucherNo + " and V_type='BRV' and SecondDescription='FromPartiesThroughBank'", con, tran);
                cmdDelOld1.ExecuteNonQuery();
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID ='" + CompID + "' and V_NO=" + ModelPaymentVoucher.VoucherNo + " and V_type='BRV' and DescriptionOfBillNo='FromPartiesThroughBank'", con, tran);
                cmdDelOld2.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();

                return new BaseModel() { Success = false, Message = ex.Message };

            }

        }
    }
}
