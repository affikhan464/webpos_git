﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.ModelBinding;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;

namespace WebPOS
{
    public partial class ExpenseEntryEdit : System.Web.UI.Page
    {
        static string CompID = "01";
        //string CashAccountGLCode="";
        static string CashAccountGLCode = "01" + "01010100001";
        ModGLCode objModGLCode = new ModGLCode();
        Module1 objModule1 = new Module1();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveExpenceVoucherEdit(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();



                string SecondDescription = "OperationalExpencesThroughCash";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;


                SqlCommand cmdDelOld = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='CPV' and SecondDescription='OperationalExpencesThroughCash'", con, tran);
                cmdDelOld.ExecuteNonQuery();


                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string GLCode = ModelPaymentVoucher.PartyCode;
                string GLTitle = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;


                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,datedr,Narration,Code1,System_Date_Time,SecondDescription,BillNo,CompId) values('" + GLCode + "','" + "Cash-(" + Narration + ")" + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + Convert.ToDateTime(VoucherDate) + "','" + Narration + "','" + CashAccountGLCode + "','" + SysTime + "','" + SecondDescription + "','" + GLCode + "','" + CompID + "')", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountCr,V_No,V_Type,datedr,Narration,Code1,System_Date_Time,SecondDescription,BillNo,CompId) values('" + CashAccountGLCode + "','" + GLTitle + "(" + Narration + ")" + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + Convert.ToDateTime(VoucherDate) + "','" + Narration + "','" + GLCode + "','" + SysTime + "','" + SecondDescription + "','" + GLCode + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Cash Expence Voucher Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

                if (ModelPaymentVoucher.Narration=="delete")
                {

                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();



                    string SecondDescription = "OperationalExpencesThroughCash";
                    var VoucherNo = ModelPaymentVoucher.VoucherNo;


                    SqlCommand cmdDelOld = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='CPV' and SecondDescription='OperationalExpencesThroughCash'", con, tran);
                    cmdDelOld.ExecuteNonQuery();

                    string GLCode = ModelPaymentVoucher.PartyCode;

                    objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Deleted Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
                }
                else
                {
                    return new BaseModel() { Success = false, Message  = "cant delete voucher"};
                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}