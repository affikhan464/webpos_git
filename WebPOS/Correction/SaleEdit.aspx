﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="SaleEdit.aspx.cs" Inherits="WebPOS.SaleEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
    <link href="../css/allitems.css" rel="stylesheet" />


    <style type="text/css">
        .TFtable {
            width: 100%;
            border-collapse: collapse;
        }

            .TFtable td {
                text-align: left;
            }
            /* provide some minimal visual accomodation for IE8 and below */
            .TFtable tr {
                background: #b8d1f3;
            }
                /*  Define the background color for all the ODD background rows  */
                .TFtable tr:nth-child(odd) {
                    background: #b0c1d8;
                }
                /*  Define the background color for all the EVEN background rows  */
                .TFtable tr:nth-child(even) {
                    background: #dae5f4;
                }
    </style>



</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    Invoice # 
                                <asp:TextBox ID="txtInvoiceNo" CssClass="InvoiceNumber" Style="width: 100px; height: 20px" AutoPostBack="false" runat="server"></asp:TextBox>



    <input type="button" value="<" class="btn btn-primary" />
    <input type="button" value="Get" class="btn btn-primary" onclick="GetInvoice()" />
    <input type="button" value=">" class="btn btn-primary" />


    <asp:Label ID="Label9" runat="server" Text="ManInv" Style="margin-left: 0px;"></asp:Label>
    <asp:TextBox CssClass="ManInv inv1" Style="width: 122px; height: 20px;" AutoPostBack="true" runat="server"></asp:TextBox>
    SaleMan      
                                <asp:DropDownList ID="lstSaleMan" Style="width: 80px; height: 20px; top: 0px; left: 0px;" AutoPostBack="true" runat="server"></asp:DropDownList>

    <input type="text" id="txtNorBal" class="PartyNorBalance inv1" style="width: 10px; margin-left: 1px;" />

    <asp:Label ID="Label2" runat="server" Text="Client" Style="margin-left: 7px;"></asp:Label>
    <asp:TextBox ID="txtPartyName" CssClass="PartyBox PartyName autocomplete inv1" data-type="Party" data-id="txtPartyName" data-function="GetParty" Style="width: 355px; margin-left: 27px;" runat="server"></asp:TextBox>
    <input type="text" id="txtDealApplyNo" class="PartyDealApplyNo inv1" style="width: 12px; margin-left: 3px;" />
    <asp:Label ID="Label7" runat="server" Text="Party Code" Style="margin-left: 1px;"></asp:Label>
    <asp:TextBox ID="txtPartyCode" CssClass="PartyCode inv1" AutoPostBack="false" ClientIDMode="Static" Style="width: 125px; height: 20px;" runat="server"></asp:TextBox>
    <br />
    Date
                                <asp:TextBox ID="txtDate" Style="margin-left: 30px; width: 100px;" AutoPostBack="false" runat="server" CssClass="datetimepicker Date"></asp:TextBox>
    <asp:Label ID="Label1" runat="server" Text="B.C." Style="margin-left: 76px;"></asp:Label>
    <asp:TextBox ID="txtBarCode" Style="width: 162px; height: 20px" AutoPostBack="true" runat="server"></asp:TextBox>


    <asp:Label ID="Label3" runat="server" Text="Address" Style="margin-left: 174px;"></asp:Label>
    <asp:TextBox ID="txtAddresss" CssClass="PartyAddress inv1" Style="width: 355px; height: 20px; margin-left: 13px;" runat="server"></asp:TextBox>

    <asp:Label ID="Label4" runat="server" Text="Phone No" Style="margin-left: 21px;"></asp:Label>
    <asp:TextBox ID="txtPhone" CssClass="PartyPhoneNumber inv1" Style="width: 124px; height: 20px; top: 0px; margin-left: 8px;" runat="server"></asp:TextBox>
    <br />
    Balance
                                <input type="text" id="txtPreviousBalance" class="PartyBalance inv1" style="margin-left: 8px; width: 100px; height: 20px; text-align: center;" value="0" />
    <asp:TextBox ID="txtItemName" ClientIDMode="Static" CssClass="autocomplete SearchBox" data-id="txtItemName" data-type="Item" data-function="GetRecords" runat="server" Style="margin-left: 45px;" Width="300px"></asp:TextBox>

    <asp:Label ID="Label5" runat="server" Text="Particular" Style="margin-left: 99px;"></asp:Label>
    <%--<asp:TextBox ID="txtParticular" CssClass="PartyParticular" Style="width: 355px; height: 20px; top: 0px; margin-left: 5px;" AutoPostBack="true" runat="server"></asp:TextBox>--%>
    <input type="text" class="PartyParticular inv1" id="txtParticular" style="width: 355px; height: 20px; top: 0px; margin-left: 5px;" />
    <asp:Label ID="Label6" runat="server" Text="Cr. Limit" Style="margin-left: 17px;"></asp:Label>
    <asp:TextBox ID="txtCreditLimit" CssClass="PartyCreditLimit inv1" Style="width: 122px; height: 20px; margin-left: 19px;" runat="server"></asp:TextBox>

    <br />
    <br />
      <div>
    <div class="divSty">
        <table class="headSection">
            <thead>
                <tr>
                    <th style="width: 2%;">S.No.</th>
                    <th style="width: 5%;">ItemCode</th>
                    <th style="width: 0%;">Description</th>
                    <th style="width: 5%;">Qty</th>
                    <th style="width: 5%;">Rate</th>
                    <th style="width: 10%;">Gross Amount</th>
                    <th style="width: 5%;">Item Dis</th>
                    <th style="width: 5%;">Per. Dis</th>
                    <th style="width: 8%;">Deal Dis</th>
                    <th style="width: 10%;">Net Amount</th>
                    <th style="width: 12%;">Avg.Cost</th>
                </tr>
            </thead>
        </table>

        <table class="TFtable" id="myTable">

            <tbody>
            </tbody>

        </table>
    </div>
    <table class="footerSection">
        <tfoot>
            <tr>
                <th style="width: 5%;">S.No</th>
                <th style="width: 5%;">ItemCode</th>
                <th style="width: 0%;">Description</th>
                <th style="width: 5%;">
                    <input class="QtyTotal" /></th>
                <th style="width: 8%;">Rate</th>
                <th style="width: 10px;">
                    <input class="AmountTotal" /></th>
                <th style="width: 5%;">
                    <input class="ItemDisTotal" /></th>
                <th style="width: 5%;">Per. Dis</th>
                <th style="width: 5%;">
                    <input class="DealDisTotal" /></th>
                <th style="width: 10%;">
                    <input class="NetAmountTotal" /></th>
                <th style="width: 10%;">
                    <input class="PurAmountTotal" /></th>
            </tr>
        </tfoot>
    </table>

          </div>








    <input type="checkbox" id="chkAutoDeal" style="margin-left: 50px;" onclick="PerDisApplyOnAllitems()" />
    Auto Deal Dis

                                 <asp:Label ID="Label19" runat="server" Text="%Discount" Style="margin-left: 414px;"></asp:Label>
    <input type="text" id="txtTotPercentageDis" class="inv3" style="width: 154px; height: 20px; text-align: right;" value="0" />

    <asp:Label ID="Label20" runat="server" Text="Total" Style="margin-left: 0px;"></asp:Label>

    <input type="text" class="GrossTotal inv3" id="txtTotal" style="margin-left: 62px; width: 220px; height: 20px; text-align: right;" value="0" />
    <br />
    <input type="checkbox" id="chkAutoPerDis" style="margin-left: 50px;" onclick="PerDisApplyOnAllitems()" />
    Auto Per Dis
                               
                                <asp:Label ID="Label21" runat="server" Text="Deal Dis." Style="margin-left: 423px;"></asp:Label>
    <input type="text" class="DealRs inv3" id="txtTotDeal" style="width: 154px; height: 20px; margin-left: 11px; text-align: right; margin-top: 2px;" value="0" />

    <asp:Label ID="Label22" runat="server" Text="Flat Dis." Style="margin-left: 2px;"></asp:Label>
    <input type="text" class="FlatPer inv3" id="txtFlatDiscountPer" style="margin-left: 37px; width: 40px; height: 20px; text-align: center;" value="0" />
    <input type="text" class="FlatDiscount inv3" id="txtFlatDiscount" style="margin-left: 7px; width: 165px; height: 20px; text-align: right;" value="0" />


    <br />
    <input type="checkbox" name="chkAutoPrint" value="1" style="margin-left: 50px;" />
    Auto Print
                           <asp:Label ID="Label8" runat="server" Text="Net Discount" Style="margin-left: 415px;"></asp:Label>
    <input type="text" class="NetDiscount inv3" id="txtTotalDiscount" style="margin-left: 11px; width: 154px; height: 20px; text-align: right;" value="0" />


    <asp:Label ID="Label23" runat="server" Text="Cash Received" Style="margin-left: 0px;"></asp:Label>
    <input type="text" class="Recieved inv3" id="txtPaid" style="width: 220px; height: 20px; text-align: right; text-align: right;" value="0" />
    <input type="button" id="btnSave" class="btnSave" value="save" onclick="save()" />
    <br />
    <asp:Label ID="Label24" runat="server" Text="Bill Total" Style="margin-left: 581px;"></asp:Label>
    <input type="text" class="BillTotal inv3" id="txtBillTotal" style="margin-left: 10px; width: 154px; height: 20px; text-align: right; margin-top: 2px;" value="0" />
    <asp:Label ID="Label25" runat="server" Text="Balance" Style="margin-left: 0px;"></asp:Label>
    <input type="text" class="Balance inv3" id="txtBalance" style="width: 220px; height: 20px; margin-left: 44px; text-align: right;" value="0" />


    <br />
    <div class="IMEPortion">
        <div>
            <label>
                <input type="checkbox" name="isIMEVerifiedChkbx" />
                Is IME Verified
            </label>
            <table id="IMEIRow">
                <tr data-rownumber="1">
                    <td>
                        <input name="Code" type="text" value="" /></td>
                    <td class="name">
                        <input name="ItemName" type="text" value="" /></td>
                    <td>
                        <input class="" name="Qty" type="text" value="" /></td>
                    <td>
                        <input name="Rate" class=" calcu" type="text" value="" /></td>
                    <td>
                        <input class="" name="Amount" type="text" value="" /></td>
                    <td>
                        <input class="" type="text" value="" name="ItemDis" /></td>
                    <td>
                        <input name="PerDis" class=" calcu" type="text" value="" /></td>
                    <td>
                        <input name="DealDis" class="" type="text" value="" /></td>
                    <td>
                        <input class="" type="text" value="" name="NetAmount" /></td>
                    <td>
                        <input class="" type="text" value="" name="PurAmount" /></td>
                    <td>
                        <input class="" type="text" style="width: 200px; text-align: left" name="ImeiTxbx" placeholder="Enter IME" />
                    </td>
                </tr>
            </table>
        </div>


        <div class="imei-table" style="height: 190px;">
            <table class="headSection">
                <thead>
                    <tr>
                        <th style="width: 2%;">S.No.</th>
                        <th style="width: 5%;">ItemCode</th>
                        <th style="width: 0%;">Description</th>
                        <th style="width: 0%;">IMEI</th>
                        <th style="width: 5%;">Qty</th>
                        <th style="width: 5%;">Rate</th>
                        <th style="width: 10%;">Gross Amount</th>
                        <th style="width: 5%;">Item Dis</th>
                        <th style="width: 5%;">Per. Dis</th>
                        <th style="width: 8%;">Deal Dis</th>
                        <th style="width: 10%;">Net Amount</th>
                        <th style="width: 12%;">Avg.Cost</th>
                    </tr>
                </thead>
            </table>
            <div class="imei-body" style="height: 155px; overflow: auto">
                <table class="TFtable" id="IMEITable">

                    <tbody>
                    </tbody>

                </table>
            </div>
        </div>
        <table class="footerSection">
            <tfoot>
                <tr>
                    <th style="width: 5%;">S.No</th>
                    <th style="width: 5%;">ItemCode</th>
                    <th style="width: 0%;">Description</th>
                    <th style="width: 0%;">IMEI</th>
                    <th style="width: 5%;">
                        <input class="QtyTotal" /></th>
                    <th style="width: 8%;">Rate</th>
                    <th style="width: 10px;">
                        <input class="AmountTotal" /></th>
                    <th style="width: 5%;">
                        <input class="ItemDisTotal" /></th>
                    <th style="width: 5%;">Per. Dis</th>
                    <th style="width: 5%;">
                        <input class="DealDisTotal" /></th>
                    <th style="width: 10%;">
                        <input class="NetAmountTotal" /></th>
                    <th style="width: 10%;">
                        <input class="PurAmountTotal" /></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <script src="../Script/jquery.min.js"></script>
    <script src="../Script/jquery-ui.min.js"></script>

    <script src="../Script/Autocomplete.js"></script>
    <script src="../Script/SaleEdit/InsertDataInSaleEdit.js"></script>
    <script src="../Script/SaleEdit/calculationSaleEdit.js"></script>
    <script src="../Script/SaleEdit/Sale_Save_Edit.js"></script>


    <div style="display:none">
   
         
                <table class="TFtable" id="Table_OldData ">

                    <tbody>
                    </tbody>

                </table>
        
        </div>

    <script type="text/javascript">   
        $(document).on('input', '.calcu', function () {
            calculationSale();
        })

        function clearInvoice() {
            $("#myTable tbody tr").remove();
            $("#Table_OldData tbody tr").remove();

            $(".inv1").val("");
            $(".inv3").val("0");


            //$("input").val("0");
            //calculationSale();
        }
        function LoadInvoice(InvoiceData)
        {
            var date = $(".Date").val();
            
            clearInvoice();
            //$(".InvoiceNumber").val(BaseModel.d.LastInvoiceNumber);
            $(".Date").val(date);




            var ClientData = InvoiceData.ClientData;
            var Products = InvoiceData.Products;
            var TotalData = InvoiceData.TotalData;
            var IMEsList = InvoiceData.IMEItems

            var Date = ClientData.Date;
            var PartyName = ClientData.Name;
            var PartyCode = ClientData.Code;
            var Address = ClientData.Address;
            var Particular = ClientData.Particular;
            var NorBalance = ClientData.NorBalance;
            var DealApplyNo = ClientData.DealApplyNo;
            var Phone = ClientData.PhoneNumber;
            var CreditLimit = ClientData.CreditLimit;
            var PrevBalance = ClientData.Balance;

            $(".Date").val(Date);
            $(".PartyName").val(PartyName);
            $(".PartyCode").val(PartyCode);
            $(".PartyAddress").val(Address);
            $(".PartyParticular").val(Particular);
            $(".PartyNorBalance").val(NorBalance);
            $(".PartyDealApplyNo").val(DealApplyNo);
            $(".PartyBalance").val(PrevBalance);
            $(".PartyPhoneNumber").val(Phone);
            $(".PartyCreditLimit").val(CreditLimit);
            

            $("#myTable tbody tr").remove();
            $("#Table_OldData tbody tr").remove();
            
            for (var i = 0; i < Products.length; i++) {
                debugger
                var Sno = i+1;
                var ItemCode = Products[i].Code;
                var ItemName = Products[i].ItemName;
                var Qty = Products[i].Qty;
                var Rate = Products[i].Rate;

                var Amount = Products[i].Amount;
                var ItemDis = Products[i].ItemDis;
                var PerDis = Products[i].PerDis;
                var DealDis = Products[i].DealDis;
                
                var NetAmount = (parseFloat(Amount) - parseFloat(ItemDis) - parseFloat( DealDis));
                var PurAmount = Products[i].PurAmount;
                var avrgCost = Products[i].AverageCost;
                var revCode = Products[i].RevenueCode;
                var cgsCode = Products[i].CGSCode;
               
                var rows = "<tr id='Row_" + Sno + "' data-rownumber='" + Sno + "'><td class='SNo'>" + Sno + "</td><td><input  class='" + Sno + "_Code' name='Code' type='text' value='" + ItemCode + "'  /></td><td class='name'><input  class='" + Sno + "_ItemName' name='ItemName' type='text' value='" + ItemName + "'  /></td><td><input  class='" + Sno + "_Qty' name='Qty' type='text' value='" + Qty + "'  /></td><td><input name='Rate'  class='" + Sno + "_Rate'  type='text' value='" + Rate + "' /></td><td><input class='" + Sno + "_Amount' name='Amount'  type='text' value='" + Amount + "' /></td><td><input class='" + Sno + "_ItemDis' type='text' value='" + ItemDis + "' name='ItemDis' /></td><td><input  name='PerDis'  class='" + Sno + "_PerDis' type='text' value='" + PerDis + "' /></td><td><input name='DealDis'  class='" + Sno + "_DealDis' type='text' value='" + DealDis + "' /></td><td><input class='" + Sno + "_NetAmount' type='text' value='" + NetAmount + "' name='NetAmount' /></td><td><input class='" + Sno + "_PurAmount' type='text' value='" + PurAmount + "' name='PurAmount' /></td><td hidden> <input hidden='hidden' class='" + Sno + "_AverageCost' value='" + avrgCost + "' /></td>   <td hidden> <input hidden='hidden' class='" + Sno + "_RevCode' value='" + revCode + "' /></td><td hidden> <input hidden='hidden' class='" + Sno + "_CgsCode' value='" + cgsCode + "' /></td>   <td><i data-itemcode='" + ItemCode + "' class='fas fa-times'></i> <i data-rownumber='" + Sno + "' class='fas fa-barcode' onclick='appendRowInIMEI(this)'></i> </td></tr>";
                $(rows).appendTo("#myTable tbody");
                var rows = "<tr id='Row_" + Sno + "' data-rownumber='" + Sno + "'><td class='SNoOld'>" + Sno + "</td><td><input  class='" + Sno + "_CodeOld' name='CodeOld' type='text' value='" + ItemCode + "'  /></td><td class='name'><input  class='" + Sno + "_ItemName' name='ItemName' type='text' value='" + ItemName + "'  /></td><td><input  class='" + Sno + "_QtyOld' name='QtyOld' type='text' value='" + Qty + "'  /></td></tr>";
                $(rows).appendTo("#Table_OldData tbody");
            }
            
            var FlatPer = TotalData.FlatPer;
            var Recieved = TotalData.Recieved;

            $(".FlatPer").val(FlatPer);
            $(".Recieved").val(Recieved);


            calculationSale();

            $("#IMEITable tbody tr").remove();
            $(".IMEPortion").fadeIn();

            for (var i = 0; i < IMEsList.length; i++) {
                var ItemCode = IMEsList[i].Code;

                var item = getItem(ItemCode, Products);

                var Sno = i + 1;
               
                var ItemName = item.ItemName;
                var IME = IMEsList[i].IMEI;
                var Qty = 1;
                var Rate = item.Rate;

                var Amount = item.Amount;
                var ItemDis = item.ItemDis;
                var PerDis = item.PerDis;
                var DealDis = item.DealDis;
                

                var NetAmount = (parseFloat(Amount) - parseFloat(ItemDis) - parseFloat(DealDis));
                debugger;
                var averageCost = item.AverageCost;
                averageCost = isNaN(averageCost) ? 0 : parseFloat(averageCost);
                if (averageCost == 0) {
                    averageCost = Rate;
                }

                var PurAmount = (averageCost * Number(item.Qty)).toFixed(2);

               
                
                var rows = "<tr data-itemcode='" + ItemCode + "' id='Row_" + Sno + "' data-rownumber='" + Sno + "'><td class='SNo'>" + Sno + "</td><td><input  class='" + Sno + "_Code' name='Code' type='text' value='" + ItemCode + "'  /></td><td class='name'><input  class='" + Sno + "_ItemName' name='ItemName' type='text' value='" + ItemName + "'  /></td><td><input  class='" + Sno + "_IMEI IMEI' name='IMEI' type='text' value='" + IME + "'  /></td><td><input  class='" + Sno + "_Qty calcu' data-code='" + ItemCode + "'  name='Qty' type='text' value='" + Qty + "'  /></td><td><input name='Rate'  class='" + Sno + "_Rate calcu'  type='text' value='" + Rate + "' /></td><td><input class='" + Sno + "_Amount' name='Amount'  type='text' value='" +Amount + "' /></td><td><input class='" + Sno + "_ItemDis' type='text' value='" + ItemDis + "' name='ItemDis' /></td><td><input  name='PerDis'  class='" + Sno + "_PerDis calcu' type='text' value='" + PerDis + "' /></td><td><input name='DealDis'  class='" + Sno + "_DealDis' type='text' value='" + DealDis + "' /></td><td><input class='" + Sno + "_NetAmount' type='text' value='" + NetAmount + "' name='NetAmount' /></td><td><input class='" + Sno + "_PurAmount' type='text' value='" + PurAmount + "' name='PurAmount' /></td>  <td><i class='fas fa-times'></i> </td></tr>";
                $(rows).appendTo("#IMEITable tbody");
               
            }

          

        
        }

        function getItem(Code, Products) {
            var item;
            for (var i = 0; i < Products.length; i++) {
                if (Products[i].Code == Code) {
                    item= Products[i];
                } 
             
            }
            return item;
        }

        function GetInvoice() {
            if ($(".InvoiceNumber").val().trim() != "") {

                $.ajax({
                    url: '/WebPOSService.asmx/GetInvoiceData',
                    type: "Post",

                    data: { "InvoiceNo": $(".InvoiceNumber").val() },


                    success: function (InvoiceData) {
                        if (InvoiceData.Success) {
                        LoadInvoice(InvoiceData);
                        calculationSale();
                        } else {
                            swal("Error", InvoiceData.Message, "error");
                        }
                     

                    },
                    fail: function (jqXhr, exception) {

                    }
                });
            }
            else {
                swal("", "Enter Invoice Number", "error");
            }
            



        }

    </script>


</asp:Content>
