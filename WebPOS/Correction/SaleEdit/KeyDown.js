﻿$(document).on("keypress", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();
    if (e.which == 13) {
        e.preventDefault();
        if (txbxName == "Qty") {
            $("#myTable ." + sno + "_Rate").focus();
            $("#myTable ." + sno + "_Rate").select();

        }
        else if (txbxName == "Rate") {
            $("#myTable ." + sno + "_PerDis").focus();
            $("#myTable ." + sno + "_PerDis").select();
        }
        else if (txbxName == "PerDis") {
            $("#myTable ." + sno + "_DealDis").focus();
            $("#myTable ." + sno + "_DealDis").select();
        }
        else if (txbxName == "DealDis") {

            $(".SearchBox").focus();
            $(".SearchBox").val("");
        }
        else if (txbxName == "ImeiTxbx") {


            appendRowInIMEI(this, true);

            $(`#myTable .${sno}_ScannedItem`).val("").focus();
        }
            calculationSale();
            updateIMETable(ItemCode, sno);
            calculationSale();
        

    }

        else {
            return true;
        }
    });
$(document).on("input", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();

    calculationSale();
    updateIMETable(ItemCode, sno);
    calculationSale();
    updateRowRateDisDeal(sno);
});
$(document).on("click", "#IMEITable .fas.fa-times", function () {


    swal({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Remove',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                this.parentElement.parentElement.remove();
                rerenderIMEITableSerialNumber();
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })

});
$(document).on("click", "#myTable .fas.fa-times", function () {

    swal({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Remove',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                this.parentElement.parentElement.remove();
                var itemCode = this.dataset.itemcode;
                var itemRowNumber = $(this).parents("tr").data("rownumber");
                removeIMEs(itemCode, itemRowNumber);
                calculationSale();
                rerenderIMEITableSerialNumber();
                rerenderSerialNumber();
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })


});