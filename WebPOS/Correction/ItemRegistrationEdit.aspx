﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemRegistrationEdit.aspx.cs" Inherits="WebPOS.ItemRegistrationEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
   <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" /> 
   <link href="../css/allitems.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
                       <h2 style="text-align:center; font-size:30px; " >Edit Item Registration</h2>  
    <br />
                        

    <asp:Label Text="Item Code" ID="Label2" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <input type="text" id="txtItemCode"  class="ItemCode empty1" style="width: 174px; height: 15px;margin-left:36px;" />
                                    <br />
    <asp:Label Text="Bar Code" ID="Label1" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <input type="text" id="txtBarCode" class="BarCode empty1" style="width: 250px; height: 15px;margin-left:42px;" />
     <asp:Label Text="BarCode Categ" ID="Label23" runat="server"  Style="margin-left:2px;"></asp:Label>
                                     <select id="lstBarCodeCategory" style="list-style-type:disc;width:91px; margin-left:5px;">
                                    </select>
                                    <br />
   <asp:Label Text="Item Name" ID="Label18" runat="server"  Style="margin-left:290px;"></asp:Label>   
                                    <input type="text" id="txtItemName" class="SearchBox empty1" style="width: 454px; height: 20px;margin-left:31px;margin-top:2px;" />  
                                    <input type="button" id="btnSave1"  value="reset" onclick="empty1()" />
                                    <br />
    <asp:Label Text="Refernce-1" ID="Label15" runat="server"  Style="margin-left:290px;"></asp:Label> 
                                    <input type="text" id="txtReference1" class="Reference1 empty1" style="width: 150px; height: 18px;margin-left:32px;margin-top:2px;" />
    <asp:Label Text="Refernce-2:" ID="Label16" runat="server"  Style="margin-left:0px;"></asp:Label>                     
                                    <input type="text" id="txtReference2" class="Reference2 empty1" style="width: 150px; height: 18px;" />
   
    <asp:Label Text="Unit" ID="Label19" runat="server"  Style="margin-left:0px;"></asp:Label>  
                                    <select id="lstUnit" style="width: 68px; height: 18px;margin-left:8px;">    
                                    </select> 
                                    <a  target="_blank" href="/Registration/Managment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn2">Add</a>
                                    <button type="button" value="ref"  onclick="AccountUnitList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
   
<br />
        <asp:Label Text="Sale Price/Unit" ID="Label20" runat="server"  Style="margin-left:290px;"></asp:Label>  
                                    <input type="text" id="txtSellingPrice" class="SellingPrice empty1" style="width: 90px; height: 18px;margin-left:6px;margin-top:2px;" />
                       Min Limit
                                    <input type="text" value="0" id="txtMinLimit" class="MinLimit empty1" style="width: 90px; height: 18px;" />
                       Max Limit
                                    <input type="text" value="0" id="txtMaxLimit" class="MaxLimit empty1" style="width: 98px; height: 18px;" />
                       Pieces in Packing
                                <input type="text" value="1" id="txtPiecesInPacking" class="PiecesInPacking empty1" style="width: 90px; height: 18px;" />
    <br />
    <asp:Label Text="Brand Name" ID="Label21" runat="server"  Style="margin-left:290px;"></asp:Label>  
                      <select id="lstBrand"  style="list-style-type:disc;width:128px; margin-left:22px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/BrandManagement.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn4">Add</a>
                                    <button type="button" value="ref"  onclick="GetBrandList2()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    
                       Reorder Point
                                    <input type="text"  id="txtReOrdLevel" class="ReOrdLevel empty1" style="width: 90px; height: 18px;margin-top:2px;" />
                       Reorder Quantity
                                    <input type="text" value="1" id="txtReorderQty" class="ReorderQty empty1" style="width: 90px; height: 18px;" />
                                    
                                    
                            <br />
    <asp:Label Text="Item Type" ID="Label22" runat="server"  Style="margin-left:290px;"></asp:Label>  
                       <select id="lstItemType" style="width: 250px; height: 20px;margin-left:38px;">    
                                    </select> 
                                    
                                    <label><input type="checkbox" id="chkItemIsActive" checked/>Item is Active</label>
                                    <label>  <input type="checkbox" id="ChkVisiable" checked   /> Visiable</label>
                                    
                               <br />
    <asp:Label Text="Other Info." ID="Label26" runat="server"  Style="margin-left:290px;"></asp:Label>   
                                    <input type="text" id="txtOtherInFo" class="OtherInFo empty1" style="width: 454px; height: 15px;margin-left:31px;margin-top:2px;" />
    <hr />    
  
    <asp:Label Text="Manufacturer" ID="Label3" runat="server"  Style="margin-left:290px;"></asp:Label>
                                     <select id="lstManufacturer" style="list-style-type:disc;width:300px; margin-left:16px; height:20px;">
                                    </select>
                                    <a  target="_blank" href="/Registration/ManufacturerManagment.aspx"  style="height: 30px; border-radius: 5px;" id="ManfBtn5">Add</a>
                                    <button type="button" value="ref"  onclick="GetManufacturerList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
    <asp:Label Text="Packing" ID="Label4" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstPacking" style="list-style-type:disc;width:300px; margin-left:51px;height:20px;margin-top:2px;">
                                    </select>                         
                                    <a  target="_blank" href="/Registration/ItemRegMannufacturerManagement.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn4">Add</a>
                                    <button type="button" value="ref"  onclick="GetPackingList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
     <asp:Label Text="Category" ID="Label5" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstCategory" style="list-style-type:disc;width:300px; margin-left:44px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/CategoryManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn1">Add</a>
                                    <button type="button" value="ref"  onclick="GetCategoryList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
     <asp:Label Text=" Class/Warranty" ID="Label6" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstClass" style="list-style-type:disc;width:300px; margin-left:5px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/ClassManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn2">Add</a>
                                    <button type="button" value="ref"  onclick="GetClassList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
     <asp:Label Text="Godown" ID="Label7" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstGoDown" style="list-style-type:disc;width:300px; margin-left:48px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/GoDownManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn3">Add</a>
                                    <button type="button" value="ref"  onclick="GetGoDownList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
                                    <br />
    <asp:Label Text="Order Quantity" ID="Label8" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <input type="text" value="0" id="txtOrderQTY" class="OrderQTY empty1" style="width: 100px; height: 17px;margin-left:6px;margin-top:2px;" />
    <asp:Label Text="Bonus Quantity" ID="Label9" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <input type="text" value="0" id="txtBonusQTY" class="BonusQTY empty1" style="width: 100px; height: 17px;"  />
    <asp:Label Text="GST %" ID="Label10" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <input type="text" value="0" id="txtGST_Rate" class="GST_Rate empty1" style="width: 100px; height: 17px;" />
                                    <input type="checkbox" id="chkGSTApply"  />Apply GST
                                    <br />
    <asp:Label Text="Item Nature" ID="Label11" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstItemNature" style="list-style-type:disc;width:300px; margin-left:26px;height:20px;margin-top:2px;">    
                                    </select>

                            
    <asp:Label Text="Income Account" ID="Label12" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <select id="lstIncomeAccount" style="width: 174px; height: 20px;">    
                                    </select>
                                    
                                    <br />
    <asp:Label Text="COGS Account" ID="Label13" runat="server"  Style="margin-left:290px;"></asp:Label>
    <select id="lstCOGS" style="width: 174px; height: 20px;margin-left:2px;">    
                                    </select>
                                    
    <asp:Label Text="Asset Account" ID="Label14" runat="server"  Style="margin-left:0px;"></asp:Label>
                                    <select id="lstAssetAccount1" style="width: 174px; height: 20px;">    
                                    </select>
                                    <br />
    <asp:Label Text="Height" ID="Label24" runat="server"  Style="margin-left:290px;"></asp:Label>
                                    <select id="lstHeight" style="list-style-type:disc;width:70px; margin-left:59px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href=""  style="height: 30px; border-radius: 5px;" id="ManfBtn5">Add</a>
                                    <button type="button" value="ref"  onclick=" HeightList()" style="border:none;" ><img src="/Images/arrow.png" /></button>

    <asp:Label Text="Width" ID="Label25" runat="server"  Style="margin-left:24px;"></asp:Label>
                                    <select id="lstWidth" style="list-style-type:disc;width:70px; margin-left:26px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href=""  style="height: 30px; border-radius: 5px;" id="ManfBtn5">Add</a>
                                    <button type="button" value="ref"  onclick="WidthList()" style="border:none;" ><img src="/Images/arrow.png" /></button>
    <asp:Label Text="Color" ID="Label17" runat="server"  Style="margin-left:0px;"></asp:Label>  
                                    <select id="lstColor" style="list-style-type:disc;width:80px; margin-left:0px;height:20px;margin-top:2px;">    
                                    </select>
                                    <a  target="_blank" href="/Registration/ColorManagment.aspx" class="btn btn-warning" style="height: 30px; border-radius: 5px;" id="ManfBtn2">Add</a>
                                    <button type="button"   onclick="GetColorList()" style="border:none;" ><img src="/Images/arrow.png" /></button>

    <br />
    <br />
     <center>                       <input type="button" id="btnSave" class="btnSave1" value="save" onclick="save()" />
                                    <input type="button" id="btnEdit"  value="delete" onclick="DeleteItem()" />
     </center>
   
    
   

    <script src="../Script/jquery.min.js"></script>
    <script src="../Script/jquery-ui.min.js"></script>
        
    <script src="../Script/Autocomplete.js"></script>
   



    <script type="text/javascript">

       
        function empty1() {
            $(".empty1").val("");
        }
        
        function GetItemData() {
            
            var ItCode = $(".ItemCode").val();
            
            empty1();
            $(".ItemCode").val(ItCode);
            var ItemCode = $(".ItemCode").val();
            $.ajax({
                url: '/WebPOSService.asmx/GeItemDataAgainstItemCode',
                type: "Post",
                data: { "ItemCode": ItemCode },
                success: function (ItemModel) {
                    
                    document.getElementById("chkItemIsActive").checked = false;
                    var Active = ItemModel.Active;
                    if (Active == 1) { document.getElementById("chkItemIsActive").checked = true; }

                    document.getElementById("ChkVisiable").checked = false;
                    var Visiable = ItemModel.Visiable;
                    
                    if (Visiable == 1) { document.getElementById("ChkVisiable").checked = true; }
                    


                    document.getElementById("chkGSTApply").checked = false;
                    var GST_Apply = ItemModel.GST_Apply;
                    if (GST_Apply == 1) { document.getElementById("chkGSTApply").checked = true; }
                    
                    
                    //document.getElementById("chkCreditLimitApply").checked = false;
                    //if (PartyModel.CreditLimitApply == 1) { document.getElementById("chkCreditLimitApply").checked = true; }
                    //document.getElementById("chkDeal").checked = false;
                    //if (PartyModel.chkDeal == 1) { document.getElementById("chkDeal").checked = true; }

                    //document.getElementById("chkPerDiscount").checked = false;

                    //if (PartyModel.chkPerDiscount == 1) { document.getElementById("chkPerDiscount").checked = true; }
                    


                    $(".SearchBox").val(ItemModel.Description);
                    $(".SellingPrice").val(ItemModel.SellingCost1);
                    $(".Reference1").val(ItemModel.Reference);
                    $(".Reference2").val(ItemModel.Reference2);
                    $(".MinLimit").val(ItemModel.MinLimit);
                    $(".MaxLimit").val(ItemModel.MaxLimit);
                    $(".PiecesInPacking").val(ItemModel.PiecesInPacking);
                    $(".ReOrdLevel").val(ItemModel.ReorderLevel);
                    $(".ReorderQty").val(ItemModel.ReorderQty);
                    $(".OtherInFo").val(ItemModel.OtherInFo);
                    $(".OrderQTY").val(ItemModel.OrderQTY);
                    $(".BonusQTY").val(ItemModel.BonusQTY);
                    $(".GST_Rate").val(ItemModel.GST_Rate);

                    var BarCodeCategory = ItemModel.BarCodeCategory;
                    $("#lstBarCodeCategory").val(BarCodeCategory);
                    
                    
                    var AccountUnit = ItemModel.AccountUnit;
                    
                    $("#lstUnit").val(AccountUnit);
                    
                    var Brand = ItemModel.Brand;
                    $("#lstBrand").val(Brand);
                    
                    var ItemType = ItemModel.ItemType;
                    $("#lstItemType").val(ItemType);
                    
                    var Manufacturer = ItemModel.Manufacturer;
                    $("#lstManufacturer").val(Manufacturer);
                    
                    var Packing = ItemModel.Packing;
                    $("#lstPacking").val(Packing);
                    
                    var Category = ItemModel.Category;
                    $("#lstCategory").val(Category);
                    
                    var Class = ItemModel.Class;
                    $("#lstClass").val(Class);
                    

                    var Godown = ItemModel.Godown;
                    $("#lstGoDown").val(Godown);
                    

                    var Nature = ItemModel.Nature;
                    $("#lstItemNature").val(Nature);
                    
                    var Revenue_Code = ItemModel.Revenue_Code;
                    $("#lstIncomeAccount").val(Revenue_Code);
                   
                    

                    var Color = ItemModel.Color;
                    $("#lstColor").val(Color);
                    

                    var Height = ItemModel.Height;
                    $("#lstHeight").val(Height);
                    
                   

                    var Width = ItemModel.Length;
                   
                    $("#lstWidth").val(Width);
                    
                    
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function DeleteItem() {
            var ItemCode = $(".ItemCode").val();
            

            var ItemModel = {
                Code: ItemCode,
            
            }


            debugger
            $.ajax({
                url: "/Correction/ItemRegistrationEdit.aspx/DeleteItem",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ ItemModel: ItemModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        $(".empty1").val("");
                        alert(BaseModel.d.Message);
                    }
                    else {
                        alert(BaseModel.d.Message);
                    }


                }

            });

        }
        
        function save() {
            var ItemCode=$(".ItemCode").val();
            var BarCode = $(".BarCode").val();
            var Description = $("#txtItemName").val();
            var Reference1 = $(".Reference1").val();
            var Reference2 = $(".Reference2").val();
            var PiecesInPacking = $(".PiecesInPacking").val();
            var Brand = $("#lstBrand").val();
            var chkVisiable = document.getElementById("ChkVisiable").checked ? "1" : "0";
            
            var ReOrdLevel = $(".ReOrdLevel").val();
            var Manufacturer = $("#lstManufacturer").val();
            var Packing = $("#lstPacking").val();
            var Category = $("#lstCategory").val();
            var Class = $("#lstClass").val();
            var GoDown = $("#lstGoDown").val();
            var ReOrdQuantity = $(".ReorderQty").val();
            var OrderQty = $(".OrderQTY").val();
            var BonusQty = $(".BonusQTY").val();
            var ItemNature = $("#lstItemNature").val();
            var AccountUnit = $("#lstUnit").val();
            var SellingCost = $(".SellingPrice").val();
            var Revenue_Code = $("#lstIncomeAccount").val();
            var chkActive = document.getElementById("chkItemIsActive").checked ? "1" : "0";
            var GSTRate = $(".GST_Rate").val();
            var chkGSTApply = document.getElementById("chkGSTApply").checked ? "1" : "0";
            var MinLimit = $(".MinLimit").val();
            var MaxLimit = $(".MaxLimit").val();
            var lstCGS_Code = $("#lstCOGS").val();
            var lstItemType=$("#lstItemType").val();
            var lstWidth=$("#lstWidth").val();
            var lstHeight=$("#lstHeight").val();
            var lstBarCodeCategory=$("#lstBarCodeCategory").val();
            var lstColor=$("#lstColor").val();
            var OtherInFo = $(".OtherInFo").val();
            if (PiecesInPacking == "") { PiecesInPacking = 1 }
            if (ReOrdLevel == "") { ReOrdLevel = 1 }
            if (ReOrdQuantity == "") { ReOrdQuantity = 1 }
            if (OrderQty == "") { OrderQty = 0 }
            if (BonusQty == "") { BonusQty = 0 }
            if (SellingCost == "") { SellingCost = 0 }
            if (GSTRate == "") { GSTRate = 0 }
            if (MinLimit == "") { MinLimit = 0 }
            if (MaxLimit == "") { MaxLimit = 0 }

            PiecesInPacking = isNaN(PiecesInPacking) ? 1 : PiecesInPacking;
            ReOrdLevel = isNaN(ReOrdLevel) ? 1 : ReOrdLevel;
            ReOrdQuantity = isNaN(ReOrdQuantity) ? 1 : ReOrdQuantity;
            OrderQty = isNaN(OrderQty) ? 0 : OrderQty;
            BonusQty = isNaN(BonusQty) ? 0 : BonusQty;
            SellingCost = isNaN(SellingCost) ? 0 : SellingCost;
            GSTRate = isNaN(GSTRate) ? 0 : GSTRate;
            MinLimit = isNaN(MinLimit) ? 0 : MinLimit;
            MaxLimit = isNaN(MaxLimit) ? 0 : MaxLimit;
            
            var ItemModel = {
                Code:ItemCode,
                BarCode:BarCode,
                Description: Description,
                
                Reference: Reference1,
                Reference2: Reference2,
                PiecesInPacking:PiecesInPacking,
                Brand:Brand,
                Visiable: chkVisiable,
                ReorderLevel: ReOrdLevel,
                Manufacturer:Manufacturer,
                Packing:Packing,
                Category:Category,
                Class:Class,
                Godown:GoDown,
                ReorderQty:ReOrdQuantity,
                OrderQTY:OrderQty,
                BonusQTY:BonusQty,
                Nature:ItemNature,
                AccountUnit:AccountUnit,
                SellingCost1: SellingCost,
                Revenue_Code:Revenue_Code,
                Active:chkActive,
                GST_Rate:GSTRate,
                GST_Apply:chkGSTApply,
                MinLimit:MinLimit,
                MaxLimit:MaxLimit,
                CGS_Code:lstCGS_Code,
                ItemType:lstItemType,
                Length:lstWidth,
                Height:lstHeight,
                BarCodeCategory: lstBarCodeCategory,
                Color: lstColor,
                OtherInFo:OtherInFo
            }


            debugger
            $.ajax({
                url: "/Correction/ItemRegistrationEdit.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ ItemModel: ItemModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        $(".empty1").val("");
                        alert(BaseModel.d.Message);
                    }
                    else {
                        alert(BaseModel.d.Message);
                    }


                }

            });

        }
        $(document).ready(function () {
            GetGoDownList();
            GetManufacturerList();
            GetCategoryList();
            GetPackingList();
            GetClassList();
            GetItemNatureList();
            GetColorList();
            GetBarCodeCategoryList();
            HeightList();
            WidthList();
            
            AccountUnitList();
            ItemTypeList();
            COGSList();
            Income_List();
            GetBrandList2();
            
        });
        
        


    </script>
    
                                     
</asp:Content>
