﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="WithDrawFromBankEdit1.aspx.cs" Inherits="WebPOS.WithDrawFromBankEdit1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
     
    <style>

     .divSty {
     margin: auto;
    width: 60%;
    border: 3px solid red;
    padding: 10px;
    height:330px;
}


 </style>  


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                     

    <br />
                       <h2 style="text-align:center; font-size:30px; " >Edit - Cash WithDraw From Bank </h2>  
    <br />
    
    <div class="divSty">
        <br />
                    Voucher #
                   <asp:TextBox ID="txtVoucherNo"  Style="width: 150px; height: 30px; margin-top:4px;margin-left:38px;"  runat="server"></asp:TextBox>
                   <asp:Button runat="server" Text="Get" CssClass="btnGet" ID="Button1" OnClick="Button1_Click"  ></asp:Button>
       <br />
                    Date
                    <asp:TextBox CssClass="datepicker" ID="txtCurrentDate"  Style="width: 150px; height: 30px;margin-left:73px;margin-top:3px;" runat="server"></asp:TextBox>
        <br />
                
                    
         <br />         
                   Select Bank
                   <asp:DropDownList ID="lstBank" AutoPostBack="true" Style="width: 497px; height: 30px;margin-left:27px;"  runat="server" OnSelectedIndexChanged="lstBankSelectedIndexChanged"  ></asp:DropDownList>
                   <asp:TextBox  ID="txtBankCode"  Style="width: 141px; height: 30px;margin-left:1px;"  runat="server"   ></asp:TextBox>
        <br />
                   Cash
                   <asp:TextBox ID="txtAmount"  Style="width: 150px; height: 30px; margin-top:4px;margin-left:71px;"  runat="server"></asp:TextBox>
         <br />          
                    Cheque #
                   <asp:TextBox ID="txtChequeNo"  Style="width: 150px; height: 30px; margin-top:4px;margin-left:43px;"  runat="server"></asp:TextBox>
        <br />
                   Naration
                   <asp:TextBox ID="txtNarration"  Style="width: 642px; height: 30px;margin-left:47px;margin-top:4px;"  runat="server"></asp:TextBox>
         <br />
        <br />
        <center>       
                   <asp:Button runat="server" Text="Save"  CssClass="btnSave" ID="btnSave" OnClick="btnSave_Click" ></asp:Button>
                   <asp:Button runat="server" Text="Delete" CssClass="btnDelete"  ID="btnDelete" OnClick="btnDelete_Click"  ></asp:Button>
        </center>    
    </div>
</asp:Content>
