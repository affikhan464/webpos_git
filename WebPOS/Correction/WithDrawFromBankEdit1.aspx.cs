﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
namespace WebPOS
{
    public partial class WithDrawFromBankEdit1 : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        
        Module7 objModule7 = new Module7();
        Module4 objModule4 = new Module4();
        ModGLCode objModGLCode = new ModGLCode();
        Module1 objModule1 = new Module1();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadBankList();
                    txtCurrentDate.Text = DateTime.Now.ToString("d");
                }
            }
            catch (Exception ex)
            {
                txtNarration.Text = ex.ToString();

            }
        }
        void LoadBankList()
        {
            SqlCommand cmd = new SqlCommand("select Title from GLCode where CompID='" + CompID + "' and Code Like '01010102%' and  Lvl=5 order by Title", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBank.DataSource = dset;
            lstBank.DataBind();
            lstBank.DataValueField = "Title";
            lstBank.DataTextField = "Title";
            lstBank.DataBind();
        }
        
        protected void lstBankSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string GLCode = objModGLCode.GLCodeAgainstGLTitle(lstBank.Text);
                txtBankCode.Text = GLCode; // OpeningBalanceOfGL(GLCode);
                
            }
            catch (Exception ex)
            {
                txtNarration.Text = ex.ToString();
            }

        }
        void saveData()
        {


            

            decimal Amount = 0;
            string GLCode = "";

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            try
            {
                SqlCommand cmdDeleteOld2 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(txtVoucherNo.Text) + " and  V_Type='CRV' and SecondDescription='CashWithDrawForPettyCash'", con, tran);
                cmdDeleteOld2.ExecuteNonQuery();

                
                if (txtAmount.Text != "") { Amount = Convert.ToDecimal(txtAmount.Text); }
                GLCode = txtBankCode.Text; //  objModPartyCodeAgainstName.PartyCode(lstGL.Text);
                string CashAccountGLCode = "0101010100001";
                string SysTime = Convert.ToString(DateTime.Now);
                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,bankcode,datedr,Narration,SecondDescription,System_Date_Time,Code1,ChequeNo,CompId) values ('" + CashAccountGLCode + "','" + txtNarration.Text + "'," + Convert.ToDecimal(txtAmount.Text)  + "," + txtVoucherNo.Text + ",'" + "CRV" + "','" + txtBankCode.Text + "','"  + Convert.ToDateTime(txtCurrentDate.Text)  + "','" + txtNarration.Text + "','" + "CashWithDrawForPettyCash" + "','" + SysTime + "','" + CashAccountGLCode + "','" + txtChequeNo.Text + "','" + CompID + "')", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger(Code, Description, AmountCr, V_No, V_Type, bankcode, datedr, Narration, SecondDescription, System_Date_Time, Code1, ChequeNo,CompId) values('" + txtBankCode.Text + "', '" + txtNarration.Text + "', " + Convert.ToDecimal(txtAmount.Text) + ", " + txtVoucherNo.Text + ", '" + "CRV" + "', '" + txtBankCode.Text + "', '" + Convert.ToDateTime(txtCurrentDate.Text) + "', '" + txtNarration.Text + "', '" + "CashWithDrawForPettyCash" + "', '" + SysTime + "', '" + CashAccountGLCode + "', '" + txtChequeNo.Text + "', '" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();

                tran.Commit();
                
                con.Close();
                txtAmount.Text = "";
                txtChequeNo.Text = "";
                txtBankCode.Text = "";
                txtNarration.Text = "";
            }
            catch (Exception ex)
            {
                con.Close();
                tran.Rollback();
                txtNarration.Text = ex.ToString();
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtBankCode.Text != "" && txtAmount.Text != "")
            {
                saveData();
            }
        }

        public void GetDataofVoucher()
        {
            txtBankCode.Text = "";
            txtNarration.Text = "";
            txtAmount.Text = "";

            SqlDataAdapter cmd = new SqlDataAdapter("SELECT dateDr, AmountDr, AmountCr, V_Type, V_No, Description, VenderCode, Narration, BankCode,ChequeNo FROM GeneralLedger WHERE V_Type ='CRV' AND SecondDescription = 'CashWithDrawForPettyCash' and V_No =" + txtVoucherNo.Text, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            DateTime Date1;
            if (dt.Rows.Count > 0)
            {
                Date1 = Convert.ToDateTime(dt.Rows[0]["dateDr"]);
                txtCurrentDate.Text = Date1.ToString("d");
                if (Convert.ToDecimal(dt.Rows[0]["AmountCr"]) > 0)
                {
                    txtAmount.Text = Convert.ToString(dt.Rows[0]["AmountCr"]);
                }
                else
                { txtAmount.Text = Convert.ToString(dt.Rows[0]["AmountDr"]); }
                txtBankCode.Text = Convert.ToString(dt.Rows[0]["BankCode"]);
                lstBank.Text = objModGLCode.GLTitleAgainstCode(Convert.ToString(dt.Rows[0]["BankCode"]));
                txtChequeNo.Text = Convert.ToString(dt.Rows[0]["ChequeNo"]);
                txtNarration.Text = Convert.ToString(dt.Rows[0]["Narration"]);

            }
        }
        private void DeleteVoucher()
        {

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";

            try
            {


                SqlCommand cmdDeleteOld2 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(txtVoucherNo.Text) + " and  V_Type='CRV' and SecondDescription='CashWithDrawForPettyCash'", con, tran);
                cmdDeleteOld2.ExecuteNonQuery();



                txtAmount.Text = "";
                txtBankCode.Text = "";
                txtNarration.Text = "";
                txtChequeNo.Text = "";
                tran.Commit();
                con.Close();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                txtNarration.Text = ex.ToString();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            GetDataofVoucher();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtVoucherNo.Text != "")
            {
                DeleteVoucher();
            }
        }
    }
}