﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyRegistrationEdit.aspx.cs" Inherits="WebPOS.PartyRegistrationEdit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-user-plus">Party Registration</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                        <span class="input input--hoshi">
                            <input id="txtPartyCode" disabled="disabled" class="PartyCode input__field input__field--hoshi" disabled="disabled" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi radio--hoshi disableClick">
                            <label>
                                <input id="Client" name="NorBalance" checked="checked" value="1" class="radio" type="radio" />
                                <span></span>
                                Client
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi radio--hoshi disableClick">
                            <label>
                                <input id="Supplyer" name="NorBalance" class="radio" value="2" type="radio" />
                                <span></span>
                                Supplyer
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-10">
                        <span class="input input--hoshi">
                            <input id="txtPartyName" class="PartyBox PartyName autocomplete empty1  input__field input__field--hoshi" data-type="Party" data-id="txtPartyName" data-function="GetParty" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Name </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  href="" ><i class="fas fa-sync"></i>Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtContactPerson" class="ContactPerson empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Contact Person Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtMobileNo" class="MobileNo empty input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Mobile No. </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtFaxNo" class="FaxNo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Fax No. </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtLandLineNo" class="LandLineNo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Land Line No. </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">
                            <input id="txtAddress" class="Address empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="txtEmail" class="Email empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">E-mail </span>
                            </label>
                        </span>
                    </div>


                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi  input--filled">
                            <select id="lstSellingPriceNo" class="round input__field--hoshi">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Price </span>
                            </label>
                        </span>
                    </div>




                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi  input--filled">
                            <select id="lstDealApplyNo" class="round input__field--hoshi">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Deal Apply No. </span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtCreditLimit" class="CreditLimit empty1 input__field input__field--hoshi" min="0" type="number" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Credit Limit  </span>
                            </label>
                        </span>
                    </div>
                     <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="chkCreditLimitApply" class="checkbox" type="checkbox" />
                                <span></span>
                                Credit Limit Apply
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtOtherInfo" class="OtherInfo empty1 input__field input__field--hoshi" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Other Info </span>
                            </label>
                        </span>
                    </div>

                    

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                        <span class="input input--hoshi  input--filled">
                            <select id="lstSaleMan" class="round input__field--hoshi">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">SaleMan </span>
                            </label>
                        </span>
                    </div>
                    
                     <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="chkDeal" class="checkbox" type="checkbox" />
                                <span></span>
                                Deal
                            </label>
                        </span>
                    </div>
                    
                     <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="chkPerDiscount" class="checkbox" type="checkbox" />
                                <span></span>
                                Per. Discount
                            </label>
                        </span>
                    </div>

                </div>
                <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a  id="btnDelete" onclick="deleteParty()" class="btnDelete btn btn-3 btn-bm btn-3e fa-trash w-100">Delete</a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()"  class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

    </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="../Script/Autocomplete.js"></script>
    <script type="text/javascript">
        function SaveBefore() {
            var NorBalance = 0;
            NorBalance = document.getElementById("Client").checked ? "1" : "2";
            var PartyName = $(".PartyName").val();
            if (NorBalance == 0 && PartyName == "") {
                if (NorBalance == 0) {
                    swal("Failed", "Define Nature", "error");
                }
                else
                    swal("Failed", "Enter Party Name", "error");

            }
            else save();





        }
        function empty1() {
            $(".empty1").val("");
            document.getElementById("chkCreditLimitApply").checked = false;
            document.getElementById("chkPerDiscount").checked = false;
            document.getElementById("Supplyer").checked = false;
            document.getElementById("Client").checked = true;
            document.getElementById("chkDeal").checked = false;
        }
        function save() {
            var NorBalance = document.getElementById("Client").checked ? "1" : "2";
            var PartyCode=$(".PartyCode").val();
            var PartyName = $(".PartyName").val();
            var ContactPerson = $(".ContactPerson").val();
            var MobileNo = $(".MobileNo").val();
            var FaxNo = $(".FaxNo").val();
            var LandLineNo = $(".LandLineNo").val();
            var Address = $(".Address").val();
            var Email = $(".Email").val();
            var lstSellingPriceNo = $("#lstSellingPriceNo").val();
            var lstDealApplyNo = $("#lstDealApplyNo").val();
            var CreditLimit = $("#txtCreditLimit").val();
            var chkCreditLimitApply = document.getElementById("chkCreditLimitApply").checked ? "1" : "0";
            

            var OtherInfo = $(".OtherInfo").val();
            var lstSaleMan = $("#lstSaleMan").val();
            
            var chkDeal = document.getElementById("chkDeal").checked ? "1" : "0";
            var chkPerDiscount = document.getElementById("chkPerDiscount").checked ? "1" : "0";


            if (CreditLimit == "") { CreditLimit = 0 }
            CreditLimit = isNaN(CreditLimit) ? 0 : CreditLimit;
            
            var PartyModel = {
                NorBalance: NorBalance,
                PartyCode:PartyCode,
                 PartyName: PartyName,
                 ContactPerson: ContactPerson,
                 MobileNo: MobileNo,
                 FaxNo: FaxNo,
                 LandLineNo: LandLineNo,
                 Address: Address,
                 Email: Email,
                 lstSellingPriceNo: lstSellingPriceNo,
                 lstDealApplyNo: lstDealApplyNo,
                 CreditLimit: CreditLimit,
                 CreditLimitApply: chkCreditLimitApply,
                 OtherInfo: OtherInfo,
                 lstSaleMan: lstSaleMan,
                 chkDeal: chkDeal,
                 chkPerDiscount: chkPerDiscount 
            }
            debugger
            $.ajax({
                url: "/Correction/PartyRegistrationEdit.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ PartyModel: PartyModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        empty1();
                        swal("Updated", BaseModel.d.Message, "success");
                       
                    }
                    else {
                        swal("Failed", BaseModel.d.Message, "error");
                    }


                }

            });

        }
       
        function deleteParty() {

            var PartyCode = $(".PartyCode").val();


            var PartyModel = {
                PartyCode: PartyCode
            }
            swal({
                title: 'Are you sure to delete?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes Delete It',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {
                        deleteAttribute(PartyModel);
                        resolve();
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            })




        }
        function deleteAttribute(PartyModel) {
          
            $.ajax({
                url: "/Correction/PartyRegistrationEdit.aspx/deleteParty",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ PartyModel: PartyModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        empty1();
                        swal("Deleted!!", BaseModel.d.Message, "success");
                    }
                    else {
                        swal("Failed!!", BaseModel.d.Message, "error");

                    }


                }

            });
        }

        $(document).ready(function () {
            GetSaleManList();
            GetSellingPriceList();
            GetDealApplyNoList();
        });
        function GetPartyData(PartyCode) {
            debugger
            $.ajax({
                url: '/WebPOSService.asmx/GetPartyDataAgainstPartyCode',
                type: "Post",
                data: { "PartyCode": PartyCode },
                success: function (PartyModel) {
                    var PCode = $(".PartyCode").val();
                    empty1();
                    $(".PartyCode").val(PartyCode);

                    document.getElementById("Client").checked = true;
                    var NorBalance = PartyModel.NorBalance;
                    if (NorBalance == 2) { document.getElementById("Supplyer").checked = true; }
                    
                    
                    document.getElementById("chkCreditLimitApply").checked = false;
                    if (PartyModel.CreditLimitApply == 1) { document.getElementById("chkCreditLimitApply").checked = true;}
                    
                    document.getElementById("chkDeal").checked = false;
                    if (PartyModel.chkDeal == 1) { document.getElementById("chkDeal").checked = true; }

                    document.getElementById("chkPerDiscount").checked=false;
                    
                    if (PartyModel.chkPerDiscount == 1) { document.getElementById("chkPerDiscount").checked = true; }

                    


                    $(".PartyName").val(PartyModel.PartyName);
                    $(".ContactPerson").val(PartyModel.ContactPerson);
                    $("#txtMobileNo").val(PartyModel.MobileNo);
                    
                    $(".FaxNo").val(PartyModel.FaxNo);
                    $(".LandLineNo").val(PartyModel.LandLineNo);
                    $(".Address").val(PartyModel.Address);
                    $(".Email").val(PartyModel.Email);
                    $("#lstSellingPriceNo").val(PartyModel.lstSellingPriceNo);
                    $("#lstDealApplyNo").val(PartyModel.lstDealApplyNo);
                    $("#txtCreditLimit").val(PartyModel.CreditLimit);
                    
                    $(".OtherInfo").val(PartyModel.OtherInfo);
                    $("#lstSaleMan").val(PartyModel.lstSaleMan);
                    updateInputStyle();
                    
                },
                fail: function (jqXhr, exception) {
                }
            });
        }

        function GetSaleManList() {
            $.ajax({
                url: '/WebPOSService.asmx/SaleMan_List',
                type: "Post",
                success: function (BrandList) {
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        if (code == 1) {
                            $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstSaleMan");
                        } else {
                            $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstSaleMan");
                        }
                    }
                },
                fail: function (jqXhr, exception) {
                }
            });
        }
        function GetSellingPriceList() {

            for (var i = 1; i < 6; i++) {
                var code = i;
                var name = i;
                $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstSellingPriceNo");

            }
        }
        function GetDealApplyNoList() {

            for (var i = 1; i < 4; i++) {
                var code = i;
                var name = i;
                $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstDealApplyNo");
            }
        }

    </script>
    


</asp:Content>
