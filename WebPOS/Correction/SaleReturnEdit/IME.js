﻿
$(document).on("keypress", "#txtBarCodeSR1", function (e) {

    if (e.which == 13 && $("#txtBarCodeSR1").val().length && $('.PartyCode').val().length) {

        dataFunction = "GetRecordSR";
        textboxid = "txtBarCodeSR1";
        getRecordsSR();
        $("#" + textboxid).select();
    } else if (!$("#txtBarCodeSR1").val().length) {
        playError();
    } else if (!$('.PartyCode').val().length) {
        playError();
        swal('Select Party First!!', '', 'error');
    }
})

function getRecordsSR() {   //  Sale New/Edit 11111111111111
    
    var ajaxData = "";
    if (dataFunction == "GetRecordSR") {

        var itemName = $("[data-id=" + textboxid + "]").length ? $("[data-id=" + textboxid + "]").val() : $("#" + textboxid).val();
        if (itemName.length > 10 && !isNaN(itemName)) {
            ajaxData = { "key": itemName + ',' + $('.PartyCode').val() + ',' + $('.PartyDealApplyNo').val() };
        }
        else
            ajaxData = { "key": itemName };

    }
    else {
        ajaxData = { "key": $("[data-id=" + textboxid + "]").val() };
    }
    debugger
    $.ajax({
        async: false,
        url: '/WebPOSService.asmx/' + dataFunction,
        type: "Post",

        data: ajaxData,

        success: function (items) {

            if (items == "") {
                playError();
            }

            if (items.length != 0) {

                if (items[0].isIMESR) {
                    var imei = $("#" + textboxid).val();
                    if (!items[0].isItemAvailble) {
                        swal('IME Not Found or Sold to Other Party!!', '', 'error');
                        playError();
                        return false;
                    }
                    var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                    var Sno = rowCount;
                    var itemExist = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                    if (itemExist > 0 && !isDuplicateIMEI(imei)) {
                        Sno = itemExist;
                        var qty = Number($("#myTable ." + Sno + "_Qty").val()) + 1;
                        $("#myTable ." + Sno + "_Qty").val(qty);
                        calculationSale();

                        //Ime Row Insert Start --> This insert IME Row

                        $("#IMEIRow [name=ImeiTxbx]").val(imei);
                        var element = $("#myTable tr[data-rownumber=" + Sno + "]")[0];
                        appendRowInIMEI(element, true);
                        //Ime Row Insert End
                    }
                    else if (!isDuplicateIMEI(imei)) {
                        insertSaleRow(Sno, items[0].Code, items[0].Description, items[0].Qty, items[0].Rate, 0, items[0].ItemDis, items[0].PerDis, items[0].DealDis, 0, 0, items[0].avrgCost, items[0].revCode, items[0].cgsCode, items[0].actualSellingPrice)
                        calculationSale();

                        //Ime Row Insert Start --> This insert IME Row

                        $("#IMEIRow [name=ImeiTxbx]").val(imei);
                        var rowNumer = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                        var element = $("#myTable tr[data-rownumber=" + rowNumer + "]")[0];
                        appendRowInIMEI(element, true);
                        //Ime Row Insert End
                    }
                    else if (isDuplicateIMEI(imei)) {
                        swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
                        playError();

                        return false;
                    }




                    rerenderSerialNumber();

                }
            }



                else {
                    $("#" + dataType + "Table").remove();
                    $("#" + dataType + "HeadTable").remove();
                    $("#" + dataType + "Table table").remove();
                    $("#" + dataType + "HeadTable table").remove();
                    $(".selected").remove();
                }
            }, fail: function (jqXhr, exception) {

            }
        });
}



















