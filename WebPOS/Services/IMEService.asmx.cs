﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using MoreLinq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS.Services
{
    /// <summary>
    /// Summary description for IMEService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class IMEService : System.Web.Services.WebService
    {

        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIMEList(string key,string descriptionType)
        {
            try
            {
                var descriptionTypes = descriptionType.Split(',');
                var descriptionTypeWhereClause = "[InventorySerialNoFinal].[Description]='" + descriptionTypes[0] + "'";
                if (descriptionTypes.Length>1)
                {
                    descriptionTypeWhereClause = string.Empty;
                    for (int i = 0; i < descriptionTypes.Length; i++)
                    {
                        if (i==0)
                        {
                            descriptionTypeWhereClause += "[InventorySerialNoFinal].[Description]='" + descriptionTypes[i] + "'";
                        }
                        else
                        {
                            descriptionTypeWhereClause += " Or [InventorySerialNoFinal].[Description]='" + descriptionTypes[i] + "'";
                        }

                    }
                }
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT 
                        InvCode.Description
                        ,[InventorySerialNoFinal].[Description] as IMEDescrition
                        ,[IME]
                    FROM[InventorySerialNoFinal]
                    Left join InvCode on InventorySerialNoFinal.ItemCode = InvCode.Code
                    WHERE IME like '%" + key + "%' and (" +
                   descriptionTypeWhereClause+ ") and sold=0 order by IME desc";

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                var data = objDs.Tables[0];
                var rows = new List<IMEModel>();

                if (objDs.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string itemName = data.Rows[i]["Description"].ToString();
                        string iMEDescrition = data.Rows[i]["IMEDescrition"].ToString();
                        string iME = data.Rows[i]["IME"].ToString();

                        var row = new IMEModel()
                        {
                            IME = iME,
                            IMEDescrition = iMEDescrition,
                            ItemName = itemName
                        };
                        rows.Add(row);
                    }
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(rows.DistinctBy(a=>a.IME).OrderBy(a=> a.IME.Contains(key)).Take(20)));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemIMEList(string key,string descriptionType, string itemCode)
        {
            try
            {
                var descriptionTypes = descriptionType.Split(',');
                var descriptionTypeWhereClause = "[InventorySerialNoFinal].[Description]='" + descriptionTypes[0] + "'";
                if (descriptionTypes.Length>1)
                {
                    descriptionTypeWhereClause = string.Empty;
                    for (int i = 0; i < descriptionTypes.Length; i++)
                    {
                        if (i==0)
                        {
                            descriptionTypeWhereClause += "[InventorySerialNoFinal].[Description]='" + descriptionTypes[i] + "'";
                        }
                        else
                        {
                            descriptionTypeWhereClause += " Or [InventorySerialNoFinal].[Description]='" + descriptionTypes[i] + "'";
                        }

                    }
                }
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT 
                        InvCode.Description
                        ,[InventorySerialNoFinal].[Description] as IMEDescrition
                        ,[IME]
                    FROM[InventorySerialNoFinal]
                    Left join InvCode on InventorySerialNoFinal.ItemCode = InvCode.Code
                    WHERE InvCode.Code="+itemCode+ " and IME like '%" + key + "%' and (" +
                   descriptionTypeWhereClause+ ") order by System_Date_Time desc";

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                var data = objDs.Tables[0];
                var rows = new List<IMEModel>();

                if (objDs.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string itemName = data.Rows[i]["Description"].ToString();
                        string iMEDescrition = data.Rows[i]["IMEDescrition"].ToString();
                        string iME = data.Rows[i]["IME"].ToString();

                        var row = new IMEModel()
                        {
                            IME = iME,
                            IMEDescrition = iMEDescrition,
                            ItemName = itemName
                        };
                        rows.Add(row);
                    }
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(rows.DistinctBy(a=>a.IME).OrderBy(a=> a.IME.Contains(key)).Take(20)));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }
        [WebMethod]
        public string HelloWorld(string key)
        {
            return "Hello World";
        }
    }
}
