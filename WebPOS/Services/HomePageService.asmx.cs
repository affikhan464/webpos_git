﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS
{
    /// <summary>
    /// Summary description for ItemService
    /// </summary>
    [WebService(Namespace = "http://webpos.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HomePageService : System.Web.Services.WebService
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetStats()
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = @"
            Select  top 1
           ISNULL((Select sum(qty) from InvCode),0) as AllItems,
            ISNULL((Select sum(qty) from InvCode where active=1),0) as ActiveItems,
            ISNULL((Select sum(qty) from Invoice2),0) as SoldItems,
            ISNULL((Select sum(qty) from Purchase2),0) as PurchasedItems,
            (Select count(Code) from PartyCode where NorBalance=1) as TotalClient,
            (Select count(Code) from PartyCode where NorBalance=2) as TotalVendors,
            ISNULL((Select sum(qty) from Purchase2),0) as PurchasedItems
            from InvCode";

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];

            var AllItems = Convert.ToDecimal(data.Rows[0]["AllItems"]) == 0 ? 1 : Convert.ToDecimal(data.Rows[0]["AllItems"]);
            var ActiveItems = Convert.ToDecimal(data.Rows[0]["ActiveItems"]);
            var ItemsSold = Convert.ToDecimal(data.Rows[0]["SoldItems"]);
            var PurchasedItems = Convert.ToDecimal(data.Rows[0]["PurchasedItems"]) == 0 ? 1 : Convert.ToDecimal(data.Rows[0]["PurchasedItems"]);
            var TotalClient = Convert.ToDecimal(data.Rows[0]["TotalClient"]);
            var TotalVendors = Convert.ToDecimal(data.Rows[0]["TotalVendors"]);

            var totalParties = (TotalClient + TotalVendors) == 0 ? 1 : (TotalClient + TotalVendors);
            var TotalClientPercentage = (TotalClient / totalParties) * 100;
            var TotalVendorsPercentage = (TotalVendors / totalParties) * 100;
            var ActiveItemPercentage = (ActiveItems / AllItems) * 100;
            var ItemsSoldPercentage = (ItemsSold / PurchasedItems) * 100;

            var stat = new StatsViewModel()
            {
                ActiveItems = ActiveItems,
                ActiveItemPercentage = ActiveItemPercentage,
                ItemsSold = ItemsSold,
                ItemsSoldPercentage = ItemsSoldPercentage,
                TotalActiveParties = ItemsSoldPercentage,
                TotalActivePartiesPercentage = ItemsSoldPercentage,
                TotalVendors = TotalVendors,
                TotalClient = TotalClient,
                TotalVendorsPercentage = TotalVendorsPercentage,
                TotalClientPercentage = TotalClientPercentage
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(stat));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemsWithSales()
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = @"
            SELECT top 10 InvCode.Code
                    ,InvCode.Description,
                    sum(Invoice2.qty)as Qty,
	                max(Invoice1.Date1)as Date
            FROM InvCode
            Join Invoice2 on InvCode.Code=Invoice2.Code
            Join Invoice1 on Invoice2.InvNo=Invoice1.InvNo
            group by InvCode.Code,InvCode.Description
            order by InvCode.Description";

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var items = new List<Product>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                var Description = data.Rows[i]["Description"].ToString();
                var Qty = data.Rows[i]["Qty"].ToString();
                var Date = Convert.ToDateTime(data.Rows[i]["Date"]).ToString("dd MMM, yyyy");
               


                var item = new Product()
                {
                    Description = Description,
                    Qty = Qty,
                    LastDateSold = Date
                };
                items.Add(item);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(items));
        }
    }
}
