﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS.Services
{
    /// <summary>
    /// Summary description for AttributeService
    /// </summary>
    [WebService(Namespace = "http://webpos.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AttributeService : System.Web.Services.WebService
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();
        static SqlTransaction tran;

        #region Save

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveGLCode(ChartOfAccountsViewModel model)
        {


            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open(); 

                }

                tran = con.BeginTransaction();


                Module1 objModule1 = new Module1();
                var message = "";

                if (model.Code != null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update GLCode set Balance='" + model.OpeningBalance + "',Title='" + model.Title + "',   NorBalance='" + model.NorBalance + "', GroupDet='" + model.Group + "' where Code='" + model.Code + "'", con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Account UpDated successfully";

                }
                else
                {
                    var type = "Header";
                    var level = Convert.ToInt32(model.Level);
                    if (level > 4)
                    {
                        type = "Detail";
                    }
                    var MaxGLCode = objModule1.MaxGLCode(model.ParentCode, level);
                    SqlCommand cmdDelete = new SqlCommand("Insert into GLCode(Code,Title,NorBalance,Lvl,GroupDet,Header_Detail,CompID) Values('" + MaxGLCode + "','" + model.Title + "'," + model.NorBalance + "," + model.Level + ",'" + model.Group + "','" + type + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "Account Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveBrand(AttributeViewModel model)
        {


            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open(); 

                }

                tran = con.BeginTransaction();

                var BrandCode = model.AttributeCode;
                var BrandName = model.AttributeName;
                var Blocked = model.Blocked ? 1 : 0;
                var Verify = model.VerifyIME ? 1 : 0;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(BrandCode) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update BrandName set VerifyIME=" + Verify + ",   Blocked='" + Blocked + "', Name='" + BrandName + "' where Code=" + BrandCode, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Brand UpDated";

                }
                else
                {
                    var MaxBrandCode = objModule1.MaxBrandCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into BrandName(Code,Name,Blocked,VerifyIME,CompID) Values(" + MaxBrandCode + ",'" + BrandName + "'," + Blocked + "," + Verify + ",'" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Brand Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveAccountUnit(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update AccountUnit set Name='" + name + "' where Code=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Unit Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxAccountUnitCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into AccountUnit(Code,Name,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Unit Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SavePacking(AttributeViewModel model)
        {


            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Packing set Name='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Packing Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxPackingCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Packing(ID,Name,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Packing Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveManufacturer(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Manufacturers set Name='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Manufacturer Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxManufCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Manufacturers(ID,Name,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Manufacturer Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveGroup(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update GroupItem set GroupName='" + name + "' where GroupId=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Group Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxManufCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into GroupItem(GroupID,GroupName,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Group Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveCategory(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Category set Name='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Category Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxCategoryCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Category(ID,Name,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Category Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveGodown(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update GoDown set Name='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "GoDown Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxGodown();
                    SqlCommand cmdDelete = new SqlCommand("Insert into GoDown(ID,Name,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New GoDown Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveClass(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Class set Name='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Class Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxClassCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Class(ID,Name,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Class Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveWidth(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Length set Length='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Width Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxLengthCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Length(ID,Length,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Width Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel SaveHeight(AttributeViewModel model)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open();

                }

                tran = con.BeginTransaction();

                var code = model.AttributeCode;
                var name = model.AttributeName;

                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(code) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Height set Height='" + name + "' where Id=" + code, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Height Updated";

                }
                else
                {
                    var MaxCode = objModule1.MaxHeightCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Height(ID,Height,CompID) Values(" + MaxCode + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Height Registered successfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }
        #endregion

        #region Delete

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteGLCode(string id)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();


                var cmdUpdate = new SqlCommand("Delete from GLCode  where Code='" + id + "'", con, tran);
                cmdUpdate.ExecuteNonQuery();

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteBrand(int id)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();

                var cmd = new SqlCommand("Select Code from InvCode where Brand=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    var cmdUpdate = new SqlCommand("Delete from BrandName  where Code=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteAccountUnit(int id)
        {


            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where AccountUnit=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from AccountUnit  where Code=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeletePacking(int id)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Packing=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from Packing  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteManufacturer(int id)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Manufacturer=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    var cmdUpdate = new SqlCommand("Delete from Manufacturers  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }



        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteGroup(int id)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where GroupId=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    var cmdUpdate = new SqlCommand("Delete from GroupItem  where GroupId=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }



        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteCategory(int id)
        {


            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Category=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from Category  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteGodown(int id)
        {


            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Godown=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from GoDown  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteClass(int id)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                     con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Class=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from Class  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteWidth(int id)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Length=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from Length  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public BaseModel DeleteHeight(int id)
        {

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open();

                }

                tran = con.BeginTransaction();
                var cmd = new SqlCommand("Select Code from InvCode where Height=" + id, con, tran);
                var attributes = cmd.ExecuteScalar();
                if (attributes == null)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Delete from Height  where Id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Can not Delete because it is used in Item Code: " + attributes };
                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully" };

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }
        #endregion 
    }
}
