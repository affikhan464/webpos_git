﻿namespace WebPOS
{
    internal class SearchItemModelStandard
    {
        public SearchItemModelStandard()
        {
        }


        public string Code { get; set; }
        public string Description { get; set; }
        public string TotalQty { get; set; }

        public string Rate { get; set; }
        public string AverageCost_Hide { get; set; }
        public string DealRs { get; set; }
        public string DealRs2_Hide { get; set; }
        public string DealRs3_Hide { get; set; }
        public string RevenueCode_Hide { get; set; }
        public string CGSCode_Hide { get; set; }
        public string ActualSellingPrice_Hide { get; set; }
        public string Reference { get;  set; }
        public string Reference2 { get;  set; }
        public string PerPieceCommission_Hide { get;  set; }
        public string LP { get; set; }
        public string Nature { get; set; }

    }
}