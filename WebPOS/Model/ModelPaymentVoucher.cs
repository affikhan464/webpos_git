﻿namespace WebPOS
{
    public class ModelPaymentVoucher
    {
        public ModelPaymentVoucher()
        {
        }

        public string VoucherNo { get; set; }
        public string Date { get; set; }
        public string CashPaid { get; set; }
        public string OldPartyCode { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string Address { get; set; }
        public string Narration { get; set; }
        public string Balance { get; set; }
        public string PartyCodeOld { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string ChqNo { get; set; }
        public bool Success { get; set; }
        public bool Success1 { get; set; }

        public string Message { get; set; }
        public string AccountNo { get; set; }
        public bool IsLastVoucherNo { get; set; }
        public string DepartmentId { get; set; }
        public string CashGLCode { get; set; }
        public string DisRs { get; set; }


    }
}