﻿namespace WebPOS.Model
{
    public class TotalData
    {
        public string Balance { get; set; }
        public string DealRs { get; set; }
        public string NetPayable { get; set; }
        public string BillTotal { get; set; }
        public string FlatDiscount { get; set; }
        public string FlatPer { get; set; }
        public string GrossTotal { get; set; }
        public string NetDiscount { get; set; }
        public string NetTotal { get; set; }
        public string Recieved { get; set; }
        public string Pcs { get; set; }
        public string TotalPcs { get; set; }
        public string GridTotalDiscount { get; set; }
        public string GridTotalPurchase { get; set; }
        public string OtherCharges { get; set; }
        public string PreviousBalance { get; set; }
        public string TotalAverageCost { get; set; }
        public string CashReceivedGLCode { get; set; }
    }
}