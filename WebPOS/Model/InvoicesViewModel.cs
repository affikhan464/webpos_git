﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class InvoiceViewModel
    {
        public string GLTitle { get; set; }
        public string VoucherType { get; set; }
        public string PartyCode { get; set; }
        public string ChequeNo { get; set; }
        public string VoucherNo { get; set; }
        public string Dis_Per_Rate { get; set; }
        public string Item_Disc { get; set; }
        public string InvoiceNumber { get; set; }
        public string Date { get; set; }
        public string PartyName { get; set; }
        public string ManualInvoiceNumber { get; set; }
        public string SalesManName { get; set; }
        public string NetDiscount { get; set; }
        public string Paid { get; set; }
        public string Total { get; set; }
        public string BillTotal { get; set; }
        public string FlatDiscount { get; set; }
        public string PhoneNumber { get; set; }
        public string Quantity { get; set; }
        public string DealRs { get; set; }
        public string Balance { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string BrandName { get; set; }
        public string ReturnAmount { get; set; }
        public string ReturnQty { get; set; }
        public string NetQtySold { get; set; }
        public string NetAmountSold { get; set; }
        public string InHandQty { get; set; }
        public string Payable { get; set; }
        public string NumberOfInvoices { get; set; }
        public string Rate { get; set; }
        public string DiscountPer { get; set; }
        public string CurrentBalance { get; set; }
        public string OpeningBalance { get; set; }
        public string TotalSale { get; set; }
        public string TotalReturn { get; set; }
        public string NetSale { get; set; }
        public string TotalReceived { get; set; }
        public string TotalPayment { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string ClosingBalance { get; set; }
        public string DepartmentName { get; set; }
        public string Code { get; set; }
        public string Particular { get; set; }
        public string IME { get; set; }
        public string Transaction { get; set; }
        public string Price { get; set; }

        public string Sale { get; set; }
        public string Cost { get; set; }
        public string Profit { get; set; }
        public string ClassType { get;  set; }
		public string Address { get; set; }
        public string MobileNumber { get; set; }
        public string MainTitle { get; set; }
    }
}