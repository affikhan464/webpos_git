﻿using System.Collections.Generic;

namespace WebPOS.Model
{
    public class ReportViewModel
    {
        public string ItemCode { get; set; }
        public string BrandName { get; set; }
        public string Amount { get; set; }
        public string Quantity { get; set; }
        public string Description { get; set; }
        public string Rate { get; set; }
        public string AttributeType { get; set; }
        public string Attribute { get; set; }
        public int ReorderQty { get; set; }
        public int ReorderLevel { get; set; }
        public string Date { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string Balance { get; set; }
        public string Qty { get; set; }
        public string DisRs { get; set; }
        public string DisPer { get; set; }
        public string BillNo { get; set; }
        public string DescriptionOfBill { get; set; }
        public int UnitCost { get; set; }
        public string DaysAfterSale { get; set; }
        public decimal PartyCode { get; set; }
        public string PartyName { get; set; }
        public string Nature { get; set; }
        public string Address { get; set; }
        public string SellingCost { get;  set; }
        public string PurchasingCost { get;  set; }
        public List<int> Quantities { get; set; }
        public int QuantityCount { get; set; }
        public List<decimal> Sales { get;  set; }
        public List<decimal> Expences { get;  set; }
        public int AmountCount { get;  set; }
        public string VoucherNumber { get; set; }
        public string VoucherType { get; set; }
    }
}