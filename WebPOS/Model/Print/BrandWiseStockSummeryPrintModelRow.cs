﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class BrandWiseStockSummeryPrintModelRow
    {
        public int SNo { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Rate { get; set; }
        public string Qty { get; set; }
        public string Discount { get; set; }
        public string Amount { get; set; }
    }
}