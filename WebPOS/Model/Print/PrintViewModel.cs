﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class PrintViewModel
    {
        public string InvoiceNumber { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        public string Particular { get; set; }
        public string Date { get; set; }
        public string PhoneNumber { get; set; }
        public List<Object> Rows { get; set; }
        public decimal TotalQty { get;  set; }
        public decimal TotalAmount { get;  set; }
        public string BrandName { get;  set; }
        public List<IDictionary<string, object>> DRows { get;  set; }
        public string SaleManName { get;  set; }
        public string InvoiceName { get; set; }
        public decimal TotalFooterDiscount { get; set; }
        public decimal TotalFooterBillTotal { get; set; }
        public decimal TotalFooterReceived { get; set; }
        public decimal TotalFooterNetBalance { get; set; }
        public decimal PreviousBalance { get;  set; }
        public string DateFrom { get;  set; }
        public string DateTo { get;  set; }
    }
}