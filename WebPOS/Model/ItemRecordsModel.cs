﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class ItemRecordsModel
    {
        public ItemRecord LastRecord { get; set; }
        public List<ItemRecord> AllRecords { get; set; }
        
    }
}