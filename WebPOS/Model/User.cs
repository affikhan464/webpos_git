﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class User
    {
        public string Code { get;  set; }
        public string Name { get;  set; }
        public string UserName { get;  set; }
        public string Password { get;  set; }
    }
}