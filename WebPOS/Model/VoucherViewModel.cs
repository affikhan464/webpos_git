﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class VoucherViewModel
    {
        public List<Voucher> Rows { get; set; }
        public string VoucherType { get; set; }
        public string VoucherDate { get; set; }
        public string VoucherNumber { get; set; }
        public List<Voucher> OldRows { get; set; }
        public string VoucherNumbarOld { get; set; }
        public string CreditGLCodeOld { get; set; }
        public string CreditGLCode { get; set; }
        public string CreditGLTitle { get; set; }
        public string CreditAmount { get; set; }
        public string CreditNarration { get; set; }
        public bool IsLastVoucherNo { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }



    }
}