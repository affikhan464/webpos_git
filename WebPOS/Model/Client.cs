﻿namespace WebPOS.Model
{
    public class Client
    {
        public string Name { get; set; }
        public string Balance { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string DealRs { get; set; }
        public string DisPer { get; set; }
        public string PhoneNumber { get; set; }
        public string OtherInfo { get; set; }
        public string Particular { get; set; }
        public string ManInv { get; set; }
        public string Date { get; set; }
        public string BC { get; set; }
        public string InvoiceNumber { get; set; }
        public string SalesManName { get; set; }
        public string SalesManId { get; set; }
        public string NorBalance { get; set; }
        public string DealApplyNo { get; set; }
        public string CreditLimit { get; set; }
    }
}