﻿namespace WebPOS
{
    public class ItemModel
    {
        public ItemModel()
        {

        }
        public string Price { get; set; }
        public string IME { get; set; }
        public string PartyName { get; set; }
        public string BillNo { get; set; }

        public string Date{ get; set; }

        public string Code { get; set; }
        public string BarCode { get; set; }
        public string Description { get; set; }
        public string SellingCost1 { get; set; }
        public string SellingCost2 { get; set; }
        public string SellingCost3 { get; set; }
        public string SellingCost4 { get; set; }
        public string SellingCost5 { get; set; }
        public string LastPurchase { get; set; }
        public string FixedCost { get; set; }
        public string qty { get; set; }
        public string Brand { get; set; }
        public string Reference { get; set; }
        public string Reference2 { get; set; }
        public string Category { get; set; }
        public string Godown { get; set; }
        public string Ave_Cost { get; set; }
        public string DealRs1 { get; set; }
        public string DealRs2 { get; set; }
        public string DealRs3 { get; set; }
        public string MinLimit { get; set; }
        public string MaxLimit { get; set; }
        public string PiecesInPacking { get; set; }
        public string Visiable { get; set; }
        public string ReorderLevel { get; set; }
        public string Manufacturer { get; set; }
        public string Packing { get; set; }
        public string Class { get; set; }
        public string ReorderQty { get; set; }
        public string OrderQTY { get; set; }
        public string BonusQTY { get; set; }
        public string Nature { get; set; }
        public string AccountUnit { get; set; }
        public string Revenue_Code { get; set; }
        public string Active { get; set; }
        public string GST_Rate { get; set; }
        public string GST_Apply { get; set; }
        public string CGS_Code { get; set; }
        public string ItemType { get; set; }
        public string Length { get; set; }
        public string Height { get; set; }
        public string BarCodeCategory { get; set; }
        public string Color { get; set; }

        public string OtherInFo { get; set; }
        public string PerPieceCommission { get; set; }
        public string Year1 { get; set; }
        public string Group { get; set; }
        public string MSP { get; set; }
        public string ActualCost { get; set; }
        public string LP { get; set; }
    }
}
