﻿namespace WebPOS
{
    internal class TrialBalance
    {
        public string GLCode { get; set; }
        public string GLTitle { get; set; }
        public string Level { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Amount { get; set; }
        public string Code { get; set; }
    }
}



