﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class ModelSetting
    {
        
        public string CompanyName { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyAddress4 { get; set; }
        public string CompanyPhone { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string InvoiceFormat { get; set; }
        public string IMEL1 { get; set; }
        public string IMEL2 { get; set; }
        public string IMEL3 { get; set; }
        public string CGSMethod { get; set; }
        


    }
}