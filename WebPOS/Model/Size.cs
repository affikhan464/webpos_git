﻿namespace WebPOS.Model
{
    public class AttributeModel
    {
        public string Attribute { get;  set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}