﻿namespace WebPOS
{
    internal class SearchBankModel
    {
        public SearchBankModel()
        {
        }
        public string Code { get; set; }
        public string Title { get; set; }
        public string AccountNo { get; set; }
        public string Balance { get; set; }


    }
}