﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public enum Configurations
    {
        PrinterType = 1,
        SpecialDiscount,
        LengthOfCode,
        VerifySerialNoDuringSale,
        InvoiceFormat,
        PartyWiseLedgerColorScheme,
        ItemSelectionThroughBarCodeOnInvoice,
        SaleRateVarificationRate,
        PrintSerialNoOnInvoice,
        SoundSystem,
        GeneralStore,
        InvoiceMessage,
        InvoiceMessage1,
        InvoiceMessage2,
        LockedInvoiceRateEdditing,
        LenthOfSerialNo1 = 16,
        LenthOfSerialNo2 = 17,
        LenthOfSerialNo3 = 18,
        ShowPurchasePrinceOnInvoice,
        NoOfInvoicePrinting,
        NoOfComputers,
        CGS_Method,
        InvoiceTopLess,
        ShowItemRef2CostOnSaleInvoice,
        SellingPrice,
        ReOrderQtyDisplay,
        TempSaleFrameDisplay,
        PrevBalanceOnInvoice,
        PrintPreViewInvoice,
        Messaging,
        PortMobile,
        MobileNoOwner,
        StockAlert,
        MultipleItemSelectionOnInvoice,
        DriveBM,
        SpecialDiscountMethod,
        ItemSelectionMethodOnInvoice,
        BonusApply,
        ItemSelectionSpeed,
        AfterScanningSetFocus,
        PurchaseRateRoundUpTO,
        ManualItemSelectionOnInvoiceSetFocus,
        StopSaleItemQTYLesThenOrEqualToZero,
        StockCalculationOnSaleOrPurchase,
    }
    public struct MenuPages
    {
        public const string HomePage = "/home.aspx";
    }
    public struct UserRole
    {
        public const string Admin = "0";
    }
}