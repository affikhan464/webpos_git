﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;
namespace WebPOS
{
    public partial class IncentiveReceiving : System.Web.UI.Page
    {
        static string CompID = "01";
        static string IncentiveIncomeGLCode = "0103050100001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        Module4 objModule4 = new Module4();
        Module1 objModule1 = new Module1();
        ModGLCode objModGLCode = new ModGLCode();
        static SqlTransaction tran;

        protected void Page_Load(object sender, EventArgs e)
        {


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveIncentiveInCome(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

                
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "IncentiveIncome";
                var VoucherNo = Convert.ToString(objModule1.MaxJVNo());

                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string PartyCode = ModelPaymentVoucher.PartyCode;
                string PartyName = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;



                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId) values('" + PartyCode + "','" + Convert.ToDateTime(VoucherDate) + "','" + "Incentive-(" + PartyName + ")" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + Convert.ToDecimal(VoucherNo) + ",'" + "JV" + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + Narration + "','" + CompID + "')", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId) values('" + IncentiveIncomeGLCode + "','" + Convert.ToDateTime(VoucherDate) + "','" + "Accounts Payable (" + PartyName + ")" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + Convert.ToDecimal(VoucherNo) + ",'" + "JV" + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + Narration + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();

                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,BillNo,CompId) values('" + PartyCode + "','" + Convert.ToString(VoucherDate) + "','" + "Incentive(Income) -JV # " + VoucherNo + "-" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + Convert.ToDecimal(VoucherNo) + ",'" + "JV" + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + VoucherNo + "','" + CompID + "')", con, tran);
                cmd3.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Incentive InCome Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}
