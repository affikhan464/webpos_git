﻿<%@ Page Title="Purchase Item" Language="C#" MasterPageFile="~/Transactions/TransactionsMasterPage.master" AutoEventWireup="true" CodeBehind="PurchaseReturnNew.aspx.cs" Inherits="WebPOS.Transactions.PurchaseReturnNew" %>

<asp:Content ContentPlaceHolderID="StyleContentPlaceHolder" runat="server">
    <link href="../css/purchaseReturn.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderAndGridContentPlaceHolder" runat="server">


    <asp:TextBox ID="TextBox1" Style="display: none" CssClass="defaultPartyCode" runat="server"></asp:TextBox>
    <div class="searchAppSection">
        <div class="container-fluid transactionPage">
            <div class="sale row partySection clearfix">
                <div class="invDiv col  col-12 col-sm-4 col-md-4  col-lg-3 display-flex">
                    <span class="userlLabel">Invoice #</span>
                    <asp:TextBox ID="txtInvoiceNo" disabled CssClass="InvoiceNumber form-control" name="Invoice" runat="server"></asp:TextBox>
                    <div class="leftRightButtons display-flex">
                        <a class=" invoiceEditBtn"><i class="fas fa-edit" aria-hidden="true"></i></a>
                        <a class=" print"><i class="fas fa-print" aria-hidden="true"></i></a>

                    </div>
                </div>
                <div class="manInv  col-12 col-sm-4 col-md-4  col-lg-3">
                    <div class="w-100 display-flex">
                        <input name="ManInv" type="text" class="form-control ManInv p-1" placeholder="Manual Inv." />

                        <select id="salesManDD" class="w-100">
                            
                        </select>
                    </div>

                    <%--<asp:DropDownList ID="lstSaleMan" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                    --%>
                    <span id="txtNorBal" data-toggle="tooltip" title="Nor Balance" class="badge badge-success PartyNorBalance badge-bm inv1"></span>


                </div>
                <div class="searchDiv  col  col-12 col-sm-4 col-md-4  col-lg-3 display-flex">
                    <input id="PartySearchBox" data-id="PartyTextbox" data-type="Party" data-function="GetParty" class="p-0 clientInput PartyName clearable PartySearchBox autocomplete form-control" name="PartyName" type="text" placeholder="Enter a party name" />
                    <span id="txtDealApplyNo" data-toggle="tooltip" title="Party Deal Apply Number!" class="badge badge-success badge-bm PartyDealApplyNo inv1"></span>
                    <%    
                        var isAdmin = HttpContext.Current.Session["UserId"].ToString() == "0";
                        var listPages = HttpContext.Current.Session["UserPermittedPages"] as List<WebPOS.Model.MenuPage>;
                        if (isAdmin || listPages.Any(a => a.Url.Contains("PartyRegistration")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons display-flex w-10">
                        <a class="w-100" onclick="$('#partyRegistrationModal').modal()" data-toggle="tooltip" title="Add Party"><i class="fas fa-plus" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Code</span>
                    <input class="clientInput form-control PartyCode inv1" type="text" id="txtPartyCode" disabled="disabled" name="PartyCode" placeholder="Enter party code" />

                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Date</span>
                    <%--<input class="form-control datetimepicker" name="Date" type="text" placeholder="01/01/2016" />--%>
                    <asp:TextBox ID="txtDate" AutoPostBack="false" runat="server" name="Date" CssClass="datetimepicker Date form-control"></asp:TextBox>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Bar Code</span>
                    <input id="txtBarCodePR" class="form-control clientInput" type="text" placeholder="Bar Code" name="BC" />
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Address</span>
                    <input type="text" class="form-control clientInput PartyAddress" id="txtAddresss" name="PartyAddress" placeholder="Address" />

                </div>

                <div class="col  col-12 col-sm-4 col-md-4 col-lg-3">
                    <span class="userlLabel">Phone No.</span>
                    <input type="text" id="txtPhone" class="PartyPhoneNumber inv1 form-control clientInput" name="PartyPhoneNumber" placeholder="Phone No." />

                </div>

                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Balance</span>
                    <input type="text" disabled="disabled" class="PartyBalance inv1 clientInput form-control" id="txtPreviousBalance" name="PartyBalance" placeholder="Balance" />

                </div>

                <div class="searchDiv col   col-12 col-sm-4 col-md-4 col-lg-3">
                    <input id="SearchBox" data-id="itemTextbox" data-type="Item" data-function="GetRecords" class="clientInput SearchBox autocomplete form-control" type="text" placeholder="Enter an item name or item code" />

                </div>

                <div class=" col   col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Particular</span>
                    <input type="text" placeholder="Particular" class="form-control clientInput PartyParticular" name="PartyParticular" />

                </div>
                <div class=" col   col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="userlLabel">Credit Limit</span>
                    <input class="form-control PartyCreditLimit clientInput" type="text" name="PartyCreditLimit" placeholder="Credit Limit" />
                </div>

            </div>
        </div>
    </div>



    <div class="businessManagerSellSection businessManager BMtable">

        <div class="container-fluid">

            <div class="recommendedSection allItemsView table-responsive-md">
                <div id="mainTable" class="table-wrapper">


                    <div class="itemsHeader d-flex ">
                        <div class="three flex-1 pl-0 phCol">
                            <a href="javascript:void(0)">S#
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Code
                            </a>
                        </div>
                        <div class="three flex-6 phCol">
                            <a href="javascript:void(0)">Description
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Qty
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Rate
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">G. Amount
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Item D
											
                            </a>
                        </div>
                        <div class="three flex-3  phCol">
                            <a href="javascript:void(0)">Dis.%
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Deal <span>Rs.</span>

                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">N<span>et</span> Amount
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Ac<span>tual </span>Cost
                            </a>
                        </div>

                        <div class="deleteRow three flex-3 phCol">
                            <a href="javascript:void(0)"></a>
                        </div>
                    </div>
                    <!-- itemsHeader -->
                    <div class="itemsSection">
                        <div>



                            <table class="TFtable w-100" id="myTable">

                                <tbody>
                                </tbody>

                            </table>



                        </div>

                    </div>
                    <div class="itemsFooter">
                        <div class='itemRow newRow d-flex'>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-3'><span></span></div>
                            <div class='three flex-6'><span></span></div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='QtyTotal' />
                            </div>
                            <div class='three flex-3'><span></span></div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='AmountTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                            </div>
                            <div class='three flex-3'><span></span></div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='DealDisTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='PurAmountTotal' />
                            </div>
                            <div class='three flex-3'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- recommendedSection -->



    </div>

    <footer class="footerTotalSection">
        <div class="container-fluid ">
            <div class="footerTotalTextSection row">

                <div class="col-6 col-md-2  mt-2">


                    <label>T. pcs 0:</label>
                    <label>Brand:</label>
                    <label>Present Qty:</label>
                    <label>SP:</label>
                    <label>Selling Price:</label>

                </div>
                <div class="col-6 col-md-2  mt-2">

                    <label>
                        <input onchange="DealDisApplyOnAllitems()" id="chkAutoDeal" type="checkbox" class="checkbox rounded" /><span></span>Auto Deal Dis</label>
                    <label>
                        <input onchange="PerDisApplyOnAllitems()" id="chkAutoPerDis" type="checkbox" class="checkbox rounded" /><span></span>Auto % Dis</label>
                    <label>
                        <input id="BarCodeScanning" type="checkbox" class="checkbox rounded" /><span></span>BarCode Scanning</label>
                    <label>
                        <input id="chkAutoPrint" name="chkAutoPrint" type="checkbox" class="checkbox rounded" /><span></span>Auto Print</label>

                    <label>
                        <input id="ShowSellingprice" type="checkbox" class="checkbox rounded" /><span></span>Show Selling price</label>

                </div>
                <div class="col-12 col-md-2  mt-2">
                    <a class="btn btn-3 btn-bm btn-3c fa-save w-100 btnSave" onclick="SaveBefore()"><i class="fas fa-save"></i>Save</a>
                </div>

                <div class="itemMeta clearfix col-12 col-md-6  mt-2">
                    <div class="row">

                        <div class="col-12 col-md-6 display-flex">
                            <span class="userlLabel">% Discount</span>
                            <input id="txtTotPercentageDis" class="inv3 form-control " name="txtTotPercentageDis" disabled="disabled" type="text" placeholder="Net Discount" />
                        </div>
                        <div class="col-12 col-md-6 display-flex">
                            <span class="userlLabel">Total</span>
                            <input id="txtTotal" class="GrossTotal inv3 form-control " name="GrossTotal" disabled="disabled" type="text" placeholder="Total" />
                        </div>
                        <div class="col-12 col-md-6 display-flex">
                            <span class="userlLabel">Deal Rs.</span>
                            <input class="DealRs inv3 form-control " id="txtTotDeal" disabled="disabled" name="DealRs" type="text" placeholder="Deal Rs." />
                        </div>

                        <div class="splitDiv  col-12 col-md-6 display-flex">
                            <span class="userlLabel">Flat Disc %</span>
                            <input name="flatPer" type="text" class=" form-control FlatPer inv3 calcu firstSplitInput" id="txtFlatDiscountPer" placeholder=" %" />
                            <input name="flatDiscount" type="text" class="form-control FlatDiscount inv3 calcu" id="txtFlatDiscount" placeholder="Flat Discount" />
                        </div>

                        <div class="col-12 col-md-6 display-flex">
                            <span class="userlLabel">Net Discount</span>
                            <input name="netDiscount" class="form-control NetDiscount inv3" id="txtTotalDiscount" disabled="disabled" type="text" placeholder="Net Discount" />
                        </div>
                        <div class="totalReceived col-12 col-md-6 display-flex">
                            <span class="userlLabel">Cash Received</span>
                            <input name="recieved" class="form-control Recieved inv3 calcu" id="txtPaid" type="text" placeholder="Received" />
                        </div>


                        <div class="col-12 col-md-6 display-flex">
                            <span class="userlLabel">Bill Total</span>
                            <input type="text" class="form-control BillTotal inv3" id="txtBillTotal" name="billTotal" disabled="disabled" placeholder="Bill Total" />
                        </div>

                        <div class="totalBalance col-12 col-md-6 display-flex">
                            <span class="userlLabel">Balance</span>
                            <input name="balance" class="form-control Balance inv3" id="txtBalance" disabled="disabled" type="text" placeholder="Balance" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>

    <div class="IMEPortion  mt-2">
        <div class="container-fluid ">
            <div class="Ime-header  rounded text-center mb-3 p-2">
                <h4><i class="fas fa-barcode"></i>IMEI's</h4>

            </div>
            <label class="btn btn-bm active checkbox-btn" style="width: 180px;">
                <input type="checkbox" name="isIMEVerifiedChkbx" />
                <i class="fas fa-check mr-2"></i>
                Is IME Verified?
            </label>
            <div>

                <table id="IMEIRow">
                    <tr data-rownumber="1">
                        <td>
                            <input name="ItemRowNumber" type="hidden" />
                            <input name="Code" type="text" class="form-control" value="" /></td>
                        <td class="name">
                            <input name="ItemName" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Qty" type="text" value="" /></td>
                        <td>
                            <input name="Rate" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Amount" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="ItemDis" /></td>
                        <td>
                            <input name="PerDis" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input name="DealDis" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="NetAmount" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="ActualCost" /></td>
                        <td>
                            <input class="form-control" type="text" style="width: 200px; text-align: left" name="ImeiTxbx" placeholder="Enter IME" />
                        </td>
                    </tr>
                </table>
            </div>


            <div class="imei-table">
                <a class="btn btn-bm w-auto" onclick="reArrangeIMETabeRows()"><i class="fas fa-list-ol"></i>Re-Arrange Table</a>
                <div class="businessManagerSellSection businessManager BMtable">


                    <div class="recommendedSection allItemsView">
                        <div class="itemsHeader">
                            <div class="three pl-0 phCol">
                                <a href="javascript:void(0)">S#
                                </a>
                            </div>
                            <div class="four  phCol">
                                <a href="javascript:void(0)">Code
                                </a>
                            </div>
                            <div class="fifteen  phCol">
                                <a href="javascript:void(0)">Description
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">IMEI
                                </a>
                            </div>
                            <div class="five phCol">
                                <a href="javascript:void(0)">Qty
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Rate
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">G. Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Item D
											
                                </a>
                            </div>
                            <div class="six  phCol">
                                <a href="javascript:void(0)">Dis.%
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Deal <span>Rs.</span>

                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">N<span>et</span> Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Ac<span>tual </span>Cost
											
                                </a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                        </div>

                        <div class="itemsSection">
                            <div class="imei-body">
                                <table class="TFtable" id="IMEITable">

                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="itemsFooter">
                            <div class='itemRow newRow'>
                                <div class='three'><span></span></div>
                                <div class='four'><span></span></div>
                                <div class='fifteen  '><span></span></div>
                                <div class='thirteen '><span></span></div>

                                <div class='five '>
                                    <input disabled="disabled" class="form-control" name='QtyTotal' />
                                </div>
                                <div class='eight '><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='AmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                                </div>
                                <div class='six'><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='DealDisTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='ActualCostTotal' />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>
    
    <div class="modal fade" id="partyRegistrationModal" tabindex="-1" role="dialog" aria-labelledby="partyRegistrationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="attributeModalLabel">Party Registration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                     <%@ Register Src="~/Registration/_PartyRegistration.ascx" TagName="_PartyRegistration" TagPrefix="uc" %>
                    <uc:_PartyRegistration ID="_PartyRegistrationHead1" runat="server" />
                </div>
            </div>
        </div>
    </div>
      
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptsContentPlaceHolder" runat="server">
    <script src="/Script/PartyRegistration.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            appendAttribute("", "IncomeAccount");
            appendAttribute("", "COGSAccount");
            $(".invoiceEditBtn").on("click", function () {
                var invoiceNumber = $(".InvoiceNumber").val();
                window.location.href = "/correction/PurchaseReturnEditNew.aspx?InvNo=" + invoiceNumber;
            })
        });
    </script>

    <script src="../Script/PurchaseReturn/InsertDataInPurchaseReturn.js"></script>
    <script src="../Script/CommonScript/calculationPurchase.js"></script>
    <script src="../Script/PurchaseReturn/Save.js"></script>

    <script type="text/javascript">
        $(document).on('input', '.calcu', function () {
            calculationSale();
        })
        $(document).ready(function () {
            $("#app").addClass("hide");
            resizeTable();
            appendAttribute("salesManDD", "SaleManList");

        });
    </script>
</asp:Content>
