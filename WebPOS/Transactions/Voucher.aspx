﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Voucher.aspx.cs" Inherits="WebPOS.Transactions.Voucher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <div class="reports searchAppSection">
				<div class="contentContainer">
				 <h2 class="text-align-center"><i class=" fas fa-file-alt" aria-hidden="true"></i> Voucher</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                       
                         <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Voucher#</span>
                            <asp:TextBox ID="VoucherNumber" ClientIDMode="Static" runat="server"></asp:TextBox>
                             
							</div>
							<div  class="col col-12 col-sm-6 col-md-3">
                             <span class="userlLabel">Date</span>
                            <asp:TextBox ID="VoucherDate" CssClass="datetimepicker Date" ClientIDMode="Static" runat="server"></asp:TextBox>

							</div>
                        <div  class="col col-12 col-sm-6 col-md-2">
                            <asp:TextBox data-id="Voucher" data-type="AllVouchers" data-function="GLList" data-glcode="010402"  CssClass="autocomplete AllVouchersTitle descritionTxbx" placeholder="Enter Description"  ID="TextBox1" ClientIDMode="Static" runat="server"></asp:TextBox>
                             <%--<input data-id="Voucher" data-type="AllVouchers" data-function="GLList" data-glcode="010402"  class="autocomplete AllVouchersTitle descritionTxbx" placeholder="Enter Description" />--%>
                        </div>
                        <div class="DDWrapper col col-12 col-sm-6 col-md-2">
                            <select id="VoucherTypeDD" class="dropdown">
                                <option value="JournalVoucher">Journal Voucher</option>
                                <option value="JournalVoucher">Journal Voucher</option>
                                <option value="JournalVoucher">Journal Voucher</option>
                                <option value="JournalVoucher">Journal Voucher</option>
                                <option value="JournalVoucher">Journal Voucher</option>
                            </select>
                        </div>
                        
                        <div class="col col-12 col-sm-3 col-md-1">
                            <a href="#" id="saveBtn" class="btn btn-bm" title="save"><i class="fas fa-save"></i> </a>
						</div>
                        <div class="col col-12 col-sm-3 col-md-1">
                            <a href="#" id="get" class="btn btn-bm" title="Get Voucher"><i class="fas fa-sync"></i></a>
						</div>
					</div><!-- searchAppSection -->
				</div>
			</div>
    <div class="BMtable">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                  
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Serial No.
										</a>
									</div>
									<div class="sixteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    <div class="sixteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
                                    <div class="sixteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Narration
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Debit
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Credit
											
										</a>
									</div>
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow'>
								<div class='seven'><span></span></div>
								<div class='sixteen '><span></span></div>
								<div class='sixteen '><span></span></div>
								<div class='sixteen '><input  disabled="disabled" value="Total:"/><span>Total</span></div>
								<div class='thirteen'><input  disabled="disabled" value="0"  name='totalDebit'/></div>
								<div class='thirteen'><input  disabled="disabled" value="0"   name='totalCredit'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
	   </asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Script/Autocomplete.js"></script>
    <script src="/Script/voucher.js"></script>
        <script type="text/javascript">

       var counter = 0;
       
       $(document).ready(function () {
          
           $("#VoucherTypeDD").ddslick(
               {
                   width:"100%"
               });
               
                resizeTable();
           
            });

            $(window).resize(function () {
                resizeTable();
              
            });

          
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
         </script>
</asp:Content>
