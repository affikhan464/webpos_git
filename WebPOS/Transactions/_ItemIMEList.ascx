﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_ItemIMEList.ascx.cs" Inherits="WebPOS.Transactions._ItemIMEList" %>

<div class="modal fade" id="ItemIMEDetailModal" tabindex="-1" role="dialog" aria-labelledby="ItemIMEDetailModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close position-absolute rt-10" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-0">
                <div id="generic_price_table" class="m-0 ">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix m-0 pb-3">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Search IME</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->
                                        <!--PRICE START-->
                                        <div class="generic_price_btn clearfix m-0">
                                            <div class="row justify-content-center">
                                                <div class="col-12">
                                                    <span class="d-block">Item Name: <span class="IME_ItemName font-weight-bold mb-3"></span></span>
                                                    <span class="d-block">Item Code: <span class="IME_ItemCode font-weight-bold mb-4"></span></span>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <span class="input input--hoshi mr-1">
                                                        <input  type="hidden" class="IME_ItemCode"/>
                                                        <input  type="hidden" class="IME_ItemRowNumber"/>
                                                        <input id="txbxItemIMEList" data-id="txbxItemIMEList" data-type="Party"  
                                                            data-preventautocomplete="true" 
                                                            data-service="Services/IMEService" 
                                                            data-function="GetItemIMEList" 
                                                            data-descriptiontype="<%=this.DescriptionType %>" 
                                                            data-callback="AppendItemIMEList" 
                                                            class="input__field input__field--hoshi clientInput txbxItemIMEList clearable 
                                                            autocomplete" name="txbxItemIMEList" type="text" />
                                                        <label class="input__label input__label--hoshi">
                                                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-barcode"></i>Search for IME of selected item</span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//PRICE END-->

                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list pb-2">
                                        <table class="w-100">
                                            <thead>
                                                <tr class="p-1">
                                                    <th>IME</th>
                                                    <th>Item Name</th>
                                                    <th>IME Status</th>
                                                </tr>
                                            </thead>
                                            <tbody class="imeModalRows">
                                            </tbody>

                                        </table>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <%-- <div class="generic_price_btn clearfix">
                                        <a class="" href="">Sign up</a>
                                    </div>--%>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


