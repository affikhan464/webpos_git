﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_IMEList.ascx.cs" Inherits="WebPOS.Transactions._IMEList" %>

<div class="modal fade" id="IMEDetailModal" tabindex="-1" role="dialog" aria-labelledby="IMEDetailModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close position-absolute rt-10" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-0">
                <div id="generic_price_table" class="m-0 ">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix m-0 pb-3">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Search IME</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->
                                        <!--PRICE START-->
                                        <div class="generic_price_btn clearfix m-0">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-sm-6">
                                                    <span class="input input--hoshi mr-1">
                                                        <input id="txbxIMEList" data-id="txbxIMEList" data-type="Party"  
                                                            data-preventautocomplete="true" 
                                                            data-service="Services/IMEService" 
                                                            data-function="GetIMEList" 
                                                            data-descriptiontype="<%=this.DescriptionType %>" 
                                                            data-callback="AppendIMEList" 
                                                            class="input__field input__field--hoshi clientInput txbxIMEList clearable 
                                                            autocomplete" name="txbxIMEList" type="text" />
                                                        <label class="input__label input__label--hoshi">
                                                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-barcode"></i>Search for IME </span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//PRICE END-->

                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list pb-2">
                                        <table class="w-100">
                                            <thead>
                                                <tr class="p-1">
                                                    <th>IME</th>
                                                    <th>Item Name</th>
                                                    <th>IME Status</th>
                                                </tr>
                                            </thead>
                                            <tbody class="imeModalRows">
                                            </tbody>

                                        </table>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <%-- <div class="generic_price_btn clearfix">
                                        <a class="" href="">Sign up</a>
                                    </div>--%>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


