﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public partial class IncentiveGiven : System.Web.UI.Page
    {
        static string CompID = "01";
        static string CashAccountGLCode = "0101010100001";
        static string IncentiveLossGLCode = "0104020200005";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        static Module4 objModule4 = new Module4();
        static Module1 objModule1 = new Module1();
        static ModGLCode objModGLCode = new ModGLCode();
        //private object txtCurrentDate;
        //private object txtPartyCode;
        //private object txtBalance;
        //private object lstPartyName;
        //private object txtCashPaid;

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {
            

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";
            PartyCode = ModelPaymentVoucher.PartyCode;
            string PartyCodeOld = "";
            PartyCodeOld = ModelPaymentVoucher.OldPartyCode;

            try
            {

                ModelPaymentVoucher.VoucherNo = Convert.ToString(objModule1.MaxJVNo());
                string SysTime = Convert.ToString(DateTime.Now);
                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();

                decimal CashPaid = 0;
                if (!string.IsNullOrEmpty(ModelPaymentVoucher.CashPaid)) { CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid); }

                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId) values('" + IncentiveLossGLCode + "','" + VoucherDate + "','" + "Receivables-(" + ModelPaymentVoucher.PartyName + ")" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "' )", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId) values('" + ModelPaymentVoucher.PartyCode + "','" + VoucherDate + "','" + "Incentive-(" + ModelPaymentVoucher.PartyName + ")" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "' )", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,BillNo,CompId) values('" + ModelPaymentVoucher.PartyCode + "','" + VoucherDate + "','" + "Incentive(Loss) -JV # " + ModelPaymentVoucher.VoucherNo + "-" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.VoucherNo + "','" + CompID + "' )", con, tran);
                cmd3.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}
