﻿function insertDataInSale() {

    var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
    var Sno = rowCount; //document.getElementById("txtSNO").value;

    var ItemCode = $(".selectedItemCode").val();
    var ItemName = $(".selectedItemDescription").val();
    var Qty = 1;//Number($(".selectedItemQty").val());
    var Rate = Number($(".selectedItemRate").val());
    Qty = Number(Qty);
    Rate = Number(Rate);
    var Amount = Qty * Rate;
    var ItemDis = 0;
    var PerDis = 0;
    var DealDis = 0;


    if ($("#chkAutoDeal:checked").length > 0) {

        var dealApplyNo = Number($("#txtDealApplyNo").text());
        if (dealApplyNo == 2)
            DealDis = Number($(".selectedItemDealRs2").val());

        else if (dealApplyNo == 3)
            DealDis = Number($(".selectedItemDealRs3").val());

        else
            DealDis = Number($(".selectedItemDealRs").val());
    }

    var revCode = $(".selectedItemRevenueCode").val();
    var cgsCode = $(".selectedItemCGSCode").val();

    var NetAmount = 0; //Amount - ItemDis - DealDis;
    var actualSellingPrice = $(".selectedItemActualSellingPrice").val();

    var avrgCost = Number($(".selectedItemAverageCost").val()).toFixed(2);
    var PurAmount = 0; //avrgCost * Qty;
    var rows = "";
    //var itemExist = itemAlreadyExist(ItemCode, Rate, PerDis, DealDis);
    //if (itemExist > 0) {
    //    Sno = itemExist;
    //    var qty = Number($("#myTable ." + Sno + "_Qty").val());
    //    $("#myTable ." + Sno + "_Qty").val(qty);
    //    calculationSale();
    //}
    //else {
        insertPRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, actualSellingPrice)
        calculationSale();
    //}
    $("#myTable ." + Sno + "_Qty").focus();
    $("#myTable ." + Sno + "_Qty").select();
    rerenderSerialNumber();
    updateDisPer();
}

function insertPRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, actualSellingPrice) {
    var rows = `
        <tr   class='d-flex pl-1' id='Row_${ Sno}' data-itemcode='${ItemCode}' data-itemrate='${Rate}' data-itemdisper='${PerDis}' data-itemdealdis='${DealDis}' data-rownumber='${Sno}'>
            <td class='SNo three flex-0'><input type='text' value='${Sno}'  /></td>
            <td  class='three flex-1 name' ><input  class='${Sno}_Code' name='Code' type='text' value='${ItemCode}'  /></td>
            <td class='name three flex-7'><input  class='${Sno}_ItemName ' name='ItemName' type='text' value='${ItemName}'  /></td>
            <td class='three flex-1'><input  class='${Sno}_Qty calcu' name='Qty' type='text' value='${Qty}'  /></td>
            <td class='three flex-2'><input name='Rate'  class='${Sno}_Rate calcu'  type='text' value='${Rate}' /></td>
            <td class='three flex-3'><input class='${Sno}_Amount' name='Amount'  type='text' value='${Amount}' /></td>
            <td class='three flex-2'><input class='${Sno}_ItemDis' type='text' value='${ItemDis}' name='ItemDis' /></td>
            <td class='three flex-1'><input  name='PerDis'  class='${Sno}_PerDis calcu' type='text' value='${PerDis}' /></td>
            <td class='three flex-2'><input name='DealDis'  class='${Sno}_DealDis' type='text' value='${DealDis}' /></td>
            <td class='three flex-3'><input class='${Sno}_NetAmount' type='text' value='${NetAmount}' name='NetAmount' /></td>
            <td class='three flex-3'><input class='${Sno}_PurAmount' type='text' value='${PurAmount}' name='PurAmount' /></td>
            <td class='three flex-1'><input class='${Sno}_SQ' type='text' value='' name='SQ' /></td>
            <td class='three flex-4'><input class='${Sno}_ScannedItem' type='text' value='' name='ImeiTxbx' data-rownumber='${Sno}' data-itemcode='${ItemCode}' /></td>
            
            <td hidden> <input hidden='hidden' class='${Sno}_AverageCost' value='${avrgCost}' /></td>  
            <td hidden> <input hidden='hidden' class='${Sno}_ActualSellingPrice' value='${actualSellingPrice}' /></td> 
            <td hidden> <input hidden='hidden' class='${Sno}_RevCode' value='${revCode}' /></td>
            <td hidden> <input hidden='hidden' class='${Sno}_CgsCode' value='${cgsCode}' /></td>  
            <td class='three flex-3 actionBtn text-center'>
	    	    <i data-itemcode='${ItemCode}' class='fas fa-times'></i> 
		        <i data-rownumber='${Sno}' class='fas fa-barcode IMETrigerBtn' onclick='appendRowInIMEI(this)'></i> 
	        </td>
        </tr>`;
    $(rows).appendTo("#myTable tbody");
    $(".selected").remove();
}
