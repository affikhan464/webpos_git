﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PaymentToPartyThroughCash.aspx.cs" Inherits="WebPOS.PaymentToPartyThroughCash" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<div class="container-fluid transactionPage salePage">--%>

    <div class="container salePage">
        <div class="mt-5">
           
                <section class="form">
                    <h2 class="form-header fa-money-bill-alt"> Cash Payment</h2>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <span class="input input--hoshi">
                                <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Voucher</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <span class="input input--hoshi">
                               <a class="btn btn-3 btn-bm btn-3e fa-edit w-100"  onclick="EditVoucher()"> Edit </a>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtCurrentDate" class="input__field input__field--hoshi datetimepicker Date" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Date</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtPartyName" class=" input__field input__field--hoshi PartyBox PartyName autocomplete empt" autocomplete="off" data-type="Party" data-id="txtPartyName" data-function="GetParty" data-nextfocus=".CashPaid" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Party Name </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtPartyCode"  class=" PartyCode empt input__field input__field--hoshi" autocomplete="off" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Party Code </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <span class="input input--hoshi">
                                <input id="txtAddress" class=" PartyAddress empt input__field input__field--hoshi" autocomplete="off" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Address </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtCashPaid" class=" CashPaid empt input__field input__field--hoshi"  type="number" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Cash Paid</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="OldBalance" class=" PartyBalance empt input__field input__field--hoshi"  type="number"  />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Old Balance </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input  id="txtBalance" class="PartyBalance empt input__field input__field--hoshi"  type="number"  />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Balance </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtNarration" class="Narration empt input__field input__field--hoshi" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Narration </span>
                                </label>
                            </span>
                        </div>
                         <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                            <span class="input input--hoshi radio--hoshi">
                                <label>
                                    <input id="Client_RadioButton" class="radio" type="radio" name="PartiesRB" />
                                    <span></span>
                                    Client
                                </label>
                            </span>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                            <span class="input input--hoshi radio--hoshi">
                               <label>
                                    <input id="Supplier_RadioButton" class="radio" type="radio" name="PartiesRB" />
                                    <span></span>
                                   Vendor
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <a id="btnSave" onclick="SaveBefore()" class="btnSave btn btn-3 btn-bm btn-3e fa-save w-100"> Save</a>
                            </span>
                        </div>
                    </div>

                </section>
            </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="Server">

      <%--<script src="../Script/Autocomplete.js"></script>--%>
    <script src="../Script/AutocompKhalid.js"></script>
    <script src="../Script/Voucher/PaymentToPartyCash_Save.js"></script>
    <script>

         $(document).ready(function () {
             
                $(".PartyName").focus();
                //GetVoucherData();
        });
         function calculation() {
        var NorBalance = 0;
         NorBalance = document.getElementById("Client_RadioButton").checked ? "1" : "2";
        
        var Balance = $(".PartyBalance").val();
             var Paid = $(".CashPaid").val();
             Balance = Number(Balance);
        Paid = Number(Paid);
        
        var NewBalance = 0;
             if (Paid == "") { Paid = 0; }
             if (Paid > 0) {
                 if (NorBalance == 1 && Balance > 0) {
                     NewBalance = (Balance + Paid);
                     $("#txtBalance").val(NewBalance);

                 } else
                     if (NorBalance == 1 && Balance < 0) {
                         NewBalance = (Math.abs(Balance) - Paid);
                         $("#txtBalance").val((NewBalance * -1));
                     } else
                         if (NorBalance == 2 && Balance > 0) {
                             NewBalance = (Balance - Paid);
                             $("#txtBalance").val(NewBalance);
                         } else
                             if (NorBalance == 2 && Balance < 0) {
                                 NewBalance = (Math.abs(Balance) + Paid);
                                 $("#txtBalance").val((NewBalance * -1));
                             }else {
                                 NewBalance = (Balance- Paid);
                                 $("#txtBalance").val(NewBalance);
                             }


             }else
                if (Paid == 0) {
                    $("#txtBalance").val($("#OldBalance").val());
                }

        }
         $(document).on("input","#txtCashPaid", function (event) {
             
                    calculation();
              
        });
        
         
         function EditVoucher() {
             var voucherId = $("#txtVoucherNo").val();
             window.location.href = "/Correction/PaymentToPartyThroughCashEdit.aspx?v=" + voucherId;
        }
        function insertInToCasPaymentToParty() {
            var norblnc = $(".selectedPartyNorBalance").val();
            if (norblnc == 1) {
                $("#Client_RadioButton").prop("checked", true)
            } else {
                $("#Supplier_RadioButton").prop("checked", true)
            }
            $(".PartyName").val($(".selectedPartyName").val());
            $(".PartyCode").val($(".selectedPartyCode").val());
            $(".PartyAddress").val($(".selectedPartyAddress").val());
            $(".PartyBalance").val($(".selectedPartyBalance").val());
           
           
            //let balance = Number($(".selectedPartyBalance").val());
            //if (norblnc == "1" && (balance > 0 || balance < 0)) {
            //    $(".PartyBalance").val(balance);
            //}
            //if (norblnc == "2" && (balance > 0 || balance < 0)) {
            //    $(".PartyBalance").val(-1*balance);
            //}
            
        

             calculation();
        }

    </script>
</asp:Content>