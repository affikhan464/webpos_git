﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.ModelBinding;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;


namespace WebPOS
{
    public partial class PaymentToPartyThroughCash : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        string UserSession = "001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        static string CashAccountGLCode = "0101010100001";



        Module4 objModule4 = new Module4();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //txtCurrentDate.Text = DateTime.Now.ToString("d");
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

                
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashPaidToParty";
                var VoucherNo = Convert.ToString(objModule1.MaxCPV());
                var selectedDate = ModelPaymentVoucher.Date;
                var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
                var VoucherDate = convertedDate.ToString("yyyy-MM-dd");

            
                string PartyCode = ModelPaymentVoucher.PartyCode;
                string PartyName = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;


                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,datedr,Code1,System_Date_Time,SecondDescription,VenderCode,Narration,CompId) values('" + PartyCode + "','" + "Cash Account" + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + Convert.ToDateTime(VoucherDate) + "','" + CashAccountGLCode + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + Narration + "','" + CompID + "')", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountCr,V_No,V_Type,datedr,Narration,Code1,System_Date_Time,SecondDescription,VenderCode,CompId) values('" + CashAccountGLCode + "','" + PartyName + "-" + Narration + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + VoucherDate + "','" + Narration + "','" + PartyCode + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountDr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,BillNo,CompId) values('" + PartyCode + "','" + "Cash Payment-" + VoucherNo + "-" + Narration + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + VoucherDate + "','" + SysTime + "','" + SecondDescription + "','" + VoucherNo + "','" + CompID + "')", con, tran);
                cmd3.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Cash Payment Voucher Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }




    }
}
