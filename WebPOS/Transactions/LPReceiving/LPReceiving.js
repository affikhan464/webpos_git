﻿var lpItems = [];
var groupedTableItems = [];

$(document).on("keypress", "#txbxIMELPRec", function (e) {


    if (e.which == 13 && $("#txbxIMELPRec").val().length && $('.PartyCode').val().length) {

        dataFunction = "GetRecordsLPRec";
        textboxid = "txbxIMELPRec";
        GetRecordsLPRec();
        $("#" + textboxid).select();
    } else if (!$('.PartyCode').val().length) {
        playError();
        swal({
            title: "Business Manager",
            text: 'Select Party First!!',
            type: 'error',
            focusConfirm: true,
        });
    }
})
function GetRecordsLPRec() {
    debugger
    var ajaxData = "";


    var IME = $("#txbxIMELPRec").val(); //$("[data-id=" + txbxIMELPRec + "]").length ? $("[data-id=" + txbxIMELPRec + "]").val() : $("#" + txbxIMELPRec).val();
    if (IME.length > 10 && !isNaN(IME)) {
        ajaxData = { "key": IME + ',' + $('.PartyCode').val() };

    }

    if (isDuplicateIMEI(IME)) {
        swal({
            title: "Business Manager",
            text: "IMEI Already Exist",
            type: 'error',
            focusConfirm: true,
        });
        playError();
        return false;

    }
    else {

        $.ajax({
            async: false,
            url: '/WebPOSService.asmx/GetRecordLPReceiving',
            type: "Post",

            data: ajaxData,

            success: function (items) {
                debugger
                if (items == "") {
                    swal({
                        title: "Business Manager",
                        text: "IMEI Not sold to this party or not found",
                        type: 'error',
                        focusConfirm: true,
                    });
                    playError();
                }
                if (items) {



                    var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                    var Sno = rowCount;

                    insertLPReceivingRow(items[0])
                    lpItems.push(items[0]);
                    calculateLP();
                    playSuccess();

                    //calculationSale();

                    //Ime Row Insert Start --> This insert IME Row

                }






                //rerenderSerialNumber();



            }, fail: function (jqXhr, exception) {

            }
        });

    }
}
function calculateLP() {
    var LPSum = Enumerable.from(lpItems).select(a => a.LP).sum((LP) => Number(LP));
    $(".LPAmount").val(LPSum.toFixed(2));
    var balance = Number($(".PartyBalance").val());

    var total = balance - LPSum;
    $(".NewBalance").val(total.toFixed(2));

    appendToTable2();
    updateInputStyle();

}

function assignSerialNumber(table) {
    var serialNumbers = $(table + " .serialNumber");
    serialNumbers.each(function (index, element) {
        var rownumber = index + 1;
        $(element).text(rownumber);
        $(element).parents("li").data("rownumber", rownumber);
        $(element).parents("li").find("[name=SNo]").val(rownumber);
    });
}
function appendToTable2() {
    groupedTableItems = [];
    var linq = Enumerable.from(lpItems);
    var groupedItems =
        linq.groupBy(function (x) { return x.Code; })
            .select(function (x) {
                return {
                    Code: x.key(),
                    Qty: x.count()
                };
            }).toArray();

    var rows = "";

    for (var i = 0; i < groupedItems.length; i++) {
        debugger

        var LPItemSum = Enumerable.from(lpItems).where(x => x.Code == groupedItems[i].Code).select(a => a.LP).sum((LP) => Number(LP)); //
        var item = Enumerable.from(lpItems).firstOrDefault(x => x.Code == groupedItems[i].Code);

        var code = item.Code;
        var brand = item.BrandName;
        var brandCode = item.BrandCode;
        var ime = item.IME;
        var qty = groupedItems[i].Qty;
        var LP = item.LP;
        var LPTotalAmount = Number(item.LP) * Number(groupedItems[i].Qty);

        var groupedTableItem = {
            LP: LP,
            Qty: qty,
            Amount: LPTotalAmount,
            BrandCode: brandCode,
            BrandName: brand,
            IME: ime,
            Code: code
        };
        groupedTableItems.push(groupedTableItem);
        rows += `<li class="item item-data">
                            <div class="item-row  pl-1 pr-2">
                                <div class="item-col flex-1">
                                    <div class="item-heading ">S#</div>
                                    <div>
                                        <span class="serialNumber"></span>
                                        <input type="hidden" name="SNo" readonly >
                                    </div>
                                </div>
                                <div class="item-col flex-3">
                                    <div class="item-heading">Code</div>
                                    <div>
                                        ${code} 
                                        <input type="hidden" name="Code" readonly value="${code}">
                                    </div>
                                </div>
                                <div class="item-col flex-5 no-overflow">
                                    <div>
                                        ${brand}  
                                       <input name='BrandName' type="hidden" readonly value='${brand}' class="form-control"/>
                                    </div>
                                </div>
                                <div class="item-col flex-3 no-overflow">
                                    <div>
                                       <input name='Qty' value='${qty}' type='text' readonly class="text-right px-1 form-control"  />
                                    </div>
                                </div>
                                <div class="item-col flex-4 no-overflow">
                                    <div>
                                       <input name='LP' value='${LP}' type='text' readonly class="text-right px-2 form-control"  />
                                    </div>
                                </div>
                                <div class="item-col flex-4 no-overflow">
                                    <div>
                                       <input name='LPTotalAmount' value='${LPTotalAmount}' readonly type='text' class="text-right px-2 form-control"  />
                                    </div>
                                </div>
                            </div>
                        </li>`;
    }
    $(".table-2 .item-list.data-list").html(rows);
    assignSerialNumber(".table-2");

}
function insertLPReceivingRow(model) {
    debugger
    var code = model.Code;
    var description = model.Description;
    var brand = model.BrandName;
    var brandCode = model.BrandCode;
    var LP = model.LP;
    var ime = model.IME;
    var row = `<li class="item item-data">
        <div class="item-row  pl-1 pr-2">
            <div class="item-col flex-1">
                <div class="item-heading">S#</div>
                <div>
                    <span class="serialNumber"></span>
                    <input type="hidden" name="SNo" readonly>
                </div>
            </div>
            <div class="item-col flex-2">
                <div class="item-heading">Code</div>
                <div>
                    ${code} 
                    <input type="hidden" name="Code" readonly value="${code}">
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                    <a class="">
                        <h4 class="item-title no-wrap">
                            ${description}  
                            <input type="hidden" readonly name="Description" value="${description}">
                        </h4>
                    </a>
                </div>
            </div>
            <div class="item-col flex-2 no-overflow">
                <div>
                    ${brand}  
                   <input name='BrandName' type="hidden" readonly value='${brand}' class="form-control"/>
                </div>
            </div>
            <div class="item-col flex-5 no-overflow">
                <div>
                   <input name='IME' value='${ime}' type='text' readonly class="text-right IMEI px-1 form-control"  />
                </div>
            </div>
            <div class="item-col flex-3 no-overflow">
                <div>
                   <input name='BrandCode' value='${LP}' type='text' readonly class="text-right px-2 form-control"  />
                </div>
            </div>
            <div class="item-col flex-2 no-overflow">
                <div>
                   <input name='BrandCode' value='${brandCode}' type='text' readonly class="text-right px-2 form-control"  />
                </div>
            </div>

            <div class="item-col item-col-date flex-1">
                <div>
                    <i onclick="deleteItem(this)" data-ime="${ime}" readonly class="fas fa-times mr-3"></i>
                </div>
            </div>
        </div>
    </li>`;
    $(".table-1 .item-list.data-list").append(row);
    assignSerialNumber(".table-1");

}

function reArrangeItems() {
    $(".table-1 .item-list.data-list").html("");
    groupedTableItems.forEach((item) => {
        var items = Enumerable.from(lpItems).where(a => a.Code == item.Code).toArray();
        for (var i = 0; i < items.length; i++) {
            insertLPReceivingRow(items[i]);
        }
    });
    calculateLP();
    playSuccess();
    updateInputStyle();

}

function deleteItem(btn) {
    debugger
    var ime = btn.dataset.ime;
    var item = Enumerable.from(lpItems).firstOrDefault(x => x.IME == ime);
    const index = lpItems.indexOf(item);
    if (index > -1) {
        lpItems.splice(index, 1);
    }
    var row = $(btn).parents(".item-data");
    row.remove();
    calculateLP();
    sum("Debit");
}

function save() {
    swal({
        title: "Business Manager",
        text: "Are You Sure to save a transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Save it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        focusConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                resolve();
                saveVoucher();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

}
function saveVoucher() {
    var VoucherDate = $(".VoucherDate").val();
    var PartyCode = $(".PartyCode ").val();
    var model = {
        LeftTable: lpItems,
        RightTable: groupedTableItems,
        PartyCode: PartyCode
    };
    $.ajax({
        url: "/Transactions/LPReceiving.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: model }),
        success: function (response) {           
            if (response.d.Success) {
                swal({
                    title: "Success",
                    text: response.d.Message,
                    type: 'success',
                    focusConfirm: true,
                });
                groupedTableItems = [];
                lpItems = [];
                reArrangeItems();
                updateInputStyle();
            }
            else {
                swal({
                    title: "Error",
                    text: response.d.Message,
                    type: 'error',
                    focusConfirm: true,
                });
            }


        }



    })
}
$(document).on("click", "#saveBtn", function () {
    save();
});
$(document).on("keydown", function (e) {
    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }
})

function isDuplicateIMEI(IMEI) {
    var isImeiAlreardyExist = $(".table-1 [value=" + IMEI + "].IMEI").length > 0;

    return isImeiAlreardyExist;
}

//function EditVoucher() {
//    var voucherId = $(".VoucherNumber").val();
//    window.location.href = "/Correction/" + location.pathname.split('/')[2].replace(".aspx", '') + "Edit.aspx?v=" + voucherId;
//}