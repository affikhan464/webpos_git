﻿
$(document).on("keypress", "#txtBarCode", function (e) {
    
    if (e.which == 13 && $("#txtBarCode").val().length) {
       
        textboxid = "txtBarCode";
        getRecordsAgainstSaleIME();

        $("#" + textboxid).select();
    } else if (!$("#txtBarCode").val().length) {
        playError();
    }
})

function getRecordsAgainstSaleIME() {   //  Sale New/Edit 11111111111111
        var ajaxData = "";
        var itemName = $("[data-id=" + textboxid + "]").length ? $("[data-id=" + textboxid + "]").val() : $("#" + textboxid).val();
        
       
        ajaxData = { "key": itemName + ',' + $('.PartyCode').val() + ',' + $('.PartyDealApplyNo').val() };
        
    
    $.ajax({
        url: '/WebPOSService.asmx/GetRecordsAgainstIME',
        type: "Post",

        data: ajaxData,

        success: function (items) {

            if (items == "") {
                playError();
            }

            if (items.length != 0) {
               
                var imei = $("#txtBarCode").val();
                    if (!items[0].isItemAvailble) {
                        swal('IME Not Found!!', '', 'error');
                        playError();
                        return false;
                    }
                    var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
                    var Sno = rowCount;
                    var itemExist = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                    if (itemExist > 0 && !isDuplicateIMEI(imei)) {
                        Sno = itemExist;
                        var qty = Number($("#myTable ." + Sno + "_Qty").val()) + 1;
                        $("#myTable ." + Sno + "_Qty").val(qty);
                        calculationSale();

                        //Ime Row Insert Start --> This insert IME Row
                        if (items[0].BarCodeCategory == 1) {
                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var element = $("#myTable tr[data-rownumber=" + Sno + "]")[0];
                            appendRowInIMEI(element, true, true);
                        }
                        //Ime Row Insert End
                    }
                    else if (!isDuplicateIMEI(imei)) {
                        insertSaleRow(Sno, items[0].Code, items[0].Description, items[0].Qty, items[0].Rate, 0, items[0].ItemDis, items[0].PerDis, items[0].DealDis, 0, 0, items[0].avrgCost, items[0].revCode, items[0].cgsCode, items[0].actualSellingPrice, items[0].PerPieceCommission)
                        calculationSale();

                        //Ime Row Insert Start --> This insert IME Row
                        if (items[0].BarCodeCategory == 1) {
                            $("#IMEIRow [name=ImeiTxbx]").val(imei);
                            var rowNumer = itemAlreadyExist(items[0].Code, items[0].Rate, items[0].PerDis, items[0].DealDis);
                            var element = $("#myTable tr[data-rownumber=" + rowNumer + "]")[0];
                            appendRowInIMEI(element, true,true);
                        }
                        //Ime Row Insert End
                    }
                    else if (isDuplicateIMEI(imei)) {
                        swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
                        playError();

                        return false;
                    }
                    rerenderSerialNumber();
                }
               

            
            else {
                $("#" + dataType + "Table").remove();
                $("#" + dataType + "HeadTable").remove();
                $("#" + dataType + "Table table").remove();
                $("#" + dataType + "HeadTable table").remove();
                $(".selected").remove();
            }
        }, fail: function (jqXhr, exception) {

        }
    });
}

function removeIMEs(itemCode, itemRowNumber) {
    $(".imei-table table tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + itemCode + "]").remove();
}
function itemAlreadyExist(code, rate, disPer, dealDis) {
    var rowNo = $("#myTable [data-itemcode=" + code + "][data-itemrate=" + rate + "][data-itemdisper=" + disPer + "][data-itemdealdis=" + dealDis + "]").data("rownumber");
    return rowNo;
}

function appendRowInIMEI(element, insertIntoTableAsWell = false, scannedThroughBarcodeTxbx = false) {
    var itemRowNumber = $("#IMEIRow [name=ItemRowNumber]").val();
    var ItemCode;
    if (insertIntoTableAsWell == false) {

        $("#IMEIRow [name=ImeiTxbx]").val("").select().focus();
        var ItemCode = $(element).parents("tr").find("input[name=Code]").val();

    } else {
        var ItemCode = element.dataset.itemcode;
    }


    $(".IMEPortion").fadeIn();

    $.ajax({
        async: false,
        url: '/WebPOSService.asmx/IsItemIMEVerifiedAgainstItemCode',
        type: "Post",
        data: { "itemCode": ItemCode },
        success: function (isChecked) {

            $('[name=isIMEVerifiedChkbx]').prop('checked', isChecked);
            var rowNumber = element.dataset.rownumber;

            var rowInputs = $("#myTable #Row_" + rowNumber + " input");
            rowInputs.each(function (index, input) {

                var name = $(input).attr("name");
                var value = $(input).val();
                if (name == "Qty") {
                    $("#IMEIRow [name=" + name + "]").val(1);
                } else {
                    $("#IMEIRow [name=" + name + "]").val(value);
                }


            });
            $("#IMEIRow  [name=ItemRowNumber]").val(rowNumber);
            var ime = $(`#myTable .${rowNumber}_ScannedItem`).val();
            if (ime == "") {
                ime = $("[name=BC]").val();
            }
            //Scanned through Barcode Reader
            if (insertIntoTableAsWell) {
                inserIMEItem(rowNumber, scannedThroughBarcodeTxbx);
                if (scannedThroughBarcodeTxbx) {
                    $("[name=BC]").focus().select();
                }
            } else {
                scrollTo("#IMEIRow [name=ImeiTxbx]");

            }
        },
        fail: function (jqXhr, exception) {
        }
    });

}

function rerenderIMEITableSerialNumber() {
    var rows = $("#IMEITable tbody tr .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html("<input type='text' value='" + sno + "'  />");

    })
}

function rerenderSerialNumber() {
    var rows = $("#myTable tbody tr .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html("<input type='text' value='" + sno + "'  />");
    })
}

$(document).on("keydown", "#IMEIRow [name=ImeiTxbx]", function (e) {

    if (e.keyCode == 13) {
        inserIMEItem();
    }

});
function inserIMEItem() {
    
    var imei = $("#IMEIRow [name=ImeiTxbx]").val();
    var itemCode = $("#IMEIRow [name=Code]").val();
    
    if (!isDuplicateIMEI(imei) && isQuantityAvailable(itemCode)) {

        $.ajax({
            async: false,
            url: '/WebPOSService.asmx/IsItemIMELegal',
            type: "Post",
            data: { "itemCode": itemCode, "imeCode": imei },
            success: function (model) {
                if (!model.Error) {
                    if (model.isLengthCorrect) {
                        if (!model.isSold) {
                            if (model.isItemMatch) {
                                var rowCount = $("#IMEITable td").closest("tr").length == 0 ? 1 : $("#IMEITable td").closest("tr").length + 1;
                                var Sno = rowCount;

                                var rows = createIMERow(itemRowNumber, Sno, $("#IMEIRow [name=Code]").val(), $("#IMEIRow [name=ItemName]").val(), imei, $("#IMEIRow [name=Qty]").val()
                                    , $("#IMEIRow [name=Rate]").val(), $("#IMEIRow [name=Amount]").val(),
                                    $("#IMEIRow [name=ItemDis]").val(), $("#IMEIRow [name=PerDis]").val(), $("#IMEIRow [name=DealDis]").val(),
                                    $("#IMEIRow [name=NetAmount]").val(), $("#IMEIRow [name=PurAmount]").val());

                                var lastChildofTableCode = $("#IMEITable tbody tr:last-child").length > 0 ? $("#IMEITable tbody tr:last-child").data().itemcode : null;
                                var lastChildofTableItemRowNumber = $("#IMEITable tbody tr:last-child").length > 0 ? $("#IMEITable tbody tr:last-child").data().itemrownumber : null;

                                if ((lastChildofTableCode == itemCode && lastChildofTableItemRowNumber == itemRowNumber) || lastChildofTableCode == null) {
                                    $(rows).appendTo("#IMEITable tbody");
                                    
                                } else {
                                    var rowOfSpecifiedItemCode = $("#IMEITable tbody tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + itemCode + "]");
                                    var isChildOfSpecifiedCodeAvailable = rowOfSpecifiedItemCode.length > 0;
                                    if (isChildOfSpecifiedCodeAvailable) {
                                        var lastChildOfSpecificCodeId = "#IMEITable #" + $("#IMEITable tbody tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + itemCode + "]").last().attr("id");
                                        $(rows).insertAfter(lastChildOfSpecificCodeId);
                                    } else {
                                        $(rows).appendTo("#IMEITable tbody");
                                    }


                                }

                                $("#IMEIRow [name=ImeiTxbx]").val("");
                                $("#IMEIRow input").val("");
                                $("#txtBarCode").val("").select().focus();
                                calculationSale();
                                rerenderIMEITableSerialNumber()
                                playSuccess();
                                return true;

                            } else {

                                var itemName = $("#IMEIRow [name=ItemName]").val();
                                swal("Error", "Please scan for " + itemName, "error");
                                $("#IMEIRow [name=ImeiTxbx]").val("");
                                playError();
                                return false;

                            }
                           
                        }
                        else {
                            swal("Error", "IME Not Available !!", "error");
                            $("#IMEIRow [name=ImeiTxbx]").val("");
                            playError();

                            return false;
                        }
                    } else {
                        swal("Error", "IME length is not correct !!", "error");
                        $("#IMEIRow [name=ImeiTxbx]").val("");
                        playError();

                        return false;
                    }
                } else {
                    swal("Error", model.ErrorMessage, "error");
                    return false;
                }
            },
            fail: function (jqXhr, exception) {
                debugger;
            }
        });
        
    }
    else {
        swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
        playError();

    }

}

function isDuplicateIMEI(IMEI) {
    var isImeiAlreardyExist = $("#IMEITable [value=" + IMEI + "].IMEI").length > 0;

    return isImeiAlreardyExist;
}
function isQuantityAvailable(code) {
    var rows = $("#myTable  [value=" + code + "][name=Code]");
    var qtyInMainTable = 0;
    rows.each(function (index, element) {
        var rownumber = $(element).parents("tr").data().rownumber;
        qtyInMainTable += Number($("#myTable  ." + rownumber + "_Qty").val());

    });

    var qtysInIMETable = $("#IMEITable [data-code=" + code + "]");
    var sumqtys = 0;
    qtysInIMETable.each(function (index, element) {
        var qty = Number(element.value);
        sumqtys += qty;
    });

    return sumqtys < qtyInMainTable;

}

function updateIMETable(itemCode, rowNumber) {

    var Rate = $("#myTable ." + rowNumber + "_Rate").val();
    var PerDis = $("#myTable ." + rowNumber + "_PerDis").val();
    var DealDis = $("#myTable ." + rowNumber + "_DealDis").val();
    var ItemDis = $("#myTable ." + rowNumber + "_ItemDis").val();
    var Amount = $("#myTable ." + rowNumber + "_Amount").val();
    var NetAmount = $("#myTable ." + rowNumber + "_NetAmount").val();
    var PurAmount = 0;

    if ($("#myTable ." + rowNumber + "_ActualCost").length > 0)
        PurAmount = $("#myTable ." + rowNumber + "_ActualCost").val();
    else
        PurAmount = $("#myTable ." + rowNumber + "_PurAmount").val();

    var rows = $("#IMEITable [data-itemrownumber=" + rowNumber + "][data-itemcode=" + itemCode + "]");

    rows.each(function (index, element) {

        $(element).find("[name=Rate]").val(Rate);
        $(element).find("[name=PerDis]").val(PerDis);
        $(element).find("[name=DealDis]").val(DealDis);
        $(element).find("[name=ItemDis]").val(ItemDis);
        $(element).find("[name=Amount]").val(Amount);
        $(element).find("[name=NetAmount]").val(NetAmount);
        $(element).find("[name=PurAmount]").val(PurAmount);

    });

}

function updateRowRateDisDeal(rowNumber) {
    var Rate = $("#myTable ." + rowNumber + "_Rate").val();
    var PerDis = $("#myTable ." + rowNumber + "_PerDis").val();
    var DealDis = $("#myTable ." + rowNumber + "_DealDis").val();
    $("#myTable [data-rownumber=" + rowNumber + "]")
        .data('itemrate', Rate).data('itemdisper', PerDis).data('itemdealdis', DealDis)
        .attr('data-itemrate', Rate).attr('data-itemdisper', PerDis).attr('data-itemdealdis', DealDis);
}
function reArrangeIMETabeRows() {
    var items = $("#myTable tr");
    var newArrangedRows;
    items.each(function (index, element) {
        var rowNumber = element.dataset.rownumber;
        var itemCode = element.dataset.itemcode;
        var IMEIRows = $(`#IMEITable tr[data-itemrownumber=${rowNumber}][data-itemcode=${itemCode}]`);
        IMEIRows.each(function (i, e) {
            newArrangedRows += e.outerHTML
        });

    });

    $(`#IMEITable tbody`).html(newArrangedRows);
    rerenderIMEITableSerialNumber();
    calculationSale();
}
function createIMERow(itemRowNumber, Sno, ItemCode, ItemName, IME, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount) {
    var row = `
    <tr  data-itemrownumber='${itemRowNumber}' data-itemcode='${ItemCode}'  data-rate='${Rate}' data-perdis='${PerDis}' data-dealrs='${DealDis}' id='Row_${Sno}' data-rownumber='${Sno}'>
        <td class='SNo three  '>
            ${Sno}
        </td>
        <td class='four name'>
            <input class='${Sno}_Code' name='Code' type='text' value='${ItemCode}' />
        </td>
        <td class='name fifteen '>
            <input class='${Sno}_ItemName' name='ItemName' type='text' value='${ItemName}' />
        </td>
        <td class='thirteen'>
            <input class='${Sno}_IMEI IMEI' name='IMEI' type='text' value='${IME}' />
        </td>
        <td class='five'>
            <input class='${Sno}_Qty calcu' data-code='${ItemCode}' name='Qty' type='text' value='${Qty}' />
        </td>
        <td class='eight'>
            <input name='Rate' class='${Sno}_Rate calcu' type='text' value='${Rate}' /></td>
        <td class='eight'>
            <input class='${Sno}_Amount' name='Amount' type='text' value='${Amount}' />
        </td>
        <td class='eight'>
            <input class='${Sno}_ItemDis' type='text' value='${ItemDis}' name='ItemDis' />
        </td>
        <td class='six'>
            <input name='PerDis' class='${Sno}_PerDis calcu' type='text' value='${PerDis}' />
        </td>
        <td class='eight'>
            <input name='DealDis' class='${Sno}_DealDis' type='text' value='${DealDis}' />
        </td>
        <td class='eight'>
            <input class='${Sno}_NetAmount' type='text' value='${NetAmount}' name='NetAmount' />
        </td>
        <td class='eight'>
            <input class='${Sno}_PurAmount' type='text' value='${PurAmount}' name='PurAmount' />
        </td>
        <td class='six actionBtn text-center'><i class='fas fa-times'></i> </td>
    </tr>`;
    return row;
}

function insertSaleRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, actualSellingPrice, perPieceCommission) {
    var rows = `
          <tr class='d-flex pl-1' id='Row_${ Sno}' data-itemcode='${ItemCode}' data-itemrate='${Rate}' data-itemdisper='${PerDis}' data-itemdealdis='${DealDis}' data-rownumber='${Sno}'>
            <td class='SNo three flex-1'><input type='text' value='${Sno}'  /></td>
            <td  class='three flex-3 name' ><input  class='${Sno}_Code' name='Code' type='text' value='${ItemCode}'  /></td>
            <td class='name three flex-6'><input  class='${Sno}_ItemName ' name='ItemName' type='text' value='${ItemName}'  /></td>
            <td class='three flex-1'><input  class='${Sno}_Qty calcu' name='Qty' type='text' value='${Qty}'  /></td>
            <td class='three flex-3'><input name='Rate'  class='${Sno}_Rate calcu'  type='text' value='${Rate}' /></td>
            <td class='three flex-3'><input class='${Sno}_Amount' name='Amount'  type='text' value='${Amount}' /></td>
            <td class='three flex-3'><input class='${Sno}_ItemDis' type='text' value='${ItemDis}' name='ItemDis' /></td>
            <td class='three flex-3'><input  name='PerDis'  class='${Sno}_PerDis calcu' type='text' value='${PerDis}' /></td>
            <td class='three flex-3'><input name='DealDis'  class='${Sno}_DealDis' type='text' value='${DealDis}' /></td>
            <td class='three flex-3'><input class='${Sno}_NetAmount' type='text' value='${NetAmount}' name='NetAmount' /></td>
            <td class='three flex-3'><input class='${Sno}_PurAmount' type='text' value='${PurAmount}' name='PurAmount' /></td>
            
            <td hidden> <input hidden='hidden' class='${Sno}_AverageCost' value='${avrgCost}' /></td>  
            <td hidden> <input hidden='hidden' class='${Sno}_ActualSellingPrice' value='${actualSellingPrice}' /></td> 
            <td hidden> <input hidden='hidden' class='${Sno}_RevCode' value='${revCode}' /></td>
            <td hidden> <input hidden='hidden' class='${Sno}_CgsCode' value='${cgsCode}' /></td>  
            <td hidden> <input hidden='hidden' name='PerPieceCommission' class='${Sno}_PerPieceCommission' value='${perPieceCommission}' /></td>  
            <td class='three flex-3 actionBtn text-center'>
              <i data-itemcode='${ItemCode}' class='fas fa-times'></i> 
              <i data-rownumber='${Sno}' class='fas fa-barcode IMETrigerBtn' onclick='appendRowInIMEI(this)'></i> 
            </td>
          </tr>`;
    $(rows).appendTo("#myTable tbody");
    $(".selected").remove();
}