﻿<%@ Page Title="Sale New Items" Language="C#" MasterPageFile="~/Transactions/TransactionsMasterPage.master" AutoEventWireup="true" CodeBehind="SaleNewItems.aspx.cs" Inherits="WebPOS.Transactions.NewSale.SaleNewItems" %>

<asp:Content ID="Content3" ContentPlaceHolderID="StyleContentPlaceHolder" runat="server">
    <link href="saleNew.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderAndGridContentPlaceHolder" runat="server">


    <asp:TextBox ID="TextBox1" Style="display: none" CssClass="defaultPartyCode" runat="server"></asp:TextBox>
    <div class="searchAppSection">
        <div class="container-fluid transactionPage salePage">
            <div class="row  headerSection " style="border: ridge; padding: 5px;">
                <div class=" col-sm-2 col-md-3 col-lg-3">
                    <div class="row">
                        <div class="col col-sm-2 col=md-3 col-lg-3">
                            <label style="margin-top: 1px;">Invoice#</label>
                        </div>
                        <div class="col col-sm-2 col=md-3 col-lg-6">
                            <asp:TextBox ID="txtInvoiceNo" disabled="disabled" CssClass="form-control InvoiceNumber " ClientIDMode="Static" name="Invoice" runat="server" autocomplete="off"></asp:TextBox>
                        </div>
                        <div class="col col-sm-2 col=md-3 col-lg-3">
                            <a class=" invoiceEditBtn"><i class="mt-1 fas fa-edit" aria-hidden="true"></i></a>
                            <a class="printInvoiceBtn print"><i class="mt-1 fas fa-print" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-sm-2 col=md-3 col-lg-3">
                            <label style="margin-top: 1px;">Date</label>
                        </div>
                        <div class="col col-sm-2 col=md-3 col-lg-9">
                            <asp:TextBox ID="txtDate" AutoPostBack="false" runat="server" name="Date" Enabled="true" CssClass="form-control datetimepicker Date"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-sm-2 col=md-3 col-lg-3">
                            <label style="margin-top: 1px;">Pre. Bal.</label>
                        </div>
                        <div class="col col-sm-2 col=md-3 col-lg-9">
                            <input type="text" disabled="disabled" class="form-control PartyBalance inv1 " id="txtPreviousBalance" name="PartyBalance" style="text-align: right;" />
                        </div>
                    </div>
                </div>
                <div class=" col-sm-2 col-md-3 col-lg-3">
                    <div class="row  headerSection">
                        <div class=" col-sm-2 col-md-3 col-lg-6">
                            <input name="ManInv" type="text" class="ManInv  form-control inv1" />
                        </div>
                        <div class=" col-sm-2 col-md-3 col-lg-6">
                            <select id="salesManDD" data-ddtype="classic" class="w-100">
                            </select>
                        </div>
                    </div>
                    <input id="txtBarCode" class="form-control inv1 BarcodeTxbx" type="text" name="BC" />
                    <input id="SearchBox" data-id="itemTextbox" data-type="Item" data-function="GetRecords" class=" SearchBox autocomplete form-control inv1" type="text" placeholder="Type Item Name Here" />
                </div>
                <div class=" col-sm-2 col-md-3 col-lg-6" style="padding: 5px;">
                    <div class="row  headerSection">
                        <div class=" col-sm-2 col-md-3 col-lg-7">
                            <input id="PartySearchBox" data-id="PartyTextbox" data-nextfocus="#SearchBox" data-type="Party" data-function="GetParty" class="p-0 clientInput PartyName clearable PartySearchBox autocomplete form-control" name="PartyName" type="text" placeholder="Enter a party name" />
                        </div>
                        <div class=" col-sm-2 col-md-3 col-lg-3">
                            <input class="clientInput form-control PartyCode inv11" type="text" id="txtPartyCode" disabled="disabled" name="PartyCode" placeholder="Enter party code" />
                        </div>
                        <div class=" col-sm-2 col-md-3 col-lg-2">
                            <a class="w-100" onclick="$('#partyRegistrationModal').modal()" data-toggle="tooltip" title="Add Party"><i class="pt-1 fas fa-plus" aria-hidden="true"></i></a>
                            <span id="txtNorBal" data-toggle="tooltip" title="Nor Balance" class="px-2 pt-2 badge badge-success PartyNorBalance badge-bm inv1" hidden="hidden"></span>
                            <span id="txtDealApplyNo" data-toggle="tooltip" title="Party Deal Apply Number!" class="px-2 pt-2 badge badge-success badge-bm PartyDealApplyNo inv1" hidden="hidden"></span>
                        </div>
                    </div>
                    <div class="row  headerSection">
                        <div class=" col-sm-2 col-md-3 col-lg-7">
                            <input type="text" class="form-control clientInput PartyAddress" id="txtAddresss" name="PartyAddress" placeholder="Address" />
                        </div>
                        <div class=" col-sm-2 col-md-3 col-lg-4">
                            <input type="text" id="txtPhone" class="PartyPhoneNumber inv11 form-control clientInput" name="PartyPhoneNumber" placeholder="Phone No." />
                        </div>
                    </div>
                    <div class="row  headerSection">
                        <div class=" col-sm-2 col-md-3 col-lg-12">
                            <input type="text" placeholder="Particular" class="form-control clientInput PartyParticular" name="PartyParticular" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="businessManagerSellSection businessManager BMtable">

        <div class="1container-fluid">

            <div class="recommendedSection allItemsView table-responsive-md">
                <div id="mainTable" class="table-wrapper">


                    <div class="itemsHeader d-flex pr-3">
                        <div class="three flex-0 pl-0 phCol">
                            <a href="javascript:void(0)">S#
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Code
                            </a>
                        </div>
                        <div class="three flex-7 phCol">
                            <a href="javascript:void(0)">Description
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Qty
                            </a>
                        </div>
                        <div class="three flex-2 phCol">
                            <a href="javascript:void(0)">Rate
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">G. Amount
											
                            </a>
                        </div>
                        <div class="three flex-2 phCol">
                            <a href="javascript:void(0)">Item D
											
                            </a>
                        </div>
                        <div class="three flex-1  phCol">
                            <a href="javascript:void(0)">Dis.%
											
                            </a>
                        </div>
                        <div class="three flex-2 phCol">
                            <a href="javascript:void(0)">Deal <span>Rs.</span>

                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">N<span>et</span> Amount
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Av<span>g.</span> Cost
											
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">SQ
											
                            </a>
                        </div>
                        <div class="three flex-4 phCol">
                            <a href="javascript:void(0)">Scann
											
                            </a>
                        </div>
                        <div class="deleteRow three flex-3 phCol">
                            <a href="javascript:void(0)"></a>
                        </div>
                    </div>
                    <!-- itemsHeader -->
                    <div class="itemsSection">
                        <div>



                            <table class="TFtable w-100" id="myTable">

                                <tbody>
                                </tbody>

                            </table>



                        </div>

                    </div>
                    <div class="itemsFooter">
                        <div class='itemRow newRow d-flex pr-3'>
                            <div class='three flex-0'><span></span></div>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-7'><span></span></div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='QtyTotal' />
                            </div>
                            <div class='three flex-2'><span></span></div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='AmountTotal' />
                            </div>
                            <div class='three flex-2'>
                                <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                            </div>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-2'>
                                <input disabled="disabled" class="form-control" name='DealDisTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control PurAmountTotal" name='PurAmountTotal' />
                            </div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='SQ_Total' />
                            </div>
                            <div class='three flex-4'>
                            </div>

                            <div class='three flex-3'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- recommendedSection -->



    </div>

    <footer class="footerTotalSection">
        <div class="container-fluid ">
            <div class="row " style="padding-top: 5px;">
                <div class="divbcColor1 col-sm-2 col-md-3 col-lg-3">
                    <label>
                        <input type="radio" onclick="calculationSale()" id="Zero" name="Mod" class="mod" value="0" checked="checked">No</label>
                    <label>
                        <input type="radio" onclick="calculationSale()" id="Three" name="Mod" class="mod" value="3">3</label>
                    <label>
                        <input type="radio" onclick="calculationSale()" id="Six" name="Mod" class="mod" value="6">6</label>
                    <label>
                        <input type="radio" onclick="calculationSale()" id="Nine" name="Mod" class="mod" value="9">9</label>
                    <label>
                        <input type="radio" onclick="calculationSale()" id="Twelve" name="Mod" class="mod" value="12">12</label>
                    <input type="text" class="info" disabled="disabled" />

                </div>
                <div class="divbcColor1 col-sm-2 col-md-3 col-lg-3">
                    <label>
                        <input onchange="DealDisApplyOnAllitems()" id="chkAutoDeal" type="checkbox" class="checkbox " /><span></span>Auto Deal Dis</label><br />
                    <label>
                        <input onchange="PerDisApplyOnAllitems()" id="chkAutoPerDis" type="checkbox" class="checkbox " /><span></span>Auto % Dis</label><br />
                    <label>
                        <input id="BarCodeScanning" type="checkbox" class="checkbox " /><span></span>BarCode Scanning</label><br />
                    <label>
                        <input id="chkAutoPrint" name="chkAutoPrint" type="checkbox" class="checkbox " /><span></span>Auto Print</label><br />
                    <label>
                        <input id="ShowSellingprice" type="checkbox" class="checkbox " /><span></span>Show Selling price</label>
                    <br />
                    <div class="btn btn-primary fa-save1 w-50 btnSave" onclick="SaveBefore()">Save</div>
                </div>
                <div class="divbcColor1 col-sm-2 col-md-3 col-lg-3">
                    <div class="divbcColor1 col-sm-2 col-md-3 col-lg-12">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-5">
                                <label>% Discount </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-7">
                                <input disabled id="txtTotPercentageDis" name="txtTotPercentageDis" class="inv3 form-control" type="text" autocomplete="off">
                            </div>



                        </div>
                    </div>
                    <div class=" col-12 col-md-12 display-flex">

                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-5">
                                <label>Net Discount</label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-7">
                                <input disabled id="txtTotalDiscount" name="netDiscount" class="NetDiscount inv3 form-control" type="text" autocomplete="off">
                            </div>
                        </div>

                    </div>
                    <div class=" col-12 col-md-12 display-flex">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-5">
                                <label>Bill Total </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-7">
                                <input disabled id="txtBillTotal" name="billTotal" class="BillTotal inv3 form-control" type="text" autocomplete="off">
                            </div>

                        </div>
                    </div>
                    <div class=" col-12 col-md-12 display-flex">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-5">
                                <label>Credit Limit</label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-7">
                                <input disabled id="CreditLimit" name="PartyCreditLimit" class="PartyCreditLimit  inv3 form-control" type="text" autocomplete="off" placeholder="Credit Limit">
                            </div>
                        </div>
                    </div>



                </div>
                <div class="divbcColor1 col-sm-2 col-md-3 col-lg-3">
                    <div class=" col-sm-2 col-md-3 col-lg-12">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <label>Total</label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-9">
                                <input disabled id="txtTotal" name="GrossTotal" class="GrossTotal inv3 form-control" type="text" autocomplete="off">
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-md-12 display-flex">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <label>DealRs </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-9">
                                <input disabled id="txtTotDeal" name="DealRs" class="DealRs inv33 form-control" type="text" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 display-flex">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <label>Disc % </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <input id="txtFlatDiscountPer" name="flatPer" class="FlatPer inv33 calcu form-control" type="text" autocomplete="off">
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <label>D.Rs. </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <input id="txtFlatDiscount" name="flatDiscount" class="FlatDiscount inv3 calcu form-control" type="text" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 display-flex">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <label>Cash R </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-5">
                                <input id="txtPaid" name="Recieved" class="Recieved inv3 calcu form-control" type="text" autocomplete="off">
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-4">
                                <select class="CashReceivedCategoryDD w-100 " id="CashReceivedCategoryDD" name="CashReceivedCategoryDD" data-ddtype="classic" data-glcode="01010101">
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-12 display-flex">
                        <div class="row">
                            <div class=" col-sm-2 col-md-3 col-lg-3">
                                <label>Balance </label>
                            </div>
                            <div class=" col-sm-2 col-md-3 col-lg-9">
                                <input disabled id="txtBalance" name="balance" class="Balance inv3 form-control" type="text" autocomplete="off">
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>

    <div class="IMEPortion  mt-2">
        <div class="container-fluid ">
            <div class="Ime-header  rounded text-center mb-3 p-2">
                <h4><i class="fas fa-barcode"></i>IMEI's</h4>

            </div>
            <label class="btn btn-bm active checkbox-btn" style="width: 180px;">
                <input type="checkbox" name="isIMEVerifiedChkbx" />
                <i class="fas fa-check mr-2"></i>
                Is IME Verified?
            </label>
            <div>

                <table id="IMEIRow">
                    <tr data-rownumber="1">
                        <td>
                            <input name="ItemRowNumber" type="hidden" />
                            <input name="Code" type="text" class="form-control" value="" /></td>
                        <td class="name">
                            <input name="ItemName" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Qty" type="text" value="" /></td>
                        <td>
                            <input name="Rate" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Amount" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="ItemDis" /></td>
                        <td>
                            <input name="PerDis" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input name="DealDis" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="NetAmount" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="PurAmount" /></td>
                        <td>
                            <input class="form-control" type="text" style="width: 200px; text-align: left" name="ImeiTxbx" placeholder="Enter IME" />
                        </td>
                    </tr>
                </table>
            </div>


            <div class="imei-table">
                <a class="btn btn-bm w-auto" onclick="reArrangeIMETabeRows()"><i class="fas fa-list-ol"></i>Re-Arrange Table</a>
                <div class="businessManagerSellSection businessManager BMtable">


                    <div class="recommendedSection allItemsView">
                        <div class="itemsHeader">
                            <div class="three pl-0 phCol">
                                <a href="javascript:void(0)">S#
                                </a>
                            </div>
                            <div class="four  phCol">
                                <a href="javascript:void(0)">Code
                                </a>
                            </div>
                            <div class="fifteen  phCol">
                                <a href="javascript:void(0)">Description
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">IMEI
                                </a>
                            </div>
                            <div class="five phCol">
                                <a href="javascript:void(0)">Qty
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Rate
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">G. Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Item D
											
                                </a>
                            </div>
                            <div class="six  phCol">
                                <a href="javascript:void(0)">Dis.%
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Deal <span>Rs.</span>

                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">N<span>et</span> Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Av<span>g.</span> Cost
											
                                </a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                        </div>

                        <div class="itemsSection1" style="background-color: aqua !important;">
                            <div class="imei-body">
                                <table class="TFtable" id="IMEITable">

                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="itemsFooter">
                            <div class='itemRow newRow'>
                                <div class='three'><span></span></div>
                                <div class='four'><span></span></div>
                                <div class='fifteen  '><span></span></div>
                                <div class='thirteen '><span></span></div>

                                <div class='five '>
                                    <input disabled="disabled" class="form-control" name='QtyTotal' />
                                </div>
                                <div class='eight '><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='AmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                                </div>
                                <div class='six'><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='DealDisTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='PurAmountTotal' />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>

    <%@ Register Src="~/Transactions/_IMEList.ascx" TagName="_IMEList" TagPrefix="uc" %>
    <uc:_imelist id="_IMEList" runat="server" descriptiontype="SaleReturn,Purchase" />
    <%@ Register Src="~/Transactions/_ItemIMEList.ascx" TagName="_ItemIMEList" TagPrefix="uc" %>
    <uc:_itemimelist id="_ItemIMEList1" runat="server" descriptiontype="SaleReturn,Purchase" />

    <div class="modal fade" id="partyRegistrationModal" tabindex="-1" role="dialog" aria-labelledby="partyRegistrationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="attributeModalLabel">Party Registration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <%@ Register Src="~/Registration/_PartyRegistration.ascx" TagName="_PartyRegistration" TagPrefix="uc" %>
                    <uc:_partyregistration id="_PartyRegistrationHead1" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptsContentPlaceHolder" runat="server">
    <script src="/Script/PartyRegistration.js"></script>
    <script src="IME.js"></script>
    <script src="/Script/IME/IME.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            appendAttribute("", "IncomeAccount");
            appendAttribute("", "COGSAccount");
            $(".invoiceEditBtn").on("click", function () {
                var invoiceNumber = $(".InvoiceNumber").val();
                window.location.href = "/Correction/SaleEdit/SaleEditNew.aspx?InvNo=" + invoiceNumber;
            });
            document.getElementById("PartySearchBox").focus();

        });

        //For CTRL+P
        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printThisTransaction();
            }
        });
        function printThisTransaction() {
            window.open(

                '/Transactions/TransactionPrints/SaleNewItemCR.aspx?InvoiceNo=' + $(".InvoiceNumber").val(),
                "DescriptiveWindowName",
                "resizable,scrollbars,status"
            );
        }
        //For Print Button
        $(document).on('click', '.printInvoiceBtn', function () {
            printThisTransaction();
        })

    </script>
    <script src="InsertDataInNewSale.js"></script>
    <script src="calculationNewSale.js"></script>
    <script src="SaveNewSale.js"></script>
    <script src="KeyDown.js"></script>

    <script type="text/javascript">

        $(document).on('input', '.calcu', function () {
            calculationSale();
        });

        $(document).ready(function () {
            $("#app").addClass("hide");
            resizeTable();
            appendAttribute("salesManDD", "SaleManList");
            appendAttribute("CashReceivedCategoryDD", "GLCode", "0101010100001");

        });


    </script>
</asp:Content>
