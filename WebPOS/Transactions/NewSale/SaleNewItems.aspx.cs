﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Web;

namespace WebPOS.Transactions.NewSale
{
    public partial class SaleNewItems : System.Web.UI.Page
    {


        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompId"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;

        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        ModViewInventory objModViewInventory = new ModViewInventory();
        
        Module4 objModule4 = new Module4();
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        private string lastInvoiceNumber;

        protected void Page_Load(object sender, EventArgs e)
        {
            lastInvoiceNumber = objModule1.MaxInvNoVerify().ToString();
            txtInvoiceNo.Text = lastInvoiceNumber;
            txtDate.Text = DateTime.Today.ToString("MM/dd/yyyy");

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveProductViewModel saveProductViewModel)
        {



            try
            {

                if (HttpContext.Current.Session["UserId"] != null)
                {
                    Module1 objModule1 = new Module1();
                    Module4 objModule4 = new Module4();
                    ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                    if (con.State != ConnectionState.Open)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }

                    }

                    tran = con.BeginTransaction();

                    var invoiceNumber = objModule1.MaxInvNo();
                    //string PartyCode = "";
                    //PartyCode = saveProductViewModel.ClientData.Code.ToString();
                    Int64 PG = 1;
                    /////PG = "objModPartyCodeAgainstName.PartyGroupIdAgainstCode(PartyCode);


                    //if (lstSaleMan.Text != "") {SaleManID = 1; //objModPartyCodeAgainstName.SaleManCodeAgainstSaleManName(lstSaleMan.Text); }
                    var selectedDate = saveProductViewModel.ClientData.Date;
                    var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
                    var InvoiceDate = convertedDate.ToString("yyyy-MM-dd");
                    var PartyName = saveProductViewModel.ClientData.Name.ToString();
                    var SaleManName = saveProductViewModel.ClientData.SalesManName;
                    var SaleManID = saveProductViewModel.ClientData.SalesManId;

                    var PartyCode = saveProductViewModel.ClientData.Code.ToString();
                    var Particular = saveProductViewModel.ClientData.Particular.ToString();
                    var Addresss = saveProductViewModel.ClientData.Address.ToString();

                    var ManualInvNo = saveProductViewModel.ClientData.ManInv.ToString();
                    var PartyGRoup = "1";
                    var OperatorID = HttpContext.Current.Session["UserId"];
                    var PartyPhone = saveProductViewModel.ClientData.PhoneNumber.ToString();
                    var SysTime = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");

                    var NorBalance = Convert.ToInt32(saveProductViewModel.ClientData.NorBalance);
                    SqlCommand cmd = new SqlCommand("insert into invoice1 (InvNo,name,salesman,Date1,PartyCode,Particular,Address,SaleManCode,Manual_InvNo,PartyGPID,OperatorID,Phone,System_Date_Time,CompID) values(" + invoiceNumber + ",'" + PartyName + "','" + SaleManName + "','" + InvoiceDate + "','" + PartyCode + "','" + Particular + "','" + Addresss + "'," + SaleManID + ",'" + ManualInvNo + "'," + PartyGRoup + "," + OperatorID + ",'" + PartyPhone + "','" + SysTime + "','" + CompID + "')", con, tran);
                    cmd.ExecuteNonQuery();

                    

                    //----------------------------------------------------------------------Invoice 2
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var ItemDiscount = Convert.ToDecimal(item.ItemDis);
                        var PerDiscount = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurchaseAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sold Inv # " + invoiceNumber + ",Party-" + PartyName;
                        var actualSellingPrice = string.IsNullOrEmpty(item.ActualSellingPrice) ? 0 : Convert.ToDecimal(item.ActualSellingPrice);
                        var PerPieceCommission = string.IsNullOrEmpty(item.PerPieceCommission) ? 0 : Convert.ToDecimal(item.PerPieceCommission);


                        SqlCommand cmd3 = new SqlCommand("insert into invoice2 (InvNo,SN0,Code,Description ,Rate,Qty,Amount,Item_Discount,PerDiscount,Pcs,DealRs,CompID,Purchase_Amount,ActualSellingPrice,PerPieceCommission)values(" + invoiceNumber + ",'" + SNO + "','" + Code + "','" + Description + "'," + Rate + "," + Quantity + "," + Amount + "," + ItemDiscount + "," + PerDiscount + "," + Quantity + "," + DealRs + ",'" + CompID + "'," + PurchaseAmount + "," + actualSellingPrice + "," + PerPieceCommission + ")", con, tran);
                        cmd3.ExecuteNonQuery();
                        SqlCommand cmd5 = new SqlCommand("insert into inventory (Dat,Icode,Description,Issued,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,System_Date_Time,CompID) values('" + InvoiceDate + "'," + Code + ",'" + Descrip + "'," + Quantity + "," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + SysTime + "','" + CompID + "')", con, tran);
                        cmd5.ExecuteNonQuery();



                        objModule4.ClosingBalanceNewTechniqueIssue(Convert.ToDecimal(Code), Quantity, tran, con);



                        Quantity = 0;
                        Rate = 0;
                        Amount = 0;
                        ItemDiscount = 0;
                        PerDiscount = 0;
                        DealRs = 0;
                        NetAmount = 0;
                        PurchaseAmount = 0;
                    }

                    //------------------------------------------------------------------Invoice 3
                    decimal TotalPcs = 0;
                    decimal Total = 0;
                    decimal Paid = 0;
                    decimal FlatDisRs = 0;
                    decimal FlatDisRate = 0;
                    decimal Balance = 0;
                    decimal NetDiscount = 0;
                    decimal PreviousBalance = 0;



                   

                    Total = Convert.ToDecimal(saveProductViewModel.TotalData.GrossTotal);
                    Paid = Convert.ToDecimal(saveProductViewModel.TotalData.Recieved);
                    FlatDisRs = Convert.ToDecimal(saveProductViewModel.TotalData.FlatDiscount);
                    Balance = Convert.ToDecimal(saveProductViewModel.TotalData.Balance);
                    NetDiscount = Convert.ToDecimal(saveProductViewModel.TotalData.NetDiscount);
                    PreviousBalance = Convert.ToDecimal(saveProductViewModel.TotalData.PreviousBalance);
                    FlatDisRate = Convert.ToDecimal(saveProductViewModel.TotalData.FlatPer);


                    SqlCommand cmd4 = new SqlCommand("insert into invoice3 (InvNo,Total,CashPaid,Balance,Balance1,Discount1,Date1,TotalPcs,PrevBalance,FlatDiscount,Flat_Discount_Per,CompID) values(" + invoiceNumber + "," + Total + "," + Paid + "," + Balance + "," + Balance + "," + NetDiscount + ",'" + InvoiceDate + "','" + TotalPcs + "'," + PreviousBalance + "," + FlatDisRs + "," + FlatDisRate + ",'" + CompID + "')", con, tran);
                    cmd4.ExecuteNonQuery();
                    ///======================================================================IME
                    foreach (var IME in saveProductViewModel.IMEItems)
                    {
                        var SysTimeIME = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");
                        var SNO = Convert.ToInt16(IME.SNo);
                        var Code = Convert.ToString(IME.Code);
                        PartyName = saveProductViewModel.ClientData.Name.ToString();
                        var Description = IMEDescription.Sale;
                        var InvNo = invoiceNumber;
                        var IMEI = IME.IMEI;
                        var Rate = IME.Rate;
                        var DisPer = IME.PerDis;
                        var DisRs = IME.ItemDis;
                        var DealRs = IME.DealDis;


                        SqlCommand cmd3IME = new SqlCommand(@"insert into InventorySerialNoFinal(Date1, ItemCode, Description, VoucherNo, IME, PartyCode, Price_Cost, System_Date_Time, Sold, 
                                                            CompID, DisPer, DisRs, DealRs) values('" + InvoiceDate + "', '" + Code + "', '" + Description + "', '" + InvNo + "', '"
                                                            + IMEI + "', '" + PartyCode + "', " + Rate + ", '" + SysTimeIME + "', " + 1 + ", '" +
                                                            CompID + "', " + DisPer + ", " + DisRs + ", " + DealRs + ")", con, tran);
                        cmd3IME.ExecuteNonQuery();
                        SqlCommand cmd4IME = new SqlCommand("Update InventorySerialNoFinal set Sold=1 where CompID='" + CompID + "' and IME='" + IMEI + "'", con, tran);
                        cmd4IME.ExecuteNonQuery();
                    }


                    //////////        //=========================================================General Entry-------------------------------
                    decimal a = objModule1.MaxJVNo();
                    SqlCommand cmd6 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,System_Date_Time_Actual) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv# " + invoiceNumber + " / " + ManualInvNo + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv# " + invoiceNumber + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                    cmd6.ExecuteNonQuery();

                    var revenueAmounts = saveProductViewModel.Products.Where(e => !string.IsNullOrWhiteSpace(e.RevenueCode)).GroupBy(c => c.RevenueCode)
                        .Select(x => new GLModel()
                        {
                            Amount = x.Sum(o => Convert.ToDecimal(o.Amount)),
                            Code = x.First().RevenueCode
                        }).ToList();

                    foreach (var revenue in revenueAmounts)
                    {
                        if (revenue.Amount > 0)
                        {
                            SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,System_Date_Time_Actual) values('" + revenue.Code + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                            cmd7.ExecuteNonQuery();
                        }
                    }


                    SqlCommand cmd8 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv#" + invoiceNumber + " / " + ManualInvNo + " - Ptc:" + Particular + ",TPcs:" + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                    cmd8.ExecuteNonQuery();
                    //////////        //---------------------------------------------------------Detail of bill into party ledger---------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var DisRs = Convert.ToDecimal(item.ItemDis);
                        var DisPer = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurchaseAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sold Inv # " + invoiceNumber + ",Party-" + PartyName;


                        SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,Code,DescriptionDr,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,DisRs,DisPer,DealRS,CompID) values('" + InvoiceDate + "','" + PartyCode + "','" + Description + "','" + Quantity + "','" + Rate + "','" + Amount + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "'," + DisRs + "," + DisPer + "," + DealRs + ",'" + CompID + "')", con, tran);
                        cmd10.ExecuteNonQuery();
                    }

                    //////////        //'--------------------------------Discount------------------------------------------------------------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    if (NetDiscount > 0)
                    {
                        a = objModule1.MaxJVNo();                                                                                                                                                                              // 0103020300001
                        SqlCommand cmd11 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,System_Date_Time_Actual) values('" + "0103020300001" + "','" + InvoiceDate + "','" + "Accounts Receivable" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Accounts Receivable" + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                        cmd11.ExecuteNonQuery();
                        SqlCommand cmd12 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,System_Date_Time_Actual) values('" + PartyCode + "','" + SysTime + "','" + "Discount Account" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Discount Account,Inv No. " + invoiceNumber + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                        cmd12.ExecuteNonQuery();

                        SqlCommand cmd13 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount-Inv#" + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                        cmd13.ExecuteNonQuery();


                    }
                    //////////        //'--------------------------------Cash Received-------------------------------------------------------------------------------------------------------------------------------- 103= A/R,


                    if (Paid > 0)
                    {
                        SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();

                        decimal b = objModule1.MaxCRV(tran,con);
                        string CashAtOfficeGLCode = saveProductViewModel.TotalData.CashReceivedGLCode;
                        SqlCommand cmd14 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,Code1,CompID,System_Date_Time_Actual) values('" + CashAtOfficeGLCode + "','" + InvoiceDate + "','" + "Receivable-" + PartyName + ",Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + PartyName + ",Inv#" + invoiceNumber + "','" + CashAtOfficeGLCode + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                        cmd14.ExecuteNonQuery();

                        SqlCommand cmd15 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Code1,Narration,CompID,System_Date_Time_Actual) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Account-Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CashAtOfficeGLCode + "','" + "Cash Account-Inv#" + invoiceNumber + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                        cmd15.ExecuteNonQuery();

                        SqlCommand cmd16 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash - Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                        cmd16.ExecuteNonQuery();
                    }
                    //////////        //'--------------------------------CGS Entry-------------------------------------------------------------------------------------------------------------------------------- 103= A/R,

                    //CGS = logon.CompID & "04010100001"

                    //Inventory = logon.CompID & "01010600001"
                    string CGS = "0104010100001";
                    string Inventory = "0101010600001";
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    decimal TotalPurAmount = Convert.ToDecimal(saveProductViewModel.TotalData.TotalAverageCost);
                    if (TotalPurAmount > 0)
                    {
                        //////////AddedTime = AddedTime.AddSeconds(5);


                        SqlCommand cmd45 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,System_Date_Time_Actual) values('" + CGS + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "'," + TotalPurAmount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Inv#" + invoiceNumber + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                        cmd45.ExecuteNonQuery();

                        var CGSAmounts = saveProductViewModel.Products.Where(e => !string.IsNullOrWhiteSpace(e.RevenueCode)).GroupBy(c => c.CGSCode)
                       .Select(x => new GLModel()
                       {
                           Amount = x.Sum(o => Convert.ToDecimal(o.PurAmount)),
                           Code = x.First().CGSCode
                       }).ToList();

                        a = objModule1.MaxJVNo();
                        foreach (var CostGS in CGSAmounts)
                        {
                            if (CostGS.Amount > 0)
                            {
                                SqlCommand cmd44 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,System_Date_Time_Actual) values('" + CostGS.Code + "','" + InvoiceDate + "','" + "Inv#" + invoiceNumber + "'," + CostGS.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Inv#" + invoiceNumber + "','" + CompID + "','" + DateTime.Now.ToString("MM/dd/yyyy") + "')", con, tran);
                                cmd44.ExecuteNonQuery();
                            }

                        }
                        


                    }
                    //////////        //--------------------------------UpDate Party Balance-------------------------------------------------------

                    objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                    //if (NorBalance == 1)
                    //{

                    //    SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                    //    cmdUpDatePartyBalance.ExecuteNonQuery();
                    //    SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                    //    cmdUpDatePartyBalance2.ExecuteNonQuery();
                    //}
                    //else
                    //    if (NorBalance == 2)
                    //{
                    //    SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                    //    cmdUpDatePartyBalance.ExecuteNonQuery();
                    //    SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                    //    cmdUpDatePartyBalance2.ExecuteNonQuery();
                    //}


                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "New Sale Saved", LastInvoiceNumber = invoiceNumber.ToString() };
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Your session has been expired, login again and continue to Save this page.", LoginAgain = true };

                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}