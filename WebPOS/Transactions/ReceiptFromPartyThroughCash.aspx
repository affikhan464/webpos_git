﻿<%@ page title="" language="C#" masterpagefile="~/masterpage.Master" autoeventwireup="true" codebehind="ReceiptFromPartyThroughCash.aspx.cs" inherits="WebPOS.ReceiptFromPartyThroughCash" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Cash Receipt</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi">
                           
                             <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" autocomplete="off" type="text" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Voucher</span>
                            </label>
                        </span>
                        
                            <select id="DepartmentDD" name="DepartmentDD" class="DepartmentDD w-100">
                            </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="btnGet" onclick="EditVoucher()"><i class="fas fa-edit"></i> Edit</a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <span class="input input--hoshi">
                            <a class="printBtn btn-3 btn-bm btn-3e w-100" id="printBtn" onclick="EditVoucher()"><i class="fas fa-edit"></i> Print</a>
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
                        </span>
                    </div>
                   
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtCurrentDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyName" data-type="Party" data-id="txtPartyName" data-nextfocus=".CashPaid" data-function="GetParty" name="PartyName" class=" PartyBox PartyName autocomplete empt input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyCode" disabled="disabled" name="PartyCode" class="input__field input__field--hoshi PartyCode empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtAddress" class="input__field input__field--hoshi PartyAddress empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="Client_RadioButton" checked="checked" class="radio" name="PartiesRB" type="radio" />
                                <span></span>
                                Client
                            </label>
                        </span>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input id="Supplier_RadioButton" class="radio" name="PartiesRB" type="radio" />
                                <span></span>
                                Vender
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 d-flex">
                        <span class="input input--hoshi">

                            <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Cash Paid</span>
                            </label>
                        </span>
                        
                            <select id="CashReceivedCategoryDD" name="CashReceivedCategoryDD" data-glcode="01010101" class="CashReceivedCategoryDD w-100">
                            </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">

                            <input id="DisRs" class="input__field input__field--hoshi DisRs empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Discount Rs.</span>
                            </label>
                        </span>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">

                            <input id="OldBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Old Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>
                    
                </div>

                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                             
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>



  
    </asp:content>
<asp:content runat="server" id="content4" contentplaceholderid="Scripts">

    <%--<script src="/Script/Autocomplete.js"></script>--%>
    <script src="../Script/AutocompKhalid.js"></script>
    <script src="/Script/Dropdown.js"></script>
    <script src="/Script/Voucher/ReceiptFromPartyCash_Save.js"></script>

    <script type="text/javascript">
        $(document).on('input', '.CashPaid', function () {
            calculation();

        })
         $(document).on('click', '.printBtn', function () {
            window.open(
                
                '/Transactions/TransactionPrints/SaleNewItemCR.aspx?InvoiceNo='+ $(".InvoiceNumber").val() ,
                '_blank'
            );
        })

        $(document).ready(function () {
            $("#txtCashPaid").keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    calculation();
                    $("#txtNarration").focus();
                }
            });

        });
        $(document).ready(function () {
            appendAttribute("CashReceivedCategoryDD", "GLCode","0101010100001");
            appendAttribute("DepartmentDD", "Dept",1);

            $("#txtNarration").keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    calculation();
                    $("#btnSave").focus();
                }
            });

        });

        function EditVoucher() {
            var voucherId = $("#txtVoucherNo").val();
            window.location.href = "/Correction/ReceiptFromPartyThroughCashEdit.aspx?v=" + voucherId;
        }

        function calculation() {
            var NorBalance = 0;
         NorBalance = document.getElementById("Client_RadioButton").checked ? "1" : "2";

            var Balance = $(".PartyBalance").val();
            var Paid = $(".CashPaid").val();
            var DisRs = $(".DisRs").val();
            
            Balance = Number(Balance);
            Paid = Number(Paid) + Number(DisRs);

            var NewBalance = 0;


            if (Paid == "") { Paid = 0; }
            if (Paid > 0) {
                if (NorBalance == 1 && Balance > 0) {
                    NewBalance = (Balance - Paid);
                    $("#txtBalance").val(NewBalance);

                } else
                    if (NorBalance == 1 && Balance < 0) {
                        NewBalance = (Math.abs(Balance) + Paid);
                        $("#txtBalance").val((NewBalance * -1));
                    } else
                        if (NorBalance == 2 && Balance > 0) {
                            NewBalance = (Balance - Paid);
                            $("#txtBalance").val(NewBalance);
                        } else
                            if (NorBalance == 2 && Balance < 0) {
                                NewBalance = (Math.abs(Balance) - Paid);
                                $("#txtBalance").val((NewBalance * -1));
                            } else {
                                NewBalance = (Balance - Paid);
                                $("#txtBalance").val(NewBalance);
                            }

            } else
                if (Paid == 0) {
                    $("#txtBalance").val($("#OldBalance").val());
                }
        }

        function insertInToCashReceiptFromParty() {
            var norblnc = $(".selectedPartyNorBalance").val();
            if (norblnc == 1) {
                $("#Client_RadioButton").prop("checked", true)
            } else {
                $("#Supplier_RadioButton").prop("checked", true)
            }
            $(".PartyName").val($(".selectedPartyName").val());
            $(".PartyCode").val($(".selectedPartyCode").val());
            $(".PartyAddress").val($(".selectedPartyAddress").val());
            $(".PartyBalance").val($(".selectedPartyBalance").val());
            //$(".PartyBalance").val($(".selectedPartyBalance").val());           
            let balance = Number($(".selectedPartyBalance").val());
            if (norblnc == "1" && (balance > 0 || balance < 0)) {
                $(".PartyBalance").val(balance);
            }
            if (norblnc == "2" && (balance > 0 || balance < 0)) {
                $(".PartyBalance").val(-1*balance);
            }
             calculation();
        }

    </script>

</asp:content>
