﻿using System;
using System.Web.UI;
namespace WebPOS.Transactions
{
    public partial class _IMEList : UserControl
    {
        public string DescriptionType { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.DescriptionType = DescriptionType;
        }

    }

}