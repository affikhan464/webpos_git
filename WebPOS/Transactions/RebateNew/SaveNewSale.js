﻿function SaveBefore() {

    var SaveOk = "Yes";

    var PartyCode = $(".PartyCode").val();


    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    //if (SaveOk == "Yes") { save(); }

    if (SaveOk == "Yes") {
        swal({
            title: "Business Manager",
            text: "Are You Sure to made a transaction?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                    document.getElementById("PartySearchBox").focus();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });


    }
    
}



function save() {
    var partyName = $(".PartyName").val();
    var Code = $(".PartyCode").val();
    var invCode = $(".InvoiceNumber").val();

    if (partyName.trim() == "" && Code.trim() == "") {
        swal("Error", "Select Party", "error");

    } else if (invCode.trim() == "") {
        swal("Error", "Enter Invoice Number", "error");

    } else if (!$(".btnSave").hasClass("disableClick")) {

        $(".btnSave").addClass("disableClick");
        var Party_Data = {

            Name: $(".PartyName").val(),
            Code: $(".PartyCode").val(),
            Balance: $(".PartyBalance").val(),
            Address: $(".PartyAddress").val(),
            PhoneNumber: $(".PartyPhoneNumber").val(),
            Particular: $(".PartyParticular").val(),
            ManInv: $(".ManInv").val(),
            Date: $(".Date").val(),
            InvoiceNumber: $(".InvoiceNumber").val(),
            NorBalance: $(".PartyNorBalance").val(),           
            SalesManName: $("#salesManDD option:selected").text(),
            SalesManId: $("#salesManDD").val()


        }

        var Items = [];
        var ItemRows = $("#myTable tbody tr");
        for (var i = 0; i < ItemRows.length; i++) {

            var rno = $(ItemRows[i]).data().rownumber;

            var Item = {
                SNo: i + 1,
                Code: $(ItemRows[i]).find("." + rno + "_Code").val(),
                ItemName: $(ItemRows[i]).find("." + rno + "_ItemName").val(),
                Qty: $(ItemRows[i]).find("." + rno + "_Qty").val(),
                Rate: $(ItemRows[i]).find("." + rno + "_Rate").val(),
                Amount: $(ItemRows[i]).find("." + rno + "_Amount").val(),
                


            }
            Items.push(Item);

        }


        


        var Total_Data = {

            GrossTotal: Number($(".GrossTotal").val()),
            
            Balance: Number($(".Balance").val()),
           
            PreviousBalance: Number($(".PartyBalance").val()),

            
        }


        


        var saveProductViewModel = {
            TotalData: Total_Data,
            ClientData: Party_Data,
            Products: Items,
            ProductsOld: ItemsOld,
            IMEItems: IMEItems

        };
        calculationSale();
        $.ajax({
            url: "/Transactions/RebateNew/RebateNewItems.aspx/Save",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ saveProductViewModel: saveProductViewModel }),
            success: function (BaseModel) {
                debugger
                if (BaseModel.d.Success) {
                   
                    var date =$(".Date").val();
                    smallSwal("Success", BaseModel.d.Message, "success");
                    clearInvoice();
                    $(".InvoiceNumber").val(BaseModel.d.LastInvoiceNumber);
                    $(".Date").val(date);
                    $(".btnSave").removeClass("disableClick");
                    $(".PartyName").focus().select();

                }
                else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                    $(".btnSave").removeClass("disableClick");

                    swal({
                        title: "Login Again",
                        html:
                            BaseModel.d.Message + '</b>, ' +
                            `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here</a> `,

                        type: "warning"

                    });
                }
                else {
                    $(".btnSave").removeClass("disableClick");

                    swal("Error", BaseModel.d.Message, "error");
                }
            }
        });
    }
}

$(document).on('keydown',".Recieved", function (e) {
    //Ctrl + end
    if (e.which == 13) {
        SaveBefore();
        e.preventDefault();
    }
});




function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#IMEITable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();

    $(".clientInput").val("");
    $(".inv1").text("");
    $(".inv3").text("0");
    $(".inv1").val("");
    $(".inv3").val("0");

    calculationSale();
}