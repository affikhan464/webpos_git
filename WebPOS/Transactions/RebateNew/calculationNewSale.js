﻿

$(document).on("keypress", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();
    if (e.which == 13) {
        e.preventDefault();
        if (txbxName == "Qty") {
            $("#myTable ." + sno + "_Rate").focus();
            $("#myTable ." + sno + "_Rate").select();

        }
        else if (txbxName == "Rate") {
            $(".SearchBox").focus();
            $(".SearchBox").val("");
        }
        

        calculationSale();
       
    }

    else {
        return true;
    }
});
$(document).on("input", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();

    calculationSale();  
   
});

function calculationSale() {
    
    var tableRows = $("#myTable tbody tr");
    var rowCount = tableRows.length;

    var Total = 0;
    var TotQty = 0;
    var TotDealRs = 0;
    var TotItemDis = 0;
    var FlatDisPerRate = 0
    //var TotPurchase = 0;
    
    tableRows.each(function (index, element) {
        var sr = $(element).data().rownumber;
        var Qty = Number($(element).find("." + sr + "_Qty").val());
        var Rate = Number($(element).find("." + sr + "_Rate").val());
             
       var NetAmount = Number($(element).find("." + sr + "_NetAmount").val());
                
        Qty = Number(Qty);
        Rate = Number(Rate);

        NetAmount = Number(NetAmount);
          
        sno = sr;       
        Amount = (Qty * Rate).toFixed(2);

        $("#myTable ." + sno + "_Amount").val(Amount);
        
        Total = (Number(Total) + Number(Amount)).toFixed(2);
        TotQty = (Number(TotQty) + Number(Qty)).toFixed(2);
    
        Amount = 0;       
        Qty = 0;       
    });

   

    

    //------------------------Total Grid
    var sum;
    var inputs = ["Qty", 'Amount'];
    for (var i = 0; i < inputs.length; i++) {

        var currentInput = inputs[i];
        sum = 0;
        $("#myTable input[name = '" + currentInput + "']").each(function () {

            if (this.value.trim() === "") {
                sum = (Number(sum) + Number(0));
            } else {
                sum = (Number(sum) + Number(this.value));

            }

        });



        $("#mainTable [name=" + currentInput + "Total]").val('');
        $("#mainTable [name=" + currentInput + "Total]").val((sum).toFixed(2));


    }

 
    //----------------------End Total Grid-----------------        
    document.getElementById("txtTotal").value = Number(Total).toFixed(2);    
}
