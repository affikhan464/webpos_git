﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_LastSellingPrice.ascx.cs" Inherits="WebPOS.Transactions._LastSellingPrice" %>

<div class="modal fade" id="lastSellinPriceModal" tabindex="-1" role="dialog" aria-labelledby="lastSellinPriceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close position-absolute rt-10" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-0">
                <div id="generic_price_table" class="m-0 ">
                    <section>
                        <div class="row">


                            <div class="col-12">

                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">

                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix m-0 pb-3">

                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">

                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Last Selling Price</span>
                                            </div>
                                            <!--//HEAD END-->

                                        </div>
                                        <!--//HEAD CONTENT END-->

                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">
                                            <span class="price">
                                                <span class="sign itemName"></span>
                                            </span>
                                            <span class="price">
                                                <span class="sign">Rs.</span>
                                                <span class="currency lastSellingPrice"></span>
                                                <span class="cent"></span>
                                            </span>
                                        </div>
                                        <div class="row">
                                            <div class="col-4 text-center">
                                                <div class="square-box p-3">
                                                    <h5>Last Selling Date</h5>
                                                    <span class="sellingDate"></span>
                                                </div>
                                            </div>
                                            <div class="col-4 text-center">
                                                <div class="square-box p-3">
                                                    <h5>% Discount</h5>
                                                    <span><span class="PerDis"></span>%</span>
                                                </div>
                                            </div>
                                            <div class="col-4 text-center">
                                                <div class="square-box p-3">
                                                    <h5>Deal Discount</h5>
                                                    <span class="DealDis"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//PRICE END-->

                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list pb-2">
                                        <table class="w-100">
                                            <thead>
                                                <tr class="p-1">
                                                    <th>Date</th>
                                                    <th>Item Name</th>
                                                    <th>Rate</th>
                                                    <th>Per Dis.</th>
                                                    <th>Deal Dis.</th>
                                                </tr>
                                            </thead>
                                            <tbody class="soldItems">
                                               
                                            </tbody>

                                        </table>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <%-- <div class="generic_price_btn clearfix">
                                        <a class="" href="">Sign up</a>
                                    </div>--%>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="purchaseRateModal" tabindex="-1" role="dialog" aria-labelledby="lastSellinPriceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close position-absolute rt-10" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-0">
                <div id="generic_price_table"  class="m-0">
                    <section>
                        <div class="row">


                            <div class="col-12">

                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">

                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix m-0 pb-3">

                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">

                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Last Purchased</span>
                                            </div>
                                            <!--//HEAD END-->

                                        </div>
                                        <!--//HEAD CONTENT END-->

                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">
                                            <span class="price">
                                                <span class="sign itemName"></span>
                                            </span>
                                            <span class="price">
                                                <span class="sign">Rs.</span>
                                                <span class="currency lastSellingPrice"></span>
                                                <span class="cent"></span>
                                            </span>
                                        </div>
                                        <div class="row">
                                            <div class="col-4 text-center">
                                                <div class="square-box p-3">
                                                    <h5>Last Purchased Date</h5>
                                                    <span class="sellingDate"></span>
                                                </div>
                                            </div>
                                            <div class="col-4 text-center">
                                                <div class="square-box p-3">
                                                    <h5>Vendor Name</h5>
                                                    <span class="VendorName"></span>
                                                </div>
                                            </div>
                                            <div class="col-4 text-center">
                                                <div class="square-box p-3">
                                                    <h5>Brand Name</h5>
                                                    <span class="BrandName"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//PRICE END-->

                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list pb-2">
                                        <table class="w-100">
                                            <thead>
                                                <tr class="p-1">
                                                    <th>Date Purchased</th>
                                                    <th>Item Name</th>
                                                    <th>Vendor Name</th>
                                                    <th>Brand Name</th>
                                                    <th>Rate</th>
                                                </tr>
                                            </thead>
                                            <tbody class="soldItems">
                                               
                                            </tbody>

                                        </table>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <%-- <div class="generic_price_btn clearfix">
                                        <a class="" href="">Sign up</a>
                                    </div>--%>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
