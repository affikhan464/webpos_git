﻿<%@  Control Language="C#" AutoEventWireup="true" CodeBehind="_TransactionPrintHead.ascx.cs" Inherits="WebPOS.Registration._TransactionPrintHead" %>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    
    <div class="col-12 text-center">
        <strong class="InvoiceName font-size-25 p-2"> Sale Invoice </strong>
    </div>
</div>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    <div class="col-12 ">
        <strong>Sale Man:</strong> <span class="SaleManName"></span>
    </div>
    <div class="col-6 ">
        <strong>INVOICE NO:</strong> <span class="InvoiceNumber"></span>
    </div>
    <div class="col-6 text-right">
        <strong>DATE:</strong> <span class="Date"> </span>
    </div>
    <div class="col-6 ">
        <strong>CUSTOMER NAME:</strong> <span class="PartyName"></span>
    </div>
    <div class="col-6 text-right">
        <strong>PARTICULAR:</strong> <span class="Particular"></span>
    </div>
    <div class="col-6 "> 
        <strong>ADDRESS:</strong> <span class="PartyAddress"></span>
    </div>
    <div class="col-6 text-right">
        <strong>PRE. BAL:</strong> <span class="PreviousBalance"></span>
    </div>
</div>  