﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class TherMalSaleNewItemCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            ModGL objModGL = new ModGL();
            ModItem objModItem = new ModItem();
            ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName(); 
            var cr = new TherMalSaleNewItem();
            var InvoiceNo = Request.QueryString["InvoiceNo"];
           


            //SqlCommand cmd = new SqlCommand(@"select Code,
            //    Name,
            //    Balance,
            //    Address,
            //    Norbalance,Phone,Mobile
            //    from PartyCode order by name", con);



            SqlCommand cmd = new SqlCommand(@"Select Invoice1.MOB,Invoice1.MonthRec,Invoice1.Short,Invoice1.OperatorName as OPName,
                                                Invoice1.System_Date_Time ,Invoice1.Token,Invoice1.Particular,Invoice1.Date1 as DT , 
                                                Invoice1.PartyCode as PC,Invoice1.OperatorID , Invoice1.Name as PN,
                                                Invoice1.Particular , Invoice1.address as Address, Invoice1.Manual_InvNo , 
                                                Invoice1.SalesMan, Invoice1.Printed as Printed , Invoice1.Phone as Ph , 
                                                Invoice2.SN0 as SN , Invoice2.Code as CO,Invoice2.Description as Description,
                                                Invoice2.CTN as CTN ,Invoice2.Pcs as LosePcs, Invoice2.Qty as QT,Invoice2.Rate as Rate,
                                                Invoice2.Amount as AMT,Invoice2.Bonus,Invoice2.Item_Discount ,Invoice2.PerDiscount, 
                                                Invoice2.Purchase_Amount,Invoice2.Remarks as Rem,Invoice2.DealRs  as DealRs ,
                                                Invoice3.Total as Tot,Invoice3.CashPaid as Paid ,Invoice3.Discount1 as DST,
                                                Invoice3.Balance as Bal,Invoice3.TotalPcs as PCs, Invoice3.PrevBalance as PB ,
                                                Invoice3.NetGSTPayable , isnull(Invoice3.GST_Percentage,0) as GST_Percentage, isnull(Invoice3.Change,0) as PaidBac,
                                                InvCode.MSP as MSP,Invoice3.BillTotal ,Invoice3.FlatDiscount
                                                from 
                                                    (Invoice1 INNER JOIN Invoice3 ON Invoice1.InvNo = Invoice3.InvNo) INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo  inner join invcode on invoice2.Code=InvCode.code 
                                                where  
                                                    Invoice1.CompID='" + CompID + "' and Invoice1.InvNo=" +  InvoiceNo + " order by Invoice2.SN0", con);




            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed)
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
            }

            dAdapter.Fill(dt);
            
            con.Close();

            var dset = new DataSet();
            var CompanyName = objModGL.CompanyName();
            var CompanyAddress1 = objModGL.CompanyAddress1();
            var CompanyAddress2 = objModGL.CompanyAddress2();
            var CompanyAddress3 = objModGL.CompanyAddress3();
            var CompanyAddress4 = objModGL.CompanyAddress4();
            var CompanyPhone = objModGL.CompanyPhone();
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                
                string Description = Convert.ToString(dt.Rows[i]["Description"]);
                var Qty = Convert.ToInt32 (dt.Rows[i]["QT"]);
                string Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                decimal Amount = Convert.ToDecimal(dt.Rows[i]["AMT"]);
                decimal Item_Discount = Convert.ToDecimal(dt.Rows[i]["Item_Discount"]);
                decimal DealRs = Convert.ToDecimal(dt.Rows[i]["DealRs"]);
                string SalesMan = Convert.ToString(dt.Rows[i]["SalesMan"]);
                string PN = Convert.ToString(dt.Rows[i]["PN"]);
                string Address = Convert.ToString(dt.Rows[i]["Address"]);
                string Date1 = Convert.ToDateTime(dt.Rows[i]["DT"]).ToString("dd-MMM-yyyy") ;
                var STime = Convert.ToDateTime(dt.Rows[i]["System_Date_Time"]).ToString("HH:mm:ss");
                decimal PreviousBalance = Convert.ToDecimal(dt.Rows[i]["PB"]);
                string Particular = Convert.ToString(dt.Rows[i]["Particular"]);
                decimal GrandTotal = Convert.ToDecimal(dt.Rows[i]["Tot"]);
                decimal GST_Percentage = Convert.ToDecimal(dt.Rows[i]["GST_Percentage"]);
                decimal Paid = Convert.ToDecimal(dt.Rows[i]["Paid"]);
                decimal FlatDiscount = Convert.ToDecimal(dt.Rows[i]["FlatDiscount"]);
                decimal Discount = Convert.ToDecimal(dt.Rows[i]["DST"]);
                decimal BillTotal = Convert.ToDecimal(dt.Rows[i]["BillTotal"]);
                decimal PaidBack = Convert.ToDecimal(dt.Rows[i]["PaidBac"]);
                decimal Balance =objParty.PartyBalance(Convert.ToString(dt.Rows[i]["PC"]));
                if (Convert.ToString(dt.Rows[i]["PC"]) == "0101010300000")
                {
                    PreviousBalance = 0;
                    Balance = 0;
                }


                decimal NetGSTPayable = Convert.ToDecimal(dt.Rows[i]["NetGSTPayable"]);
                

                var invoice = new
                {

                    SalesMan = SalesMan,
                    InvNoString = Convert.ToString(InvoiceNo),
                    Name= PN,
                    Address= Address,
                    Date1= Date1 + " " + STime,
                    PreviousBalance= PreviousBalance,
                    Particular= Particular,
                    SN0 = i + 1,
                    Description= Description,
                    Qty= Qty,
                    Rate= Rate,
                    Amount=Amount,
                    Item_Discount= Item_Discount,
                    DealRs= DealRs,


                    Total= GrandTotal,
                    GST_Percentage = GST_Percentage,
                    CashPaid = Paid,
                    FlatDiscount= FlatDiscount,
                    BillTotal= BillTotal,
                    Discount= Discount,
                    PaidBack= PaidBack,
                    Balance= Balance,
                    NetGSTPayable= NetGSTPayable,

                    Company = CompanyName,
                    Address1 = CompanyAddress1,
                    Address2 = CompanyAddress2,
                    Address3 = CompanyAddress3,
                    Address4 = CompanyAddress4,
                    Phone = CompanyPhone,
                    VoucherHeading = "Sale",
                  
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);



            System.Drawing.Printing.PrintDocument localPrinter = new PrintDocument();
            cr.PrintOptions.PrinterName = localPrinter.PrinterSettings.PrinterName;
            //cr.PrintToPrinter(1, false, 0, 0);


            //var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            ////if (InstalledPrinters > 4)
            ////var a= cr.PrintOptions.PrinterName.ToString();
            ////  cr.PrintToPrinter(1, false, 0, 0);
            ////else
            //cr.PrintToPrinter(1, false, 0, 0);
            //cr.PrintOptions.PrinterName = "";
            CRViewer.ReportSource = cr;

        }
        
    }
}