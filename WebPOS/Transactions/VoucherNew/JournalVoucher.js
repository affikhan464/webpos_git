﻿function insertJournalVoucherRow() {
    var rowNumber = isNaN(Number($(".debit-table .item-list.data-list .item-data:last-child").data("rownumber"))) ? 1 : Number($(".debit-table .item-list.data-list .item-data:last-child").data("rownumber")) + 1;
    var glCode = $(".selectedAllVouchersCode").val();
    var glTitle = $(".selectedAllVouchersTitle").val();
    var row = `<li class="item item-data" data-rownumber='${rowNumber}'>
        <div class="item-row  pl-3 pr-2">
            <div class="item-col flex-4">
                <div class="item-heading">Code</div>
                <div>
                    ${glCode} 
                    <input type="hidden" name="Code" value="${glCode}">
                </div>
            </div>
            <div class="item-col flex-5 no-overflow">
                <div>
                    <a class="">
                        <h4 class="item-title no-wrap">
                            ${glTitle}  
                            <input type="hidden" name="Description" value="${glTitle}">
                        </h4>
                    </a>
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                   <input  type='text' class="form-control" name='Narration' id='narrationTextbox_${rowNumber}'/>
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                   <input name='Debit' type='number'   class="text-right px-2 form-control" id='debitTextbox_${rowNumber}'  />
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                   <input name='Credit' type='number'   class="text-right px-2 form-control" id='creditTextbox_${rowNumber}'  />
                </div>
            </div>

            <div class="item-col item-col-date flex-3">
                <div>
                    <i onclick="deleteItem(this)" class="fas fa-times mr-3"></i>
                </div>
            </div>
        </div>
    </li>`;
    $(".debit-table .item-list.data-list").append(row);
    
    
    $("#narrationTextbox_" + rowNumber + "").focus();
    $("#narrationTextbox_" + rowNumber + "").select();


}
function deleteItem(btn) {
    var row = $(btn).parents(".item-data");
    row.remove();
    sum("Debit");
}
$(document).on("keydown", "[name='Narration']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13) {
        $("#debitTextbox_" + row + "").focus();
        $("#debitTextbox_" + row + "").select();
    }

    
});

$(document).on("keydown", "[name='Debit']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() == "")
        $("#creditTextbox_" + row + "").focus();
    else if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() != "" && $("#creditTextbox_" + row + "").val().trim() != "") {
        $("#creditTextbox_" + row + "").focus();
        $("#creditTextbox_" + row + "").select();
    }
    else if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() != "")
    {
        $(".AllVouchersTitle").focus();
        $(".AllVouchersTitle").select();
    }

    sum("Debit");
    sum("Credit");

});
$(document).on("input", "[name='Debit']", function (e) {

    sum("Debit");

});
$(document).on("keydown", "[name='Credit']", function (e) {
 
    if (e.keyCode == 13) {
        $(".AllVouchersTitle").focus();
        $(".AllVouchersTitle").select();
    }
    sum("Credit");

})
$(document).on("input", "[name='Credit']", function () {
    
    sum("Credit");

})
function assignSerialNumber() {
    var serialNumbers = $(".serialNumber");

    serialNumbers.each(function (index, element) {
        $(element).text(index + 1);
    })

}
function sum(name) {

    var sumValue = 0;
    var values = $("[name=" + name + "]");
    values.each(function (index, element) {
        var value = $(element).val().trim() == "" ? 0 : $(element).val();
        sumValue += Number(value);
    });

    $("[name=total" + name + "]").val(sumValue);


}

function save() {

    if ( $("[name=totalCredit]").val() == $("[name=totalDebit]").val()) {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    resolve();
                    saveVoucher();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

    }
    else {
        swal("Debit amount is not equal to credit amount", "", "error");
    }
}
function saveVoucher() {
    var serialNumbers = $(".debit-table .item-list.data-list .item-data");

    var addedRows = [];
    serialNumbers.each(function (index, element) {

        var row = $(element);
      
        var rowmodel = {
            Code: row.find("[name=Code]").val(),
            Title: row.find("[name=Description]").val(),
            Narration: row.find("[name=Narration]").val(),
            Debit: row.find("[name=Debit]").val().trim() == "" ? "0" : row.find("[name=Debit]").val(),
            Credit: row.find("[name=Credit]").val().trim() == "" ? "0" : row.find("[name=Credit]").val(),
        }
        addedRows.push(rowmodel);
    });
    var VoucherDate = $(".VoucherDate").val();
    var voucherModel = {
        Rows: addedRows,
        VoucherDate: VoucherDate,
    };
    $.ajax({
        url: "/Transactions/VoucherNew/JournalVoucher.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: voucherModel }),
        success: function (response) {
           
            if (response.d.Success) {
                $(".VoucherNumber").val(response.d.LastVoucherNumber);
                swal("Success", response.d.Message, "success");
                emptyVoucher();
                updateInputStyle();
            }
            else {
                swal("Error", response.d.Message, "error");

            }


        }



    })
}
$(document).on("click", "#saveBtn", function () {
    save();
});
$(document).on("keydown",  function (e) {

   
    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }
   

})
$(document).on("click", ".reports .fa-times", function () {
    var row = $(this).parents(".itemRow");
    row.remove();


    sum("Credit");
    sum("Debit");
    assignSerialNumber();
});
function emptyVoucher() {
    $(".debit-table .item-list.data-list  .item-data").remove();
    $(".credit-table input").val("");
    sum("Credit");
    sum("Debit");
}
function EditVoucher() {
    var voucherId = $(".VoucherNumber").val();
    window.location.href = "/Correction/VoucherEdit/JournalVoucherEdit.aspx?v=" + voucherId;
}