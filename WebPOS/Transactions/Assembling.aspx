﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Assembling.aspx.cs" Inherits="WebPOS.Transactions.Assembling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <div class="reports searchAppSection">
				<div class="contentContainer">
				 <h2 class="text-align-center"><i class=" fas fa-file-alt" aria-hidden="true"></i> Assembling</h2>
                    <p class="mb-2">
                        Main Item
                    </p>
					<div class="BMSearchWrapper clearfix row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                       
                            <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Code</span>
                                <input type="text" id="Code" class="MainItemCode" disabled="disabled"/>
							</div>
							
                            <div  class="col col-12 col-sm-6 col-md-6">
                                 <input type="text" id="SearchBox" data-id="Stocks" data-type="MainItem" data-function="GetItems"  class="autocomplete AllVouchersTitle descritionTxbx MainItemDescription" placeholder="Enter Description" />
                            </div>
                      
                        
                        <div class="col col-12 col-sm-3 col-md-3">
                            <a href="#" id="saveBtn" class="btn btn-bm"><i class="fas fa-save"></i>  Save</a>
						</div>
                       
					</div>
                    <p class="mt-2 mb-2">
                        Ingredients
                    </p>
                    <div class="BMSearchWrapper clearfix row Ingredients">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                       
                            <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Code</span>
                                <input type="text" class="IngredientCode"/>
							</div>
							
                            <div  class="col col-12 col-sm-6 col-md-3">
                                 <input type="text" data-id="IngredientTxbx" data-type="Ingredient" data-function="GetItems"  class="autocomplete descritionTxbx IngredientDescription" placeholder="Enter Description" />
                            </div>
                       
                         <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Qty</span>
                                <input type="text" class="IngredientQty" />
							</div>
                       <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Avg. Cost</span>
                                <input type="text" class="IngredientRate"/>
							</div>
                       
					</div><!-- searchAppSection -->
				</div>
			</div>
    <div class="container-fluid">
    <div class="BMtable">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                  
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
											Serial No.
										</a>
									</div>
									<div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    <div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
                                    <div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
											Cost
										</a>
									</div>
								
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
										    Amount
										</a>
									</div>
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow'>
								<div class='sixteen'><span></span></div>
								<div class='sixteen'><span></span></div>
								<div class='sixteen'><span></span></div>
								<div class='sixteen'><input type="number" disabled="disabled" value="0" name='totalQty'/></div>
								<div class='sixteen'><input type="number" disabled="disabled" value="0" name='totalRate'/></div>
								<div class='sixteen'><input type="number" disabled="disabled" value="0" name='totalAmount'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
        </div>
      </asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Script/Autocomplete.js"></script>
    <script src="/Script/Assembling.js"></script>
        <script type="text/javascript">

       var counter = 0;
       
       $(document).ready(function () {
          
                resizeTable();
           
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
          
         </script>
</asp:Content>
