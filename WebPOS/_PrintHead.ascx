﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_PrintHead.ascx.cs" Inherits="WebPOS._PrintHead" %>
<div class="row mt-4 justify-content-center">
    <div class="col-12 text-center">

        <h1 class="CompanyName"><%= ConfigurationManager.AppSettings["CompanyName"] %></h1>
    </div>
    <div class="col-6 text-center">

        <address class="m-0">
            <p class="PhoneNumber">Phone #: </p>
        </address>
    </div>
</div>
