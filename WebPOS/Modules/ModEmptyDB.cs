﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class ModEmptyDB
    {

        Module7 objModule7 = new Module7();
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        //SqlConnection con = new SqlConnection(connstr);


        public void EmptyDBAlll(SqlConnection con, SqlTransaction tran)

        {
            //SqlCommand cmdDelInv1111 = new SqlCommand("Delete  from InventoryOpeningBalance", con, tran);
            //cmdDelInv1111.CommandTimeout = 10000000; //in seconds
            //cmdDelInv1111.ExecuteNonQuery();


            //----------------------------------Opening Set Zero Start
            SqlCommand cmdDelOpe1 = new SqlCommand("Update InventoryOpeningBalance set Opening=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe1.CommandTimeout = 10000000; //in seconds
            cmdDelOpe1.ExecuteNonQuery();
            SqlCommand cmdDelOpe2 = new SqlCommand("Update InventoryOpeningBalance set Cost_Per_Unit=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe2.CommandTimeout = 10000000; //in seconds
            cmdDelOpe2.ExecuteNonQuery();
            SqlCommand cmdDelOpe3 = new SqlCommand("Update InventoryOpeningBalance set Total_Value=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe3.CommandTimeout = 10000000; //in seconds
            cmdDelOpe3.ExecuteNonQuery();
            SqlCommand cmdDelOpe4 = new SqlCommand("Update InventoryOpeningBalance set Balance_Qty=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe4.CommandTimeout = 10000000; //in seconds
            cmdDelOpe4.ExecuteNonQuery();
            SqlCommand cmdDelOpe5 = new SqlCommand("Update InventoryOpeningBalance set Sold_Qty=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe5.CommandTimeout = 10000000; //in seconds
            cmdDelOpe5.ExecuteNonQuery();
            SqlCommand cmdDelOpe6 = new SqlCommand("Update InvCode set Qty=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe6.CommandTimeout = 10000000; //in seconds
            cmdDelOpe6.ExecuteNonQuery();
            SqlCommand cmdDelOpe7 = new SqlCommand("Update InvCode set WorkShopQty=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe7.CommandTimeout = 10000000; //in seconds
            cmdDelOpe7.ExecuteNonQuery();
            SqlCommand cmdDelOpe8 = new SqlCommand("Update InvCode set WarrantyQtyVender=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe8.CommandTimeout = 10000000; //in seconds
            cmdDelOpe8.ExecuteNonQuery();
            SqlCommand cmdDelOpe9 = new SqlCommand("Update InvCode set WarrantyQtyClient=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe9.CommandTimeout = 10000000; //in seconds
            cmdDelOpe9.ExecuteNonQuery();
            SqlCommand cmdDelOpe10 = new SqlCommand("Update InvCode set Rebate=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe10.CommandTimeout = 10000000; //in seconds
            cmdDelOpe10.ExecuteNonQuery();
            SqlCommand cmdDelOpe11 = new SqlCommand("Update GLCode set Balance=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe11.CommandTimeout = 10000000; //in seconds
            cmdDelOpe11.ExecuteNonQuery();
            SqlCommand cmdDelOpe12 = new SqlCommand("Update GLOpeningBalance set OpeningBal=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe12.CommandTimeout = 10000000; //in seconds
            cmdDelOpe12.ExecuteNonQuery();
            SqlCommand cmdDelOpe13 = new SqlCommand("Update PartyOpBalance set OpBalance=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe13.CommandTimeout = 10000000; //in seconds
            cmdDelOpe13.ExecuteNonQuery();
            SqlCommand cmdDelOpe14 = new SqlCommand("Update PartyOpBalance set OpBalance1=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe14.CommandTimeout = 10000000; //in seconds
            cmdDelOpe14.ExecuteNonQuery();
            SqlCommand cmdDelOpe15 = new SqlCommand("Update PartyCode set Balance=0 where CompID='" + CompID + "'", con, tran);
            cmdDelOpe15.CommandTimeout = 10000000; //in seconds
            cmdDelOpe15.ExecuteNonQuery();

            //----------------------------------Opening Set Zero End



            SqlCommand cmdDel1 = new SqlCommand("Delete  from LP_Sending where compid = '" + CompID + "'", con, tran);
            cmdDel1.CommandTimeout = 10000000; //in seconds
            cmdDel1.ExecuteNonQuery();
            SqlCommand cmdDel2 = new SqlCommand("Delete  from LP_Receiving where CompID='" + CompID + "'", con, tran);
            cmdDel2.CommandTimeout = 10000000; //in seconds
            cmdDel2.ExecuteNonQuery();
            SqlCommand cmdDel3 = new SqlCommand("Delete  from CashReceived where CompID='" + CompID + "'", con, tran);
            cmdDel3.CommandTimeout = 10000000; //in seconds
            cmdDel3.ExecuteNonQuery();

            SqlCommand cmdDel4 = new SqlCommand("Delete  from Messages where CompID='" + CompID + "'", con, tran);
            cmdDel4.CommandTimeout = 10000000; //in seconds
            cmdDel4.ExecuteNonQuery();
            SqlCommand cmdDel5 = new SqlCommand("Delete  from StockMovReceiving_Detail where CompID='" + CompID + "'", con, tran);
            cmdDel5.CommandTimeout = 10000000; //in seconds
            cmdDel5.ExecuteNonQuery();
            SqlCommand cmdDel6 = new SqlCommand("Delete  from StockMovReceiving_Master where CompID='" + CompID + "'", con, tran);
            cmdDel6.CommandTimeout = 10000000; //in seconds
            cmdDel6.ExecuteNonQuery();
            SqlCommand cmdDel7 = new SqlCommand("Delete  from StockMovIssue_Detail where CompID='" + CompID + "'", con, tran);
            cmdDel7.CommandTimeout = 10000000; //in seconds
            cmdDel7.ExecuteNonQuery();
            SqlCommand cmdDel8 = new SqlCommand("Delete  from StockMovIssue_Master where CompID='" + CompID + "'", con, tran);
            cmdDel8.CommandTimeout = 10000000; //in seconds
            cmdDel8.ExecuteNonQuery();
            SqlCommand cmdDel9 = new SqlCommand("Delete  from LoadForm where CompID='" + CompID + "'", con, tran);
            cmdDel9.CommandTimeout = 10000000; //in seconds
            cmdDel9.ExecuteNonQuery();
            SqlCommand cmdDel10 = new SqlCommand("Delete  from Estimate1 where CompID='" + CompID + "'", con, tran);
            cmdDel10.CommandTimeout = 10000000; //in seconds
            cmdDel10.ExecuteNonQuery();
            SqlCommand cmdDel11 = new SqlCommand("Delete  from Estimate2 where CompID='" + CompID + "'", con, tran);
            cmdDel11.CommandTimeout = 10000000; //in seconds
            cmdDel11.ExecuteNonQuery();
            SqlCommand cmdDel12 = new SqlCommand("Delete  from Estimate3 where CompID='" + CompID + "'", con, tran);
            cmdDel12.CommandTimeout = 10000000; //in seconds
            cmdDel12.ExecuteNonQuery();
            SqlCommand cmdDel13 = new SqlCommand("Delete  from ProcessDept_Detail where CompID='" + CompID + "'", con, tran);
            cmdDel13.CommandTimeout = 10000000; //in seconds
            cmdDel13.ExecuteNonQuery();
            SqlCommand cmdDel14 = new SqlCommand("Delete  from ProcessDept_Master where CompID='" + CompID + "'", con, tran);
            cmdDel14.CommandTimeout = 10000000; //in seconds
            cmdDel14.ExecuteNonQuery();
            SqlCommand cmdDel15 = new SqlCommand("Delete  from Invoice1Edit where CompID='" + CompID + "'", con, tran);
            cmdDel15.CommandTimeout = 10000000; //in seconds
            cmdDel15.ExecuteNonQuery();
            SqlCommand cmdDel16 = new SqlCommand("Delete  from Invoice2Edit where CompID='" + CompID + "'", con, tran);
            cmdDel16.CommandTimeout = 10000000; //in seconds
            cmdDel16.ExecuteNonQuery();
            SqlCommand cmdDel17 = new SqlCommand("Delete  from Invoice3Edit where CompID='" + CompID + "'", con, tran);
            cmdDel17.CommandTimeout = 10000000; //in seconds
            cmdDel17.ExecuteNonQuery();
            SqlCommand cmdDel18 = new SqlCommand("Delete  from Reminder where CompID='" + CompID + "'", con, tran);
            cmdDel18.CommandTimeout = 10000000; //in seconds
            cmdDel18.ExecuteNonQuery();
            SqlCommand cmdDel19 = new SqlCommand("Update InvCode set Ave_Cost=0 where CompID='" + CompID + "'", con, tran);
            cmdDel19.CommandTimeout = 10000000; //in seconds
            cmdDel19.ExecuteNonQuery();
            SqlCommand cmdDel20 = new SqlCommand("Delete  from StockAdjustment_Detail where CompID='" + CompID + "'", con, tran);
            cmdDel20.CommandTimeout = 10000000; //in seconds
            cmdDel20.ExecuteNonQuery();
            SqlCommand cmdDel21 = new SqlCommand("Delete  from StockAdjustment_Master where CompID='" + CompID + "'", con, tran);
            cmdDel21.CommandTimeout = 10000000; //in seconds
            cmdDel21.ExecuteNonQuery();
            SqlCommand cmdDel22 = new SqlCommand("Delete  from InvoiceSavedDetail where CompID='" + CompID + "'", con, tran);
            cmdDel22.CommandTimeout = 10000000; //in seconds
            cmdDel22.ExecuteNonQuery();
            SqlCommand cmdDel23 = new SqlCommand("Delete  from InvoiceSavedMaster where CompID='" + CompID + "'", con, tran);
            cmdDel23.CommandTimeout = 10000000; //in seconds
            cmdDel23.ExecuteNonQuery();

            SqlCommand cmdDel25 = new SqlCommand("Delete  from InventorySerialNoFinal where CompID='" + CompID + "'", con, tran);
            cmdDel25.CommandTimeout = 10000000; //in seconds
            cmdDel25.ExecuteNonQuery();
            SqlCommand cmdDel26 = new SqlCommand("Delete  from DeleteValues where CompID='" + CompID + "'", con, tran);
            cmdDel26.CommandTimeout = 10000000; //in seconds
            cmdDel26.ExecuteNonQuery();
            SqlCommand cmdDel27 = new SqlCommand("Delete  from FreeSample_Detail where CompID='" + CompID + "'", con, tran);
            cmdDel27.CommandTimeout = 10000000; //in seconds
            cmdDel27.ExecuteNonQuery();
            SqlCommand cmdDel28 = new SqlCommand("Delete  from FreeSample_Master where CompID='" + CompID + "'", con, tran);
            cmdDel28.CommandTimeout = 10000000; //in seconds
            cmdDel28.ExecuteNonQuery();
            SqlCommand cmdDel29 = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "'", con, tran);
            cmdDel29.CommandTimeout = 10000000; //in seconds
            cmdDel29.ExecuteNonQuery();
            SqlCommand cmdDel30 = new SqlCommand("Delete  from Inventory where CompID='" + CompID + "'", con, tran);
            cmdDel30.CommandTimeout = 10000000; //in seconds
            cmdDel30.ExecuteNonQuery();
            SqlCommand cmdDel31 = new SqlCommand("Delete  from InventoryLost1 where CompID='" + CompID + "'", con, tran);
            cmdDel31.CommandTimeout = 10000000; //in seconds
            cmdDel31.ExecuteNonQuery();
            SqlCommand cmdDel32 = new SqlCommand("Delete  from InventoryLost2 where CompID='" + CompID + "'", con, tran);
            cmdDel32.CommandTimeout = 10000000; //in seconds
            cmdDel32.ExecuteNonQuery();
            SqlCommand cmdDel33 = new SqlCommand("Delete  from InventorySerialNo where CompID='" + CompID + "'", con, tran);
            cmdDel33.CommandTimeout = 10000000; //in seconds
            cmdDel33.ExecuteNonQuery();
            SqlCommand cmdDel34 = new SqlCommand("Delete  from InventorySerialNoPurchaseReturn where CompID='" + CompID + "'", con, tran);
            cmdDel34.CommandTimeout = 10000000; //in seconds
            cmdDel34.ExecuteNonQuery();
            SqlCommand cmdDel35 = new SqlCommand("Delete  from InventorySerialNoSaleReturn where CompID='" + CompID + "'", con, tran);
            cmdDel35.CommandTimeout = 10000000; //in seconds
            cmdDel35.ExecuteNonQuery();
            SqlCommand cmdDel36 = new SqlCommand("Delete  from InventorySerialNoTemp where CompID='" + CompID + "'", con, tran);
            cmdDel36.CommandTimeout = 10000000; //in seconds
            cmdDel36.ExecuteNonQuery();
            SqlCommand cmdDel37 = new SqlCommand("Delete  from InventorySerialNoWarrantyIn where CompID='" + CompID + "'", con, tran);
            cmdDel37.CommandTimeout = 10000000; //in seconds
            cmdDel37.ExecuteNonQuery();
            SqlCommand cmdDel38 = new SqlCommand("Delete  from InventorySerialNoWarrantyOut where CompID='" + CompID + "'", con, tran);
            cmdDel38.CommandTimeout = 10000000; //in seconds
            cmdDel38.ExecuteNonQuery();
            SqlCommand cmdDel39 = new SqlCommand("Delete  from InventorySerialNoWarrantyTemp where CompID='" + CompID + "'", con, tran);
            cmdDel39.CommandTimeout = 10000000; //in seconds
            cmdDel39.ExecuteNonQuery();
            SqlCommand cmdDel40 = new SqlCommand("Delete  from invoice1 where CompID='" + CompID + "'", con, tran);
            cmdDel40.CommandTimeout = 10000000; //in seconds
            cmdDel40.ExecuteNonQuery();
            SqlCommand cmdDel41 = new SqlCommand("Delete  from invoice2 where CompID='" + CompID + "'", con, tran);
            cmdDel41.CommandTimeout = 10000000; //in seconds
            cmdDel41.ExecuteNonQuery();
            SqlCommand cmdDel42 = new SqlCommand("Delete  from invoice3 where CompID='" + CompID + "'", con, tran);
            cmdDel42.CommandTimeout = 10000000; //in seconds
            cmdDel42.ExecuteNonQuery();
            SqlCommand cmdDel43 = new SqlCommand("Delete  from PartiesLedger where CompID='" + CompID + "'", con, tran);
            cmdDel43.CommandTimeout = 10000000; //in seconds
            cmdDel43.ExecuteNonQuery();
            SqlCommand cmdDel44 = new SqlCommand("Delete  from WorkShopSerialNoTemp where CompID='" + CompID + "'", con, tran);
            cmdDel44.CommandTimeout = 10000000; //in seconds
            cmdDel44.ExecuteNonQuery();
            SqlCommand cmdDel45 = new SqlCommand("Delete  from Purchase1 where CompID='" + CompID + "'", con, tran);
            cmdDel45.CommandTimeout = 10000000; //in seconds
            cmdDel45.ExecuteNonQuery();
            SqlCommand cmdDel46 = new SqlCommand("Delete  from Purchase2 where CompID='" + CompID + "'", con, tran);
            cmdDel46.CommandTimeout = 10000000; //in seconds
            cmdDel46.ExecuteNonQuery();
            SqlCommand cmdDel47 = new SqlCommand("Delete  from Purchase3 where CompID='" + CompID + "'", con, tran);
            cmdDel47.CommandTimeout = 10000000; //in seconds
            cmdDel47.ExecuteNonQuery();
            SqlCommand cmdDel48 = new SqlCommand("Delete  from PurchaseReturn1 where CompID='" + CompID + "'", con, tran);
            cmdDel48.CommandTimeout = 10000000; //in seconds
            cmdDel48.ExecuteNonQuery();
            SqlCommand cmdDel49 = new SqlCommand("Delete  from PurchaseReturn2 where CompID='" + CompID + "'", con, tran);
            cmdDel49.CommandTimeout = 10000000; //in seconds
            cmdDel49.ExecuteNonQuery();
            SqlCommand cmdDel50 = new SqlCommand("Delete  from PurchaseReturnSerialNo where CompID='" + CompID + "'", con, tran);
            cmdDel50.CommandTimeout = 10000000; //in seconds
            cmdDel50.ExecuteNonQuery();
            SqlCommand cmdDel51 = new SqlCommand("Delete  from Rebate1 where CompID='" + CompID + "'", con, tran);
            cmdDel51.CommandTimeout = 10000000; //in seconds
            cmdDel51.ExecuteNonQuery();
            SqlCommand cmdDel52 = new SqlCommand("Delete  from Rebate2 where CompID='" + CompID + "'", con, tran);
            cmdDel52.CommandTimeout = 10000000; //in seconds
            cmdDel52.ExecuteNonQuery();
            SqlCommand cmdDel53 = new SqlCommand("Delete  from SaleReturn1 where CompID='" + CompID + "'", con, tran);
            cmdDel53.CommandTimeout = 10000000; //in seconds
            cmdDel53.ExecuteNonQuery();
            SqlCommand cmdDel54 = new SqlCommand("Delete  from SaleReturn2 where CompID='" + CompID + "'", con, tran);
            cmdDel54.CommandTimeout = 10000000; //in seconds
            cmdDel54.ExecuteNonQuery();
            SqlCommand cmdDel55 = new SqlCommand("Delete  from SaleReturnSerialNo where CompID='" + CompID + "'", con, tran);
            cmdDel55.CommandTimeout = 10000000; //in seconds
            cmdDel55.ExecuteNonQuery();
            SqlCommand cmdDel56 = new SqlCommand("Delete  from Warranty1 where CompID='" + CompID + "'", con, tran);
            cmdDel56.CommandTimeout = 10000000; //in seconds
            cmdDel56.ExecuteNonQuery();
            SqlCommand cmdDel57 = new SqlCommand("Delete  from Warranty2 where CompID='" + CompID + "'", con, tran);
            cmdDel57.CommandTimeout = 10000000; //in seconds
            cmdDel57.ExecuteNonQuery();
            SqlCommand cmdDel58 = new SqlCommand("Delete  from WarrantyLedger where CompID='" + CompID + "'", con, tran);
            cmdDel58.CommandTimeout = 10000000; //in seconds
            cmdDel58.ExecuteNonQuery();
            SqlCommand cmdDel59 = new SqlCommand("Delete  from WarrantySerialNo where CompID='" + CompID + "'", con, tran);
            cmdDel59.CommandTimeout = 10000000; //in seconds
            cmdDel59.ExecuteNonQuery();
            SqlCommand cmdDel60 = new SqlCommand("Delete  from WarrintySlip where CompID='" + CompID + "'", con, tran);
            cmdDel60.CommandTimeout = 10000000; //in seconds
            cmdDel60.ExecuteNonQuery();
            SqlCommand cmdDel61 = new SqlCommand("Delete  from WorkShop1 where CompID='" + CompID + "'", con, tran);
            cmdDel61.CommandTimeout = 10000000; //in seconds
            cmdDel61.ExecuteNonQuery();
            SqlCommand cmdDel62 = new SqlCommand("Delete  from WorkShop2 where CompID='" + CompID + "'", con, tran);
            cmdDel62.CommandTimeout = 10000000; //in seconds
            cmdDel62.ExecuteNonQuery();
            SqlCommand cmdDel63 = new SqlCommand("Delete  from WorkshopInvoice1 where CompID='" + CompID + "'", con, tran);
            cmdDel63.CommandTimeout = 10000000; //in seconds
            cmdDel63.ExecuteNonQuery();
            SqlCommand cmdDel64 = new SqlCommand("Delete  from WorkshopInvoice2 where CompID='" + CompID + "'", con, tran);
            cmdDel64.CommandTimeout = 10000000; //in seconds
            cmdDel64.ExecuteNonQuery();
            SqlCommand cmdDel65 = new SqlCommand("Delete  from WorkShopLedger where CompID='" + CompID + "'", con, tran);
            cmdDel65.CommandTimeout = 10000000; //in seconds
            cmdDel65.ExecuteNonQuery();
            SqlCommand cmdDel66 = new SqlCommand("Delete  from CashIncentive where CompID='" + CompID + "'", con, tran);
            cmdDel66.CommandTimeout = 10000000; //in seconds
            cmdDel66.ExecuteNonQuery();
            SqlCommand cmdDel67 = new SqlCommand("Delete  from WorkShopSerialNo where CompID='" + CompID + "'", con, tran);
            cmdDel67.CommandTimeout = 10000000; //in seconds
            cmdDel67.ExecuteNonQuery();
            SqlCommand cmdDel68 = new SqlCommand("Delete  from VoucherNoVerification where CompID='" + CompID + "'", con, tran);
            cmdDel68.CommandTimeout = 10000000; //in seconds
            cmdDel68.ExecuteNonQuery();



            //-----------------------Call DeleteAllItemsOp Start------------
            SqlCommand cmdDelInv1 = new SqlCommand("Delete  from InventoryOpeningBalance where CompID='" + CompID + "'", con, tran);
            cmdDelInv1.CommandTimeout = 10000000; //in seconds
            cmdDelInv1.ExecuteNonQuery();
            SqlCommand cmdDelInv2 = new SqlCommand("Delete from WarrantyOpeningBalance where CompID='" + CompID + "'", con, tran);
            cmdDelInv2.CommandTimeout = 10000000; //in seconds
            cmdDelInv2.ExecuteNonQuery();
            SqlCommand cmdDelInv3 = new SqlCommand("Delete from WorkShopOpeningBalance where CompID='" + CompID + "'", con, tran);
            cmdDelInv3.CommandTimeout = 10000000; //in seconds
            cmdDelInv3.ExecuteNonQuery();


            SqlCommand cmd = new SqlCommand(@"Select Code from InvCode where CompID='" + CompID + "' order by Code", con, tran);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            Int32 InvCode = 1;
            Module7 objModule7 = new Module7();
            Module2 objModule2 = new Module2();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InvCode = Convert.ToInt32(dt.Rows[i]["Code"]);
                SqlCommand cmd44 = new SqlCommand("Insert into InventoryOpeningBalance(ICode,Dat,Opening,Cost_Per_Unit,Total_Value,Balance_Qty,Sold_Qty,CompID)values('" + InvCode + "','" + objModule7.StartDate() + "'," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ",'" + CompID + "')", con, tran);
                cmd44.ExecuteNonQuery();
                SqlCommand cmd45 = new SqlCommand("Insert into WarrantyOpeningBalance(ICode,Dat,OpeningVender,OpeningClient,CompID)values('" + InvCode + "','" + objModule7.StartDate() + "'," + 0 + "," + 0 + ",'" + CompID + "')", con, tran);
                cmd45.ExecuteNonQuery();
                SqlCommand cmd46 = new SqlCommand("Insert into WorkShopOpeningBalance(ICode,Dat,Opening,CompID)values('" + InvCode + "','" + objModule7.StartDate() + "'," + 0 + ",'" + CompID + "')", con, tran);
                cmd46.ExecuteNonQuery();
            }

            //-----------------------Call DeleteAllItemsOp End------------



            //-----------------------Call DeleteAllGLCodeOp Start------------

            SqlCommand cmdDelGL1 = new SqlCommand("Delete from GLOpeningBalance where compID='" + CompID + "'", con, tran);
            cmdDelGL1.CommandTimeout = 10000000; //in seconds
            cmdDelGL1.ExecuteNonQuery();
            SqlCommand cmdDelGL2 = new SqlCommand("Delete from PartyOpBalance where compID='" + CompID + "'", con, tran);
            cmdDelGL2.CommandTimeout = 10000000; //in seconds
            cmdDelGL2.ExecuteNonQuery();
            SqlCommand cmd2 = new SqlCommand(@"Select Code,NorBalance from GLCode where compID='" + CompID + "' and Lvl=5 order by Code", con, tran);
            SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            adpt2.Fill(dt2);
            string GLCode = "";
            Int32 NorBalance = 1;
            ModGLCode objModGLCode = new ModGLCode();
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                GLCode = Convert.ToString(dt2.Rows[i]["Code"]);
                NorBalance = Convert.ToInt32(dt2.Rows[i]["NorBalance"]);
                SqlCommand cmd444 = new SqlCommand("Insert into GLOpeningBalance(Code,Date1,OpeningBal,CompID)values('" + GLCode + "','" + objModule7.StartDate() + "'," + 0 + ",'" + CompID + "')", con, tran);
                cmd444.ExecuteNonQuery();

                if (GLCode.Substring(0, 8) == "01010103" || GLCode.Substring(0, 8) == "01020101")
                {
                    SqlCommand cmd455 = new SqlCommand("Insert into PartyOpBalance(Code,Dat,OpBalance,OpBalance1,Norbalance,CompID)values('" + GLCode + "','" + objModule7.StartDate() + "'," + 0 + "," + 0 + "," + NorBalance + ",'" + CompID + "')", con, tran);
                    cmd455.ExecuteNonQuery();
                }

            }

            //-----------------------Call DeleteAllGLCodeOp End------------


            SqlCommand cmd4555 = new SqlCommand("Delete  from Voucher_No_Available where CompID='" + CompID + "'", con, tran);
            cmd4555.ExecuteNonQuery();
            SqlCommand cmd4556 = new SqlCommand("insert into Voucher_No_Available(Description,Max_Value,CompID) values('" + "JV" + "','" + 1 + "','" + CompID + "')", con, tran);
            cmd4556.ExecuteNonQuery();
            SqlCommand cmd4557 = new SqlCommand("insert into Voucher_No_Available(Description,Max_Value,CompID) values('" + "CRV" + "','" + 1 + "','" + CompID + "')", con, tran);
            cmd4557.ExecuteNonQuery();

            //-----------------------Call DeleteAllGLCodeOp Start------------








            //-----------------------Call DeleteAllGLCodeOp End------------
            //DoEvents
            //Call DeleteAllGLCodeOp
            //DoEvents

            //Unload frmProgressBar
            //logon.adoconn.Execute "Delete  from Voucher_No_Available where CompID='" & logon.CompID & "'"
            //ModMaxVoucherNo.UpDateMaxJV
            //ModMaxVoucherNo.UpdateMaxCRV
            //Call Main1.StockRequired
            //DoEvents
            //MsgBox "OK"
            //End If

        }
        public void DeleteAllItems(SqlConnection con, SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand(@"Select Code from InvCode where CompID='" + CompID + "' order by Code", con, tran);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            Int32 InvCode = 1;
            Module7 objModule7 = new Module7();
            Module2 objModule2 = new Module2();
            ModQtyOfInventory objModQtyOfInventory = new ModQtyOfInventory();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InvCode = Convert.ToInt32(dt.Rows[i]["Code"]);
                var TransactionExistOfItem = objModQtyOfInventory.TransactionExistOFItem(InvCode);

                if (TransactionExistOfItem == "No")
                {

                    SqlCommand cmd44 = new SqlCommand("Delete  from  InvCode where CompID='" + CompID + "' and Code=" + InvCode, con, tran);
                    cmd44.ExecuteNonQuery();
                    SqlCommand cmd45 = new SqlCommand("Delete  from InventoryOpeningBalance where  CompID='" + CompID + "' and ICode=" + InvCode, con, tran);
                    cmd45.ExecuteNonQuery();
                    SqlCommand cmd46 = new SqlCommand("Delete  from WarrantyOpeningBalance where  CompID='" + CompID + "' and ICode=" + InvCode, con, tran);
                    cmd46.ExecuteNonQuery();

                    SqlCommand cmd47 = new SqlCommand("Delete  from WorkshopOpeningBalance where  CompID='" + CompID + "' and ICode=" + InvCode, con, tran);
                    cmd47.ExecuteNonQuery();
                    SqlCommand cmd48 = new SqlCommand("Delete  from InvBarCode where   CompID='" + CompID + "' and ItemCode=" + InvCode, con, tran);
                    cmd48.ExecuteNonQuery();
                }
            }
        }
        public void DeleteAllClients(SqlConnection con, SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand(@"Select Code from GLCode where lvl=5 and Code like '01010103%' and code <> '0101010300000'", con, tran);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            string PartyCode = "";
            Module7 objModule7 = new Module7();
            Module2 objModule2 = new Module2();
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PartyCode = Convert.ToString(dt.Rows[i]["Code"]);
                var TransactionExist = objModPartyCodeAgainstName.TransactionExistAgainstGLCode(PartyCode);

                if (TransactionExist == "No")
                {
                    SqlCommand cmd44 = new SqlCommand("Delete  from GLCode where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd44.ExecuteNonQuery();
                    SqlCommand cmd45 = new SqlCommand("Delete  from GLOpeningBalance where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd45.ExecuteNonQuery();
                    SqlCommand cmd46 = new SqlCommand("Delete  from PartyCode where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd46.ExecuteNonQuery();

                    SqlCommand cmd47 = new SqlCommand("Delete  from PartyOpBalance where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd47.ExecuteNonQuery();
                    SqlCommand cmd48 = new SqlCommand("Delete  from PartyGPCode where  Code='" + PartyCode + "'", con, tran);
                    cmd48.ExecuteNonQuery();
                }
            }

        }
        public void DeleteAllVenders(SqlConnection con, SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand(@"Select Code from GLCode where lvl=5 and Code like '01020101%'", con, tran);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            string PartyCode = "";
            Module7 objModule7 = new Module7();
            Module2 objModule2 = new Module2();
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PartyCode = Convert.ToString(dt.Rows[i]["Code"]);
                var TransactionExist = objModPartyCodeAgainstName.TransactionExistAgainstGLCode(PartyCode);

                if (TransactionExist == "No")
                {
                    SqlCommand cmd44 = new SqlCommand("Delete  from GLCode where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd44.ExecuteNonQuery();
                    SqlCommand cmd45 = new SqlCommand("Delete  from GLOpeningBalance where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd45.ExecuteNonQuery();
                    SqlCommand cmd46 = new SqlCommand("Delete  from PartyCode where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd46.ExecuteNonQuery();
                    SqlCommand cmd47 = new SqlCommand("Delete  from PartyOpBalance where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmd47.ExecuteNonQuery();
                    SqlCommand cmd48 = new SqlCommand("Delete  from PartyGPCode where  Code='" + PartyCode + "'", con, tran);
                    cmd48.ExecuteNonQuery();
                }
            }


        }

        //public string aaaTransactionExistOFItem(decimal ItemCode)
        //{
        //    //SqlDataAdapter cmd = new SqlDataAdapter("select Top 1 ICode from Inventory Where ICode=" + ItemCode, con);
        //    //DataTable dt = new DataTable();
        //    //cmd.Fill(dt);
        //    //string  TransactionExistOFItem = "No";
        //    //if (dt.Rows.Count > 0)
        //    //{
        //    //    TransactionExistOFItem = "Yes";
        //    //}
        //    //return TransactionExistOFItem ;
        //}


    }
}