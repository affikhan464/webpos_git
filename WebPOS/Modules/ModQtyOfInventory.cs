﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class ModQtyOfInventory
    {
        

        Module7 objModule7 = new Module7(); 
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public void QtyAvailableOne(string ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(Opening),0) as Opening from InventoryOpeningBalance where  CompID='" + CompID + "' and ICode=" + ItemCode + " and Dat='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal OpeningBalance = 0;
            if (dt.Rows.Count > 0)
            {
                OpeningBalance = Convert.ToDecimal(dt.Rows[0]["Opening"]);
            }
            SqlDataAdapter cmd2 = new SqlDataAdapter("select isnull(sum(received),0) as received , isnull(sum(issued),0) as issued from inventory where  CompID='" + CompID + "' and ICode=" + ItemCode + " and dat>='" + objModule7.StartDate() + "' and dat<='" + objModule7.EndDate(DateTime.Today) + "'", con);
            DataTable dt2 = new DataTable();
            cmd2.Fill(dt2);
            decimal UnitsReceived = 0;
            decimal UnitsIssued = 0;

            if (dt2.Rows.Count > 0)
            {
                UnitsReceived = Convert.ToDecimal(dt2.Rows[0]["received"]);
                UnitsIssued = Convert.ToDecimal(dt2.Rows[0]["issued"]);
            }
            decimal QtyAvailable = 0;
            QtyAvailable = (OpeningBalance + UnitsReceived) - UnitsIssued;

            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd3 = new SqlCommand("update InvCode set qty=" + QtyAvailable + " where  CompID='" + CompID + "' and Code=" + ItemCode, con);
            cmd3.ExecuteNonQuery();
            con.Close();
        }

        public string TransactionExistOFItem(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select Top 1 ICode from Inventory Where ICode=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string  TransactionExistOFItem = "No";
            if (dt.Rows.Count > 0)
            {
                TransactionExistOFItem = "Yes";
            }
            return TransactionExistOFItem ;
        }


    }
}