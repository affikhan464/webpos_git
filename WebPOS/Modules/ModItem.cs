﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public class ModItem
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;


        public decimal ItemPurchasedQtyAgainstItemCode(decimal ItemCode, DateTime StartDate, DateTime EndDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(QTY,0) as QTY from Purchase2 where Code='" + ItemCode + "' and date1>='" + StartDate + "' and date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal QtyPur = 0;
            if (dt.Rows.Count > 0)
            {
                QtyPur = Convert.ToDecimal(dt.Rows[0]["QTY"]);
            }
            return QtyPur;
        }
        public Product GetItemByCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("" +
                "Select *,BrandName.Name as ItemBrand,BrandName.Code as ItemBrandCode from invcode " +
                "join BrandName on BrandName.Code = invcode.Brand " +
                "where invcode.Code=" + ItemCode , con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var item = new Product();
            if (dt.Rows.Count > 0)
            {
                item.Code = Convert.ToString(dt.Rows[0]["Code"]);
                item.Description = Convert.ToString(dt.Rows[0]["Description"]);
                item.BrandName = Convert.ToString(dt.Rows[0]["ItemBrand"]);
                item.LP = Convert.ToString(dt.Rows[0]["LP"]);
                item.MSP = Convert.ToString(dt.Rows[0]["MSP"]);
                item.BrandCode = Convert.ToString(dt.Rows[0]["ItemBrandCode"]);
            }
            return item;
        }
        public string IMELengthAgainstType(Int32 Valu)
        {
            string str = "";
            if (Valu == 1)
            {
                str = "Select valueNo from Configuration Where  CompID = '" + CompID + "' and Description = 'LenthOfSerialNo1'";
            }
            else if (Valu == 2)
            {
                str = "Select valueNo from Configuration Where  CompID = '" + CompID + "' and Description = 'LenthOfSerialNo2'";
            }
            else if (Valu == 3)
            {
                str = "Select valueNo from Configuration Where  CompID = '" + CompID + "' and Description = 'LenthOfSerialNo3'";

            }

                SqlDataAdapter cmd = new SqlDataAdapter(str, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string LengthIME = "";
            if (dt.Rows.Count > 0)
            {
                LengthIME = Convert.ToString(dt.Rows[0]["valueNo"]);
            }
            return LengthIME;
        }
        public Int32 ConfigurationGeneralStore()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select valueNo from Configuration where CompID='" + CompID + "' and Description='GeneralStore'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            Int32 GeneralStore = 0;
            if (dt.Rows.Count > 0)
            {
                GeneralStore = Convert.ToInt32(dt.Rows[0]["valueNo"]);
            }
            return GeneralStore;
        }
        public decimal ItemSaleReturnedQtyAgainstItemCode(decimal ItemCode, DateTime StartDate, DateTime EndDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(QTY,0) as QTY from SaleReturn2 where Code='" + ItemCode + "' and date1>='" + StartDate + "' and date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Qty = 0;
            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToDecimal(dt.Rows[0]["QTY"]);
            }
            return Qty;
        }
        public decimal ItemSoldQtyAgainstItemCode(decimal ItemCode, DateTime StartDate, DateTime EndDate)
        {
            if (con.State == ConnectionState.Closed) { con.Open(); }
            SqlDataAdapter cmd = new SqlDataAdapter("Select  isnull(sum(Invoice2.QTY),0) as QTY  from (Invoice2 INNER JOIN Invoice1 ON Invoice2.InvNo = Invoice1.InvNo) where Invoice2.Code='" + ItemCode + "' and Invoice1.date1>='" + StartDate + "' and Invoice1.date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Qty = 0;
            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToDecimal(dt.Rows[0]["QTY"]);
            }
            return Qty;
        }
        public int BarcodeCategoryAgainstItemCode(int ItemCode)
        {
            if (con.State == ConnectionState.Closed) { con.Open(); }
            SqlDataAdapter cmd = new SqlDataAdapter("Select  isnull(BarCodeCategory,1) as code  from InvCode where Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int code = 0;
            if (dt.Rows.Count > 0)
            {
                code = Convert.ToInt32(dt.Rows[0]["code"]);
            }
            return code;
        }
        public int ItemCodeAgainstIME(string IME)
        {
            int GeneralStore = ConfigurationGeneralStore();
            if (con.State == ConnectionState.Closed) { con.Open(); }
            
            var str = String.Empty;
            str = "Select ItemCode from InventorySerialNoFinal where  CompID='" + CompID + "' and IME='" + IME + "'";
            if (GeneralStore == 1)
            {
                str = "Select ItemCode from InvBarCode where  CompID='" + CompID + "' and BarCode='" + IME + "'";

            } else
                 if (GeneralStore == 0 || GeneralStore == 2)
            {
                str = "Select ItemCode from InventorySerialNoFinal where  CompID='" + CompID + "' and IME='" + IME + "'";

            }



            SqlDataAdapter cmd = new SqlDataAdapter(str, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int code = 0;
            if (dt.Rows.Count > 0)
            {
                code = Convert.ToInt32(dt.Rows[0]["ItemCode"]);
            }else
            if(GeneralStore == 2 && code == 0)
            {

                str = "Select ItemCode from InvBarCode where  CompID='" + CompID + "' and BarCode='" + IME + "'";
                SqlDataAdapter cmd2 = new SqlDataAdapter(str, con);
                DataTable dt2 = new DataTable();
                cmd2.Fill(dt2);
                
                if (dt2.Rows.Count > 0)
                {
                    code = Convert.ToInt32(dt2.Rows[0]["ItemCode"]);
                }

            }





            return code;
        }

        public decimal ItemPurchasedReturnAgainstItemCode(decimal ItemCode, DateTime StartDate, DateTime EndDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(sum(Qty),0) as QTY from PurchaseReturn2 where Code='" + ItemCode + "' and date1 >='" + StartDate + "' and date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Qty = 0;
            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToDecimal(dt.Rows[0]["QTY"]);
            }
            return Qty;
        }

        public decimal MinLimitSaleRateAgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(MinLimit,0) as MinLimit from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MinLimit = 0;
            if (dt.Rows.Count > 0)
            {
                MinLimit = Convert.ToDecimal(dt.Rows[0]["MinLimit"]);
            }
            return MinLimit;
        }
        public decimal LPRateAgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(LP,0) as LP from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal LP = 0;
            if (dt.Rows.Count > 0)
            {
                LP = Convert.ToDecimal(dt.Rows[0]["LP"]);
            }
            return LP;
        }
        public decimal MaxLimitSaleRateAgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(MaxLimit,0) as MaxLimit from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxLimit = 0;
            if (dt.Rows.Count > 0)
            {
                MaxLimit = Convert.ToDecimal(dt.Rows[0]["MaxLimit"]);
            }
            return MaxLimit;
        }
        public string PurchaseBillAlreadyExist(string InvoiceNo)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select InvNo from Purchase1 where CompID='" + CompID + "' and InvNo='" + InvoiceNo + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            String InvoiceExist = "No";
            if (dt.Rows.Count > 0)
            {
                InvoiceExist = "Yes";
            }
            return InvoiceExist;
        }
        public int AverageMethodItemReceived(decimal ItemCode, decimal ItemQty, decimal ItemCost, SqlTransaction tran, SqlConnection con)
        {
            ModItem objModItem = new ModItem();
            decimal PrevQty = 0;
            decimal PrevCost = 0;
            decimal NewAvgCost = 0;
            decimal PrevAmount = 0;
            decimal NewQTY = 0;
            decimal CurrentNewAmount = 0;
            decimal FinalAmount = 0;


            var cmd = new SqlCommand("Select Qty,isnull(Ave_Cost,0) as Ave_Cost from InvCode  where  Code=" + ItemCode, con, tran);
            var adptr = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adptr.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                PrevQty = Convert.ToDecimal(dt.Rows[0]["Qty"]);
                PrevCost = Convert.ToDecimal(dt.Rows[0]["Ave_Cost"]);
            }


            if (PrevCost > 0 && PrevQty > 0)    // ' agar already avg cost is set or > 0 then calculate other wise set current cost as average
            {
                if (PrevQty > 0) { PrevAmount = (PrevCost * PrevQty); }
                CurrentNewAmount = (ItemQty * ItemCost);
                NewQTY = (PrevQty + ItemQty);
                FinalAmount = (PrevAmount + CurrentNewAmount);
                if (FinalAmount > 0 && NewQTY > 0)
                {
                    NewAvgCost = (FinalAmount) / NewQTY;
                    SqlCommand cmd3 = new SqlCommand("Update invCode set Ave_Cost=" + NewAvgCost + " where  CompID='" + CompID + "' and Code=" + ItemCode, con, tran);
                    cmd3.ExecuteNonQuery();

                    //SqlCommand cmd4 = new SqlCommand("Update Ingridients_Cost set Ingridient_Rate=" + NewAvgCost + " where  CompID='" + CompID + "' and Ingridient_Code=" + ItemCode, con, tran);
                    //cmd4.ExecuteNonQuery();
                }

            }
            else
            {
                SqlCommand cmd5 = new SqlCommand("Update invCode set Ave_Cost=" + ItemCost + " where  CompID='" + CompID + "' and Code=" + ItemCode, con, tran);
                cmd5.ExecuteNonQuery();

                // SqlCommand cmd6 = new SqlCommand("Update Ingridients_Cost set Ingridient_Rate=" + ItemCost + " where  CompID='" + CompID + "' and Ingridient_Code=" + ItemCode, con, tran);
                // cmd6.ExecuteNonQuery();
            }
            return 1;
        

    }
    public decimal ItemSellingPrice_5_AgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(SellingCost5,0) as SellingCost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal SellingPrice = 0;
            if (dt.Rows.Count > 0)
            {
                SellingPrice = Convert.ToDecimal(dt.Rows[0]["SellingCost"]);

            }
            return SellingPrice;
        }
        public bool BrandAssignedToItem(string BrandCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from InvCode where CompID='" + CompID + "' and Brand='" + BrandCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var BrandAssignedToItem = dt.Rows.Count > 0;

            return BrandAssignedToItem;
        }
        public bool ColorAssignedToItem(string ColorCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Color from InvCode where CompID='" + CompID + "' and Color=" + ColorCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            bool ColorAssignedToItem = dt.Rows.Count > 0;

            return ColorAssignedToItem;
        }
        public bool GoDownAssignedToItem(string GodownCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Godown from InvCode where CompID='" + CompID + "' and Godown='" + GodownCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            bool GoDownAssignedToItem = dt.Rows.Count > 0;

            return GoDownAssignedToItem;
        }

        public bool PackingAssignedToItem(string code)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Packing from InvCode where CompID='" + CompID + "' and Packing='" + code + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var GoDownAssignedToItem = dt.Rows.Count > 0;

            return GoDownAssignedToItem;
        }
        public bool CategoryAssignedToItem(string CategoryCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from InvCode where CompID='" + CompID + "' and Category=" + CategoryCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            bool CategoryAssignedToItem = dt.Rows.Count > 0;

            return CategoryAssignedToItem;
        }
        public bool SaleManAlreadyAssignedToSaleInvoice(string SaleManCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select SaleManCode from Invoice1 where CompID='" + CompID + "' and SaleManCode=" + SaleManCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var SaleManAlreadyAssignedToSaleInvoice = dt.Rows.Count > 0;

            return SaleManAlreadyAssignedToSaleInvoice;
        }


        public bool ManufacturerAssignedToItem(string ManufacturerCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Manufacturer from InvCode where CompID='" + CompID + "' and Manufacturer=" + ManufacturerCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var ManufacturerAssignedToItem = dt.Rows.Count > 0;

            return ManufacturerAssignedToItem;
        }
        public bool ClassAssignedToItem(string ClassCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Class from InvCode where CompID='" + CompID + "' and Class=" + ClassCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            bool ClassAssignedToItem = dt.Rows.Count > 0;

            return ClassAssignedToItem;
        }
        public decimal ItemSellingPrice_4_AgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(SellingCost4,0) as SellingCost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal SellingPrice = 0;
            if (dt.Rows.Count > 0)
            {
                SellingPrice = Convert.ToDecimal(dt.Rows[0]["SellingCost"]);

            }
            return SellingPrice;
        }
        public decimal ItemSellingPrice_3_AgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(SellingCost3,0) as SellingCost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal SellingPrice = 0;
            if (dt.Rows.Count > 0)
            {
                SellingPrice = Convert.ToDecimal(dt.Rows[0]["SellingCost"]);

            }
            return SellingPrice;
        }
        public decimal ItemSellingPrice_2_AgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(SellingCost2,0) as SellingCost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal SellingPrice = 0;
            if (dt.Rows.Count > 0)
            {
                SellingPrice = Convert.ToDecimal(dt.Rows[0]["SellingCost"]);

            }
            return SellingPrice;
        }
        public decimal FixedCostAgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(FixedCost,0) as FixedCost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal FixedCost = 0;
            if (dt.Rows.Count > 0)
            {
                FixedCost = Convert.ToDecimal(dt.Rows[0]["FixedCost"]);
            }
            return FixedCost;
        }

        public decimal DealRsAgainstItemCode(string ItemCode, string DealApplyNo)
        {
            if (con.State == ConnectionState.Closed) { con.Open(); }
            var str = string.Empty;
            DealApplyNo = string.IsNullOrEmpty(DealApplyNo) ? "1" : DealApplyNo;
            if (DealApplyNo == "1")
            {
                str = "Select isnull(DealRs, 0) as DealRs from InvCode where Code = " + ItemCode;
            }
            else
            if (DealApplyNo == "2")
            {
                str = "Select isnull(DealRs2, 0) as DealRs from InvCode where Code = " + ItemCode;
            }
            else
            if (DealApplyNo == "3")
            {
                str = "Select isnull(DealRs3, 0) as DealRs from InvCode where Code = " + ItemCode;
            }



            SqlDataAdapter cmd = new SqlDataAdapter(str, con);





            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal DealRs = 0;
            if (dt.Rows.Count > 0)
            {
                DealRs = Convert.ToDecimal(dt.Rows[0]["DealRs"]);
            }
            return DealRs;
        }

        public int BrandCodeAgainstBrandName(string BrandName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from BrandName where CompID='" + CompID + "' and Name='" + BrandName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int BrandID = 1;



            if (dt.Rows.Count > 0)
            {
                BrandID = Convert.ToInt32(dt.Rows[0]["Code"]);

            }


            return BrandID;
        }
        public int PackingCodeAgainstName(string PackingNAme)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select id from Packing where  CompID='" + CompID + "' and Name='" + PackingNAme + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int PackingID = 1;
            if (dt.Rows.Count > 0)
            {
                PackingID = Convert.ToInt32(dt.Rows[0]["id"]);
            }
            return PackingID;
        }

        public int ManufacturerCodeAgainstName(string ManufacturerNAme)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select id from Manufacturers where CompID='" + CompID + "' and Name='" + ManufacturerNAme + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int ManufacturerID = 1;
            if (dt.Rows.Count > 0)
            {
                ManufacturerID = Convert.ToInt32(dt.Rows[0]["id"]);
            }
            return ManufacturerID;
        }

        public string ItemTypeNameAgainstItemTypeCode(int ItemTypeCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Name from  ItemType where CompID='" + CompID + "' and Code=" + ItemTypeCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string ItemTypeName = "";
            if (dt.Rows.Count > 0)
            {
                ItemTypeName = Convert.ToString(dt.Rows[0]["Name"]);
            }
            return ItemTypeName;
        }
        public int ItemTypeCodeAgainstItemTypeNAme(string ItemType)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from  ItemType where CompID='" + CompID + "' and Name='" + ItemType + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int ItemTypeName = 1;
            if (dt.Rows.Count > 0)
            {
                ItemTypeName = Convert.ToInt16(dt.Rows[0]["Code"]);
            }
            return ItemTypeName;
        }
        public int WidthIDAgainstWidthName(string WidthName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from  Length where CompID='" + CompID + "' and Length='" + WidthName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int WidthID = 1;
            if (dt.Rows.Count > 0)
            {
                WidthID = Convert.ToInt16(dt.Rows[0]["ID"]);
            }
            return WidthID;
        }
        public int HeightIDAgainstWidthName(string HeightName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from  Height where CompID='" + CompID + "' and Height='" + HeightName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int HeightID = 1;
            if (dt.Rows.Count > 0)
            {
                HeightID = Convert.ToInt16(dt.Rows[0]["ID"]);
            }
            return HeightID;
        }
        public int BarCodeCategoryIDAgainstName(string BarCodeCategoryNAme)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from  BarCodeCategory where CompID='" + CompID + "' and Name='" + BarCodeCategoryNAme + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int BarCodeCategoryID = 1;
            if (dt.Rows.Count > 0)
            {
                BarCodeCategoryID = Convert.ToInt16(dt.Rows[0]["ID"]);
            }
            return BarCodeCategoryID;
        }
        public int ItemNatureCodeAgainstName(string ItemNatureName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from  ItemNature where  CompID='" + CompID + "' and ItemNature='" + ItemNatureName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int ItemNatureID = 1;
            if (dt.Rows.Count > 0)
            {
                ItemNatureID = Convert.ToInt32(dt.Rows[0]["Code"]);
            }
            return ItemNatureID;
        }
        public int GodownCodeAgainstName(string GodownName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from  Godown where  CompID='" + CompID + "' and Name='" + GodownName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int GodownID = 1;
            if (dt.Rows.Count > 0)
            {
                GodownID = Convert.ToInt32(dt.Rows[0]["ID"]);
            }
            return GodownID;
        }
        public string ColorNameAgainstColorCode(int ColorCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Color from Color where CompID='" + CompID + "' and ID=" + ColorCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string ColorName = "";
            if (dt.Rows.Count > 0)
            {
                ColorName = Convert.ToString(dt.Rows[0]["Color"]);
            }
            return ColorName;
        }
        public int ColorCodeAgainstName(string ColorName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from  Color where  CompID='" + CompID + "' and Color='" + ColorName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int ColorID = 1;
            if (dt.Rows.Count > 0)
            {
                ColorID = Convert.ToInt32(dt.Rows[0]["ID"]);
            }
            return ColorID;
        }
        public int ClassCodeAgainstName(string ClassName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select id from  Class where  CompID='" + CompID + "' and Name='" + ClassName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int ClassID = 1;
            if (dt.Rows.Count > 0)
            {
                ClassID = Convert.ToInt32(dt.Rows[0]["id"]);
            }
            return ClassID;
        }
        public int CategoryCodeAgainstName(string CategoryName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select id from  Category where  CompID='" + CompID + "' and Name='" + CategoryName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int CategoryID = 1;
            if (dt.Rows.Count > 0)
            {
                CategoryID = Convert.ToInt32(dt.Rows[0]["id"]);
            }
            return CategoryID;
        }
        public int AccUnitCodeAgainstName(string AccUnitName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from  AccountUnit where  CompID='" + CompID + "' and Name='" + AccUnitName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int AccUnitID = 1;
            if (dt.Rows.Count > 0)
            {
                AccUnitID = Convert.ToInt32(dt.Rows[0]["Code"]);
            }
            return AccUnitID;
        }
        public decimal ItemSellingPrice_1_AgainstItemCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select SellingCost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal SellingPrice = 0;



            if (dt.Rows.Count > 0)
            {
                SellingPrice = Convert.ToDecimal(dt.Rows[0]["SellingCost"]);

            }


            return SellingPrice;
        }
        public string BrandNameAgainstBrandCode(Int32 BrandCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Name from BrandName where CompID='" + CompID + "' and Code=" + BrandCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string BrandName = "undefined brand";
            if (dt.Rows.Count > 0)
            {
                BrandName = Convert.ToString(dt.Rows[0]["Name"]);
            }
            return BrandName;
        }
        public Int32 BrandCodeAgainstItemCode(Int32 ItemCode)
        {
            if (con.State == ConnectionState.Closed) { con.Open(); }
            SqlDataAdapter cmd = new SqlDataAdapter("Select Brand from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            Int32 BrandCode = 0;

            if (dt.Rows.Count > 0)
            {
                BrandCode = Convert.ToInt32(dt.Rows[0]["Brand"]);

            }


            return BrandCode;
        }
        public bool IMEVerifiedAgainstBrandCode(int brandCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select VerifyIME from BrandName where CompID='" + CompID + "' and Code=" + brandCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var VerifyIME = false;



            if (dt.Rows.Count > 0)
            {
                VerifyIME = Convert.ToInt32(dt.Rows[0]["VerifyIME"]) == 1;

            }


            return VerifyIME;
        }
        public decimal LastPurchasePrice(string ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Purchase2.Date1  ,   Purchase2.Cost  as Cost ,   Purchase2.InvNo from Purchase2  where   CompID='" + CompID + "' and Purchase2.Code='" + ItemCode + "' order by Purchase2.System_Date_Time Desc", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal LstPurchasePrice = 0;
            if (dt.Rows.Count > 0)
            {
                LstPurchasePrice = Convert.ToDecimal(dt.Rows[0]["Cost"]);
            }
            if (LstPurchasePrice == 0) { LstPurchasePrice = OpeingItemCostAgainstItemCode(ItemCode); }
            //if (LstPurchasePrice == 0) { LstPurchasePrice =100; }
            return LstPurchasePrice;
        }
        public decimal OpeingItemCostAgainstItemCode(string ItemCode)
        {
            Module7 objModule7 = new Module7();

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Cost_Per_Unit,0) as Cost_Per_Unit from InventoryOpeningBalance  where   CompID='" + CompID + "' and ICode=" + Convert.ToInt16(ItemCode) + "  and Dat='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal OpeingCost = 0;
            if (dt.Rows.Count > 0)
            {
                OpeingCost = Convert.ToDecimal(dt.Rows[0]["Cost_Per_Unit"]);
            }
            //OpeingCost = 200;
            return OpeingCost;
        }
        public decimal ItemAvailableQty(string ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Qty from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Qty = 0;



            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToDecimal(dt.Rows[0]["Qty"]);

            }


            return Qty;
        }
        public int CategoryIDAgainstCategoryName(string CategoryName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select id from Category where CompID='" + CompID + "' and Name='" + CategoryName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int CategoryID = 0;
            if (dt.Rows.Count > 0)
            {
                CategoryID = Convert.ToInt16(dt.Rows[0]["ID"]);
            }

            return CategoryID;
        }
        public int ClassIDAgainstClassName(string ClassName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from Class where CompID='" + CompID + "' and Name='" + ClassName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int ClassID = 0;



            if (dt.Rows.Count > 0)
            {
                ClassID = Convert.ToInt16(dt.Rows[0]["ID"]);

            }


            return ClassID;
        }

        public int GoDownIDAgainstGoDownName(string GoDownName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from GoDown where CompID='" + CompID + "' and Name='" + GoDownName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int GoDownID = 0;



            if (dt.Rows.Count > 0)
            {
                GoDownID = Convert.ToInt16(dt.Rows[0]["ID"]);

            }


            return GoDownID;
        }
        public int PackingIDAgainstPackingName(string PackingName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from Packing where CompID='" + CompID + "' and Name='" + PackingName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int PackingID = 0;



            if (dt.Rows.Count > 0)
            {
                PackingID = Convert.ToInt16(dt.Rows[0]["ID"]);

            }


            return PackingID;
        }
        public decimal ItemAverageCost(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Ave_Cost,0) as Ave_Cost from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal AvgCost = 0;
            if (dt.Rows.Count > 0)
            {
                AvgCost = Convert.ToDecimal(dt.Rows[0]["Ave_Cost"]);
            }
            return AvgCost;
        }
        public string ItemNameAgainstCode(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Description from InvCode where CompID='" + CompID + "' and Code=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string Name = "";
            if (dt.Rows.Count > 0)
            {
                Name = dt.Rows[0]["Description"].ToString() ;
            }
            return Name;
        }
        public decimal ItemOpeningUnits(decimal ItemCode, DateTime StartDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Opening,0) as Opening from InventoryOpeningBalance where CompID='" + CompID + "' and ICode=" + ItemCode + " and Dat='" + StartDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Opening = 0;



            if (dt.Rows.Count > 0)
            {
                Opening = Convert.ToDecimal(dt.Rows[0]["Opening"]);

            }


            return Opening;
        }
        public decimal ItemCodeAgainstItemName(string ItemName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from InvCode where CompID='" + CompID + "' and Description='" + ItemName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal ItemCode = 0;



            if (dt.Rows.Count > 0)
            {
                ItemCode = Convert.ToDecimal(dt.Rows[0]["Code"]);

            }


            return ItemCode;
        }

        public bool IsIMELengthCorrect(string imeCode)
        {
            try
            {
                var verifyIMELength = 1;
                var isLengthCorrect = false;
                if (verifyIMELength == 1)
                {
                    var cmd = new SqlCommand("Select valueNo from Configuration where CompID='" + CompID + "' and Description like '%LenthOfSerialNo%'", con);
                    if (con.State == ConnectionState.Closed)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }
                    }
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (Convert.ToInt32(reader["valueNo"]) == imeCode.Length)
                            {
                                isLengthCorrect = Convert.ToInt32(reader["valueNo"]) == imeCode.Length;

                            }
                        }
                    }
                }
                return isLengthCorrect;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
            }

        }

        public IMEModel isIMESold(string itemCode, string imeCode)
        {
            try
            {
                var cmd = new SqlCommand("Select Top 1 ItemCode,Description from InventorySerialNoFinal where CompID='" + CompID + "' and IME='" + imeCode + "' order by System_Date_Time desc ", con);
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }
                var isSold = false;
                var isItemMatch = false;
                var isItemAvailable = false;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    if (dt.Rows.Count > 0)
                    {
                        isSold = dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.Sale.ToLower() || dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.PurchaseReturn.ToLower();
                        isItemMatch = (dt.Rows[0]["ItemCode"]).ToString() == itemCode;

                        //is item available for sale
                        isItemAvailable = dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.Purchase.ToLower()
                                        || dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.SaleReturn.ToLower();
                    }
                    else
                        isSold = true;

                }
                return new IMEModel() { isSold = isSold, isItemMatch = isItemMatch, isItemAvailable = isItemAvailable };
            }
            catch (Exception ex)
            {
                return new IMEModel() { isSold = true, isItemMatch = false, Error = true, ErrorMessage = ex.Message };
            }
            finally
            {
                con.Close();
            }


        }


        public IMEModel isIMEPurchased(string itemCode, string imeCode)
        {
            try
            {
                var cmd = new SqlCommand("Select ItemCode,Description from InventorySerialNoFinal where CompID='" + CompID + "' and IME='" + imeCode + "' order by System_Date_Time desc ", con);
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }
                var isPurchased = false;
                var isItemMatch = false;
                var isItemAvailable = false;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    if (dt.Rows.Count > 0)
                    {
                        isPurchased = dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.Purchase.ToLower() || dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.SaleReturn.ToLower();
                        isItemMatch = (dt.Rows[0]["ItemCode"]).ToString() == itemCode;

                        //is item available to be Purchased
                        isItemAvailable = dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.Sale.ToLower()
                                      || dt.Rows[0]["Description"].ToString().ToLower() == IMEDescription.PurchaseReturn.ToLower();

                    }
                    else
                        isPurchased = true;

                }
                return new IMEModel() { isPurchased = isPurchased, isItemMatch = isItemMatch, isItemAvailable = isItemAvailable };
            }
            catch (Exception ex)
            {
                return new IMEModel() { isPurchased = true, isItemMatch = false, Error = true, ErrorMessage = ex.Message };
            }
            finally
            {
                con.Close();
            }


        }
        public decimal QtyOnSaleBillAgainstItemCodeAndBillNo(decimal ItemCode,string VoucherNo)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Qty,0) as Qty from Invoice2 where Code=" + ItemCode + " and InvNo='" + VoucherNo + "' and CompID='" + CompID + "'" , con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Qty = 0;



            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToDecimal(dt.Rows[0]["Qty"]);

            }


            return Qty;
        }
        public IMEModel WeHaveSoldSaleReturn(string itemCode, string IME, string PartyCode)
        {
            try
            {

                //var cmd = new SqlCommand("Select IME,Description,IsNull(Price_Cost,0) as Price_Cost,IsNull(DisRs,0) as DisRs,IsNull(DisPer,0) as DisPer,IsNull(DealRs,0) as DealRs from InventorySerialNoFinal where CompID = '" + CompID + "' and PartyCode = '" + PartyCode + "' and ItemCode = " + itemCode + " and IME = '" + IME + "' and Description = 'Sale' order by System_Date_Time desc", con);
                var cmd = new SqlCommand("Select top 1 IME,Description,IsNull(Price_Cost,0) as Price_Cost,IsNull(DisRs,0) as DisRs,IsNull(DisPer,0) as DisPer,IsNull(DealRs,0) as DealRs,PartyCode,VoucherNo  from InventorySerialNoFinal where CompID = '" + CompID + "' and IME = '" + IME + "' order by System_Date_Time desc", con);
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                var IMEm = new IMEModel();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    if (dt.Rows.Count > 0 && dt.Rows[0]["PartyCode"].ToString()==PartyCode && dt.Rows[0]["Description"].ToString() == IMEDescription.Sale)
                    {
                        var QtyOnBill = QtyOnSaleBillAgainstItemCodeAndBillNo(Convert.ToDecimal(itemCode), dt.Rows[0]["VoucherNo"].ToString());
                        IMEm.isSold = true;
                        IMEm.Rate = Convert.ToDecimal(dt.Rows[0]["Price_Cost"]);
                        IMEm.DisRs = Convert.ToDecimal(dt.Rows[0]["DisRs"]);
                        IMEm.DisPer = Convert.ToDecimal(dt.Rows[0]["DisPer"]);
                        IMEm.DealRs = Convert.ToDecimal(dt.Rows[0]["DealRs"]);
                        if (QtyOnBill > 1)
                        {
                            IMEm.DealRs = Math.Round(IMEm.DealRs / QtyOnBill);
                        }
                        IMEm.isItemAvailable = true;

                    }
                    else
                        IMEm.isSold = false;

                }
                return IMEm;
            }
            catch (Exception ex)
            {
                return new IMEModel() { isSold = true, isItemMatch = false, Error = true, ErrorMessage = ex.Message };
            }
            finally
            {
                con.Close();
            }


        }

        public string AlreadyLPPaid(string IME)
        {


            SqlDataAdapter cmd = new SqlDataAdapter("Select IME, InvNo, ReceivingPartyCode, PurchasedPartyCode from LP_Receiving where CompID = '" + CompID + "' and IME = '" + IME + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string LPPaid = "No";



            if (dt.Rows.Count > 0)
            {
                LPPaid = "Yes";

            }


            return LPPaid; 


            }



        public IMEModel WeHavePurchasedFrom(string itemCode, string IME, string PartyCode)
        {
            try
            {

                var cmd = new SqlCommand("Select IME,Description,IsNull(Price_Cost,0) as Price_Cost,IsNull(DisRs,0) as DisRs,IsNull(DisPer,0) as DisPer,IsNull(DealRs,0) as DealRs  from InventorySerialNoFinal where CompID = '" + CompID + "' and PartyCode = '" + PartyCode + "' and ItemCode = " + itemCode + " and IME = '" + IME + "' and Description = '" + IMEDescription.Purchase + "' order by System_Date_Time desc", con);
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                var IMEm = new IMEModel();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    if (dt.Rows.Count > 0)
                    {
                        IMEm.isPurchased = true;
                        IMEm.Rate = Convert.ToDecimal(dt.Rows[0]["Price_Cost"]);
                        IMEm.DisRs = Convert.ToDecimal(dt.Rows[0]["DisRs"]);
                        IMEm.DisPer = Convert.ToDecimal(dt.Rows[0]["DisPer"]);
                        IMEm.DealRs = Convert.ToDecimal(dt.Rows[0]["DealRs"]);
                        IMEm.isItemAvailable = true;

                    }
                    else
                        IMEm.isPurchased = false;

                }
                return IMEm;
            }
            catch (Exception ex)
            {
                return new IMEModel() { isPurchased = true, isItemMatch = false, Error = true, ErrorMessage = ex.Message };
            }
            finally
            {
                con.Close();
            }


        }



    }
}