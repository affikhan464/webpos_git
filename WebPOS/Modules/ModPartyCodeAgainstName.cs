﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace WebPOS
{
    public class ModPartyCodeAgainstName
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static ModItem objModItem = new ModItem();
        static ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        

        public string PartyCode(string PartyName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from PartyCode where  CompID='" + CompID + "' and Name='" + PartyName +  "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string PartCo = "";
            


            if (dt.Rows.Count > 0)
            {
                PartCo = Convert.ToString( dt.Rows[0]["Code"]);
                
            }


            return PartCo;
        }
        public Int32 SaleManCodeAgainstSaleManName(string SaleManName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select ID from SaleManList where CompID='" + CompID + "' and Name='" + SaleManName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            Int32 SaleManID = 1;

            if (dt.Rows.Count > 0)
            {
                SaleManID = Convert.ToInt16(dt.Rows[0]["ID"]);

            }
            return SaleManID;
        }
        public string SaleManNameAgainstSaleManCode(int SaleManID)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Name from SaleManList where CompID='" + CompID + "' and id=" + SaleManID, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string SaleManNameAgainstSaleManCode = "";

            if (dt.Rows.Count > 0)
            {
                SaleManNameAgainstSaleManCode = dt.Rows[0]["Name"].ToString() ;

            }
            return SaleManNameAgainstSaleManCode;
        }
        public Int16 PartyGroupIDAgainstGroupName(string GroupName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select GroupID from PartyGroup where  CompID='" + CompID + "' and GroupName='" + GroupName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            Int16 PartGroupCode = 1;

            if (dt.Rows.Count > 0)
            {
                PartGroupCode = Convert.ToInt16(dt.Rows[0]["GroupID"]);

            }
            return PartGroupCode;
        }
        public Int16 PartyNorBalanceAgainstCode(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(NorBalance,1) as NorBalance from PartyCode where  CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            Int16 NBal = 1;

            if (dt.Rows.Count > 0)
            {
                NBal = Convert.ToInt16(dt.Rows[0]["NorBalance"]);

            }
            return NBal;
        }
        
        public string PartyAddress(string PartyName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Address from PartyCode where  CompID='" + CompID + "' and Name='" + PartyName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string PartAddress = "";



            if (dt.Rows.Count > 0)
            {
                PartAddress = Convert.ToString(dt.Rows[0]["Address"]);

            }


            return PartAddress;
        }
        public string PartyAddressAgainstPartyCode(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Address from PartyCode where  CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string PartAddress = "";
            if (dt.Rows.Count > 0)
            {
                PartAddress = Convert.ToString(dt.Rows[0]["Address"]);
            }
            return PartAddress;
        }
        public string PartyPhoneAgainstPartyCode(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Mobile,Phone from PartyCode where  CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string Phone = "";
            if (dt.Rows.Count > 0)
            {
                Phone = Convert.ToString(dt.Rows[0]["Mobile"]) + "/" + Convert.ToString(dt.Rows[0]["Phone"]);
            }
            return Phone;
        }

        public decimal PartyBalance(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Balance from PartyCode where  CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal PartBalance = 0;



            if (dt.Rows.Count > 0)
            {
                PartBalance = Convert.ToDecimal(dt.Rows[0]["Balance"]);

            }


            return PartBalance;
        }
        public string PartyNameAgainstCode(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Name from PartyCode where  CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string PartName = "";



            if (dt.Rows.Count > 0)
            {
                PartName = Convert.ToString(dt.Rows[0]["Name"]);

            }


            return PartName;
        }
        public string TransactionExistAgainstGLCode(string GLCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code from GeneralLedger where  CompID='" + CompID + "' and Code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string TransactionExistAgainstGLCode = "No";
            if (dt.Rows.Count > 0)
            {
                TransactionExistAgainstGLCode = "Yes";
            }
            return TransactionExistAgainstGLCode;
        }

        public string DeleteGLCodeLevel5(string GLCode,SqlConnection con, SqlTransaction tran)
        {

            ////////var TransactionExistAgainstGL = objModPartyCodeAgainstName.TransactionExistAgainstGLCode(GLCode);
            ////////var message = "";
            ////////if (con.State==ConnectionState.Closed) { con.Open(); }
            ////////tran = con.BeginTransaction();
            ////////try
            ////////{
            ////////    if (TransactionExistAgainstGL == "No")
            ////////    {
            ////////        message = "Party deleted Successfully.";
            ////////        SqlCommand cmdDel1 = new SqlCommand("Delete  from GLCode where  CompID='" + CompID + "' and code='" + GLCode + "'", con, tran);
            ////////        cmdDel1.ExecuteNonQuery();

            ////////        SqlCommand cmdDel2 = new SqlCommand("Delete  from GLOpeningBalance where  CompID='" + CompID + "' and code='" + GLCode + "'", con, tran);
            ////////        cmdDel2.ExecuteNonQuery();

            ////////        SqlCommand cmdDel3 = new SqlCommand("Delete  from PartyCode where  CompID='" + CompID + "' and code='" + GLCode + "'", con, tran);
            ////////        cmdDel3.ExecuteNonQuery();

            ////////        SqlCommand cmdDel4 = new SqlCommand("Delete  from PartyOpBalance where  CompID='" + CompID + "' and code='" + GLCode + "'", con, tran);
            ////////        cmdDel4.ExecuteNonQuery();

            ////////        SqlCommand cmdDel5 = new SqlCommand("Delete  from PartyGPCode where  Code='" + GLCode + "'", con);
            ////////        cmdDel5.ExecuteNonQuery();
            ////////        tran.Commit();
            ////////        con.Close();
            ////////        return message;
            ////////    }
            ////////    else
            ////////    {
            ////////        return "Transaction Exist";
            ////////    }
            ////////}
            ////////catch (Exception ex)
            ////////{
            ////////    tran.Rollback();
            ////////    con.Close();
            ////////    return ex.Message;
            ////////}
            return "asdf";
        }

        public decimal BrandWiseDiscountRateAgainstPartyCode(string PartyCode, string ItemCode)
        {
            if (con.State == ConnectionState.Closed) { con.Open(); }
            var BrandCode = objModItem.BrandCodeAgainstItemCode( Convert.ToInt16(ItemCode));
            
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Disc_Rate,0) as  Disc_Rate from BrandWiseDiscount where  CompID='" + CompID + "' and PartyCode='" + PartyCode + "' and BrandCode="+ Convert.ToDecimal(BrandCode), con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Disc_Rate = 0;



            if (dt.Rows.Count > 0)
            {
                Disc_Rate = Convert.ToDecimal(dt.Rows[0]["Disc_Rate"]);

            }

            
            return Disc_Rate;
        }
        public int GroupCodeAgainstGroupName(string GroupName)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select GroupID from PartyGroup where  CompID='" + CompID + "' and GroupName='" + GroupName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int GroupID = 1;



            if (dt.Rows.Count > 0)
            {
                GroupID = Convert.ToInt16(dt.Rows[0]["GroupID"]);

            }


            return GroupID;
        }




        }


    
}