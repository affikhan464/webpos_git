
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='CustomMigrations' and xtype='U')
   CREATE TABLE CustomMigrations (
    id INT IDENTITY PRIMARY KEY,
    [version] [nvarchar] (10) NOT NULL
);

-- version='1.0'

IF NOT EXISTS (SELECT * FROM CustomMigrations WHERE version='1.0')
Insert Into customMigrations (version) values('1.0');

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='MenuPages' and xtype='U')
   CREATE TABLE [dbo].[MenuPages](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[CompID] [nvarchar](50) NOT NULL,
	[Url] [nvarchar](500) NULL,
	[StationId] [int] NULL,
 CONSTRAINT [PK_MenuPages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='UserMenuPages' and xtype='U')
CREATE TABLE [dbo].[UserMenuPages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompID] [nvarchar](50) NOT NULL,
	[UserId] [int] NULL,
	[PageId] [int] NULL,
	[StationId] [int] NULL,
 CONSTRAINT [PK_UserMenuPages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]

IF COL_LENGTH('PartyCode', 'GroupID') IS NULL
BEGIN
    Alter Table PartyCode
		Add 
		GroupID int null
END

IF COL_LENGTH('PartyCode', 'DealRs') IS NULL
BEGIN
    Alter Table PartyCode
		Add 
		DealRs float null
END

IF COL_LENGTH('PartyCode', 'DisPer') IS NULL
BEGIN
    Alter Table PartyCode
		Add 
		DisPer float null
END

IF COL_LENGTH('PartyCode', 'DealApplyNo') IS NULL
BEGIN
    Alter Table PartyCode
		Add 
		DealApplyNo float null
END

IF COL_LENGTH('InvCode', 'DealRs') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		DealRs float null
END

IF COL_LENGTH('InvCode', 'Year1') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		Year1 nvarchar (100) null
END

IF COL_LENGTH('InvCode', 'PerPieceCommission') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		PerPieceCommission float null
END

IF COL_LENGTH('InvCode', 'DealRs2') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		DealRs2 float null
END

IF COL_LENGTH('InvCode', 'DealRs3') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		DealRs3 float null
END

-- version='1.1'
IF NOT EXISTS (SELECT * FROM CustomMigrations WHERE version='1.1')
Insert Into customMigrations (version) values('1.1');

IF COL_LENGTH('InvCode', 'MSP') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		MSP float null
END

IF COL_LENGTH('InvCode', 'GroupId') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		GroupId int null
END

IF COL_LENGTH('InvCode', 'BarCodeCategory') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		BarCodeCategory int null
END

IF COL_LENGTH('InvCode', 'Ingridients') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		Ingridients float null
END
IF COL_LENGTH('InvCode', 'OtherInFo') IS NULL
BEGIN
    Alter Table InvCode
		Add 
		OtherInFo nvarchar(500) null
END

IF COL_LENGTH('Userss', 'UserName') IS NULL
BEGIN
    Alter Table [Userss]
		Add 
		UserName [nvarchar] (255) null;

	Update [Userss] Set UserName =  Sno;

END
IF COL_LENGTH('Userss', 'AddedBy') IS NULL
BEGIN
    Alter Table [Userss]
		Add 
		AddedBy int null;
END
IF COL_LENGTH('Userss', 'UpdatedBy') IS NULL
BEGIN
    Alter Table [Userss]
		Add 
		UpdatedBy int null;
END

-- version='1.2'

IF NOT EXISTS (SELECT * FROM CustomMigrations WHERE version='1.2')
Insert Into customMigrations (version) values('1.2');

IF COL_LENGTH('CustomMigrations', 'Description') IS NULL
BEGIN
    Alter Table CustomMigrations
		Add 
		Description nvarchar (1000) null
END
IF COL_LENGTH('Invoice2', 'PerPieceCommission') IS NULL
BEGIN
    Alter Table Invoice2
		Add 
		PerPieceCommission float null default(0)
END
IF COL_LENGTH('InventorySerialNoFinal', 'DisPer') IS NULL
BEGIN
    Alter Table InventorySerialNoFinal
		Add 
		DisPer float null
END
IF COL_LENGTH('InventorySerialNoFinal', 'DisRs') IS NULL
BEGIN
    Alter Table InventorySerialNoFinal
		Add 
		DisRs float null
END
IF COL_LENGTH('InventorySerialNoFinal', 'DealRs') IS NULL
BEGIN
    Alter Table InventorySerialNoFinal
		Add 
		DealRs float null
END
IF COL_LENGTH('SaleReturn1', 'Phone') IS NULL
BEGIN
    Alter Table SaleReturn1
		Add 
		Phone nvarchar (500) null
END
IF COL_LENGTH('SaleReturn1', 'System_Date_Time') IS NULL
BEGIN
    Alter Table SaleReturn1
		Add 
		System_Date_Time smalldatetime null
END
IF COL_LENGTH('SaleReturn1', 'OperatorName') IS NULL
BEGIN
    Alter Table SaleReturn1
		Add 
		OperatorName nvarchar (500) null
END
IF COL_LENGTH('PurchaseReturn1', 'Phone') IS NULL
BEGIN
    Alter Table PurchaseReturn1
		Add 
		Phone nvarchar (500) null
END
IF COL_LENGTH('PurchaseReturn1', 'Flat_Discount_Per') IS NULL
BEGIN
    Alter Table PurchaseReturn1
		Add 
		Flat_Discount_Per float null
END


-- version='1.3'

IF NOT EXISTS (SELECT * FROM CustomMigrations WHERE version='1.3')
Insert Into customMigrations (version) values('1.3');

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='UserPermissions' and xtype='U')
CREATE TABLE [dbo].[UserPermissions](
	[Id] int IDENTITY(1,1) NOT NULL,
	[Type] nvarchar(500)  NOT NULL,
	[Value] bit  NOT NULL,
	[CompID] nvarchar(50) NOT NULL,
	[UserId] int  NOT NULL,
	[StationId] [int]  NOT NULL,
 CONSTRAINT [PK_UserPermissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]